USE [Taegutec_Sales_Budget]
GO
/****** Object:  UserDefinedFunction [dbo].[get_divide_by]    Script Date: 10/15/2018 8:11:34 AM ******/
DROP FUNCTION [dbo].[get_divide_by]
GO
/****** Object:  StoredProcedure [dbo].[sp_getTartgets_total]    Script Date: 10/15/2018 8:11:34 AM ******/
DROP PROCEDURE [dbo].[sp_getTartgets_total]
GO
/****** Object:  StoredProcedure [dbo].[sp_getTartgets]    Script Date: 10/15/2018 8:11:34 AM ******/
DROP PROCEDURE [dbo].[sp_getTartgets]
GO
/****** Object:  StoredProcedure [dbo].[getsalesbycustomer_variation]    Script Date: 10/15/2018 8:11:34 AM ******/
DROP PROCEDURE [dbo].[getsalesbycustomer_variation]
GO
/****** Object:  StoredProcedure [dbo].[getReportsDashboardData]    Script Date: 10/15/2018 8:11:34 AM ******/
DROP PROCEDURE [dbo].[getReportsDashboardData]
GO
/****** Object:  StoredProcedure [dbo].[getReportsDashboardData]    Script Date: 10/15/2018 8:11:34 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Declare @B VARCHAR(MAX)
--SET @B='''HYD'''
--exec getReportsDashboardData @B,null,null,'C','Customers','desc',3000000,'TTA'
CREATE Procedure [dbo].[getReportsDashboardData]
(
	@BranchCode varchar(50)=null,
	@salesengineer_id varchar(50)=null,
	@territory_engineer_id varchar(10)=null, 
	@customer_type  varchar(10)=null,
	@topfiveflag varchar(100)=null,
	@sortby varchar(50)=null,
	@budget_val varchar(50)=null,
	@cter varchar(10)=null
)
AS
BEGIN
DECLARE @ActualYear int, @BudgetYear int, @ActualMonth int,@MonthsRemaining int, @Today int, @DaysInMonth int
		SET @BudgetYear = [dbo].[get_profile_value]('BUDGET_YEAR')
		SET @ActualYear = [dbo].[get_profile_value]('ACTUAL_YEAR')
		SET @ActualMonth = [dbo].[get_profile_value]('ACTUAL_MONTH')
		SET @MonthsRemaining=(12-@ActualMonth)+1			
		SET @Today =  DAY(GETDATE()) - 1 
		--new added code
		IF @Today = 0 BEGIN SET @Today = 1 END
		SET @DaysInMonth = DAY(convert(datetime,convert(date,dateadd(dd,-(day(dateadd(mm,1,getdate()))),dateadd(mm,1,getdate())),100),100))
BEGIN
If(@topfiveflag='Branches')
BEGIN
--WITH Top5Branches(region_description,Customer_region,Sales_value_year_0,Sales_ytd_value,budget_value,Sales_ytm_value)AS(
SELECT tmp.region_description,  tmp.Customer_region,
COALESCE (CEILING(SUM(ISNULL(Sales_value_year_0,0)) + ((sum(Sales_value_month_0)/@DaysInMonth)*@Today) ),0)Sales_value_year_0,       
COALESCE (CEILING(SUM(Sales_ytd_value)),0)Sales_ytd_value,
CEILING(dbo.get_budget_val(@BudgetYear,null, null, @salesengineer_id, tmp.Customer_region,@customer_type,@cter)) budget_value,
COALESCE (CEILING(SUM(Sales_ytm_value)),0)Sales_ytm_value INTO #TEMPBRANCH
           FROM (SELECT rgn.region_description,  c.Customer_region, c.customer_number , 
						 Sales_value_month_0 = 	CASE WHEN s.inv_month = @ActualMonth  and s.inv_year = @ActualYear - 1 THEN value END,	       
						 Sales_value_year_0 = CASE WHEN s.inv_month < @ActualMonth  and s.inv_year = @ActualYear - 1 THEN value END,
						 Sales_ytd_value = CASE WHEN s.inv_month <= @ActualMonth and s.inv_year = @ActualYear THEN value END,
						 Sales_ytm_value = CASE WHEN s.inv_month < @ActualMonth and s.inv_year = @ActualYear THEN value END
				FROM tt_region rgn,   tt_customer_master c LEFT OUTER JOIN  tt_salesnumbers s on s.customer_number=c.customer_number
				WHERE
				(c.assigned_salesengineer_id =ISNULL(@salesengineer_id,c.assigned_salesengineer_id))
					AND  (c.customer_type =ISNULL(@customer_type,c.customer_type))
					AND c.Customer_region =ISNULL(@BranchCode,c.Customer_region)
					AND (@territory_engineer_id IS NULL OR (c.territory_engineer_id = @territory_engineer_id  ))
					AND rgn.region_code=c.Customer_region
				)tmp
            GROUP BY tmp.Customer_region,tmp.region_description
--Order by Sales_ytd_value desc
--)
select  region_description,
CEILING(NULLIF(Sales_value_year_0,0)) as Sales_value_year_0,
CEILING(NULLIF(budget_value,0)) as budget_value,
CEILING(NULLIF(Sales_ytd_value,0)) Sales_ytd_value ,
CEILING(Sales_ytm_value+((budget_value-Sales_ytm_value)/@MonthsRemaining)) ytd_plan,
CEILING(((Sales_ytd_value)/NULLIF(Sales_ytm_value+((budget_value-Sales_ytm_value)/@MonthsRemaining),0))*100) as achvmnt,
CEILING(((budget_value-Sales_ytd_value))/NULLIF(@MonthsRemaining,0)) as askrate,
0 as NOVALUES,
CEILING(((NULLIF(Sales_ytd_value,0)/NULLIF(Sales_value_year_0,0))-1)*100)  growth  INTO #TEMPBRANCH_FINAL
from #TEMPBRANCH 
WHERE budget_value IS NOT NULL AND budget_value NOT IN(0) 
order by ---0 added to make the column count equal in all cases
CEILING(((Sales_ytd_value)/NULLIF(Sales_ytm_value+((budget_value-Sales_ytm_value)/@MonthsRemaining),0))*100) DESC

select * FROM #TEMPBRANCH_FINAL
union ALL
select 'Total' region_description,
SUM(Sales_value_year_0) as Sales_value_year_0,
SUM(budget_value) as budget_value,
SUM(Sales_ytd_value) Sales_ytd_value ,
SUM(Sales_ytm_value+((budget_value-Sales_ytm_value)/@MonthsRemaining)) ytd_plan,
CEILING((SUM(NULLIF(Sales_ytd_value,0))/(NULLIF(SUM(NULLIF(Sales_ytm_value,0))+((SUM(NULLIF(budget_value,0))-SUM(NULLIF(Sales_ytm_value,0)))/@MonthsRemaining),0)))*100) as achvmnt,
--SUM(ISNULL(CEILING(((Sales_ytd_value)/NULLIF(Sales_ytm_value+((budget_value-Sales_ytm_value)/@MonthsRemaining),0))*100),0)) as achvmnt,
SUM(((budget_value-Sales_ytd_value))/NULLIF(@MonthsRemaining,0)) as askrate,
--(((SUM(NULLIF(budget_value,0))-SUM(NULLIF(Sales_ytd_value,0)))/NULLIF(@MonthsRemaining,0))) as Askrate,
0 as NOVALUES,
--SUM(((NULLIF(Sales_ytd_value,0)/NULLIF(Sales_value_year_0,0))-1)*100)  growth 
(((SUM(NULLIF(Sales_ytd_value,0))/(SUM(NULLIF(Sales_value_year_0,0))))-1)*100)  growth 
from #TEMPBRANCH 
WHERE budget_value IS NOT NULL AND budget_value NOT IN(0) 


select  region_description,
CEILING(NULLIF(Sales_value_year_0,0)) as Sales_value_year_0,
CEILING(NULLIF(budget_value,0)) as budget_value,
CEILING(NULLIF(Sales_ytd_value,0)) Sales_ytd_value ,
CEILING(Sales_ytm_value+((budget_value-Sales_ytm_value)/@MonthsRemaining)) ytd_plan,
CEILING(((Sales_ytd_value)/NULLIF(Sales_ytm_value+((budget_value-Sales_ytm_value)/@MonthsRemaining),0))*100) as achvmnt,
CEILING(((budget_value-Sales_ytd_value))/NULLIF(@MonthsRemaining,0)) as askrate,
0 as NOVALUES,
ISNULL(CEILING(((NULLIF(Sales_ytd_value,0)/NULLIF(Sales_value_year_0,0))-1)*100),0)  growth   INTO #TEMPBRANCH_SUM 
from #TEMPBRANCH 
WHERE budget_value IS NOT NULL AND budget_value NOT IN(0) 
order by ---0 added to make the column count equal in all cases
ISNULL(CEILING(((NULLIF(Sales_ytd_value,0)/NULLIF(Sales_value_year_0,0))-1)*100),0) DESC

SELECT * FROM #TEMPBRANCH_SUM
UNION ALL
select 'Total' region_description,
SUM(Sales_value_year_0) as Sales_value_year_0,
SUM(budget_value) as budget_value,
SUM(Sales_ytd_value) Sales_ytd_value ,
SUM(Sales_ytm_value+((budget_value-Sales_ytm_value)/@MonthsRemaining)) ytd_plan,
--SUM(ISNULL(CEILING(((Sales_ytd_value)/NULLIF(Sales_ytm_value+((budget_value-Sales_ytm_value)/@MonthsRemaining),0))*100),0)) as achvmnt,
CEILING((SUM(NULLIF(Sales_ytd_value,0))/(NULLIF(SUM(NULLIF(Sales_ytm_value,0))+((SUM(NULLIF(budget_value,0))-SUM(NULLIF(Sales_ytm_value,0)))/@MonthsRemaining),0)))*100) as achvmnt,
SUM(((budget_value-Sales_ytd_value))/NULLIF(@MonthsRemaining,0)) as askrate,
--((SUM(NULLIF(budget_value,0))-SUM(NULLIF(Sales_ytd_value,0)))/NULLIF(@MonthsRemaining,0))) as Askrate,
0 as NOVALUES,
--SUM(ISNULL(CEILING(((NULLIF(Sales_ytd_value,0)/NULLIF(Sales_value_year_0,0))-1)*100),0))  growth   
(((SUM(NULLIF(Sales_ytd_value,0))/(SUM(NULLIF(Sales_value_year_0,0))))-1)*100)  growth 
from #TEMPBRANCH 
WHERE budget_value IS NOT NULL AND budget_value NOT IN(0) 

END

--Case:Sales Engineers 
Else IF(@topfiveflag='salesEngineers' )
BEGIN
--WITH salesEngineers_CTE(assigned_salesengineer_id,EngineerName,Sales_value_year_0,Sales_ytd_value,budget_value,Sales_ytm_value)AS(
SELECT    tmp.assigned_salesengineer_id,tmp.EngineerName, tmp.Branch, tmp.RoleId,     
COALESCE (CEILING(SUM(ISNULL(Sales_value_year_0,0)) + ((sum(Sales_value_month_0)/@DaysInMonth)*@Today) ),0)Sales_value_year_0,       
COALESCE (CEILING(SUM(Sales_ytd_value)),0)Sales_ytd_value,
CEILING(dbo.get_budget_val(@BudgetYear,null, null, tmp.assigned_salesengineer_id, @BranchCode,@customer_type,@cter)) budget_value,
COALESCE (CEILING(SUM(Sales_ytm_value)),0)Sales_ytm_value INTO #TEMPSE
          FROM (SELECT c.assigned_salesengineer_id,li.EngineerName, c.customer_number, li.RoleId,
				--(select region_description from tt_region where region_code=c.Customer_region) 
				tr.region_description Branch,
						Sales_value_month_0 = 	CASE WHEN s.inv_month = @ActualMonth  and s.inv_year = @ActualYear - 1 THEN value END,	       
						Sales_value_year_0 = CASE WHEN s.inv_month < @ActualMonth  and s.inv_year = @ActualYear - 1 THEN value END,
						Sales_ytd_value = CASE WHEN s.inv_month <= @ActualMonth and s.inv_year = @ActualYear THEN value END,
						Sales_ytm_value = CASE WHEN s.inv_month <= @ActualMonth and s.inv_year = @ActualYear THEN value END
				FROM    tt_customer_master c LEFT OUTER JOIN  tt_salesnumbers s on
						s.customer_number=c.customer_number
						JOIN tt_region tr ON tr.region_code=c.Customer_region,LoginInfo li
				where li.EngineerId=c.assigned_salesengineer_id
				AND(c.assigned_salesengineer_id =ISNULL(@salesengineer_id,c.assigned_salesengineer_id))
				AND  (c.customer_type =ISNULL(@customer_type,c.customer_type))
				--AND c.Customer_region =ISNULL(@BranchCode,c.Customer_region)
				AND (@BranchCode IS NULL OR c.Customer_region in (select Item from SplitString(@BranchCode,''',''')))
				AND (@territory_engineer_id IS NULL OR (c.territory_engineer_id = @territory_engineer_id  ))
				
				)tmp
            GROUP BY tmp.assigned_salesengineer_id,tmp.EngineerName, tmp.Branch, tmp.RoleId
--)

select  EngineerName, Branch, CEILING(NULLIF(Sales_value_year_0,0)) as Sales_value_year_0,CEILING(NULLIF(budget_value,0)) as budget_value,
CEILING(NULLIF(Sales_ytd_value,0)) Sales_ytd_value ,
CEILING(Sales_ytm_value+((budget_value-Sales_ytm_value)/@MonthsRemaining)) ytd_plan,
ISNULL(CEILING(((Sales_ytd_value)/NULLIF(Sales_ytm_value+((budget_value-Sales_ytm_value)/@MonthsRemaining),0))*100),0) as achvmnt,
CEILING(((budget_value-Sales_ytd_value))/NULLIF(@MonthsRemaining,0)) as Askrate,
0 as No_Column,
ISNULL(CEILING(((NULLIF(Sales_ytd_value,0)/NULLIF(Sales_value_year_0,0))-1)*100),0) growth  INTO #TEMPSE_FINAL1   
from #TEMPSE  
WHERE budget_value IS NOT NULL AND budget_value NOT IN(0)  order by ---0 added to make the column count equal in all cases
ISNULL(CEILING(((Sales_ytd_value)/NULLIF(Sales_ytm_value+((budget_value-Sales_ytm_value)/@MonthsRemaining),0))*100),0) DESC

SELECT * FROM #TEMPSE_FINAL1
UNION ALL
select 'Total' EngineerName, '' Branch, SUM(Sales_value_year_0) as Sales_value_year_0,SUM(budget_value) as budget_value,
SUM(Sales_ytd_value) Sales_ytd_value ,
SUM(Sales_ytm_value+((budget_value-Sales_ytm_value)/@MonthsRemaining)) ytd_plan,
--SUM(ISNULL(CEILING(((Sales_ytd_value)/NULLIF(Sales_ytm_value+((budget_value-Sales_ytm_value)/@MonthsRemaining),0))*100),0)) as achvmnt,
CEILING((SUM(NULLIF(Sales_ytd_value,0))/(NULLIF(SUM(NULLIF(Sales_ytm_value,0))+((SUM(NULLIF(budget_value,0))-SUM(NULLIF(Sales_ytm_value,0)))/@MonthsRemaining),0)))*100) as achvmnt,
SUM(((budget_value-Sales_ytd_value))/NULLIF(@MonthsRemaining,0)) as Askrate,
--(((SUM(NULLIF(budget_value,0))-SUM(NULLIF(Sales_ytd_value,0)))/NULLIF(@MonthsRemaining,0))) as Askrate,
0 as No_Column,
--SUM(ISNULL(CEILING(((NULLIF(Sales_ytd_value,0)/NULLIF(Sales_value_year_0,0))-1)*100),0)) growth  
(((SUM(NULLIF(Sales_ytd_value,0))/(SUM(NULLIF(Sales_value_year_0,0))))-1)*100)  growth 
from #TEMPSE  
WHERE budget_value IS NOT NULL AND budget_value NOT IN(0)

select  EngineerName, Branch, CEILING(NULLIF(Sales_value_year_0,0)) as Sales_value_year_0,CEILING(NULLIF(budget_value,0)) as budget_value,
CEILING(NULLIF(Sales_ytd_value,0)) Sales_ytd_value ,
CEILING(Sales_ytm_value+((budget_value-Sales_ytm_value)/@MonthsRemaining)) ytd_plan,
ISNULL(CEILING(((Sales_ytd_value)/NULLIF(Sales_ytm_value+((budget_value-Sales_ytm_value)/@MonthsRemaining),0))*100),0) as achvmnt,
CEILING(((budget_value-Sales_ytd_value))/NULLIF(@MonthsRemaining,0)) as Askrate,
0 as No_Column,
ISNULL(CEILING(((NULLIF(Sales_ytd_value,0)/NULLIF(Sales_value_year_0,0))-1)*100),0) growth  INTO #TEMPSE_FINAL2    
from #TEMPSE  
WHERE budget_value IS NOT NULL AND budget_value NOT IN(0)

 order by ---0 added to make the column count equal in all cases
ISNULL(CEILING(((NULLIF(Sales_ytd_value,0)/NULLIF(Sales_value_year_0,0))-1)*100),0) DESC

SELECT * FROM #TEMPSE_FINAL2
UNION ALL
select 'Total' EngineerName,'', SUM(Sales_value_year_0) as Sales_value_year_0,SUM(budget_value) as budget_value,
SUM(Sales_ytd_value) Sales_ytd_value ,
SUM(Sales_ytm_value+((budget_value-Sales_ytm_value)/@MonthsRemaining)) ytd_plan,
--SUM(ISNULL(CEILING(((Sales_ytd_value)/NULLIF(Sales_ytm_value+((budget_value-Sales_ytm_value)/@MonthsRemaining),0))*100),0)) as achvmnt,
CEILING((SUM(NULLIF(Sales_ytd_value,0))/(NULLIF(SUM(NULLIF(Sales_ytm_value,0))+((SUM(NULLIF(budget_value,0))-SUM(NULLIF(Sales_ytm_value,0)))/@MonthsRemaining),0)))*100) as achvmnt,
SUM(((budget_value-Sales_ytd_value))/NULLIF(@MonthsRemaining,0)) as Askrate,
--(((SUM(NULLIF(budget_value,0))-SUM(NULLIF(Sales_ytd_value,0)))/NULLIF(@MonthsRemaining,0))) as Askrate,
0 as No_Column,
--SUM(ISNULL(CEILING(((NULLIF(Sales_ytd_value,0)/NULLIF(Sales_value_year_0,0))-1)*100),0)) growth   
(((SUM(NULLIF(Sales_ytd_value,0))/(SUM(NULLIF(Sales_value_year_0,0))))-1)*100)  growth 
from #TEMPSE  
WHERE budget_value IS NOT NULL AND budget_value NOT IN(0)

select  EngineerName, Branch, CEILING(NULLIF(Sales_value_year_0,0)) as Sales_value_year_0,CEILING(NULLIF(budget_value,0)) as budget_value,
CEILING(NULLIF(Sales_ytd_value,0)) Sales_ytd_value ,
CEILING(Sales_ytm_value+((budget_value-Sales_ytm_value)/@MonthsRemaining)) ytd_plan,
ISNULL(CEILING(((Sales_ytd_value)/NULLIF(Sales_ytm_value+((budget_value-Sales_ytm_value)/@MonthsRemaining),0))*100),0) as achvmnt,
CEILING(((budget_value-Sales_ytd_value))/NULLIF(@MonthsRemaining,0)) as Askrate,
0 as No_Column,
ISNULL(CEILING(((NULLIF(Sales_ytd_value,0)/NULLIF(Sales_value_year_0,0))-1)*100),0) growth   INTO #TEMPSE_FINAL3 
from #TEMPSE  
WHERE budget_value IS NOT NULL AND budget_value NOT IN(0) AND RoleId='SE' order by ---0 added to make the column count equal in all cases
ISNULL(CEILING(((Sales_ytd_value)/NULLIF(Sales_ytm_value+((budget_value-Sales_ytm_value)/@MonthsRemaining),0))*100),0) 

SELECT * FROM #TEMPSE_FINAL3
UNION ALL
select 'Total' EngineerName, '', SUM(Sales_value_year_0) as Sales_value_year_0,SUM(budget_value) as budget_value,
SUM(Sales_ytd_value) Sales_ytd_value ,
SUM(Sales_ytm_value+((budget_value-Sales_ytm_value)/@MonthsRemaining)) ytd_plan,
--SUM(ISNULL(CEILING(((Sales_ytd_value)/NULLIF(Sales_ytm_value+((budget_value-Sales_ytm_value)/@MonthsRemaining),0))*100),0)) as achvmnt,
CEILING((SUM(NULLIF(Sales_ytd_value,0))/(NULLIF(SUM(NULLIF(Sales_ytm_value,0))+((SUM(NULLIF(budget_value,0))-SUM(NULLIF(Sales_ytm_value,0)))/@MonthsRemaining),0)))*100) as achvmnt,
SUM(((budget_value-Sales_ytd_value))/NULLIF(@MonthsRemaining,0)) as Askrate,
--(((SUM(NULLIF(budget_value,0))-SUM(NULLIF(Sales_ytd_value,0)))/NULLIF(@MonthsRemaining,0))) as Askrate,
0 as No_Column,
--SUM(ISNULL(CEILING(((NULLIF(Sales_ytd_value,0)/NULLIF(Sales_value_year_0,0))-1)*100),0)) growth  
(((SUM(NULLIF(Sales_ytd_value,0))/(SUM(NULLIF(Sales_value_year_0,0))))-1)*100)  growth 
from #TEMPSE  
WHERE budget_value IS NOT NULL AND budget_value NOT IN(0)

select  EngineerName, Branch, CEILING(NULLIF(Sales_value_year_0,0)) as Sales_value_year_0,CEILING(NULLIF(budget_value,0)) as budget_value,
CEILING(NULLIF(Sales_ytd_value,0)) Sales_ytd_value ,
CEILING(Sales_ytm_value+((budget_value-Sales_ytm_value)/@MonthsRemaining)) ytd_plan,
ISNULL(CEILING(((Sales_ytd_value)/NULLIF(Sales_ytm_value+((budget_value-Sales_ytm_value)/@MonthsRemaining),0))*100),0) as achvmnt,
CEILING(((budget_value-Sales_ytd_value))/NULLIF(@MonthsRemaining,0)) as Askrate,
--(((SUM(NULLIF(budget_value,0))-SUM(NULLIF(Sales_ytd_value,0)))/NULLIF(@MonthsRemaining,0))) as Askrate,
0 as No_Column,
ISNULL(CEILING(((NULLIF(Sales_ytd_value,0)/NULLIF(Sales_value_year_0,0))-1)*100),0) growth   INTO #TEMPSE_FINAL4   
from #TEMPSE  
WHERE budget_value IS NOT NULL AND budget_value NOT IN(0) AND RoleId='SE' order by ---0 added to make the column count equal in all cases
ISNULL(CEILING(((NULLIF(Sales_ytd_value,0)/NULLIF(Sales_value_year_0,0))-1)*100),0) 

SELECT * FROM #TEMPSE_FINAL4
UNION ALL
select 'Total' EngineerName, '', SUM(Sales_value_year_0) as Sales_value_year_0,SUM(budget_value) as budget_value,
SUM(Sales_ytd_value) Sales_ytd_value ,
SUM(Sales_ytm_value+((budget_value-Sales_ytm_value)/@MonthsRemaining)) ytd_plan,
--SUM(ISNULL(CEILING(((Sales_ytd_value)/NULLIF(Sales_ytm_value+((budget_value-Sales_ytm_value)/@MonthsRemaining),0))*100),0)) as achvmnt,
CEILING((SUM(NULLIF(Sales_ytd_value,0))/(NULLIF(SUM(NULLIF(Sales_ytm_value,0))+((SUM(NULLIF(budget_value,0))-SUM(NULLIF(Sales_ytm_value,0)))/@MonthsRemaining),0)))*100) as achvmnt,
SUM(((budget_value-Sales_ytd_value))/NULLIF(@MonthsRemaining,0)) as Askrate,
--(((SUM(NULLIF(budget_value,0))-SUM(NULLIF(Sales_ytd_value,0)))/NULLIF(@MonthsRemaining,0))) as Askrate,
0 as No_Column,
--SUM(ISNULL(CEILING(((NULLIF(Sales_ytd_value,0)/NULLIF(Sales_value_year_0,0))-1)*100),0)) growth      
(((SUM(NULLIF(Sales_ytd_value,0))/(SUM(NULLIF(Sales_value_year_0,0))))-1)*100)  growth 
from #TEMPSE  
WHERE budget_value IS NOT NULL AND budget_value NOT IN(0)

END

--Case: Customers
Else if(@topfiveflag='Customers' )
BEGIN
--WITH customers_CTE(customer_short_name,customer_number,Sales_value_year_0,Sales_ytd_value,budget_value,Sales_ytm_value)AS(
SELECT    tmp.customer_short_name, tmp.customer_number,   tmp.Branch, tmp.EngineerName,    
COALESCE (CEILING(SUM(ISNULL(Sales_value_year_0,0)) + ((sum(Sales_value_month_0)/@DaysInMonth)*@Today)),0)Sales_value_year_0,       
COALESCE (CEILING(SUM(Sales_ytd_value)),0)Sales_ytd_value,
CEILING(dbo.get_budget_val(@BudgetYear,null, tmp.customer_number, @salesengineer_id, @BranchCode,@customer_type,@cter)) budget_value,
COALESCE (CEILING(SUM(Sales_ytm_value)),0)Sales_ytm_value into #TEMPCUSTOMER
          FROM (SELECT c.customer_short_name,c.customer_number, tr.region_description Branch     ,  tl.EngineerName,  
						Sales_value_month_0 = 	CASE WHEN s.inv_month = @ActualMonth  and s.inv_year = @ActualYear - 1 THEN value END,	       
						Sales_value_year_0 = CASE WHEN s.inv_month < @ActualMonth  and s.inv_year = @ActualYear - 1 THEN value END,
						Sales_ytd_value = CASE WHEN s.inv_month <= @ActualMonth and s.inv_year = @ActualYear THEN value END,
						Sales_ytm_value = CASE WHEN s.inv_month <= @ActualMonth and s.inv_year = @ActualYear THEN value END
				FROM    tt_customer_master c LEFT OUTER JOIN  tt_salesnumbers s on
						s.customer_number=c.customer_number
						JOIN tt_region tr ON c.Customer_region =tr.region_code
						JOIN LoginInfo tl ON tl.EngineerId=c.assigned_salesengineer_id
				WHERE
                (c.assigned_salesengineer_id =ISNULL(@salesengineer_id,c.assigned_salesengineer_id))
					AND  (c.customer_type =ISNULL(@customer_type,c.customer_type))
					--AND c.Customer_region =ISNULL(@BranchCode,c.Customer_region)
					AND (@BranchCode IS NULL OR c.Customer_region in (select Item from SplitString(@BranchCode,''',''')))
					AND (@territory_engineer_id IS NULL OR (c.territory_engineer_id = @territory_engineer_id  ))
				 )tmp
            GROUP BY tmp.customer_short_name,tmp.customer_number, tmp.Branch, tmp.EngineerName
--Order by Sales_ytd_value desc
--)

select  customer_short_name ,Branch, EngineerName, CEILING(NULLIF(Sales_value_year_0,0)) as Sales_value_year_0,CEILING(NULLIF(budget_value,0)) as budget_value,
CEILING(NULLIF(Sales_ytd_value,0)) Sales_ytd_value ,
CEILING(Sales_ytm_value+((budget_value-Sales_ytm_value)/@MonthsRemaining)) ytd_plan,
ISNULL(CEILING(((Sales_ytd_value)/NULLIF(Sales_ytm_value+((budget_value-Sales_ytm_value)/@MonthsRemaining),0))*100),0) as achvmnt,
CEILING(((budget_value-Sales_ytd_value))/NULLIF(@MonthsRemaining,0)) as Askrate,
--(((SUM(NULLIF(budget_value,0))-SUM(NULLIF(Sales_ytd_value,0)))/NULLIF(@MonthsRemaining,0))) as Askrate,
customer_number  ,
CAST(ISNULL(dbo.get_divide_by((ISNULL(Sales_ytd_value,0)-ISNULL(Sales_value_year_0,0)),Sales_value_year_0),0)*100 AS INT)  growth
--ISNULL((((NULLIF(Sales_ytd_value,0)/NULLIF(Sales_value_year_0,0))-1)*100),0) growth 
INTO #TEMPCUSTOMER_FINAL1

from #TEMPCUSTOMER 
WHERE budget_value IS NOT NULL AND budget_value NOT IN(0)  order by 
ISNULL(CEILING(((Sales_ytd_value)/NULLIF(Sales_ytm_value+((budget_value-Sales_ytm_value)/@MonthsRemaining),0))*100),0) DESC

SELECT * FROM #TEMPCUSTOMER_FINAL1
UNION ALL
select '' customer_short_name ,'','', SUM(Sales_value_year_0) as Sales_value_year_0,SUM(budget_value) as budget_value,
SUM(Sales_ytd_value) Sales_ytd_value ,
SUM(Sales_ytm_value+((budget_value-Sales_ytm_value)/@MonthsRemaining)) ytd_plan,
--SUM(ISNULL(CEILING(((Sales_ytd_value)/NULLIF(Sales_ytm_value+((budget_value-Sales_ytm_value)/@MonthsRemaining),0))*100),0)) as achvmnt,
CEILING((SUM(NULLIF(Sales_ytd_value,0))/(NULLIF(SUM(NULLIF(Sales_ytm_value,0))+((SUM(NULLIF(budget_value,0))-SUM(NULLIF(Sales_ytm_value,0)))/@MonthsRemaining),0)))*100) as achvmnt,
SUM(((budget_value-Sales_ytd_value))/NULLIF(@MonthsRemaining,0)) as Askrate,
'Total' customer_number  ,
--SUM(ISNULL(CEILING(((NULLIF(Sales_ytd_value,0)/NULLIF(Sales_value_year_0,0))-1)*100),0)) growth 
--(((SUM(NULLIF(Sales_ytd_value,0))/(SUM(NULLIF(Sales_value_year_0,0))))-1)*100)  growth 
CAST(ISNULL(dbo.get_divide_by((SUM(ISNULL(Sales_ytd_value,0))-Sum(ISNULL(Sales_value_year_0,0))),Sum(ISNULL(Sales_value_year_0,0))),0)*100 AS INT)  growth
from #TEMPCUSTOMER 
WHERE budget_value IS NOT NULL AND budget_value NOT IN(0)

select  customer_short_name , Branch, EngineerName, CEILING(NULLIF(Sales_value_year_0,0)) as Sales_value_year_0,CEILING(NULLIF(budget_value,0)) as budget_value,
CEILING(NULLIF(Sales_ytd_value,0)) Sales_ytd_value ,
CEILING(Sales_ytm_value+((budget_value-Sales_ytm_value)/@MonthsRemaining)) ytd_plan,
ISNULL(CEILING(((Sales_ytd_value)/NULLIF(Sales_ytm_value+((budget_value-Sales_ytm_value)/@MonthsRemaining),0))*100),0) as achvmnt,
CEILING(((budget_value-Sales_ytd_value))/NULLIF(@MonthsRemaining,0)) as Askrate,
customer_number  ,
--ISNULL(CEILING(((NULLIF(Sales_ytd_value,0)/NULLIF(Sales_value_year_0,0))-1)*100),0) growth
CAST(ISNULL(dbo.get_divide_by((ISNULL(Sales_ytd_value,0)-ISNULL(Sales_value_year_0,0)),Sales_value_year_0),0)*100 AS INT)  growth
--CEILING(ISNULL((ISNULL(dbo.get_divide_by(Sales_ytd_value,Sales_value_year_0),0)-1)*100,0)) growth
 INTO #TEMPCUSTOMER_FINAL2
from #TEMPCUSTOMER 
WHERE budget_value IS NOT NULL AND budget_value NOT IN(0)  order by 
ISNULL(CEILING(((NULLIF(Sales_ytd_value,0)/NULLIF(Sales_value_year_0,0))-1)*100),0)  DESC

SELECT * FROM #TEMPCUSTOMER_FINAL2
UNION ALL
select '' customer_short_name ,'','',SUM(Sales_value_year_0) as Sales_value_year_0,
SUM(budget_value) as budget_value,
SUM(Sales_ytd_value) Sales_ytd_value ,
SUM(Sales_ytm_value+((budget_value-Sales_ytm_value)/@MonthsRemaining)) ytd_plan,
--SUM(ISNULL(CEILING(((Sales_ytd_value)/NULLIF(Sales_ytm_value+((budget_value-Sales_ytm_value)/@MonthsRemaining),0))*100),0)) as achvmnt,
CEILING((SUM(NULLIF(Sales_ytd_value,0))/(NULLIF(SUM(NULLIF(Sales_ytm_value,0))+((SUM(NULLIF(budget_value,0))-SUM(NULLIF(Sales_ytm_value,0)))/@MonthsRemaining),0)))*100) as achvmnt,
SUM(((budget_value-Sales_ytd_value))/NULLIF(@MonthsRemaining,0)) as Askrate,
--(((SUM(NULLIF(budget_value,0))-SUM(NULLIF(Sales_ytd_value,0)))/NULLIF(@MonthsRemaining,0))) as Askrate,
'Total' customer_number  ,
--(((SUM(NULLIF(Sales_ytd_value,0))/(SUM(NULLIF(Sales_value_year_0,0))))-1)*100)  growth 
--CEILING(ISNULL((ISNULL(dbo.get_divide_by(SUM(NULLIF(Sales_ytd_value,0)),SUM(NULLIF(Sales_value_year_0,0))),0)-1)*100,0)) growth
CAST(ISNULL(dbo.get_divide_by((SUM(ISNULL(Sales_ytd_value,0))-Sum(ISNULL(Sales_value_year_0,0))),Sum(ISNULL(Sales_value_year_0,0))),0)*100 AS INT)  growth
--SUM(ISNULL(CEILING(((NULLIF(Sales_ytd_value,0)/NULLIF(Sales_value_year_0,0))-1)*100),0)) growth 
from #TEMPCUSTOMER 
WHERE budget_value IS NOT NULL AND budget_value NOT IN(0)

select TOP 5 customer_short_name , Branch,EngineerName, CEILING(NULLIF(Sales_value_year_0,0)) as Sales_value_year_0,CEILING(NULLIF(budget_value,0)) as budget_value,
CEILING(NULLIF(Sales_ytd_value,0)) Sales_ytd_value ,
CEILING(Sales_ytm_value+((budget_value-Sales_ytm_value)/@MonthsRemaining)) ytd_plan,
ISNULL(CEILING(((Sales_ytd_value)/NULLIF(Sales_ytm_value+((budget_value-Sales_ytm_value)/@MonthsRemaining),0))*100),0) as achvmnt,
CEILING(((budget_value-Sales_ytd_value))/NULLIF(@MonthsRemaining,0)) as Askrate,
customer_number  ,
--ISNULL(CEILING(((NULLIF(Sales_ytd_value,0)/NULLIF(Sales_value_year_0,0))-1)*100),0) growth  
CAST(ISNULL(dbo.get_divide_by((ISNULL(Sales_ytd_value,0)-ISNULL(Sales_value_year_0,0)),Sales_value_year_0),0)*100 AS INT)  growth
--CEILING(ISNULL((ISNULL(dbo.get_divide_by(Sales_ytd_value,Sales_value_year_0),0)-1)*100,0)) growth
INTO #TEMPCUSTOMER_FINAL3 
from #TEMPCUSTOMER 
WHERE budget_value >=@budget_val  order by 
ISNULL(CEILING(((Sales_ytd_value)/NULLIF(Sales_ytm_value+((budget_value-Sales_ytm_value)/@MonthsRemaining),0))*100),0)

SELECT * FROM #TEMPCUSTOMER_FINAL3
--UNION ALL
--select '' customer_short_name ,'','',CEILING(NULLIF(Sales_value_year_0,0)) as Sales_value_year_0,CEILING(NULLIF(budget_value,0)) as budget_value,
--CEILING(NULLIF(Sales_ytd_value,0)) Sales_ytd_value ,
--CEILING(Sales_ytm_value+((budget_value-Sales_ytm_value)/@MonthsRemaining)) ytd_plan,
--ISNULL(CEILING(((Sales_ytd_value)/NULLIF(Sales_ytm_value+((budget_value-Sales_ytm_value)/@MonthsRemaining),0))*100),0) as achvmnt,
--CEILING(((budget_value-Sales_ytd_value))/NULLIF(@MonthsRemaining,0)) as Askrate,
--'Total' customer_number  ,
--ISNULL(CEILING(((NULLIF(Sales_ytd_value,0)/NULLIF(Sales_value_year_0,0))-1)*100),0) growth   
--from #TEMPCUSTOMER 
--WHERE budget_value >=@budget_val

select TOP 5 customer_short_name , Branch,EngineerName, CEILING(NULLIF(Sales_value_year_0,0)) as Sales_value_year_0,CEILING(NULLIF(budget_value,0)) as budget_value,
CEILING(NULLIF(Sales_ytd_value,0)) Sales_ytd_value ,
CEILING(Sales_ytm_value+((budget_value-Sales_ytm_value)/@MonthsRemaining)) ytd_plan,
ISNULL(CEILING(((Sales_ytd_value)/NULLIF(Sales_ytm_value+((budget_value-Sales_ytm_value)/@MonthsRemaining),0))*100),0) as achvmnt,
CEILING(((budget_value-Sales_ytd_value))/NULLIF(@MonthsRemaining,0)) as Askrate,
customer_number  ,
--ISNULL(CEILING(((NULLIF(Sales_ytd_value,0)/NULLIF(Sales_value_year_0,0))-1)*100),0) growth 
CAST(ISNULL(dbo.get_divide_by((ISNULL(Sales_ytd_value,0)-ISNULL(Sales_value_year_0,0)),Sales_value_year_0),0)*100 AS INT)  growth
--CEILING(ISNULL((ISNULL(dbo.get_divide_by(Sales_ytd_value,Sales_value_year_0),0)-1)*100,0)) growth
 INTO #TEMPCUSTOMER_FINAL4 
from #TEMPCUSTOMER 
WHERE budget_value >=@budget_val   order by 
ISNULL(CEILING(((NULLIF(Sales_ytd_value,0)/NULLIF(Sales_value_year_0,0))-1)*100),0)

SELECT * FROM #TEMPCUSTOMER_FINAL4
--UNION ALL
--select TOP 5 '' customer_short_name ,'','', CEILING(NULLIF(Sales_value_year_0,0)) as Sales_value_year_0,CEILING(NULLIF(budget_value,0)) as budget_value,
--CEILING(NULLIF(Sales_ytd_value,0)) Sales_ytd_value ,
--CEILING(Sales_ytm_value+((budget_value-Sales_ytm_value)/@MonthsRemaining)) ytd_plan,
--ISNULL(CEILING(((Sales_ytd_value)/NULLIF(Sales_ytm_value+((budget_value-Sales_ytm_value)/@MonthsRemaining),0))*100),0) as achvmnt,
--CEILING(((budget_value-Sales_ytd_value))/NULLIF(@MonthsRemaining,0)) as Askrate,
--'Total' customer_number  ,
--ISNULL(CEILING(((NULLIF(Sales_ytd_value,0)/NULLIF(Sales_value_year_0,0))-1)*100),0) growth  
--from #TEMPCUSTOMER 
--WHERE budget_value >=@budget_val

END
----CASE :Bottom customers or channel partners
--Else if(@topfiveflag='BtmCustomers' )
--BEGIN
----WITH customers_CTE(customer_short_name,customer_number,Sales_value_year_0,Sales_ytd_value,budget_value,Sales_ytm_value)AS(
--SELECT    tmp.customer_short_name, tmp.customer_number,        
--COALESCE (CEILING(SUM(ISNULL(Sales_value_year_0,0))+ ((sum(Sales_value_month_0)/@DaysInMonth)*@Today)),0)Sales_value_year_0,       
--COALESCE (CEILING(SUM(Sales_ytd_value)),0)Sales_ytd_value,
--CEILING(dbo.get_budget_val(@BudgetYear,null, tmp.customer_number, @salesengineer_id, @BranchCode,@customer_type,@cter)) budget_value,
--COALESCE (CEILING(SUM(Sales_ytm_value)),0)Sales_ytm_value into #TEMPBTMCUSTOMER
--          FROM (SELECT c.customer_short_name,c.customer_number,            
--						Sales_value_month_0 = 	CASE WHEN s.inv_month = @ActualMonth  and s.inv_year = @ActualYear - 1 THEN value END,	       
--						Sales_value_year_0 = CASE WHEN s.inv_month < @ActualMonth  and s.inv_year = @ActualYear - 1 THEN value END,
--						Sales_ytd_value = CASE WHEN s.inv_month <= @ActualMonth and s.inv_year = @ActualYear THEN value END,
--						Sales_ytm_value = CASE WHEN s.inv_month <= @ActualMonth and s.inv_year = @ActualYear THEN value END
--				FROM    tt_customer_master c LEFT OUTER JOIN  tt_salesnumbers s on
--						s.customer_number=c.customer_number
--				WHERE  (c.assigned_salesengineer_id =ISNULL(@salesengineer_id,c.assigned_salesengineer_id))
--						AND  (c.customer_type =ISNULL(@customer_type,c.customer_type))
--						AND c.Customer_region =ISNULL(@BranchCode,c.Customer_region)
--						AND (@territory_engineer_id IS NULL OR (c.territory_engineer_id = @territory_engineer_id  ))
--						)tmp
--            GROUP BY tmp.customer_short_name,tmp.customer_number
----Order by Sales_ytd_value desc
----)

--select  customer_short_name ,CEILING(NULLIF(Sales_value_year_0,0)) as Sales_value_year_0,CEILING(NULLIF(budget_value,0)) as budget_value,
--CEILING(NULLIF(Sales_ytd_value,0)) Sales_ytd_value ,
--CEILING(Sales_ytm_value+((budget_value-Sales_ytm_value)/@MonthsRemaining)) ytd_plan,
--CEILING(((Sales_ytd_value)/NULLIF(Sales_ytm_value+((budget_value-Sales_ytm_value)/@MonthsRemaining),0))*100) as achvmnt,
--CEILING(((budget_value-Sales_ytd_value))/NULLIF(@MonthsRemaining,0)) as Askrate,
--customer_number ,
--CEILING(((NULLIF(Sales_ytd_value,0)/NULLIF(Sales_value_year_0,0))-1)*100) growth    
--from #TEMPBTMCUSTOMER 
--WHERE budget_value >=@budget_val 
--order by 
--CEILING(((Sales_ytd_value)/NULLIF(Sales_ytm_value+((budget_value-Sales_ytm_value)/@MonthsRemaining),0))*100) ASC


--select  customer_short_name ,CEILING(NULLIF(Sales_value_year_0,0)) as Sales_value_year_0,CEILING(NULLIF(budget_value,0)) as budget_value,
--CEILING(NULLIF(Sales_ytd_value,0)) Sales_ytd_value ,
--CEILING(Sales_ytm_value+((budget_value-Sales_ytm_value)/@MonthsRemaining)) ytd_plan,
--CEILING(((Sales_ytd_value)/NULLIF(Sales_ytm_value+((budget_value-Sales_ytm_value)/@MonthsRemaining),0))*100) as achvmnt,
--CEILING(((budget_value-Sales_ytd_value))/NULLIF(@MonthsRemaining,0)) as Askrate,
--customer_number ,
--CEILING(((NULLIF(Sales_ytd_value,0)/NULLIF(Sales_value_year_0,0))-1)*100) growth    
--from #TEMPBTMCUSTOMER 
--WHERE budget_value >=@budget_val 
--order by 
--CEILING(((NULLIF(Sales_ytd_value,0)/NULLIF(Sales_value_year_0,0))-1)*100) ASC

--END

--CASE :GOLD CUSTOMERS


--CASE:FAMILY
Else if(@topfiveflag='Family' )
BEGIN
WITH Family_CTE(item_family_id,item_family_name,Sales_value_year_0,Sales_ytd_value,budget_value,Sales_ytm_value)AS(
SELECT tmp.item_family_id,
tmp.item_family_name,
 
COALESCE(CEILING(sum(ISNULL(Sales_value_year_0,0)) + ((sum(Sales_value_month_0)/@DaysInMonth)*@Today)),0)Sales_value_year_0,
COALESCE(CEILING(SUM(Sales_ytd_value) ),0)Sales_ytd_value,
COALESCE(CEILING([dbo].[get_budget_val_bysf] (@BudgetYear,tmp.item_family_id,null,'ALL', @salesengineer_id, @BranchCode,@customer_type,@cter)),0) budget_value,
COALESCE(CEILING(SUM(Sales_ytm_value)),0)Sales_ytm_value 
FROM   (
		SELECT  f.item_family_id, f.item_family_name, SUM(value) Sales_value_year_0, 0 Sales_ytd_value,0 Sales_ytm_value, 0 Sales_value_month_0
		FROM   tt_salesnumbers s,tt_item_master itm, tt_item_family f, tt_customer_master cust
		WHERE  inv_year = @ActualYear - 1 AND inv_month < @ActualMonth AND itm.item_code = s.item_code AND f.item_family_id = itm.item_family_id 
		  AND cust.customer_number= s.customer_number
		  --AND (@BranchCode = 'ALL' OR (cust.customer_region = @BranchCode or cust.territory_engineer_id=@BranchCode))
		  AND   (@BranchCode  = 'ALL' OR (cust.Customer_region in (select Item from SplitString(@BranchCode,''',''')) OR cust.territory_engineer_id in (select Item from SplitString(@BranchCode,''','''))))
		  AND (@salesengineer_id = 'ALL' OR (cust.assigned_salesengineer_id = @salesengineer_id))
		  AND (@customer_type = 'ALL' OR (cust.customer_type = @customer_type))
          AND (@cter IS NULL OR cust.cter = @cter)
		GROUP BY  f.item_family_id,f.item_family_name

		---YTD VALUE
		UNION

		SELECT   f.item_family_id,f.item_family_name, 0 Sales_value_year_0, SUM(Value) Sales_ytd_value,0 Sales_ytm_value, 0 Sales_value_month_0
		FROM   tt_salesnumbers s,tt_item_master itm, tt_item_family f, tt_customer_master cust
		WHERE  inv_year = @ActualYear AND itm.item_code = s.item_code AND f.item_family_id = itm.item_family_id 
		  AND inv_month <= @ActualMonth
		  AND cust.customer_number= s.customer_number
		  --AND (@BranchCode = 'ALL' OR (cust.customer_region = @BranchCode or cust.territory_engineer_id=@BranchCode))
		  AND   (@BranchCode  = 'ALL' OR (cust.Customer_region in (select Item from SplitString(@BranchCode,''',''')) OR cust.territory_engineer_id in (select Item from SplitString(@BranchCode,''','''))))
		  AND (@salesengineer_id = 'ALL' OR (cust.assigned_salesengineer_id = @salesengineer_id))
		  AND (@customer_type = 'ALL' OR (cust.customer_type = @customer_type))
		  AND (@cter IS NULL OR cust.cter = @cter)
		GROUP BY  f.item_family_id,f.item_family_name

		----YTM value
		UNION

		SELECT   f.item_family_id,f.item_family_name, 0 Sales_value_year_0, 0 Sales_ytd_value,SUM(Value) Sales_ytm_value, 0 Sales_value_month_0
		FROM   tt_salesnumbers s,tt_item_master itm, tt_item_family f, tt_customer_master cust
		WHERE  inv_year = @ActualYear AND itm.item_code = s.item_code AND f.item_family_id = itm.item_family_id 
		  AND inv_month < @ActualMonth
		  AND cust.customer_number= s.customer_number
		  --AND (@BranchCode = 'ALL' OR (cust.customer_region = @BranchCode or cust.territory_engineer_id=@BranchCode))
		  AND   (@BranchCode  = 'ALL' OR (cust.Customer_region in (select Item from SplitString(@BranchCode,''',''')) OR cust.territory_engineer_id in (select Item from SplitString(@BranchCode,''','''))))
		  AND (@salesengineer_id = 'ALL' OR (cust.assigned_salesengineer_id = @salesengineer_id))
		  AND (@customer_type = 'ALL' OR (cust.customer_type = @customer_type))
		  AND (@cter IS NULL OR cust.cter = @cter) 
		GROUP BY  f.item_family_id,f.item_family_name

		UNION
		--- Actual year - 1 (MTD vale)>> Sales_value_month_0
		SELECT  f.item_family_id, f.item_family_name, 0 Sales_value_year_0, 0 Sales_ytd_value,0 Sales_ytm_value, SUM(value) Sales_value_month_0
		FROM   tt_salesnumbers s,tt_item_master itm, tt_item_family f, tt_customer_master cust
		WHERE  inv_year = @ActualYear - 1 AND inv_month = @ActualMonth AND itm.item_code = s.item_code AND f.item_family_id = itm.item_family_id 
		   AND cust.customer_number= s.customer_number
		   --AND (@BranchCode = 'ALL' OR (cust.customer_region = @BranchCode or cust.territory_engineer_id=@BranchCode))
		   AND   (@BranchCode  = 'ALL' OR (cust.Customer_region in (select Item from SplitString(@BranchCode,''',''')) OR cust.territory_engineer_id in (select Item from SplitString(@BranchCode,''','''))))
		   AND (@salesengineer_id = 'ALL' OR (cust.assigned_salesengineer_id = @salesengineer_id))
		   AND (@customer_type = 'ALL' OR (cust.customer_type = @customer_type))
		   AND (@cter IS NULL OR cust.cter = @cter) 
		GROUP BY  f.item_family_id,f.item_family_name

		UNION

		SELECT  f.item_family_id, f.item_family_name, 0 Sales_value_year_0, 0 Sales_ytd_value,0 Sales_ytm_value, 0 Sales_value_month_0
		FROM   tt_item_master itm, tt_item_family f, tt_customer_master cust
		WHERE  itm.item_code NOT IN (select item_code from tt_salesnumbers)
			  AND f.item_family_status='ACTIVE'
			  AND f.item_family_id = itm.item_family_id 
			  --AND (@BranchCode = 'ALL' OR (cust.customer_region = @BranchCode or cust.territory_engineer_id=@BranchCode ))
			  AND   (@BranchCode  = 'ALL' OR (cust.Customer_region in (select Item from SplitString(@BranchCode,''',''')) OR cust.territory_engineer_id in (select Item from SplitString(@BranchCode,''','''))))
			  AND (@salesengineer_id = 'ALL' OR (cust.assigned_salesengineer_id = @salesengineer_id))
			  AND (@customer_type = 'ALL' OR (cust.customer_type = @customer_type))  
			  AND (@cter IS NULL OR cust.cter = @cter)
		GROUP BY  f.item_family_id,f.item_family_name
) tmp

GROUP BY tmp.item_family_id,
    tmp.item_family_name 
)

select item_family_name ,CEILING(NULLIF(Sales_value_year_0,0)) as Sales_value_year_0,CEILING(NULLIF(budget_value,0)) as budget_value,
CEILING(NULLIF(Sales_ytd_value,0)) Sales_ytd_value ,
CEILING(Sales_ytm_value+((budget_value-Sales_ytm_value)/@MonthsRemaining)) ytd_plan,
CEILING(((Sales_ytd_value)/NULLIF(Sales_ytm_value+((budget_value-Sales_ytm_value)/@MonthsRemaining),0))*100) as achvmnt,
CEILING(((budget_value-Sales_ytd_value)/NULLIF(@MonthsRemaining,0))) as Askrate,
0 as N0_Values, ---0 added to make the column count equal in all cases
--CEILING(((NULLIF(Sales_ytd_value,0)/NULLIF(Sales_value_year_0,0))-1)*100) growth 
CEILING(ISNULL((ISNULL(dbo.get_divide_by(Sales_ytd_value,Sales_value_year_0),0)-1)*100,0)) growth
INTO #TEMP
from Family_CTE    
order by item_family_id  asc


SELECT * FROM #TEMP
UNION ALL
select 'Total' item_family_name ,SUM(Sales_value_year_0) as Sales_value_year_0,SUM(budget_value) as budget_value,
SUM(Sales_ytd_value) Sales_ytd_value ,
SUM(ytd_plan) ytd_plan,
--SUM(achvmnt) as achvmnt,
CEILING((SUM(NULLIF(Sales_ytd_value,0))/SUM(NULLIF(ytd_plan,0)))*100) as achvmnt,
SUM(Askrate) as Askrate,
--(((SUM(NULLIF(budget_value,0))-SUM(NULLIF(Sales_ytd_value,0)))/NULLIF(@MonthsRemaining,0))) as Askrate,
0 as N0_Values, ---0 added to make the column count equal in all cases
--SUM(growth) growth 
--(((SUM(NULLIF(Sales_ytd_value,0))/(SUM(NULLIF(Sales_value_year_0,0))))-1)*100)  growth 
CEILING(ISNULL((ISNULL(dbo.get_divide_by(SUM(NULLIF(Sales_ytd_value,0)),SUM(NULLIF(Sales_value_year_0,0))),0)-1)*100,0)) growth
FROM #TEMP

END


--CASE: BRANCHES OF GOLD PRODUCTS
ELSE IF(@topfiveflag='Gold')
BEGIN

--WITH GoldBranches(region_description,Customer_region,Sales_value_year_0,Sales_ytd_value,budget_value,Sales_ytm_value)
--AS(
SELECT tmp.region_description,tmp.Customer_region,
COALESCE (CEILING(SUM(ISNULL(Sales_value_year_0,0)) + ((sum(Sales_value_month_0)/@DaysInMonth)*@Today)),0)Sales_value_year_0,
COALESCE (CEILING(SUM(Sales_ytd_value)),0)Sales_ytd_value,
CEILING(dbo.get_budget_by_branch_month( @ActualYear, 'GOLD', NUll, NULL,@customer_type, NULL, @salesengineer_id,tmp.Customer_region,NULL,@cter))  budget_value,       

COALESCE (CEILING(SUM(Sales_ytm_value)),0)Sales_ytm_value INTO #GOLDBRANCHES
          FROM (SELECT  rgn.region_description,  c.Customer_region,    
						Sales_value_month_0 = 	CASE WHEN s.inv_month = @ActualMonth  and s.inv_year = @ActualYear - 1 THEN value END,	       
						Sales_value_year_0 = CASE WHEN s.inv_month < @ActualMonth  and s.inv_year = @ActualYear - 1 THEN value END,
						Sales_ytd_value = CASE WHEN s.inv_month <= @ActualMonth and s.inv_year = @ActualYear THEN value END,
						Sales_ytm_value = CASE WHEN s.inv_month < @ActualMonth and s.inv_year = @ActualYear THEN value END
				FROM    tt_customer_master c ,  tt_salesnumbers s,  tt_item_master i,tt_item_family f, tt_region rgn
				where 
				s.customer_number=c.customer_number
				AND s.item_code=i.item_code
				AND i.item_family_id = f.item_family_id
				and i.gold_flag='Y' 
				--AND (@BranchCode IS NULL OR (c.Customer_region=@BranchCode OR c.territory_engineer_id=@BranchCode))
				AND   (@BranchCode IS NULL OR (c.Customer_region in (select Item from SplitString(@BranchCode,''',''')) OR c.territory_engineer_id in (select Item from SplitString(@BranchCode,''','''))))
				AND rgn.region_code=c.Customer_region
				)tmp
				group by tmp.Customer_region,tmp.region_description
--)
select  region_description,
CEILING(NULLIF(Sales_value_year_0,0)) as Sales_value_year_0,
CEILING(NULLIF(budget_value,0)) as budget_value,
CEILING(NULLIF(Sales_ytd_value,0)) Sales_ytd_value ,
CEILING(Sales_ytm_value+((budget_value-Sales_ytm_value)/@MonthsRemaining)) ytd_plan,
--CEILING(((NULLIF(budget_value,0)))/12)*@ActualMonth ytd_plan ,
CEILING(((Sales_ytd_value)/NULLIF(Sales_ytm_value+((budget_value-Sales_ytm_value)/@MonthsRemaining),0))*100) as achvmnt,
CEILING(((budget_value-Sales_ytd_value))/NULLIF(@MonthsRemaining,0)) as askrate,
0 as No_Values,  
CEILING(((NULLIF(Sales_ytd_value,0)/NULLIF(Sales_value_year_0,0))-1)*100) growth   INTO #GOLDBRANCHES_Final1
from #GOLDBRANCHES 
WHERE budget_value IS NOT NULL AND budget_value NOT IN(0) 
order by ---0 added to make the column count equal in all cases
CEILING(NULLIF(budget_value,0)) DESC  

SELECT * FROM #GOLDBRANCHES_Final1
UNION ALL
select 'Total' region_description,
SUM(Sales_value_year_0) as Sales_value_year_0,
SUM(budget_value) as budget_value,
SUM(Sales_ytd_value) Sales_ytd_value ,
SUM(Sales_ytm_value+((budget_value-Sales_ytm_value)/@MonthsRemaining)) ytd_plan,
--CEILING(((NULLIF(budget_value,0)))/12)*@ActualMonth ytd_plan ,
--SUM(((Sales_ytd_value)/NULLIF(Sales_ytm_value+((budget_value-Sales_ytm_value)/@MonthsRemaining),0))*100) as achvmnt,
CEILING((SUM(NULLIF(Sales_ytd_value,0))/(NULLIF(SUM(NULLIF(Sales_ytm_value,0))+((SUM(NULLIF(budget_value,0))-SUM(NULLIF(Sales_ytm_value,0)))/@MonthsRemaining),0)))*100) as achvmnt,
SUM(((budget_value-Sales_ytd_value))/NULLIF(@MonthsRemaining,0)) as askrate,
--(((SUM(NULLIF(budget_value,0))-SUM(NULLIF(Sales_ytd_value,0)))/NULLIF(@MonthsRemaining,0))) as Askrate,
0 as No_Values,  
--SUM(((NULLIF(Sales_ytd_value,0)/NULLIF(Sales_value_year_0,0))-1)*100) growth  
(((SUM(NULLIF(Sales_ytd_value,0))/(SUM(NULLIF(Sales_value_year_0,0))))-1)*100)  growth 
from #GOLDBRANCHES 
WHERE budget_value IS NOT NULL AND budget_value NOT IN(0)


select  region_description,
CEILING(NULLIF(Sales_value_year_0,0)) as Sales_value_year_0,
CEILING(NULLIF(budget_value,0)) as budget_value,
CEILING(NULLIF(Sales_ytd_value,0)) Sales_ytd_value ,
CEILING(Sales_ytm_value+((budget_value-Sales_ytm_value)/@MonthsRemaining)) ytd_plan,
--CEILING(((NULLIF(budget_value,0)))/12)*@ActualMonth ytd_plan ,
CEILING(((Sales_ytd_value)/NULLIF(Sales_ytm_value+((budget_value-Sales_ytm_value)/@MonthsRemaining),0))*100) as achvmnt,
CEILING(((budget_value-Sales_ytd_value))/NULLIF(@MonthsRemaining,0)) as askrate,
0 as No_Values,  
ISNULL(CEILING(((NULLIF(Sales_ytd_value,0)/NULLIF(Sales_value_year_0,0))-1)*100),0) growth   INTO #GOLDBRANCHES_Final2
from #GOLDBRANCHES 
WHERE budget_value IS NOT NULL AND budget_value NOT IN(0) 
order by ---0 added to make the column count equal in all cases
ISNULL(CEILING(((NULLIF(Sales_ytd_value,0)/NULLIF(Sales_value_year_0,0))-1)*100),0) DESC 

SELECT * FROM #GOLDBRANCHES_Final2
UNION ALL
select 'Total'  region_description,
SUM(Sales_value_year_0) as Sales_value_year_0,
SUM(budget_value) as budget_value,
SUM(Sales_ytd_value) Sales_ytd_value ,
SUM(Sales_ytm_value+((budget_value-Sales_ytm_value)/@MonthsRemaining)) ytd_plan,
--CEILING(((NULLIF(budget_value,0)))/12)*@ActualMonth ytd_plan ,
--SUM(((Sales_ytd_value)/NULLIF(Sales_ytm_value+((budget_value-Sales_ytm_value)/@MonthsRemaining),0))*100) as achvmnt,
CEILING((SUM(NULLIF(Sales_ytd_value,0))/(NULLIF(SUM(NULLIF(Sales_ytm_value,0))+((SUM(NULLIF(budget_value,0))-SUM(NULLIF(Sales_ytm_value,0)))/@MonthsRemaining),0)))*100) as achvmnt,
SUM(((budget_value-Sales_ytd_value))/NULLIF(@MonthsRemaining,0)) as askrate,
--(((SUM(NULLIF(budget_value,0))-SUM(NULLIF(Sales_ytd_value,0)))/NULLIF(@MonthsRemaining,0))) as Askrate,
0 as No_Values,  
--SUM(ISNULL(CEILING(((NULLIF(Sales_ytd_value,0)/NULLIF(Sales_value_year_0,0))-1)*100),0)) growth   
(((SUM(NULLIF(Sales_ytd_value,0))/(SUM(NULLIF(Sales_value_year_0,0))))-1)*100)  growth 
from #GOLDBRANCHES 
WHERE budget_value IS NOT NULL AND budget_value NOT IN(0) 

--WITH GoldSE(assigned_salesengineer_id,EngineerName,Sales_value_year_0,Sales_ytd_value,budget_value,Sales_ytm_value)
--AS(
SELECT tmp.assigned_salesengineer_id,tmp.EngineerName, tmp.Branch, tmp.RoleId,
COALESCE (CEILING(SUM(ISNULL(Sales_value_year_0,0)) + ((sum(Sales_value_month_0)/@DaysInMonth)*@Today)),0)Sales_value_year_0,
COALESCE (CEILING(SUM(Sales_ytd_value)),0)Sales_ytd_value,
CEILING(dbo.get_budget_by_branch_month( @ActualYear, 'GOLD', NUll, NULL,@customer_type, NULL, tmp.assigned_salesengineer_id,@BranchCode,NULL,@cter))  budget_value,       

COALESCE (CEILING(SUM(Sales_ytm_value)),0)Sales_ytm_value INTO #GOLDSE
           FROM (SELECT  c.assigned_salesengineer_id,li.EngineerName ,  tr.region_description Branch, li.RoleId,
						 Sales_value_month_0 = 	CASE WHEN s.inv_month = @ActualMonth  and s.inv_year = @ActualYear - 1 THEN value END,	       
						 Sales_value_year_0 = CASE WHEN s.inv_month < @ActualMonth  and s.inv_year = @ActualYear - 1 THEN value END,
						 Sales_ytd_value = CASE WHEN s.inv_month <= @ActualMonth and s.inv_year = @ActualYear THEN value END,
						 Sales_ytm_value = CASE WHEN s.inv_month < @ActualMonth and s.inv_year = @ActualYear THEN value END
				FROM    tt_customer_master c ,  tt_salesnumbers s,  tt_item_master i,tt_item_family f,LoginInfo li, tt_region tr
				where 
				li.EngineerId=c.assigned_salesengineer_id
				AND  s.customer_number=c.customer_number 
				AND s.item_code=i.item_code
				AND i.item_family_id = f.item_family_id
				AND c.Customer_region=tr.region_code
				and i.gold_flag='Y'
				--AND (@BranchCode IS NULL OR (c.Customer_region=@BranchCode OR c.territory_engineer_id=@BranchCode)) 
				AND   (@BranchCode IS NULL OR (c.Customer_region in (select Item from SplitString(@BranchCode,''',''')) OR c.territory_engineer_id in (select Item from SplitString(@BranchCode,''','''))))
				AND(c.assigned_salesengineer_id =ISNULL(@salesengineer_id,c.assigned_salesengineer_id)) 
				)tmp
group by tmp.assigned_salesengineer_id,tmp.EngineerName, tmp.Branch, tmp.RoleId
--)
select  EngineerName, Branch,
CEILING(NULLIF(Sales_value_year_0,0)) as Sales_value_year_0,
CEILING(NULLIF(budget_value,0)) as budget_value,
CEILING(NULLIF(Sales_ytd_value,0)) Sales_ytd_value ,
CEILING(Sales_ytm_value+((budget_value-Sales_ytm_value)/@MonthsRemaining)) ytd_plan,
CEILING(((Sales_ytd_value)/NULLIF(Sales_ytm_value+((budget_value-Sales_ytm_value)/@MonthsRemaining),0))*100) as achvmnt,
CEILING(((budget_value-Sales_ytd_value))/NULLIF(@MonthsRemaining,0)) as Askrate,
0 as No_column,  
CEILING(((NULLIF(Sales_ytd_value,0)/NULLIF(Sales_value_year_0,0))-1)*100) growth  INTO #GOLDSE_Final1
from #GOLDSE  
WHERE budget_value IS NOT NULL AND budget_value NOT IN(0) 
order by ---0 added to make the column count equal in all cases
CEILING(NULLIF(budget_value,0)) DESC

SELECT * FROM #GOLDSE_Final1
UNION ALL
select 'Total' EngineerName,'',
SUM(Sales_value_year_0) as Sales_value_year_0,
SUM(budget_value) as budget_value,
SUM(Sales_ytd_value) Sales_ytd_value ,
SUM(Sales_ytm_value+((budget_value-Sales_ytm_value)/@MonthsRemaining)) ytd_plan,
--SUM(((Sales_ytd_value)/NULLIF(Sales_ytm_value+((budget_value-Sales_ytm_value)/@MonthsRemaining),0))*100) as achvmnt,
CEILING((SUM(NULLIF(Sales_ytd_value,0))/(NULLIF(SUM(NULLIF(Sales_ytm_value,0))+((SUM(NULLIF(budget_value,0))-SUM(NULLIF(Sales_ytm_value,0)))/@MonthsRemaining),0)))*100) as achvmnt,
SUM(((budget_value-Sales_ytd_value))/NULLIF(@MonthsRemaining,0)) as Askrate,
--(((SUM(NULLIF(budget_value,0))-SUM(NULLIF(Sales_ytd_value,0)))/NULLIF(@MonthsRemaining,0))) as Askrate,
0 as No_column,  
--SUM(((NULLIF(Sales_ytd_value,0)/NULLIF(Sales_value_year_0,0))-1)*100) growth  
(((SUM(NULLIF(Sales_ytd_value,0))/(SUM(NULLIF(Sales_value_year_0,0))))-1)*100)  growth 
from #GOLDSE  
WHERE budget_value IS NOT NULL AND budget_value NOT IN(0) 


select  EngineerName, Branch,
CEILING(NULLIF(Sales_value_year_0,0)) as Sales_value_year_0,
CEILING(NULLIF(budget_value,0)) as budget_value,
CEILING(NULLIF(Sales_ytd_value,0)) Sales_ytd_value ,
CEILING(Sales_ytm_value+((budget_value-Sales_ytm_value)/@MonthsRemaining)) ytd_plan,
CEILING(((Sales_ytd_value)/NULLIF(Sales_ytm_value+((budget_value-Sales_ytm_value)/@MonthsRemaining),0))*100) as achvmnt,
CEILING(((budget_value-Sales_ytd_value))/NULLIF(@MonthsRemaining,0)) as Askrate,
--(((SUM(NULLIF(budget_value,0))-SUM(NULLIF(Sales_ytd_value,0)))/NULLIF(@MonthsRemaining,0))) as Askrate,
0 as No_Column,  
ISNULL(CEILING(((NULLIF(Sales_ytd_value,0)/NULLIF(Sales_value_year_0,0))-1)*100),0) growth  INTO #GOLDSE_Final2
from #GOLDSE  
WHERE budget_value IS NOT NULL AND budget_value NOT IN(0) 
order by ---0 added to make the column count equal in all cases
ISNULL(CEILING(((NULLIF(Sales_ytd_value,0)/NULLIF(Sales_value_year_0,0))-1)*100),0) DESC

SELECT * FROM #GOLDSE_Final2
UNION ALL
select 'Total' EngineerName,'',
SUM(Sales_value_year_0) as Sales_value_year_0,
SUM(budget_value) as budget_value,
SUM(Sales_ytd_value) Sales_ytd_value ,
SUM(Sales_ytm_value+((budget_value-Sales_ytm_value)/@MonthsRemaining)) ytd_plan,
--SUM(((Sales_ytd_value)/NULLIF(Sales_ytm_value+((budget_value-Sales_ytm_value)/@MonthsRemaining),0))*100) as achvmnt,
CEILING((SUM(NULLIF(Sales_ytd_value,0))/(NULLIF(SUM(NULLIF(Sales_ytm_value,0))+((SUM(NULLIF(budget_value,0))-SUM(NULLIF(Sales_ytm_value,0)))/@MonthsRemaining),0)))*100) as achvmnt,
SUM(((budget_value-Sales_ytd_value))/NULLIF(@MonthsRemaining,0)) as Askrate,
--(((SUM(NULLIF(budget_value,0))-SUM(NULLIF(Sales_ytd_value,0)))/NULLIF(@MonthsRemaining,0))) as Askrate,
0 as No_Column,  
--SUM(ISNULL(CEILING(((NULLIF(Sales_ytd_value,0)/NULLIF(Sales_value_year_0,0))-1)*100),0)) growth  
(((SUM(NULLIF(Sales_ytd_value,0))/(SUM(NULLIF(Sales_value_year_0,0))))-1)*100)  growth 
from #GOLDSE  
WHERE budget_value IS NOT NULL AND budget_value NOT IN(0) 

select  EngineerName, Branch,
CEILING(NULLIF(Sales_value_year_0,0)) as Sales_value_year_0,
CEILING(NULLIF(budget_value,0)) as budget_value,
CEILING(NULLIF(Sales_ytd_value,0)) Sales_ytd_value ,
CEILING(Sales_ytm_value+((budget_value-Sales_ytm_value)/@MonthsRemaining)) ytd_plan,
CEILING(((Sales_ytd_value)/NULLIF(Sales_ytm_value+((budget_value-Sales_ytm_value)/@MonthsRemaining),0))*100) as achvmnt,
CEILING(((budget_value-Sales_ytd_value))/NULLIF(@MonthsRemaining,0)) as Askrate,
--(((SUM(NULLIF(budget_value,0))-SUM(NULLIF(Sales_ytd_value,0)))/NULLIF(@MonthsRemaining,0))) as Askrate,
0 as No_column,  
CEILING(((NULLIF(Sales_ytd_value,0)/NULLIF(Sales_value_year_0,0))-1)*100) growth  INTO #GOLDSE_Final3
from #GOLDSE  
WHERE budget_value IS NOT NULL AND budget_value NOT IN(0) AND RoleId='SE'
order by ---0 added to make the column count equal in all cases
CEILING(NULLIF(budget_value,0))

SELECT * FROM #GOLDSE_Final3
UNION ALL
select 'Total' EngineerName, '',
SUM(Sales_value_year_0) as Sales_value_year_0,
SUM(budget_value) as budget_value,
SUM(Sales_ytd_value) Sales_ytd_value ,
SUM(Sales_ytm_value+((budget_value-Sales_ytm_value)/@MonthsRemaining)) ytd_plan,
--SUM(((Sales_ytd_value)/NULLIF(Sales_ytm_value+((budget_value-Sales_ytm_value)/@MonthsRemaining),0))*100) as achvmnt,
CEILING((SUM(NULLIF(Sales_ytd_value,0))/(NULLIF(SUM(NULLIF(Sales_ytm_value,0))+((SUM(NULLIF(budget_value,0))-SUM(NULLIF(Sales_ytm_value,0)))/@MonthsRemaining),0)))*100) as achvmnt,
SUM(((budget_value-Sales_ytd_value))/NULLIF(@MonthsRemaining,0)) as Askrate,
--(((SUM(NULLIF(budget_value,0))-SUM(NULLIF(Sales_ytd_value,0)))/NULLIF(@MonthsRemaining,0))) as Askrate,
0 as No_column,  
--SUM(((NULLIF(Sales_ytd_value,0)/NULLIF(Sales_value_year_0,0))-1)*100) growth  
(((SUM(NULLIF(Sales_ytd_value,0))/(SUM(NULLIF(Sales_value_year_0,0))))-1)*100)  growth 
from #GOLDSE  
WHERE budget_value IS NOT NULL AND budget_value NOT IN(0) 

select  EngineerName, Branch,
CEILING(NULLIF(Sales_value_year_0,0)) as Sales_value_year_0,
CEILING(NULLIF(budget_value,0)) as budget_value,
CEILING(NULLIF(Sales_ytd_value,0)) Sales_ytd_value ,
CEILING(Sales_ytm_value+((budget_value-Sales_ytm_value)/@MonthsRemaining)) ytd_plan,
CEILING(((Sales_ytd_value)/NULLIF(Sales_ytm_value+((budget_value-Sales_ytm_value)/@MonthsRemaining),0))*100) as achvmnt,
CEILING(((budget_value-Sales_ytd_value))/NULLIF(@MonthsRemaining,0)) as Askrate,
0 as No_column,  
ISNULL(CEILING(((NULLIF(Sales_ytd_value,0)/NULLIF(Sales_value_year_0,0))-1)*100),0) growth  INTO #GOLDSE_Final4
from #GOLDSE  
WHERE budget_value IS NOT NULL AND budget_value NOT IN(0) AND RoleId='SE'
order by ---0 added to make the column count equal in all cases
ISNULL(CEILING(((NULLIF(Sales_ytd_value,0)/NULLIF(Sales_value_year_0,0))-1)*100),0) 

SELECT * FROM #GOLDSE_Final4
UNION ALL
select 'Total'  EngineerName, '',
SUM(Sales_value_year_0) as Sales_value_year_0,
SUM(budget_value) as budget_value,
SUM(Sales_ytd_value) Sales_ytd_value ,
SUM(Sales_ytm_value+((budget_value-Sales_ytm_value)/@MonthsRemaining)) ytd_plan,
--SUM(((Sales_ytd_value)/NULLIF(Sales_ytm_value+((budget_value-Sales_ytm_value)/@MonthsRemaining),0))*100) as achvmnt,
CEILING((SUM(NULLIF(Sales_ytd_value,0))/(NULLIF(SUM(NULLIF(Sales_ytm_value,0))+((SUM(NULLIF(budget_value,0))-SUM(NULLIF(Sales_ytm_value,0)))/@MonthsRemaining),0)))*100) as achvmnt,
SUM(((budget_value-Sales_ytd_value))/NULLIF(@MonthsRemaining,0)) as Askrate,
--(((SUM(NULLIF(budget_value,0))-SUM(NULLIF(Sales_ytd_value,0)))/NULLIF(@MonthsRemaining,0))) as Askrate,
0 as No_column,  
--SUM(ISNULL(CEILING(((NULLIF(Sales_ytd_value,0)/NULLIF(Sales_value_year_0,0))-1)*100),0)) growth  
(((SUM(NULLIF(Sales_ytd_value,0))/(SUM(NULLIF(Sales_value_year_0,0))))-1)*100)  growth 
from #GOLDSE  
WHERE budget_value IS NOT NULL AND budget_value NOT IN(0) 


--WITH goldcustomers_CTE(customer_short_name,customer_number,Sales_value_year_0,Sales_ytd_value,budget_value,Sales_ytm_value)AS(
SELECT    tmp.customer_short_name, tmp.customer_number,   tmp.Branch,   tmp.EngineerName,
COALESCE (CEILING(SUM(ISNULL(Sales_value_year_0,0)) + ((sum(Sales_value_month_0)/@DaysInMonth)*@Today)),0)Sales_value_year_0,       
COALESCE (CEILING(SUM(Sales_ytd_value)),0)Sales_ytd_value,
CEILING(dbo.get_budget_by_branch_month( @ActualYear, 'GOLD', NUll, NULL,'C', tmp.customer_number, @salesengineer_id,@BranchCode,NULL,@cter))  budget_value,
COALESCE (CEILING(SUM(Sales_ytm_value)),0)Sales_ytm_value  INTO #TEMPGCUSTOMER    
--CEILING(dbo.get_budget_val(@BudgetYear,null, tmp.customer_number, @salesengineer_id, @BranchCode,@customer_type)) budget_value
           FROM (SELECT c.customer_short_name,c.customer_number, tr.region_description Branch ,     tl.EngineerName,
						Sales_value_month_0 = 	CASE WHEN s.inv_month = @ActualMonth  and s.inv_year = @ActualYear - 1 THEN value END,	       
						Sales_value_year_0 = CASE WHEN s.inv_month < @ActualMonth  and s.inv_year = @ActualYear - 1 THEN value END,
						Sales_ytd_value = CASE WHEN s.inv_month <= @ActualMonth and s.inv_year = @ActualYear THEN value END,
						Sales_ytm_value = CASE WHEN s.inv_month < @ActualMonth and s.inv_year = @ActualYear THEN value END
				FROM    tt_customer_master c ,  tt_salesnumbers s,  tt_item_master i,tt_item_family f, tt_region tr
				, LoginInfo tl
				where 
				s.customer_number=c.customer_number
				AND s.item_code=i.item_code
				AND i.item_family_id = f.item_family_id
				AND c.Customer_region=tr.region_code
				and i.gold_flag='Y'
				AND tl.EngineerId=c.assigned_salesengineer_id
				AND (c.assigned_salesengineer_id =ISNULL(@salesengineer_id,c.assigned_salesengineer_id))
				AND  (c.customer_type =ISNULL(@customer_type,c.customer_type))
				--AND (@BranchCode IS NULL OR (c.Customer_region=@BranchCode OR c.territory_engineer_id=@BranchCode))
				AND   (@BranchCode IS NULL OR (c.Customer_region in (select Item from SplitString(@BranchCode,''',''')) OR c.territory_engineer_id in (select Item from SplitString(@BranchCode,''','''))))
				)tmp
GROUP BY tmp.customer_short_name,tmp.customer_number, tmp.Branch, tmp.EngineerName
--)
select  customer_short_name , Branch,EngineerName,
CEILING(NULLIF(Sales_value_year_0,0)) as Sales_value_year_0,
CEILING(NULLIF(budget_value,0)) as budget_value,
CEILING(NULLIF(Sales_ytd_value,0)) Sales_ytd_value ,
CEILING(Sales_ytm_value+((budget_value-Sales_ytm_value)/@MonthsRemaining)) ytd_plan,
CEILING(((Sales_ytd_value)/NULLIF(Sales_ytm_value+((budget_value-Sales_ytm_value)/@MonthsRemaining),0))*100) as achvmnt,
CEILING(((budget_value-Sales_ytd_value)/NULLIF(@MonthsRemaining,0))) as Askrate,
customer_number ,
CEILING(((NULLIF(Sales_ytd_value,0)/NULLIF(Sales_value_year_0,0))-1)*100) growth INTO  #TEMPGCUSTOMER_Final1
from #TEMPGCUSTOMER  
WHERE budget_value IS NOT NULL AND budget_value NOT IN(0)  
order by 
CEILING(NULLIF(budget_value,0)) DESC

SELECT * FROM #TEMPGCUSTOMER_Final1
UNION ALL
select '' customer_short_name ,'','',
SUM(Sales_value_year_0) as Sales_value_year_0,
SUM(budget_value) as budget_value,
SUM(Sales_ytd_value) Sales_ytd_value ,
SUM(Sales_ytm_value+((budget_value-Sales_ytm_value)/@MonthsRemaining)) ytd_plan,
--SUM(((Sales_ytd_value)/NULLIF(Sales_ytm_value+((budget_value-Sales_ytm_value)/@MonthsRemaining),0))*100) as achvmnt,
CEILING((SUM(NULLIF(Sales_ytd_value,0))/(NULLIF(SUM(NULLIF(Sales_ytm_value,0))+((SUM(NULLIF(budget_value,0))-SUM(NULLIF(Sales_ytm_value,0)))/@MonthsRemaining),0)))*100) as achvmnt,
SUM(((budget_value-Sales_ytd_value)/NULLIF(@MonthsRemaining,0))) as Askrate,
--(((SUM(NULLIF(budget_value,0))-SUM(NULLIF(Sales_ytd_value,0)))/NULLIF(@MonthsRemaining,0))) as Askrate,
'Total' customer_number ,
--SUM(((NULLIF(Sales_ytd_value,0)/NULLIF(Sales_value_year_0,0))-1)*100) growth 
(((SUM(NULLIF(Sales_ytd_value,0))/(SUM(NULLIF(Sales_value_year_0,0))))-1)*100)  growth 
from #TEMPGCUSTOMER  
WHERE budget_value IS NOT NULL AND budget_value NOT IN(0) 

select  customer_short_name , Branch,EngineerName,
CEILING(NULLIF(Sales_value_year_0,0)) as Sales_value_year_0,
CEILING(NULLIF(budget_value,0)) as budget_value,
CEILING(NULLIF(Sales_ytd_value,0)) Sales_ytd_value ,
CEILING(Sales_ytm_value+((budget_value-Sales_ytm_value)/@MonthsRemaining)) ytd_plan,
CEILING(((Sales_ytd_value)/NULLIF(Sales_ytm_value+((budget_value-Sales_ytm_value)/@MonthsRemaining),0))*100) as achvmnt,
CEILING(((budget_value-Sales_ytd_value)/NULLIF(@MonthsRemaining,0))) as Askrate,
customer_number ,
ISNULL(CEILING(((NULLIF(Sales_ytd_value,0)/NULLIF(Sales_value_year_0,0))-1)*100),0) growth INTO  #TEMPGCUSTOMER_Final2   
from #TEMPGCUSTOMER  
WHERE budget_value IS NOT NULL AND budget_value NOT IN(0)  
order by 
ISNULL(CEILING(((NULLIF(Sales_ytd_value,0)/NULLIF(Sales_value_year_0,0))-1)*100),0) DESC

SELECT * FROM #TEMPGCUSTOMER_Final2
UNION ALL
select '' customer_short_name ,'','',
SUM(Sales_value_year_0) as Sales_value_year_0,
SUM(budget_value) as budget_value,
SUM(Sales_ytd_value) Sales_ytd_value ,
SUM(Sales_ytm_value+((budget_value-Sales_ytm_value)/@MonthsRemaining)) ytd_plan,
--SUM(((Sales_ytd_value)/NULLIF(Sales_ytm_value+((budget_value-Sales_ytm_value)/@MonthsRemaining),0))*100) as achvmnt,
CEILING((SUM(NULLIF(Sales_ytd_value,0))/(NULLIF(SUM(NULLIF(Sales_ytm_value,0))+((SUM(NULLIF(budget_value,0))-SUM(NULLIF(Sales_ytm_value,0)))/@MonthsRemaining),0)))*100) as achvmnt,
SUM(((budget_value-Sales_ytd_value)/NULLIF(@MonthsRemaining,0))) as Askrate,
--(((SUM(NULLIF(budget_value,0))-SUM(NULLIF(Sales_ytd_value,0)))/NULLIF(@MonthsRemaining,0))) as Askrate,
'Total' customer_number ,
--SUM(ISNULL(CEILING(((NULLIF(Sales_ytd_value,0)/NULLIF(Sales_value_year_0,0))-1)*100),0)) growth  
(((SUM(NULLIF(Sales_ytd_value,0))/(SUM(NULLIF(Sales_value_year_0,0))))-1)*100)  growth 
from #TEMPGCUSTOMER  
WHERE budget_value IS NOT NULL AND budget_value NOT IN(0)  


SELECT    tmp.customer_short_name, tmp.customer_number,   tmp.Branch,    tmp.EngineerName,
COALESCE (CEILING(SUM(ISNULL(Sales_value_year_0,0)) + ((sum(Sales_value_month_0)/@DaysInMonth)*@Today)),0) Sales_value_year_0,       
COALESCE (CEILING(SUM(Sales_ytd_value)),0) Sales_ytd_value,
CEILING(dbo.get_budget_by_branch_month( @ActualYear, 'GOLD', NUll, NULL,'D', tmp.customer_number, @salesengineer_id,@BranchCode,NULL,@cter))  budget_value,
COALESCE (CEILING(SUM(Sales_ytm_value)),0)Sales_ytm_value  INTO #TEMPGCHANNEL    
--CEILING(dbo.get_budget_val(@BudgetYear,null, tmp.customer_number, @salesengineer_id, @BranchCode,@customer_type)) budget_value
           FROM (SELECT c.customer_short_name,c.customer_number,  tr.region_description Branch,     tl.EngineerName,
						Sales_value_month_0 = 	CASE WHEN s.inv_month = @ActualMonth  and s.inv_year = @ActualYear - 1 THEN value END,	       
						Sales_value_year_0 = CASE WHEN s.inv_month < @ActualMonth  and s.inv_year = @ActualYear - 1 THEN value END,
						Sales_ytd_value = CASE WHEN s.inv_month <= @ActualMonth and s.inv_year = @ActualYear THEN value END,
						Sales_ytm_value = CASE WHEN s.inv_month < @ActualMonth and s.inv_year = @ActualYear THEN value END
				FROM    tt_customer_master c ,  tt_salesnumbers s,  tt_item_master i,tt_item_family f, tt_region tr, LoginInfo tl
				where 
				s.customer_number=c.customer_number
				AND s.item_code=i.item_code
				AND i.item_family_id = f.item_family_id
				AND c.Customer_region=tr.region_code
				and i.gold_flag='Y'
				AND tl.EngineerId=c.assigned_salesengineer_id
				AND (c.assigned_salesengineer_id =ISNULL(@salesengineer_id,c.assigned_salesengineer_id))
				AND  (c.customer_type =ISNULL(@customer_type,c.customer_type))
				--AND (@BranchCode IS NULL OR (c.Customer_region=@BranchCode OR c.territory_engineer_id=@BranchCode))
				AND   (@BranchCode IS NULL OR (c.Customer_region in (select Item from SplitString(@BranchCode,''',''')) OR c.territory_engineer_id in (select Item from SplitString(@BranchCode,''','''))))
				)tmp
GROUP BY tmp.customer_short_name,tmp.customer_number , tmp.Branch, tmp.EngineerName
--)


select  customer_short_name , Branch,EngineerName,
CEILING(NULLIF(Sales_value_year_0,0)) as Sales_value_year_0,
CEILING(NULLIF(budget_value,0)) as budget_value,
CEILING(NULLIF(Sales_ytd_value,0)) Sales_ytd_value ,
CEILING(Sales_ytm_value+((budget_value-Sales_ytm_value)/@MonthsRemaining)) ytd_plan,
CEILING(((Sales_ytd_value)/NULLIF(Sales_ytm_value+((budget_value-Sales_ytm_value)/@MonthsRemaining),0))*100) as achvmnt,
CEILING(((budget_value-Sales_ytd_value)/NULLIF(@MonthsRemaining,0))) as Askrate,
customer_number ,
CEILING(((NULLIF(Sales_ytd_value,0)/NULLIF(Sales_value_year_0,0))-1)*100) growth INTO #TEMPGCHANNEL_Final1
from #TEMPGCHANNEL  
WHERE budget_value IS NOT NULL AND budget_value NOT IN(0)  
order by 
CEILING(NULLIF(budget_value,0)) DESC

SELECT * FROM #TEMPGCHANNEL_Final1 
UNION ALL
select '' customer_short_name ,'','',
SUM(Sales_value_year_0) as Sales_value_year_0,
SUM(budget_value) as budget_value,
SUM(Sales_ytd_value) Sales_ytd_value ,
SUM(Sales_ytm_value+((budget_value-Sales_ytm_value)/@MonthsRemaining)) ytd_plan,
--SUM(((Sales_ytd_value)/NULLIF(Sales_ytm_value+((budget_value-Sales_ytm_value)/@MonthsRemaining),0))*100) as achvmnt,
CEILING((SUM(NULLIF(Sales_ytd_value,0))/(NULLIF(SUM(NULLIF(Sales_ytm_value,0))+((SUM(NULLIF(budget_value,0))-SUM(NULLIF(Sales_ytm_value,0)))/@MonthsRemaining),0)))*100) as achvmnt,
SUM(((budget_value-Sales_ytd_value)/NULLIF(@MonthsRemaining,0))) as Askrate,
--(((SUM(NULLIF(budget_value,0))-SUM(NULLIF(Sales_ytd_value,0)))/NULLIF(@MonthsRemaining,0))) as Askrate,
'Total' customer_number ,
--SUM(((NULLIF(Sales_ytd_value,0)/NULLIF(Sales_value_year_0,0))-1)*100) growth 
(((SUM(NULLIF(Sales_ytd_value,0))/(SUM(NULLIF(Sales_value_year_0,0))))-1)*100)  growth 
from #TEMPGCHANNEL  
WHERE budget_value IS NOT NULL AND budget_value NOT IN(0)  

select  customer_short_name ,Branch,EngineerName,
CEILING(NULLIF(Sales_value_year_0,0)) as Sales_value_year_0,
CEILING(NULLIF(budget_value,0)) as budget_value,
CEILING(NULLIF(Sales_ytd_value,0)) Sales_ytd_value ,
CEILING(Sales_ytm_value+((budget_value-Sales_ytm_value)/@MonthsRemaining)) ytd_plan,
CEILING(((Sales_ytd_value)/NULLIF(Sales_ytm_value+((budget_value-Sales_ytm_value)/@MonthsRemaining),0))*100) as achvmnt,
CEILING(((budget_value-Sales_ytd_value)/NULLIF(@MonthsRemaining,0))) as Askrate,
customer_number ,
ISNULL(CEILING(((NULLIF(Sales_ytd_value,0)/NULLIF(Sales_value_year_0,0))-1)*100),0) growth INTO #TEMPGCHANNEL_Final2
from #TEMPGCHANNEL  
WHERE budget_value IS NOT NULL AND budget_value NOT IN(0)  
order by 
ISNULL(CEILING(((NULLIF(Sales_ytd_value,0)/NULLIF(Sales_value_year_0,0))-1)*100),0) DESC

SELECT * FROM #TEMPGCHANNEL_Final2
UNION ALL
select '' customer_short_name ,'','',
SUM(Sales_value_year_0) as Sales_value_year_0,
SUM(budget_value) as budget_value,
SUM(Sales_ytd_value) Sales_ytd_value ,
SUM(Sales_ytm_value+((budget_value-Sales_ytm_value)/@MonthsRemaining)) ytd_plan,
--SUM(((Sales_ytd_value)/NULLIF(Sales_ytm_value+((budget_value-Sales_ytm_value)/@MonthsRemaining),0))*100) as achvmnt,
CEILING((SUM(NULLIF(Sales_ytd_value,0))/(NULLIF(SUM(NULLIF(Sales_ytm_value,0))+((SUM(NULLIF(budget_value,0))-SUM(NULLIF(Sales_ytm_value,0)))/@MonthsRemaining),0)))*100) as achvmnt,
SUM(((budget_value-Sales_ytd_value)/NULLIF(@MonthsRemaining,0))) as Askrate,
--(((SUM(NULLIF(budget_value,0))-SUM(NULLIF(Sales_ytd_value,0)))/NULLIF(@MonthsRemaining,0))) as Askrate,
'Total' customer_number ,
--SUM(ISNULL(CEILING(((NULLIF(Sales_ytd_value,0)/NULLIF(Sales_value_year_0,0))-1)*100),0)) growth
(((SUM(NULLIF(Sales_ytd_value,0))/(SUM(NULLIF(Sales_value_year_0,0))))-1)*100)  growth 
from #TEMPGCHANNEL  
WHERE budget_value IS NOT NULL AND budget_value NOT IN(0) 


--CASE:SE OF GOLD PRODUCTS



END
END 
END
RETURN






GO
/****** Object:  StoredProcedure [dbo].[getsalesbycustomer_variation]    Script Date: 10/15/2018 8:11:34 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


/***exec getsalesbycustomer_variation null,null,1,null,'C','TTA'***/
--------------------------------

CREATE Procedure [dbo].[getsalesbycustomer_variation]
(
@BranchCode varchar(50)=null,
@salesengineer_id VARCHAR(10)=null,
@negative int=null,
@positive int=null,
@customer_type varchar(10)=null,
@cter varchar(10)=null
)
AS
Declare  @Budget_year int
Declare  @Actual_year int
Declare @Actual_month int
Declare @MonthsRemaining int
BEGIN
set @Budget_year =[dbo].[get_profile_value]('BUDGET_YEAR')
set @Actual_year =[dbo].[get_profile_value]('ACTUAL_YEAR')
set @Actual_month= [dbo].[get_profile_value]('ACTUAL_MONTH')
SET @MonthsRemaining=(12-@Actual_month)+1
BEGIN 
---CASE:NEGATIVE IS NOT NULL AND POSITIVE IS NULL
if(@negative is not null AND @positive is null)
WITH Variation_CTE (customer_short_name,customer_number,customer_type, Branch, EngineerName, Sales_value_year_0, budget_value,Sales_ytm_value, Sales_ytd_value)
            AS(
SELECT tmp.customer_short_name,  
      tmp.customer_number, tmp.customer_type, tmp.Branch, tmp.EngineerName,
   COALESCE(CEILING(sum(Sales_value_year_0)),0)Sales_value_year_0, 
       COALESCE(CEILING(dbo.get_budget_val(@Budget_year,null, tmp.customer_number, @salesengineer_id, @BranchCode,@customer_type,@cter)),0) budget_value, 
       COALESCE(CEILING(SUM(Sales_ytm_value)),0)Sales_ytm_value ,
   COALESCE(CEILING(SUM(Sales_ytd_value)),0)Sales_ytd_value
  
                FROM (SELECT c.customer_short_name,c.customer_number, c.customer_type, tr.region_description Branch, tl.EngineerName,
    Sales_value_year_0 = CASE inv_year WHEN @Actual_year - 1 THEN value END,
Sales_ytm_value = CASE WHEN s.inv_month < @Actual_month and s.inv_year = @Actual_year THEN value END,
    Sales_ytd_value = CASE WHEN s.inv_month <= @Actual_month and s.inv_year = @Actual_year THEN value END
                FROM    tt_customer_master c LEFT OUTER JOIN  tt_salesnumbers s on
            s.customer_number=c.customer_number
JOIN tt_region tr ON c.Customer_region=tr.region_code
JOIN LoginInfo tl ON c.assigned_salesengineer_id=tl.EngineerId
             WHERE
                        -- (c.customer_region = ISNULL(@BranchCode,c.Customer_region))
(@BranchCode IS NULL OR (c.Customer_region=@BranchCode OR c.territory_engineer_id=@BranchCode))
AND  (c.assigned_salesengineer_id =ISNULL(@salesengineer_id,c.assigned_salesengineer_id))
AND  (c.customer_type =ISNULL(@customer_type,c.customer_type))
AND    (@cter IS NULL OR(c.cter=@cter))
)tmp
                        GROUP BY tmp.customer_short_name,tmp.customer_number,tmp.customer_type, tmp.Branch, tmp.EngineerName
    )
Select customer_short_name,customer_number,customer_type, Branch, EngineerName,Sales_value_year_0,budget_value,
CEILING(Sales_ytm_value+((budget_value-Sales_ytm_value)/@MonthsRemaining)) ytd_plan,
Sales_ytd_value,
Sales_ytd_value - (Sales_ytm_value+((budget_value-Sales_ytm_value)/@MonthsRemaining)) as Variance,
CEILING(((Sales_ytd_value-(Sales_ytm_value+((budget_value-Sales_ytm_value)/@MonthsRemaining)))/NULLIF((Sales_ytm_value+((budget_value-Sales_ytm_value)/@MonthsRemaining)), 0))*100) as variance_prcnt from Variation_CTE
where 
CEILING(COALESCE(((Sales_ytd_value-(Sales_ytm_value+((budget_value-Sales_ytm_value)/@MonthsRemaining)))/NULLIF((Sales_ytm_value+((budget_value-Sales_ytm_value)/@MonthsRemaining)), 0))*100,0)) <= @negative
--CEILING(((Sales_ytd_value-(Sales_ytm_value+((budget_value-Sales_ytm_value)/@MonthsRemaining)))/NULLIF((Sales_ytm_value+((budget_value-Sales_ytm_value)/@MonthsRemaining)), 0))*100)  <=@negative
order by customer_short_name asc


---CASE:NEGATIVE IS  NULL AND POSITIVE IS  NOT NULL
   else if(@positive is not null AND @negative is null)
WITH Variation_CTE (customer_short_name,customer_number,customer_type, Branch, EngineerName,Sales_value_year_0, budget_value,Sales_ytm_value, Sales_ytd_value)
            AS(
SELECT tmp.customer_short_name,  
      tmp.customer_number,tmp.customer_type, tmp.Branch, tmp.EngineerName,
   COALESCE(CEILING(sum(Sales_value_year_0)),0)Sales_value_year_0, 
       COALESCE(CEILING(dbo.get_budget_val(@Budget_year,null, tmp.customer_number, @salesengineer_id, @BranchCode,@customer_type,@cter)),0) budget_value, 
       COALESCE(CEILING(SUM(Sales_ytm_value)),0)Sales_ytm_value ,
   COALESCE(CEILING(SUM(Sales_ytd_value)),0)Sales_ytd_value
  
           FROM (SELECT c.customer_short_name,c.customer_number, c.customer_type, tr.region_description Branch, tl.EngineerName,
           
Sales_value_year_0 = CASE inv_year WHEN @Actual_year - 1 THEN value END,
Sales_ytm_value = CASE WHEN s.inv_month < @Actual_month and s.inv_year = @Actual_year THEN value END,
Sales_ytd_value = CASE WHEN s.inv_month <= @Actual_month and s.inv_year = @Actual_year THEN value END
FROM    tt_customer_master c LEFT OUTER JOIN  tt_salesnumbers s on
s.customer_number=c.customer_number
JOIN tt_region tr ON c.Customer_region=tr.region_code
JOIN LoginInfo tl ON c.assigned_salesengineer_id=tl.EngineerId
     WHERE
                --(c.customer_region = ISNULL(@BranchCode,c.Customer_region))
(@BranchCode IS NULL OR (c.Customer_region=@BranchCode OR c.territory_engineer_id=@BranchCode))
AND  (c.assigned_salesengineer_id =ISNULL(@salesengineer_id,c.assigned_salesengineer_id))
AND  (c.customer_type =ISNULL(@customer_type,c.customer_type))
AND    (@cter IS NULL OR(c.cter=@cter))
)tmp
                GROUP BY tmp.customer_short_name,tmp.customer_number,tmp.customer_type, tmp.Branch, tmp.EngineerName
    )
Select customer_short_name,customer_number,customer_type, Branch, EngineerName,Sales_value_year_0,budget_value,
CEILING(Sales_ytm_value+((budget_value-Sales_ytm_value)/@MonthsRemaining)) ytd_plan,
Sales_ytd_value,Sales_ytd_value - (Sales_ytm_value+((budget_value-Sales_ytm_value)/@MonthsRemaining)) as Variance,
CEILING(((Sales_ytd_value-(Sales_ytm_value+((budget_value-Sales_ytm_value)/@MonthsRemaining)))/NULLIF((Sales_ytm_value+((budget_value-Sales_ytm_value)/@MonthsRemaining)), 0))*100) 
as variance_prcnt from Variation_CTE
where 
--CEILING(((Sales_ytd_value-(Sales_ytm_value+((budget_value-Sales_ytm_value)/@MonthsRemaining)))/NULLIF((Sales_ytm_value+((budget_value-Sales_ytm_value)/@MonthsRemaining)), 0))*100)  >=@positive
    CEILING(COALESCE(((Sales_ytd_value-(Sales_ytm_value+((budget_value-Sales_ytm_value)/@MonthsRemaining)))/NULLIF((Sales_ytm_value+((budget_value-Sales_ytm_value)/@MonthsRemaining)), 0))*100,0)) >= @positive
order by customer_short_name asc



---CASE:NEGATIVE IS NOT  NULL AND POSITIVE IS  NOT NULL
  else
WITH Variation_CTE (customer_short_name,customer_number,customer_type, Branch, EngineerName,Sales_value_year_0, budget_value,Sales_ytm_value, Sales_ytd_value)
            AS(
SELECT tmp.customer_short_name,  
      tmp.customer_number,tmp.customer_type, tmp.Branch, tmp.EngineerName,
    COALESCE(CEILING(sum(Sales_value_year_0)),0)Sales_value_year_0, 
        COALESCE(CEILING(dbo.get_budget_val(@Budget_year,null, tmp.customer_number, @salesengineer_id, @BranchCode,@customer_type,@cter)),0) budget_value, 
       COALESCE(CEILING(SUM(Sales_ytm_value)),0)Sales_ytm_value ,
   COALESCE(CEILING(SUM(Sales_ytd_value)),0)Sales_ytd_value
  
           FROM (SELECT c.customer_short_name,c.customer_number, c.customer_type, tr.region_description Branch, tl.EngineerName,
           
Sales_value_year_0 = CASE inv_year WHEN @Actual_year - 1 THEN value END,
Sales_ytm_value = CASE WHEN s.inv_month < @Actual_month and s.inv_year = @Actual_year THEN value END,
Sales_ytd_value = CASE WHEN s.inv_month <= @Actual_month and s.inv_year = @Actual_year THEN value END
        FROM    tt_customer_master c LEFT OUTER JOIN  tt_salesnumbers s on
    s.customer_number=c.customer_number
JOIN tt_region tr ON c.Customer_region=tr.region_code
JOIN LoginInfo tl ON c.assigned_salesengineer_id=tl.EngineerId
            
     WHERE
                --(c.customer_region = ISNULL(@BranchCode,c.Customer_region))
(@BranchCode IS NULL OR (c.Customer_region=@BranchCode OR c.territory_engineer_id=@BranchCode))
AND  (c.assigned_salesengineer_id =ISNULL(@salesengineer_id,c.assigned_salesengineer_id))
AND  (c.customer_type =ISNULL(@customer_type,c.customer_type))
AND    (@cter IS NULL OR(c.cter=@cter))
)tmp
               GROUP BY tmp.customer_short_name,tmp.customer_number ,tmp.customer_type, tmp.Branch, tmp.EngineerName 
   )
   Select customer_short_name,customer_number,customer_type, Branch, EngineerName,Sales_value_year_0,budget_value,
   CEILING(Sales_ytm_value+((budget_value-Sales_ytm_value)/@MonthsRemaining)) ytd_plan,
   Sales_ytd_value,
   Sales_ytd_value - (Sales_ytm_value+((budget_value-Sales_ytm_value)/@MonthsRemaining)) as Variance,
   CEILING(((Sales_ytd_value-(Sales_ytm_value+((budget_value-Sales_ytm_value)/@MonthsRemaining)))/NULLIF((Sales_ytm_value+((budget_value-Sales_ytm_value)/@MonthsRemaining)), 0))*100) as variance_prcnt from Variation_CTE
   where  CEILING( COALESCE(((Sales_ytd_value-(Sales_ytm_value+((budget_value-Sales_ytm_value)/@MonthsRemaining)))/NULLIF((Sales_ytm_value+((budget_value-Sales_ytm_value)/@MonthsRemaining)), 0))*100,0) )<= @negative
   OR CEILING(COALESCE(((Sales_ytd_value-(Sales_ytm_value+((budget_value-Sales_ytm_value)/@MonthsRemaining)))/NULLIF((Sales_ytm_value+((budget_value-Sales_ytm_value)/@MonthsRemaining)), 0))*100,0)) >= @positive

order by customer_short_name asc
END
END
RETURN
GO
/****** Object:  StoredProcedure [dbo].[sp_getTartgets]    Script Date: 10/15/2018 8:11:34 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Procedure [dbo].[sp_getTartgets]
(
@branch varchar(10) = null,
@seid varchar(10) = null,
@ctype varchar(5) = null,
@cter varchar(10) = null,
@yearmonth int,
@valuein int
)
As
SELECT  t.customer_number cust_number, t.customer_name cust_name,t.branch_code , t.salesengineer_name,c.customer_type,t.cred_code cred_code, 
CEILING (t.sales_current_year / @valuein ) sales_current_year, 
CEILING (t.open_order / @valuein ) open_order, 
CEILING (t.can_supply_now / @valuein ) can_supply_now,
CEILING ( t.target_value / @valuein ) targetval, 
CEILING ( t.sales_value / @valuein ) ActualValue,  
    CEILING ((t.sales_value -  t.target_value) / @valuein)  variance , 
IsNull( CEILING ((NULLIF(CONVERT(DECIMAL(22,5),t.sales_value),0) / NULLIF(CONVERT(DECIMAL(22,5),t.target_value),0)) * 100), 0 ) prorata
FROM  tt_targets t, tt_customer_master c
WHERE  t.customer_number = c.customer_number
  AND (@branch is null or t.branch_code = @branch or c.territory_engineer_id = @branch)
  AND (@seid is null or t.salesengineer_id = @seid)
  AND (@cter is null or c.cter = @cter)
  AND (c.customer_type = @ctype) 
  AND  t.target_year_month = @yearmonth  

  ORDER BY t.customer_name
GO
/****** Object:  StoredProcedure [dbo].[sp_getTartgets_total]    Script Date: 10/15/2018 8:11:34 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE Procedure [dbo].[sp_getTartgets_total]
(
@branch varchar(10) = null,
@seid varchar(10) = null,
@ctype varchar(5) = null,
@cter varchar(10) = null,
@yearmonth int,
@valuein int
)
As
if @ctype = 'C' 
Begin
SELECT  ''cust_number,  'CUSTOMER TOTAL'  cust_name, ''salesengineer_name,''customer_type ,'' cred_code, 
CEILING (SUM(CEILING(t.sales_current_year / @valuein ))) sales_current_year ,
CEILING (SUM(CEILING(t.open_order / @valuein ))) open_order,
CEILING (SUM(CEILING(t.can_supply_now / @valuein ))) can_supply_now,
CEILING (SUM(CEILING( t.target_value / @valuein ))) targetval,
CEILING (SUM(CEILING(t.sales_value / @valuein)) ) ActualValue,
SUM(CEILING(t.sales_value/@valuein)) -  SUM(CEILING(t.target_value/@valuein))  variance ,
IsNull ( CEILING ((NULLIF(CONVERT(DECIMAL(22,5),sum(t.sales_value)),0) / NULLIF(CONVERT(DECIMAL(22,5),sum(t.target_value)),0)) * 100), 0) prorata
FROM  tt_targets t, tt_customer_master c
WHERE  t.customer_number = c.customer_number
  AND (@branch is null or t.branch_code = @branch or c.territory_engineer_id = @branch)
  AND (@seid is null or t.salesengineer_id = @seid)
  AND (@cter is null or c.cter = @cter)
  AND (c.customer_type = @ctype) 
  AND  t.target_year_month = @yearmonth  
 END
else if @ctype = 'D' 
Begin
SELECT  ''cust_number,'CHANNEL PARTNER TOTAL' cust_name, ''salesengineer_name,''customer_type ,'' cred_code, 
CEILING (SUM(CEILING(t.sales_current_year / @valuein ))) sales_current_year ,
CEILING (SUM(CEILING(t.open_order / @valuein ))) open_order,
CEILING (SUM(CEILING(t.can_supply_now / @valuein ))) can_supply_now,
CEILING (SUM(CEILING( t.target_value / @valuein ))) targetval,
CEILING (SUM(CEILING(t.sales_value / @valuein)) ) ActualValue,
SUM(CEILING(t.sales_value/@valuein)) -  SUM(CEILING(t.target_value/@valuein))  variance ,
IsNull ( CEILING ((NULLIF(CONVERT(DECIMAL(22,5),sum(t.sales_value)),0) / NULLIF(CONVERT(DECIMAL(22,5),sum(t.target_value)),0)) * 100), 0) prorata
FROM  tt_targets t, tt_customer_master c
WHERE  t.customer_number = c.customer_number
  AND (@branch is null or t.branch_code = @branch or c.territory_engineer_id = @branch)
  AND (@seid is null or t.salesengineer_id = @seid)
  AND (@cter is null or c.cter = @cter)
  AND (c.customer_type = @ctype) 
  AND  t.target_year_month = @yearmonth  
 END
else
Begin
SELECT  ''cust_number, 'GRAND TOTAL'  cust_name, '' salesengineer_name,''customer_type ,'' cred_code,
CEILING (SUM(CEILING(t.sales_current_year / @valuein ))) sales_current_year ,
CEILING (SUM(CEILING(t.open_order / @valuein ))) open_order,
CEILING (SUM(CEILING(t.can_supply_now / @valuein ))) can_supply_now,
CEILING (SUM(CEILING( t.target_value / @valuein ))) targetval,
CEILING (SUM(CEILING(t.sales_value / @valuein)) ) ActualValue,
SUM(CEILING(t.sales_value/@valuein)) -  SUM(CEILING(t.target_value/@valuein))  variance ,
IsNull ( CEILING ((NULLIF(CONVERT(DECIMAL(22,5),sum(t.sales_value)),0) / NULLIF(CONVERT(DECIMAL(22,5),sum(t.target_value)),0)) * 100), 0) prorata
FROM  tt_targets t, tt_customer_master c
WHERE  t.customer_number = c.customer_number
  AND (@branch is null or t.branch_code = @branch or c.territory_engineer_id = @branch)
  AND (@seid is null or t.salesengineer_id = @seid)
  AND (@cter is null or c.cter = @cter)
--  AND (c.customer_type = @ctype) 
  AND  t.target_year_month = @yearmonth  
 END


GO
/****** Object:  UserDefinedFunction [dbo].[get_divide_by]    Script Date: 10/15/2018 8:11:34 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE  FUNCTION [dbo].[get_divide_by] 
			(@param1	int,
			@param2	int
			)
RETURNS numeric(22,3) AS
BEGIN
DECLARE @ret  numeric(22,3)
if(@param2=0 OR @param2=NULL)
 set @ret= 0
 ELSE
	SET @ret= ISNULL((CONVERT(FLOAT,ISNULL(@param1,0))/CONVERT(FLOAT,@param2)),0)
	
RETURN @ret
END

GO
