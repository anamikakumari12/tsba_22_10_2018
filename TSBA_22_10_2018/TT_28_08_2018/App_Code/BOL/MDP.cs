﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Text;
using System.Web.UI;
using System.IO;

namespace TaegutecSalesBudget
{
    public class MDP
    {
        CommonFunctions objFunc = new CommonFunctions();
        public string customer_num, Industry, Existing_Product, Component, Competition, IsSubmitted, Goal, Remarks, Sales_Eng_Id, Reviewer, rpt_cust_num, rpt_project_number, EscalatedTo, Owner, BranchCode, rpt_remark, comments, project_number, project_type, project_title, project_status = "", new_project_number;

        public int Stage_Number, NoOfStages, Remark_Number;
        public string territory_engineer_id, Date_Created;

        public string Target_Date, Stage_Target_Date, Completion_Date, IsFrozen, distributor, subindustry_id;
        public string IsApproved, IsRejected, IsSentForApproval, Approval_Remarks;
        public decimal OverAll_Potential, Potential_Lakhs, Business_Expected, sale_ytd, monthly_expected;

        public string file_customer_num, file_project_num, file_name, file_extension, file_distributor, file_Stage_Number;

        public string customer_name, customer_class;


        public string admin_cust_num, admin_proj_num, admin_owner, admin_reviewer, admin_escalate;
        /// <summary>
        /// To load the branch manager when branch code is known
        /// </summary>
        /// <param name="BranchCode"></param>
        /// <returns></returns>
        public string LoadBranchManagerDetails(string BranchCode)
        {
            DataTable dtBranchManagerDetails = new DataTable();
            string BranchManager = null;
            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
            try
            {
                connection.Open();

                SqlCommand command = new SqlCommand("getBranchManagerbyBranchCode", connection);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@branchCode", SqlDbType.VarChar, 10).Value = BranchCode;
                command.Parameters.Add("@RoleId", SqlDbType.VarChar, 10).Value = "BM";

                SqlDataAdapter sqlDa = new SqlDataAdapter(command);
                sqlDa.Fill(dtBranchManagerDetails);
                for (int i = 0; i < dtBranchManagerDetails.Rows.Count; i++)
                {
                    BranchManager = dtBranchManagerDetails.Rows[i].ItemArray[0].ToString();
                }
            }
            catch (Exception e)
            {
                objFunc.LogError(e);
            }
            finally
            {
                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }
            }
            return BranchManager;
        }

        //public string getLatest_newcustomer()
        //{
        //    DataTable dtNewCustomer = new DataTable();
        //    string customer_number = null;
        //    SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
        //    try
        //    {
        //        connection.Open();

        //        SqlCommand command = new SqlCommand("getLatest_newcustomer", connection);
        //        command.CommandType = CommandType.StoredProcedure;


        //        SqlDataAdapter sqlDa = new SqlDataAdapter(command);
        //        sqlDa.Fill(dtNewCustomer);
        //        for (int i = 0; i < dtNewCustomer.Rows.Count; i++)
        //        {
        //            customer_number = dtNewCustomer.Rows[i].ItemArray[0].ToString();
        //        }
        //    }
        //    catch (Exception e)
        //    {

        //    }
        //    finally
        //    {
        //        if (connection.State == ConnectionState.Open)
        //        {
        //            connection.Close();
        //        }
        //    }
        //    return customer_number;
        //}
        /// <summary>
        /// 
        /// </summary>
        /// <param name="customer_number"></param>
        /// <returns></returns>
        public string getLatest_project(string customer_number)
        {
            DataTable dtNewProject = new DataTable();
            string project_number = "";
            SqlConnection connection = new SqlConnection();
            try
            {
                connection = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
                connection.Open();

                SqlCommand command = new SqlCommand("getLatest_project", connection);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@customer_number", SqlDbType.VarChar, 10).Value = customer_number;

                SqlDataAdapter sqlDa = new SqlDataAdapter(command);
                sqlDa.Fill(dtNewProject);
                for (int i = 0; i < dtNewProject.Rows.Count; i++)
                {
                    project_number = dtNewProject.Rows[i].ItemArray[0].ToString();
                }
            }


            catch (Exception e)
            {
                objFunc.LogError(e);
            }
            finally
            {
                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }
            }
            return project_number;
        }


        /// <summary>
        ///  get customername based on customer number    ---Anantha
        /// </summary>
        /// <param name="customer_number"></param>
        /// <returns></returns>
        public string getcustomer_name(string customer_number)
        {
            DataTable dtcustomername = new DataTable();
            string customer_name = "";
            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
            try
            {
                connection.Open();

                SqlCommand command = new SqlCommand("getcustomer_name", connection);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@customernumber", SqlDbType.VarChar, 10).Value = customer_number;

                SqlDataAdapter sqlDa = new SqlDataAdapter(command);
                sqlDa.Fill(dtcustomername);
                for (int i = 0; i < dtcustomername.Rows.Count; i++)
                {
                    customer_name = dtcustomername.Rows[i].ItemArray[0].ToString();
                }
            }


            catch (Exception ex)
            {
                objFunc.LogError(ex);
            }
            finally
            {
                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }
            }
            return customer_name;
        }

        public DataTable LoadProjectTitles(string customer_number)
        {
            DataTable dtProjectTitles = new DataTable();

            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
            try
            {
                connection.Open();

                SqlCommand command = new SqlCommand("getProject_Titles", connection);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@customer_number", SqlDbType.VarChar, 10).Value = customer_number;
                SqlDataAdapter sqlDa = new SqlDataAdapter(command);
                sqlDa.Fill(dtProjectTitles);

            }
            catch (Exception e)
            {
                objFunc.LogError(e);
            }
            finally
            {
                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }
            }
            return dtProjectTitles;
        }


        public string getCustomer_Industry(string customer_number)
        {
            DataTable dtIndustryDetails = new DataTable();
            string industry_code = null;
            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
            try
            {
                connection.Open();

                SqlCommand command = new SqlCommand("getIndustry", connection);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@customer_number", SqlDbType.VarChar, 10).Value = customer_number;

                SqlDataAdapter sqlDa = new SqlDataAdapter(command);
                sqlDa.Fill(dtIndustryDetails);
                for (int i = 0; i < dtIndustryDetails.Rows.Count; i++)
                {
                    industry_code = dtIndustryDetails.Rows[i].ItemArray[0].ToString();
                }
            }
            catch (Exception e)
            {
                objFunc.LogError(e);
            }
            finally
            {
                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }
            }
            return industry_code;
        }

        /// <summary>
        /// To get all the HOs
        /// </summary>
        /// <returns></returns>
        public DataTable GetAllHO()
        {
            DataTable dtAllHOs = new DataTable();
            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
            try
            {
                connection.Open();

                SqlCommand command = new SqlCommand("getAllHO", connection);
                command.CommandType = CommandType.StoredProcedure;
                // command.Parameters.Add("@RoleId", SqlDbType.VarChar, 10).Value = "HO";
                SqlDataAdapter sqlDa = new SqlDataAdapter(command);
                sqlDa.Fill(dtAllHOs);

            }
            catch (Exception e)
            {
                objFunc.LogError(e);
            }
            finally
            {
                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }
            }
            return dtAllHOs;

        }

        public DataTable GetAllReviewer()
        {
            DataTable dtAllHOs = new DataTable();
            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
            try
            {
                connection.Open();

                SqlCommand command = new SqlCommand("getAllReviewer", connection);
                command.CommandType = CommandType.StoredProcedure;
                // command.Parameters.Add("@RoleId", SqlDbType.VarChar, 10).Value = "HO";
                SqlDataAdapter sqlDa = new SqlDataAdapter(command);
                sqlDa.Fill(dtAllHOs);

            }
            catch (Exception e)
            {
                objFunc.LogError(e);
            }
            finally
            {
                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }
            }
            return dtAllHOs;

        }

        public DataTable GetAllIndustriesEntry()
        {
            DataTable dtAllIndustries = new DataTable();
            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
            try
            {
                connection.Open();

                SqlCommand command = new SqlCommand("getAllIndustries", connection);
                command.CommandType = CommandType.StoredProcedure;

                SqlDataAdapter sqlDa = new SqlDataAdapter(command);
                sqlDa.Fill(dtAllIndustries);

            }
            catch (Exception e)
            {
                objFunc.LogError(e);
            }
            finally
            {
                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }
            }
            return dtAllIndustries;

        }

        public DataTable GetAllIndustriesReports()
        {
            DataTable dtAllIndustries = new DataTable();
            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
            try
            {
                connection.Open();

                SqlCommand command = new SqlCommand("getAllIndustriesReports", connection);
                command.CommandType = CommandType.StoredProcedure;

                SqlDataAdapter sqlDa = new SqlDataAdapter(command);
                sqlDa.Fill(dtAllIndustries);

            }
            catch (Exception e)
            {
                objFunc.LogError(e);
            }
            finally
            {
                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }
            }
            return dtAllIndustries;

        }

        public DataTable GetAllSubIndustriesReports()
        {
            DataTable dtAllIndustries = new DataTable();
            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
            try
            {
                connection.Open();

                SqlCommand command = new SqlCommand("getAllSubIndustriesReports", connection);
                command.CommandType = CommandType.StoredProcedure;

                SqlDataAdapter sqlDa = new SqlDataAdapter(command);
                sqlDa.Fill(dtAllIndustries);

            }
            catch (Exception e)
            {
                objFunc.LogError(e);
            }
            finally
            {
                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }
            }
            return dtAllIndustries;

        }

        /// <summary>
        /// To get all the sales engineers
        /// </summary>
        /// <returns></returns>
        public DataTable GetAllSE()
        {
            DataTable dtAllSEs = new DataTable();
            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
            try
            {
                connection.Open();

                SqlCommand command = new SqlCommand("getAllEngineer_HO", connection);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@RoleId", SqlDbType.VarChar, 10).Value = "SE";
                SqlDataAdapter sqlDa = new SqlDataAdapter(command);
                sqlDa.Fill(dtAllSEs);

            }
            catch (Exception e)
            {
                objFunc.LogError(e);
            }
            finally
            {
                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }
            }
            return dtAllSEs;

        }



        public DataTable LoadOwnerbyCustomer(string CustomerNumber)
        {
            DataTable dtAllSEs = new DataTable();
            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
            try
            {
                connection.Open();

                SqlCommand command = new SqlCommand("LoadOwnerbyCustomer", connection);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@customer_number", SqlDbType.VarChar, 10).Value = CustomerNumber;
                SqlDataAdapter sqlDa = new SqlDataAdapter(command);
                sqlDa.Fill(dtAllSEs);

            }
            catch (Exception e)
            {
                objFunc.LogError(e);
            }
            finally
            {
                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }
            }
            return dtAllSEs;

        }

        public DataTable LoadReviewerbyCustomer(string CustomerNumber)
        {
            DataTable dtAllSEs = new DataTable();
            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
            try
            {
                connection.Open();

                SqlCommand command = new SqlCommand("LoadReviewerbyCustomer", connection);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@customer_number", SqlDbType.VarChar, 10).Value = CustomerNumber;
                SqlDataAdapter sqlDa = new SqlDataAdapter(command);
                sqlDa.Fill(dtAllSEs);

            }
            catch (Exception e)
            {
                objFunc.LogError(e);
            }
            finally
            {
                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }
            }
            return dtAllSEs;

        }

        public DataTable LoadReviewerbyCustomer_SE(string CustomerNumber, string project)
        {
            DataTable dtAllSEs = new DataTable();
            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
            try
            {
                connection.Open();

                SqlCommand command = new SqlCommand("LoadReviewerbyCustomer_SE", connection);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@customer_number", SqlDbType.VarChar, 10).Value = CustomerNumber;
                command.Parameters.Add("@project_number", SqlDbType.VarChar, 10).Value = project;
                SqlDataAdapter sqlDa = new SqlDataAdapter(command);
                sqlDa.Fill(dtAllSEs);

            }
            catch (Exception e)
            {
                objFunc.LogError(e);
            }
            finally
            {
                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }
            }
            return dtAllSEs;

        }

        public DataTable LoadEscalatorbyCustomer(string CustomerNumber)
        {
            DataTable dtAllSEs = new DataTable();
            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
            try
            {
                connection.Open();

                SqlCommand command = new SqlCommand("LoadEscalatorbyCustomer", connection);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@customer_number", SqlDbType.VarChar, 10).Value = CustomerNumber;
                SqlDataAdapter sqlDa = new SqlDataAdapter(command);
                sqlDa.Fill(dtAllSEs);

            }
            catch (Exception e)
            {
                objFunc.LogError(e);
            }
            finally
            {
                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }
            }
            return dtAllSEs;

        }

        public DataTable LoadEscalatorbyCustomer_SE(string CustomerNumber, string project)
        {
            DataTable dtAllSEs = new DataTable();
            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
            try
            {
                connection.Open();

                SqlCommand command = new SqlCommand("LoadEscalatorbyCustomer_SE", connection);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@customer_number", SqlDbType.VarChar, 10).Value = CustomerNumber;
                command.Parameters.Add("@project_number", SqlDbType.VarChar, 10).Value = project;
                SqlDataAdapter sqlDa = new SqlDataAdapter(command);
                sqlDa.Fill(dtAllSEs);

            }
            catch (Exception e)
            {
                objFunc.LogError(e);
            }
            finally
            {
                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }
            }
            return dtAllSEs;

        }


        public string GetCustomerSalesEngineer(string CustomerNumber)
        {
            DataTable dtAllSEs = new DataTable();
            string se_id = "";
            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
            try
            {
                connection.Open();

                SqlCommand command = new SqlCommand("GetCustomerSalesEngineer", connection);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@customer_number", SqlDbType.VarChar, 10).Value = CustomerNumber;
                SqlDataAdapter sqlDa = new SqlDataAdapter(command);
                sqlDa.Fill(dtAllSEs);
                for (int i = 0; i < dtAllSEs.Rows.Count; i++)
                {
                    se_id = dtAllSEs.Rows[i].ItemArray[0].ToString();
                }
            }
            catch (Exception e)
            {
                objFunc.LogError(e);
            }
            finally
            {
                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }
            }
            return se_id;
        }

        public string LoadBranchCodeby_Owner(string Sales_Eng_Id)
        {
            DataTable dtBranchCode = new DataTable();
            string branchcode = "";
            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
            try
            {
                connection.Open();

                SqlCommand command = new SqlCommand("getbranchcode_byowner", connection);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@Enginner_id", SqlDbType.VarChar, 10).Value = Sales_Eng_Id;
                SqlDataAdapter sqlDa = new SqlDataAdapter(command);
                sqlDa.Fill(dtBranchCode);
                for (int i = 0; i < dtBranchCode.Rows.Count; i++)
                {
                    branchcode = dtBranchCode.Rows[i].ItemArray[0].ToString();
                }
            }
            catch (Exception e)
            {
                objFunc.LogError(e);
            }
            finally
            {
                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }
            }
            return branchcode;
        }

        public string GetEmail_Id(string engineer_Id)
        {
            DataTable dtEnggInfo = new DataTable();
            string email = "";
            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
            try
            {
                connection.Open();

                SqlCommand command = new SqlCommand("GetEmail_Id", connection);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@Engineer_Id", SqlDbType.VarChar, 10).Value = engineer_Id;
                SqlDataAdapter sqlDa = new SqlDataAdapter(command);
                sqlDa.Fill(dtEnggInfo);
                for (int i = 0; i < dtEnggInfo.Rows.Count; i++)
                {
                    email = dtEnggInfo.Rows[i].ItemArray[0].ToString();
                }
            }
            catch (Exception e)
            {
                objFunc.LogError(e);
            }
            finally
            {
                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }
            }
            return email;


        }


        //public string LoadTerritoryByRegion(string region_code)
        //{
        //    DataTable dtterritory_engineer = new DataTable();
        //    string territory_engineer = "";
        //    SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
        //    try
        //    {
        //        connection.Open();

        //        SqlCommand command = new SqlCommand("get_territory_engineer_by_region", connection);
        //        command.CommandType = CommandType.StoredProcedure;
        //        command.Parameters.Add("@region_code", SqlDbType.VarChar, 10).Value = region_code;
        //        SqlDataAdapter sqlDa = new SqlDataAdapter(command);
        //        sqlDa.Fill(dtterritory_engineer);
        //        for (int i = 0; i < dtterritory_engineer.Rows.Count; i++)
        //        {
        //            territory_engineer = dtterritory_engineer.Rows[i].ItemArray[0].ToString();
        //        }
        //    }
        //    catch (Exception e)
        //    {

        //    }
        //    finally
        //    {
        //        if (connection.State == ConnectionState.Open)
        //        {
        //            connection.Close();
        //        }
        //    }
        //    return territory_engineer;
        //}

        public DataTable GetAllSE_BM(string branchcode)
        {
            DataTable dtAllSEs = new DataTable();
            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
            try
            {
                connection.Open();

                SqlCommand command = new SqlCommand("getAllEngineer_BM", connection);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@BranchCode", SqlDbType.VarChar, 10).Value = branchcode;
                SqlDataAdapter sqlDa = new SqlDataAdapter(command);
                sqlDa.Fill(dtAllSEs);

            }
            catch (Exception e)
            {
                objFunc.LogError(e);
            }
            finally
            {
                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }
            }
            return dtAllSEs;

        }


        public DataTable GetAllSE_TM(string strUserId)
        {
            DataTable dtAllSEs = new DataTable();
            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
            try
            {
                connection.Open();

                SqlCommand command = new SqlCommand("getAllEngineer_TM", connection);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@territory_engineer_Id", SqlDbType.VarChar, 10).Value = strUserId;
                SqlDataAdapter sqlDa = new SqlDataAdapter(command);
                sqlDa.Fill(dtAllSEs);

            }
            catch (Exception e)
            {
                objFunc.LogError(e);
            }
            finally
            {
                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }
            }
            return dtAllSEs;

        }

        /// <summary>
        /// To get all the reviewers
        /// </summary>
        /// <returns></returns>
        public DataTable GetAllReviewer_BM(string branch_manager_Id)
        {
            DataTable dtAllRE = new DataTable();
            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
            try
            {
                connection.Open();

                SqlCommand command = new SqlCommand("get_Reviewer_BM", connection);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@branch_manager_Id", SqlDbType.VarChar, 10).Value = branch_manager_Id;
                SqlDataAdapter sqlDa = new SqlDataAdapter(command);
                sqlDa.Fill(dtAllRE);

            }
            catch (Exception e)
            {
                objFunc.LogError(e);
            }
            finally
            {
                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }
            }
            return dtAllRE;
        }

        public DataTable GetAllReviewer_TM(string territory_eng_Id)
        {
            DataTable dtAllRE = new DataTable();
            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
            try
            {
                connection.Open();

                SqlCommand command = new SqlCommand("get_Reviewer_TM", connection);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@territory_eng_Id", SqlDbType.VarChar, 10).Value = territory_eng_Id;
                SqlDataAdapter sqlDa = new SqlDataAdapter(command);
                sqlDa.Fill(dtAllRE);

            }
            catch (Exception e)
            {
                objFunc.LogError(e);
            }
            finally
            {
                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }
            }
            return dtAllRE;
        }

        public DataTable GetAllReviewer_SE(string sales_eng_id)
        {
            DataTable dtAllRE = new DataTable();
            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
            try
            {
                connection.Open();

                SqlCommand command = new SqlCommand("get_Reviewer_SE", connection);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@sales_eng_id", SqlDbType.VarChar, 10).Value = sales_eng_id;
                SqlDataAdapter sqlDa = new SqlDataAdapter(command);
                sqlDa.Fill(dtAllRE);

            }
            catch (Exception e)
            {
                objFunc.LogError(e);
            }
            finally
            {
                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }
            }
            return dtAllRE;
        }



        public DataTable GetAllEscalator_BM(string branch_manager_Id)
        {
            DataTable dtAllRE = new DataTable();
            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
            try
            {
                connection.Open();

                SqlCommand command = new SqlCommand("get_Escalator_BM", connection);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@branch_manager_Id", SqlDbType.VarChar, 10).Value = branch_manager_Id;
                SqlDataAdapter sqlDa = new SqlDataAdapter(command);
                sqlDa.Fill(dtAllRE);
            }

            catch (Exception e)
            {
                objFunc.LogError(e);
            }

            finally
            {
                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }
            }
            return dtAllRE;
        }

        public DataTable GetAllEscalator_TM(string territory_engineer_Id)
        {
            DataTable dtAllRE = new DataTable();
            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
            try
            {
                connection.Open();

                SqlCommand command = new SqlCommand("get_Escalator_TM", connection);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@territory_engineer_Id", SqlDbType.VarChar, 10).Value = territory_engineer_Id;
                SqlDataAdapter sqlDa = new SqlDataAdapter(command);
                sqlDa.Fill(dtAllRE);

            }
            catch (Exception e)
            {
                objFunc.LogError(e);
            }
            finally
            {
                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }
            }
            return dtAllRE;
        }



        /// <summary>
        /// To insert project details in corresponding tables
        /// </summary>
        /// <param name="objMDP"></param>
        /// <returns></returns>
        public string insert_mdpinfo(MDP objMDP)
        {
            string strResult = string.Empty;
            try
            {
                string connstring = ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString();
                SqlConnection connection = new SqlConnection(connstring);
                connection.Open();
                SqlCommand command = new SqlCommand("insert_mdpInfo", connection);
                command.CommandType = CommandType.StoredProcedure;

                SqlParameter[] parameters ={
                                        new SqlParameter    ("@Customer_Num" ,SqlDbType.VarChar,50 ),//0
                                        new SqlParameter    ("@distributor_num" ,SqlDbType.VarChar,50 ),//1
	                                    new SqlParameter    ("@industry_Id", SqlDbType.VarChar,255 ),//2	
                                        new SqlParameter    ("@project_number" ,SqlDbType.VarChar,50 ),//3
	                                    new SqlParameter    ("@project_title", SqlDbType.VarChar,255 ),//4	
	                                    new SqlParameter    ("@Existing_Product" , SqlDbType.VarChar,255 ) ,//5
                                        new SqlParameter    ("@Component" , SqlDbType.VarChar,255 ) ,//6
                                        new SqlParameter    ("@Competition" , SqlDbType.VarChar,50 ) ,//7
                                        new SqlParameter    ("@NoOfStages" ,SqlDbType.Int ),//8
                                        new SqlParameter    ("@Target_Date" ,SqlDbType.VarChar,50),//9
                                        new SqlParameter    ("@Sales_Eng_Id" ,SqlDbType.VarChar,50 ),//10
                                        new SqlParameter    ("@OverAll_Potential" , SqlDbType.Decimal ),//11
                                        new SqlParameter    ("@Potential_Lakhs" , SqlDbType.Decimal ),//12
                                        new SqlParameter    ("@Business_Expected" , SqlDbType.Decimal ),//13
                                        new SqlParameter    ("@IsSubmitted" , SqlDbType.VarChar,30 ),//14
                                        new SqlParameter    ("@Stage_Number" ,SqlDbType.Int ),//15
                                        new SqlParameter    ("@Goal" , SqlDbType.VarChar,255 ),//16
                                        new SqlParameter    ("@Stage_Target_Date" ,SqlDbType.VarChar,50),//17
                                        new SqlParameter    ("@Completion_Date" ,SqlDbType.VarChar,50),//18
                                        new SqlParameter    ("@Reviewer" ,SqlDbType.VarChar,50 ),//19
                                        new SqlParameter    ("@Escalated_To" ,SqlDbType.VarChar,50 ),	//120
                                        new SqlParameter    ("@Remarks" , SqlDbType.VarChar,255 ),//21
                                        new SqlParameter    ("@Remark_Number",SqlDbType.Int),//22
                                        new SqlParameter    ("@BranchCode",SqlDbType.VarChar,30),//23
                                        new SqlParameter    ("@Monthly_Expected",SqlDbType.Decimal),//24
                                        new SqlParameter    ("@Project_Status",SqlDbType.VarChar,30),//25                                     
                                        new SqlParameter    ("@Comments",SqlDbType.VarChar,255),//26
                                        new SqlParameter    ("@IsSentForApproval",SqlDbType.VarChar,255),//27
                                        new SqlParameter    ("@IsApproved",SqlDbType.VarChar,255),//28
                                        new SqlParameter    ("@IsRejected",SqlDbType.VarChar,255),//29
                                        new SqlParameter    ("@Date_Created",SqlDbType.VarChar,255),//30
                                        //new SqlParameter    ("@IsFrozen",SqlDbType.VarChar,255),//30
                                        new SqlParameter    ("@Error", SqlDbType.VarChar,100),//31
                                         new SqlParameter    ("@customer_name" ,SqlDbType.VarChar,255 ),//32
                                         new SqlParameter    ("@customer_class" ,SqlDbType.VarChar,255 ),//33
                                         new SqlParameter    ("project_type",SqlDbType.VarChar,255)
                                       };

                parameters[0].Value = objMDP.customer_num;
                parameters[1].Value = objMDP.distributor;
                parameters[2].Value = objMDP.Industry;
                parameters[3].Value = objMDP.project_number;
                parameters[4].Value = objMDP.project_title;
                parameters[5].Value = objMDP.Existing_Product;
                parameters[6].Value = objMDP.Component;
                parameters[7].Value = objMDP.Competition;
                parameters[8].Value = objMDP.NoOfStages;
                parameters[9].Value = objMDP.Target_Date;
                parameters[10].Value = objMDP.Sales_Eng_Id;
                parameters[11].Value = objMDP.OverAll_Potential;
                parameters[12].Value = objMDP.Potential_Lakhs;
                parameters[13].Value = objMDP.Business_Expected;
                parameters[14].Value = objMDP.IsSubmitted;
                parameters[15].Value = objMDP.Stage_Number;
                parameters[16].Value = objMDP.Goal;
                parameters[17].Value = objMDP.Stage_Target_Date;
                parameters[18].Value = objMDP.Completion_Date;
                parameters[19].Value = objMDP.Reviewer;
                parameters[20].Value = objMDP.EscalatedTo;
                parameters[21].Value = objMDP.Remarks;
                parameters[22].Value = objMDP.Remark_Number;
                parameters[23].Value = objMDP.BranchCode;

                parameters[24].Value = objMDP.monthly_expected;
                parameters[25].Value = objMDP.project_status;
                parameters[26].Value = objMDP.comments;

                parameters[27].Value = objMDP.IsSentForApproval;
                parameters[28].Value = objMDP.IsApproved;
                parameters[29].Value = objMDP.IsRejected;
                parameters[30].Value = objMDP.Date_Created;
                //parameters[30].Value = objMDP.IsFrozen;
                parameters[32].Value = objMDP.customer_name;
                parameters[33].Value = objMDP.customer_class;
                parameters[34].Value = objMDP.project_type;
                parameters[31].Value = ParameterDirection.Output;

                Int32 j;
                for (j = 0; j <= parameters.Length - 1; j++)
                {
                    command.Parameters.Add(parameters[j]);
                }

                try
                {
                    command.ExecuteNonQuery();
                    strResult = command.Parameters["@Error"].Value.ToString();

                    return strResult;
                }

                catch (Exception ex)
                {
                    strResult = ex.Message;
                    return strResult;
                }

                finally
                {
                    if (connection.State == ConnectionState.Open)
                    {
                        connection.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                objFunc.LogError(ex);
            }
            return strResult;
        }








        /// <summary>
        /// Method to load MDP details by customer number
        /// </summary>
        /// <param name="customer_number"></param>
        /// <returns></returns>
        public DataTable LoadMDP_Details_byCustomer_Number(string customer_number, string project_number)
        {
            DataTable dtMDPDetails = new DataTable();
            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
            try
            {
                connection.Open();

                SqlCommand command = new SqlCommand("getMDPInfo_byCustomer_number", connection);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@customer_number", SqlDbType.VarChar, 30).Value = customer_number;
                command.Parameters.Add("@project_number", SqlDbType.VarChar, 30).Value = project_number;
                SqlDataAdapter sqlDa = new SqlDataAdapter(command);
                sqlDa.Fill(dtMDPDetails);
                //return dtCustomerDetails;
            }

            catch (Exception ex)
            {
                objFunc.LogError(ex);
            }

            finally
            {
                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }
            }

            // getCustomerDetails
            return dtMDPDetails;

        }

        /// <summary>
        /// To load All the stage details by customer number
        /// </summary>
        /// <param name="customer_number"></param>
        /// <param name="Stage_number"></param>
        /// <returns></returns>
        public DataTable LoadMDP_Stage_Details_byCustomer_Number(string customer_number, string Stage_number, string project_number)
        {
            DataTable dtMDP_StageDetails = new DataTable();
            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
            try
            {
                connection.Open();

                SqlCommand command = new SqlCommand("getMDPStagedetails_byCustomer_Number", connection);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@customer_number", SqlDbType.VarChar, 30).Value = customer_number;
                command.Parameters.Add("@Stage_Number", SqlDbType.VarChar, 30).Value = Stage_number;
                command.Parameters.Add("@Project_Number", SqlDbType.VarChar, 30).Value = project_number;

                SqlDataAdapter sqlDa = new SqlDataAdapter(command);
                sqlDa.Fill(dtMDP_StageDetails);
                //return dtCustomerDetails;
            }

            catch (Exception e)
            {
                objFunc.LogError(e);
            }

            finally
            {
                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }
            }

            // getCustomerDetails
            return dtMDP_StageDetails;

        }

        /// <summary>
        /// To load remarks for all the stages
        /// </summary>
        /// <param name="customer_number"></param>
        /// <param name="Stage_number"></param>
        /// <returns></returns>
        public DataTable LoadMDP_Remarks(string customer_number, string Stage_number, string Project_Number)
        {
            DataTable dtMDP_Remarks = new DataTable();
            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
            try
            {
                connection.Open();

                SqlCommand command = new SqlCommand("getMDP_Remarks", connection);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@customer_number", SqlDbType.VarChar, 30).Value = customer_number;
                command.Parameters.Add("@Stage_Number", SqlDbType.VarChar, 30).Value = Stage_number;
                command.Parameters.Add("@Project_Number", SqlDbType.VarChar, 30).Value = Project_Number;

                SqlDataAdapter sqlDa = new SqlDataAdapter(command);
                sqlDa.Fill(dtMDP_Remarks);
                //return dtCustomerDetails;
            }

            catch (Exception ex)
            {
                objFunc.LogError(ex);
            }

            finally
            {
                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }
            }

            // getCustomerDetails
            return dtMDP_Remarks;

        }



        public DataTable LoadCustomerDetails_SE(string sales_eng)
        {
            DataTable dtCustomerDetails = new DataTable();
            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
            try
            {
                connection.Open();

                SqlCommand command = new SqlCommand("getCustomerDetails_SE", connection);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@assigned_salesengineer_id", SqlDbType.VarChar, 30).Value = sales_eng;
                SqlDataAdapter sqlDa = new SqlDataAdapter(command);
                sqlDa.Fill(dtCustomerDetails);
                //return dtCustomerDetails;
            }

            catch (Exception ex)
            {
                objFunc.LogError(ex);
            }

            finally
            {
                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }
            }

            // getCustomerDetails
            return dtCustomerDetails;

        }

        public DataTable LoadCustomerDetails_TM(string territory_engineer_id)
        {
            DataTable dtCustomerDetails = new DataTable();
            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
            try
            {
                connection.Open();

                SqlCommand command = new SqlCommand("getCustomerDetails_TM", connection);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@territory_engineer_id", SqlDbType.VarChar, 30).Value = territory_engineer_id;
                SqlDataAdapter sqlDa = new SqlDataAdapter(command);
                sqlDa.Fill(dtCustomerDetails);
                //return dtCustomerDetails;
            }

            catch (Exception ex)
            {
                objFunc.LogError(ex);
            }

            finally
            {
                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }
            }

            // getCustomerDetails
            return dtCustomerDetails;

        }

        //public DataTable LoadDistributor_SE(string sales_eng)
        //{
        //    DataTable dtCustomerDetails = new DataTable();
        //    SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
        //    try
        //    {
        //        connection.Open();

        //        SqlCommand command = new SqlCommand("getDistributor_SE", connection);
        //        command.CommandType = CommandType.StoredProcedure;
        //        command.Parameters.Add("@assigned_salesengineer_id", SqlDbType.VarChar, 30).Value = sales_eng;
        //        SqlDataAdapter sqlDa = new SqlDataAdapter(command);
        //        sqlDa.Fill(dtCustomerDetails);
        //        //return dtCustomerDetails;
        //    }

        //    catch (Exception ex)
        //    {

        //    }

        //    finally
        //    {
        //        if (connection.State == ConnectionState.Open)
        //        {
        //            connection.Close();
        //        }
        //    }

        //    // getCustomerDetails
        //    return dtCustomerDetails;

        //}

        public DataTable LoadDistributor_BM(string branchcode)
        {
            DataTable dtCustomerDetails = new DataTable();
            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
            try
            {
                connection.Open();

                SqlCommand command = new SqlCommand("getDistributor_BM", connection);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@branchCode", SqlDbType.VarChar, 30).Value = branchcode;
                SqlDataAdapter sqlDa = new SqlDataAdapter(command);
                sqlDa.Fill(dtCustomerDetails);
                //return dtCustomerDetails;
            }

            catch (Exception ex)
            {
                objFunc.LogError(ex);
            }

            finally
            {
                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }
            }

            // getCustomerDetails
            return dtCustomerDetails;

        }


        public DataTable LoadDistributor_TM(string strUserId)
        {
            DataTable dtCustomerDetails = new DataTable();
            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
            try
            {
                connection.Open();

                SqlCommand command = new SqlCommand("getDistributor_TM", connection);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@territory_engineer_id", SqlDbType.VarChar, 30).Value = strUserId;
                SqlDataAdapter sqlDa = new SqlDataAdapter(command);
                sqlDa.Fill(dtCustomerDetails);
                //return dtCustomerDetails;
            }

            catch (Exception ex)
            {
                objFunc.LogError(ex);
            }

            finally
            {
                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }
            }

            // getCustomerDetails
            return dtCustomerDetails;

        }

        public DataTable LoadCustomerDetails_SE_Reports(string sales_eng)
        {
            DataTable dtCustomerDetails = new DataTable();
            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
            try
            {
                connection.Open();

                SqlCommand command = new SqlCommand("getCustomerDetails_SE_Reports", connection);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@assigned_salesengineer_id", SqlDbType.VarChar, 30).Value = sales_eng;
                SqlDataAdapter sqlDa = new SqlDataAdapter(command);
                sqlDa.Fill(dtCustomerDetails);
                //return dtCustomerDetails;
            }

            catch (Exception ex)
            {
                objFunc.LogError(ex);
            }

            finally
            {
                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }
            }

            // getCustomerDetails
            return dtCustomerDetails;

        }

        //public DataTable LoadAllDistributors()
        //{
        //    DataTable dtDistributorDetails = new DataTable();
        //    SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
        //    try
        //    {
        //        connection.Open();

        //        SqlCommand command = new SqlCommand("getDistributor", connection);
        //        command.CommandType = CommandType.StoredProcedure;
        //        SqlDataAdapter sqlDa = new SqlDataAdapter(command);
        //        sqlDa.Fill(dtDistributorDetails);
        //        //return dtCustomerDetails;
        //    }

        //    catch (Exception ex)
        //    {

        //    }

        //    finally
        //    {
        //        if (connection.State == ConnectionState.Open)
        //        {
        //            connection.Close();
        //        }
        //    }

        //    // getCustomerDetails
        //    return dtDistributorDetails;

        //}
        public void LogFile(string use1, string count, string names, string use)
        {
            StreamWriter log;
            try
            {
                if (!File.Exists("logfile_Vist.txt"))
                {
                    log = new StreamWriter(("C:\\TSBA_Log\\logfile_Vist.txt"), true);
                }
                else
                {
                    log = File.AppendText(("C:\\TSBA_Log\\logfile_Vist.txt"));
                }

                // Write to the file:
                log.WriteLine("Date Time:" + DateTime.Now.ToString());
                log.WriteLine(use1);
                log.WriteLine(count);
                log.WriteLine(names);
                log.WriteLine(use + "\n");
                // Close the stream:
                log.Close();
            }
            catch (Exception ex)
            {
                objFunc.LogError(ex);
            }
        }

        public DataTable loadDistributors_customers_v(string distributorNum)
        {
            DataTable dtnewCustomer = new DataTable();
            DataTable dtTemp = new DataTable();
            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget_CRPData"].ToString());
            try
            {
                connection.Open();

                ///Production Code - Do not delete
                SqlCommand command = new SqlCommand("SELECT DISTINCT '' as crp_blank, [CRPID] as customer_number, [CNME3] as customer_short_name, cls.[ClsName] as customer_class from [DBVW_DistibutorsCustomers] d LEFT OUTER JOIN [DBVW_Classfication] cls ON  cls.ClsID = d.ClsID WHERE d.[CUST] = " + distributorNum + " and [CTYP]='S'", connection);

                ///Local Testing code -  Do not delete
                //   SqlCommand command = new SqlCommand("SELECT DISTINCT [crp_blank] ,[customer_number] ,[customer_short_name] ,[customer_class],[DistributorNumber]  FROM [Distributor_Customers] WHERE [DistributorNumber] = " + distributorNum, connection);

                command.CommandType = CommandType.Text;
                SqlDataAdapter sqlDa = new SqlDataAdapter(command);
                sqlDa.Fill(dtnewCustomer);

                dtTemp.Columns.Add("crp_blank", typeof(string));
                dtTemp.Columns.Add("customer_number", typeof(string));
                dtTemp.Columns.Add("customer_short_name", typeof(string));
                dtTemp.Columns.Add("customer_class", typeof(string));

                for (int i = 0; i < dtnewCustomer.Rows.Count; i++)
                {
                    dtTemp.Rows.Add(dtnewCustomer.Rows[i].ItemArray[0].ToString(),
                                    dtnewCustomer.Rows[i].ItemArray[1].ToString(),
                                    dtnewCustomer.Rows[i].ItemArray[2].ToString(),
                                    dtnewCustomer.Rows[i].ItemArray[3] != null ? dtnewCustomer.Rows[i].ItemArray[3].ToString() : "");
                }

            }

            catch (Exception ex)
            {
                LogFile("loadDistributors_customers_v", ex.Message.ToString(), "", "");
                objFunc.LogError(ex);
            }

            finally
            {
                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }
            }

            // getCustomerDetails
            return dtTemp;
        }

        public DataTable LoadCustomers()
        {
            DataTable dtnewCustomer = new DataTable();
            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget_CRPData"].ToString());
            try
            {
                connection.Open();

                SqlCommand command = new SqlCommand("SELECT  [CRPID] as customer_number,[CNME3] as customer_name  FROM [DBVW_DistibutorsCustomers] where [CUST]=" + distributor + " and [CTYP]='S'", connection);
                command.CommandType = CommandType.Text;
                //command.Parameters.Add("@distributor", SqlDbType.VarChar, 30).Value = distributor;
                SqlDataAdapter sqlDa = new SqlDataAdapter(command);
                sqlDa.Fill(dtnewCustomer);
                //return dtCustomerDetails;
            }

            catch (Exception ex)
            {
                objFunc.LogError(ex);
            }

            finally
            {
                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }
            }

            // getCustomerDetails
            return dtnewCustomer;

        }

        public string GetCustomerClass_from_CRP(string CP_customerNumber)
        {
            string custclass = "";

            DataTable dtnewCustomer = new DataTable();
            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget_CRPData"].ToString());
            try
            {
                connection.Open();

                SqlCommand command = new SqlCommand("SELECT cls.[ClsName] from [DBVW_DistibutorsCustomers] d, [DBVW_Classfication] cls WHERE  cls.ClsID =  d.ClsID AND d.[CRPID] = " + CP_customerNumber, connection);
                command.CommandType = CommandType.Text;
                SqlDataAdapter sqlDa = new SqlDataAdapter(command);
                sqlDa.Fill(dtnewCustomer);
                //return dtCustomerDetails;
                if (dtnewCustomer.Rows.Count > 0)
                {
                    custclass = dtnewCustomer.Rows[0].ItemArray[0].ToString();
                }
            }
            catch (Exception ex)
            {
                objFunc.LogError(ex);
            }
            finally
            {
                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }
            }
            return custclass;
        }

        /// <summary>
        /// To load all the customers for the branch manager 
        /// </summary>
        /// <param name="branchCode"></param>
        /// <returns></returns>
        public DataTable LoadCustomerDetails_BM(string branchCode)
        {
            DataTable dtCustomerDetails = new DataTable();
            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
            try
            {
                connection.Open();

                SqlCommand command = new SqlCommand("getCustomerDetails_BM", connection);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@branchCode", SqlDbType.VarChar, 30).Value = branchCode;
                SqlDataAdapter sqlDa = new SqlDataAdapter(command);
                sqlDa.Fill(dtCustomerDetails);
                //return dtCustomerDetails;
            }

            catch (Exception ex)
            {
                objFunc.LogError(ex);
            }

            finally
            {
                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }
            }

            // getCustomerDetails
            return dtCustomerDetails;

        }


        public DataTable LoadCustomerDetails_BM_Reports(string branchCode)
        {
            DataTable dtCustomerDetails = new DataTable();
            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
            try
            {
                connection.Open();

                SqlCommand command = new SqlCommand("getCustomerDetails_BM_Reports", connection);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@branchCode", SqlDbType.VarChar, 30).Value = branchCode;
                SqlDataAdapter sqlDa = new SqlDataAdapter(command);
                sqlDa.Fill(dtCustomerDetails);
                //return dtCustomerDetails;
            }

            catch (Exception ex)
            {
                objFunc.LogError(ex);
            }

            finally
            {
                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }
            }

            // getCustomerDetails
            return dtCustomerDetails;

        }

        /// <summary>
        /// To load all the customers
        /// </summary>
        /// <returns></returns>
        public DataTable LoadCustomerDetails_HO()
        {
            DataTable dtCustomerDetails = new DataTable();
            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
            try
            {
                connection.Open();

                SqlCommand command = new SqlCommand("getCustomerDetails_HO", connection);
                command.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter sqlDa = new SqlDataAdapter(command);
                sqlDa.Fill(dtCustomerDetails);
                //return dtCustomerDetails;
            }

            catch (Exception ex)
            {
                objFunc.LogError(ex);
            }

            finally
            {
                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }
            }

            // getCustomerDetails
            return dtCustomerDetails;

        }

        //public DataTable LoadCustomerDetails_HO_Reports()
        //{
        //    DataTable dtCustomerDetails = new DataTable();
        //    SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
        //    try
        //    {
        //        connection.Open();

        //        SqlCommand command = new SqlCommand("getCustomerDetails_HO_Reports", connection);
        //        command.CommandType = CommandType.StoredProcedure;
        //        SqlDataAdapter sqlDa = new SqlDataAdapter(command);
        //        sqlDa.Fill(dtCustomerDetails);
        //        //return dtCustomerDetails;
        //    }

        //    catch (Exception ex)
        //    {

        //    }

        //    finally
        //    {
        //        if (connection.State == ConnectionState.Open)
        //        {
        //            connection.Close();
        //        }
        //    }

        //    // getCustomerDetails
        //    return dtCustomerDetails;

        //}


        public DataSet getProjectReport(string roleId, string Owner, string Reviewer, string Escalate, string Industry, string subindustry, string Customer, string customerClass, string Branch, string status, string from_target_date, string to_target_date, string from_created_date, string to_created_date, string strSearch, string cter = null, string strProjType = null)
        {
            DataSet dtmdpreports = new DataSet();
            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
            try
            {
                connection.Open();
                SqlCommand command = new SqlCommand("getProjectReport", connection);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@RoleId", SqlDbType.VarChar, 255, null).Value = roleId;
                command.Parameters.Add("@Owner", SqlDbType.VarChar, 255, null).Value = Owner;
                command.Parameters.Add("@Reviewer", SqlDbType.VarChar, 255, null).Value = Reviewer;
                command.Parameters.Add("@Escalate", SqlDbType.VarChar, 255, null).Value = Escalate;
                command.Parameters.Add("@Industry", SqlDbType.VarChar, 255, null).Value = Industry;
                command.Parameters.Add("@SubIndustry", SqlDbType.VarChar, 255, null).Value = subindustry;
                command.Parameters.Add("@Customer", SqlDbType.VarChar, 255, null).Value = Customer;
                command.Parameters.Add("@customer_class", SqlDbType.VarChar, 255, null).Value = customerClass;
                command.Parameters.Add("@Branch", SqlDbType.VarChar, 255, null).Value = Branch;
              //command.Parameters.Add("@Outcome", SqlDbType.VarChar, 255, null).Value = Outcome;
                command.Parameters.Add("@Status", SqlDbType.VarChar, 255, null).Value = status;
                command.Parameters.Add("@from_target_date", SqlDbType.VarChar, 255, null).Value = from_target_date;
                command.Parameters.Add("@to_target_date", SqlDbType.VarChar, 255, null).Value = to_target_date;
                command.Parameters.Add("@from_created_date", SqlDbType.VarChar, 255, null).Value = from_created_date;
                command.Parameters.Add("@to_created_date", SqlDbType.VarChar, 255, null).Value = to_created_date;
                command.Parameters.Add("@cter", SqlDbType.VarChar, 10, null).Value = cter;
                command.Parameters.Add("@projtype", SqlDbType.VarChar, 255, null).Value = strProjType;
                command.Parameters.Add("@search", SqlDbType.VarChar, -1, null).Value = strSearch;
                SqlDataAdapter sqlDa = new SqlDataAdapter(command);
                sqlDa.Fill(dtmdpreports);
            }

            catch (Exception ex)
            {
                objFunc.LogError(ex);
            }

            finally
            {
                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }

            }

            return dtmdpreports;
        }



        public string insertMTDReport(MDP objMDP)
        {
            string strResult = string.Empty;
            try
            {
                string connstring = ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString();
                SqlConnection connection = new SqlConnection(connstring);
                connection.Open();
                SqlCommand command = new SqlCommand("insertMDPReport", connection);
                command.CommandType = CommandType.StoredProcedure;
                SqlParameter[] parameters ={
                                        new SqlParameter    ("@cust_num" ,SqlDbType.VarChar,50 ),//0
                                        new SqlParameter   ("@project_number" ,SqlDbType.VarChar,50 ),//1	                                   
	                                    new SqlParameter    ("@rpt_remark" , SqlDbType.VarChar,255 ) ,//2
                                        new SqlParameter    ("@Error", SqlDbType.VarChar,100)//3
                                        };
                parameters[0].Value = objMDP.rpt_cust_num;
                parameters[1].Value = objMDP.rpt_project_number;
                parameters[2].Value = objMDP.rpt_remark;
                parameters[3].Value = ParameterDirection.Output;
                Int32 j;
                for (j = 0; j <= parameters.Length - 1; j++)
                {
                    command.Parameters.Add(parameters[j]);
                }

                try
                {
                    command.ExecuteNonQuery();
                    strResult = command.Parameters["@Error"].Value.ToString();

                    return strResult;
                }

                catch (Exception ex)
                {
                    strResult = ex.Message;
                    return strResult;
                }

                finally
                {
                    if (connection.State == ConnectionState.Open)
                    {
                        connection.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                objFunc.LogError(ex);
            }
            return strResult;
        }

        public string cancel_Project(MDP objMDP)
        {

            string strResult = string.Empty;
            try
            {
                string connstring = ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString();
                SqlConnection connection = new SqlConnection(connstring);
                connection.Open();
                SqlCommand command = new SqlCommand("cancel_Project", connection);
                command.CommandType = CommandType.StoredProcedure;
                SqlParameter[] parameters ={
                                        new SqlParameter    ("@customer_number" ,SqlDbType.VarChar,50 ),//0
                                         new SqlParameter    ("@project_number" ,SqlDbType.VarChar,50 ),//0
	                                 

                                        };
                parameters[0].Value = objMDP.customer_num;
                parameters[1].Value = objMDP.project_number;


                Int32 j;
                for (j = 0; j <= parameters.Length - 1; j++)
                {
                    command.Parameters.Add(parameters[j]);
                }

                try
                {
                    command.ExecuteNonQuery();
                    strResult = command.Parameters["@Error"].Value.ToString();

                    return strResult;
                }

                catch (Exception ex)
                {
                    strResult = ex.Message;
                    return strResult;
                }

                finally
                {
                    if (connection.State == ConnectionState.Open)
                    {
                        connection.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                objFunc.LogError(ex);
            }
            return strResult;
        }

        public string freezemdp_info(MDP objMDP)
        {
            string strResult = string.Empty;
            try
            {
                string connstring = ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString();
                SqlConnection connection = new SqlConnection(connstring);
                connection.Open();
                SqlCommand command = new SqlCommand("freezemdp_info", connection);
                command.CommandType = CommandType.StoredProcedure;

                SqlParameter[] parameters ={
                                        new SqlParameter    ("@Customer_Num" ,SqlDbType.VarChar,50 ),//0
	                                    new SqlParameter    ("@industry_Id", SqlDbType.VarChar,255 ),//1	
                                        new SqlParameter    ("@project_number" ,SqlDbType.VarChar,50 ),//2
	                                    new SqlParameter    ("@project_title", SqlDbType.VarChar,255 ),//3
	                                    new SqlParameter    ("@Existing_Product" , SqlDbType.VarChar,255 ) ,//4
                                        new SqlParameter    ("@Component" , SqlDbType.VarChar,255 ) ,//5
                                        new SqlParameter    ("@Competition" , SqlDbType.VarChar,50 ) ,//6
                                        new SqlParameter    ("@NoOfStages" ,SqlDbType.Int ),//7
                                        new SqlParameter    ("@Target_Date" ,SqlDbType.VarChar,50),//8
                                        new SqlParameter    ("@Sales_Eng_Id" ,SqlDbType.VarChar,50 ),//9
                                        new SqlParameter    ("@OverAll_Potential" , SqlDbType.Decimal ),//10
                                        new SqlParameter    ("@Potential_Lakhs" , SqlDbType.Decimal ),//11
                                        new SqlParameter    ("@Business_Expected" , SqlDbType.Decimal ),//12
                                        new SqlParameter    ("@Reviewer" ,SqlDbType.VarChar,50 ),//13
                                        new SqlParameter    ("@Escalated_To" ,SqlDbType.VarChar,50 ),	//14
                                        new SqlParameter    ("@BranchCode",SqlDbType.VarChar,30),//15
                                        new SqlParameter    ("@Project_Status",SqlDbType.VarChar,30),//16  
                                        new SqlParameter    ("@Date_Created",SqlDbType.VarChar,30),//17  
                                        new SqlParameter    ("@Error", SqlDbType.VarChar,100)//18
                                       };

                parameters[0].Value = objMDP.customer_num;
                parameters[1].Value = objMDP.Industry;
                parameters[2].Value = objMDP.project_number;
                parameters[3].Value = objMDP.project_title;
                parameters[4].Value = objMDP.Existing_Product;
                parameters[5].Value = objMDP.Component;
                parameters[6].Value = objMDP.Competition;
                parameters[7].Value = objMDP.NoOfStages;
                parameters[8].Value = objMDP.Target_Date;
                parameters[9].Value = objMDP.Sales_Eng_Id;
                parameters[10].Value = objMDP.OverAll_Potential;
                parameters[11].Value = objMDP.Potential_Lakhs;
                parameters[12].Value = objMDP.Business_Expected;
                parameters[13].Value = objMDP.Reviewer;
                parameters[14].Value = objMDP.EscalatedTo;
                parameters[15].Value = objMDP.BranchCode;
                parameters[16].Value = objMDP.project_status;
                parameters[17].Value = objMDP.Date_Created;
                parameters[18].Value = ParameterDirection.Output;

                Int32 j;
                for (j = 0; j <= parameters.Length - 1; j++)
                {
                    command.Parameters.Add(parameters[j]);
                }

                try
                {
                    command.ExecuteNonQuery();
                    strResult = command.Parameters["@Error"].Value.ToString();

                    return strResult;
                }

                catch (Exception ex)
                {
                    strResult = ex.Message;
                    return strResult;
                }

                finally
                {
                    if (connection.State == ConnectionState.Open)
                    {
                        connection.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                objFunc.LogError(ex);
            }
            return strResult;
        }

        public string update_project_authority(MDP objMDP)
        {
            string strResult = string.Empty;
            try
            {
                string connstring = ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString();
                SqlConnection connection = new SqlConnection(connstring);
                connection.Open();
                SqlCommand command = new SqlCommand("update_project_authority", connection);
                command.CommandType = CommandType.StoredProcedure;
                SqlParameter[] parameters ={
                                        new SqlParameter    ("@customer_number" ,SqlDbType.VarChar,30 ),//0
                                        new SqlParameter    ("@project_number" ,SqlDbType.VarChar,30 ),//1	                                   
	                                    new SqlParameter    ("@owner" , SqlDbType.VarChar,30 ) ,//2
                                        new SqlParameter    ("@reviewer", SqlDbType.VarChar,30),//3
                                        new SqlParameter    ("@escalate" , SqlDbType.VarChar,30 ) ,//4
                                        new SqlParameter    ("@Error", SqlDbType.VarChar,100)//5
                                        };
                parameters[0].Value = objMDP.admin_cust_num;
                parameters[1].Value = objMDP.admin_proj_num;
                parameters[2].Value = objMDP.admin_owner;
                parameters[3].Value = objMDP.admin_reviewer;
                parameters[4].Value = objMDP.admin_escalate;
                parameters[5].Value = ParameterDirection.Output;
                Int32 j;
                for (j = 0; j <= parameters.Length - 1; j++)
                {
                    command.Parameters.Add(parameters[j]);
                }

                try
                {
                    command.ExecuteNonQuery();
                    strResult = command.Parameters["@Error"].Value.ToString();

                    return strResult;
                }

                catch (Exception ex)
                {
                    strResult = ex.Message;
                    return strResult;
                }

                finally
                {
                    if (connection.State == ConnectionState.Open)
                    {
                        connection.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                objFunc.LogError(ex);
            }
            return strResult;
        }

        public DataTable LoadCustomerDetailstype(string SalesEngineerId, string RoleId, string custtype = null, string branchcode = null, string cter = null, string customer_class = null, string territoryEngineerId = null)
        {
            DataTable dtCustomerDetails = new DataTable();
            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
            try
            {
                connection.Open();
                SqlCommand command = new SqlCommand("getCustomerDetailsontype_reports", connection);
                command.CommandType = CommandType.StoredProcedure;
                if (SalesEngineerId == "ALL")
                {

                    // command.Parameters.Add("@assigned_salesengineer_id", SqlDbType.VarChar, 10).Value = SalesEngineerId;
                    command.Parameters.Add("@roleId", SqlDbType.VarChar, 50).Value = RoleId;
                    command.Parameters.Add("@custtype", SqlDbType.VarChar, 50).Value = custtype;
                    command.Parameters.Add("@branchcode", SqlDbType.VarChar, 50).Value = branchcode;
                    command.Parameters.Add("@cter", SqlDbType.VarChar, 50).Value = cter;
                    command.Parameters.Add("@customer_class", SqlDbType.VarChar, 50).Value = customer_class;
                    command.Parameters.Add("@territory_engineer_id", SqlDbType.VarChar, 50).Value = territoryEngineerId;

                }
                else if (SalesEngineerId != "ALL")
                {
                    command.Parameters.Add("@assigned_salesengineer_id", SqlDbType.VarChar, 10).Value = SalesEngineerId;
                    command.Parameters.Add("@roleId", SqlDbType.VarChar, 50).Value = RoleId;
                    command.Parameters.Add("@custtype", SqlDbType.VarChar, 50).Value = custtype;
                    command.Parameters.Add("@branchcode", SqlDbType.VarChar, 50).Value = branchcode;
                    command.Parameters.Add("@cter", SqlDbType.VarChar, 50).Value = cter;
                    command.Parameters.Add("@customer_class", SqlDbType.VarChar, 50).Value = customer_class;
                    command.Parameters.Add("@territory_engineer_id", SqlDbType.VarChar, 50).Value = territoryEngineerId;

                }
                SqlDataAdapter sqlDa = new SqlDataAdapter(command);
                sqlDa.Fill(dtCustomerDetails);
                //return dtCustomerDetails;
            }

            catch (Exception ex)
            {
                objFunc.LogError(ex);
            }

            finally
            {
                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }
            }

            // getCustomerDetails
            return dtCustomerDetails;

        }


        public DataTable getBranch_TM(string territory_engineer_id)
        {
            DataTable dtData = new DataTable();
            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
            try
            {
                connection.Open();
                SqlCommand command = new SqlCommand("getBranch_byTerritory_id", connection);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@territory_engineer_id", SqlDbType.VarChar, 50, null).Value = territory_engineer_id;

                SqlDataAdapter sqlDa = new SqlDataAdapter(command);
                sqlDa.Fill(dtData);

            }
            catch (Exception ex)
            {
                objFunc.LogError(ex);
            }
            finally
            {
                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }

            }

            return dtData;
        }


        public string save_file_details(MDP objMDP)
        {
            string strResult = string.Empty;
            try
            {
                string connstring = ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString();
                SqlConnection connection = new SqlConnection(connstring);
                connection.Open();
                SqlCommand command = new SqlCommand("tt_save_file_details", connection);
                command.CommandType = CommandType.StoredProcedure;
                SqlParameter[] parameters ={
                                        new SqlParameter    ("@customer_number" ,SqlDbType.VarChar,50 ),//0
                                        new SqlParameter    ("@distributor" ,SqlDbType.VarChar,50 ),//1	                                   
	                                    new SqlParameter    ("@project_number" , SqlDbType.VarChar,255 ) ,//2
                                        new SqlParameter    ("@stage_number" , SqlDbType.VarChar,255 ) ,//3
                                        new SqlParameter    ("@file_name", SqlDbType.VarChar,100)//4
                                        };
                parameters[0].Value = objMDP.file_customer_num;
                parameters[1].Value = objMDP.file_distributor;
                parameters[2].Value = objMDP.file_project_num;
                parameters[3].Value = objMDP.file_Stage_Number;
                parameters[4].Value = objMDP.file_name;
                Int32 j;
                for (j = 0; j <= parameters.Length - 1; j++)
                {
                    command.Parameters.Add(parameters[j]);
                }

                try
                {
                    command.ExecuteNonQuery();
                    strResult = command.Parameters["@Error"].Value.ToString();

                    return strResult;
                }

                catch (Exception ex)
                {
                    strResult = ex.Message;
                    return strResult;
                }

                finally
                {
                    if (connection.State == ConnectionState.Open)
                    {
                        connection.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                objFunc.LogError(ex);
            }
            return strResult;
        }

        /// <summary>
        /// Author : Anantha
        /// Date : Oct 21, 2016
        /// Desc : 
        /// </summary>
        /// <param name="objMDP"></param>
        /// <returns></returns>
        public string Delete_file_details(MDP objMDP)
        {
            string strResult = string.Empty;
            SqlCommand command;
            SqlConnection connection;
            string connstring = ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString();
            connection = new SqlConnection(connstring);
            SqlDataAdapter sqlDA;
            DataTable dt;
            try
            {
                dt = new DataTable();
                connection.Open();
                command = new SqlCommand("tt_delete_file_details", connection);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@project_number", SqlDbType.VarChar, 255, null).Value = objMDP.file_project_num;
                command.Parameters.Add("@stage_number", SqlDbType.VarChar, 255, null).Value = objMDP.file_Stage_Number;
                command.Parameters.Add("@file_name", SqlDbType.VarChar, 255, null).Value = objMDP.file_name;
                sqlDA = new SqlDataAdapter(command);
                sqlDA.Fill(dt);
                if (dt.Rows.Count > 0)
                {
                    strResult = Convert.ToString(dt.Rows[0]["OUTPUT"]);
                }
            }

            catch (Exception ex)
            {
                objFunc.LogError(ex);
                strResult = ex.Message;
                return strResult;
            }

            finally
            {
                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }
            }

            return strResult;
        }

        public DataTable getProjectListForApproval(string strUserId, string cter = null)
        {
            DataTable dtmdpreports = new DataTable();
            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
            try
            {
                connection.Open();
                SqlCommand command = new SqlCommand("getProjectListForApproval", connection);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@UserId", SqlDbType.VarChar, 255, null).Value = strUserId;
                command.Parameters.Add("@cter", SqlDbType.VarChar, 10, null).Value = cter;
                SqlDataAdapter sqlDa = new SqlDataAdapter(command);
                sqlDa.Fill(dtmdpreports);

            }
            catch (Exception ex)
            {
                objFunc.LogError(ex);
            }
            finally
            {
                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }

            }

            return dtmdpreports;
        }

        public DataTable Get_File_Names(string customer_num, string project_number)
        {
            DataTable dtfiles = new DataTable();
            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
            try
            {
                connection.Open();
                SqlCommand command = new SqlCommand("Get_File_Names", connection);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@customer_num", SqlDbType.VarChar, 255, null).Value = customer_num;
                command.Parameters.Add("@project_num", SqlDbType.VarChar, 255, null).Value = project_number;



                SqlDataAdapter sqlDa = new SqlDataAdapter(command);
                sqlDa.Fill(dtfiles);

            }
            catch (Exception ex)
            {
                objFunc.LogError(ex);
            }
            finally
            {
                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }

            }

            return dtfiles;

        }

        public DataTable Get_StageWise_File_Names(string customer_num, string stage_number, string project_number)
        {
            DataTable dtfiles = new DataTable();
            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
            try
            {
                connection.Open();
                SqlCommand command = new SqlCommand("Get_StageWise_File_Names", connection);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@customer_num", SqlDbType.VarChar, 255, null).Value = customer_num;
                command.Parameters.Add("@project_num", SqlDbType.VarChar, 255, null).Value = project_number;
                command.Parameters.Add("@stage_num", SqlDbType.VarChar, 255, null).Value = stage_number;



                SqlDataAdapter sqlDa = new SqlDataAdapter(command);
                sqlDa.Fill(dtfiles);

            }
            catch (Exception ex)
            {
                objFunc.LogError(ex);
            }
            finally
            {
                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }

            }

            return dtfiles;

        }

        public string Get_File_Names_byNumber(int file_number)
        {
            DataTable dtfiles = new DataTable();
            string file_name = "";
            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
            try
            {
                connection.Open();
                SqlCommand command = new SqlCommand("Get_File_Names_byNumber", connection);
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add("@file_number", SqlDbType.VarChar, 255, null).Value = file_number;



                SqlDataAdapter sqlDa = new SqlDataAdapter(command);
                sqlDa.Fill(dtfiles);
                for (int i = 0; i < dtfiles.Rows.Count; i++)
                {
                    file_name = dtfiles.Rows[i].ItemArray[0].ToString();
                }
            }
            catch (Exception ex)
            {
                objFunc.LogError(ex);
            }
            finally
            {
                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }

            }

            return file_name;

        }

        public string approve_Project(MDP objMDP)
        {

            string strResult = string.Empty;
            try
            {
                string connstring = ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString();
                SqlConnection connection = new SqlConnection(connstring);
                connection.Open();
                SqlCommand command = new SqlCommand("approve_Project", connection);
                command.CommandType = CommandType.StoredProcedure;
                SqlParameter[] parameters ={
                                        new SqlParameter    ("@customer_number" ,SqlDbType.VarChar,50 ),//0
                                        new SqlParameter    ("@project_number" ,SqlDbType.VarChar,50 ),//1
                                        new SqlParameter    ("@IsSentForApproval" ,SqlDbType.VarChar,50 ),//2
                                        new SqlParameter    ("@IsApproved" ,SqlDbType.VarChar,50 ),//3
                                        new SqlParameter    ("@IsRejected" ,SqlDbType.VarChar,50 ),//4
                                        new SqlParameter    ("@Approval_Remarks" ,SqlDbType.VarChar,255 ),//5
                                        
	                                 

                                        };
                parameters[0].Value = objMDP.customer_num;
                parameters[1].Value = objMDP.project_number;
                parameters[2].Value = objMDP.IsSentForApproval;
                parameters[3].Value = objMDP.IsApproved;
                parameters[4].Value = objMDP.IsRejected;
                parameters[5].Value = objMDP.Approval_Remarks;



                Int32 j;
                for (j = 0; j <= parameters.Length - 1; j++)
                {
                    command.Parameters.Add(parameters[j]);
                }

                try
                {
                    command.ExecuteNonQuery();
                    strResult = command.Parameters["@Error"].Value.ToString();

                    return strResult;
                }

                catch (Exception ex)
                {
                    strResult = ex.Message;
                    return strResult;
                }

                finally
                {
                    if (connection.State == ConnectionState.Open)
                    {
                        connection.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                objFunc.LogError(ex);
            }
            return strResult;
        }

        public string get_distributorname_by_number(string distributor_num)
        {
            DataTable dtdistributor_name = new DataTable();
            string distributor_name = "";
            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
            try
            {
                connection.Open();

                SqlCommand command = new SqlCommand("get_distributor_name_by_number", connection);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@distributor_number", SqlDbType.VarChar, 10).Value = distributor_num;
                SqlDataAdapter sqlDa = new SqlDataAdapter(command);
                sqlDa.Fill(dtdistributor_name);
                for (int i = 0; i < dtdistributor_name.Rows.Count; i++)
                {
                    distributor_name = dtdistributor_name.Rows[i].ItemArray[0].ToString();
                }
            }
            catch (Exception e)
            {
                objFunc.LogError(e);
            }
            finally
            {
                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }
            }
            return distributor_name;

        }

        /// <summary>
        /// Author : Anamika
        /// Date : Oct 18, 2016
        /// </summary>
        /// <param name="roleId"></param>
        /// <param name="branchcode"></param>
        /// <param name="customernumber"></param>
        /// <param name="ddCustomerClass"></param>
        /// <param name="strOwner"></param>
        /// <param name="strStatus"></param>
        /// <param name="strReviewer"></param>
        /// <param name="strEscalate"></param>
        /// <returns></returns>
        internal DataSet LoadFilterValue(string strUserId, string roleId, string branchcode, string customernumber, string ddCustomerClass, string strOwner, string strStatus, string strReviewer, string strEscalate, string cter)
        {
            DataSet dsmdpreports = new DataSet();
            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
            try
            {
                connection.Open();
                SqlCommand command = new SqlCommand("sp_getReportDetails", connection);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@USERID", SqlDbType.VarChar, 255, null).Value = strUserId;
                command.Parameters.Add("@ROLEID", SqlDbType.VarChar, 255, null).Value = roleId;
                command.Parameters.Add("@BRANCH", SqlDbType.VarChar, 10, null).Value = branchcode;
                command.Parameters.Add("@CUSTOMER", SqlDbType.VarChar, 255, null).Value = customernumber;
                command.Parameters.Add("@CUSTOMERCLASS", SqlDbType.VarChar, 255, null).Value = ddCustomerClass;
                command.Parameters.Add("@OWNER", SqlDbType.VarChar, 255, null).Value = strOwner;
                command.Parameters.Add("@STATUS", SqlDbType.VarChar, 255, null).Value = strStatus;
                command.Parameters.Add("@REVIEWER", SqlDbType.VarChar, 255, null).Value = strReviewer;
                command.Parameters.Add("@ESCALATE", SqlDbType.VarChar, 255, null).Value = strEscalate;
                command.Parameters.Add("@CTER", SqlDbType.VarChar, 255, null).Value = cter;
                SqlDataAdapter sqlDa = new SqlDataAdapter(command);
                sqlDa.Fill(dsmdpreports);

            }
            catch (Exception ex)
            {
                objFunc.LogError(ex);
            }
            finally
            {
                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }

            }

            return dsmdpreports;
        }

        /// <summary>
        /// Author : Anamika
        /// Date : Oct 21,2016
        /// </summary>
        /// <param name="file_project_num"></param>
        /// <returns></returns>
        internal DataTable getCurrentStage(string file_project_num)
        {

            DataTable dtStage = new DataTable();
            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
            try
            {
                connection.Open();
                SqlCommand command = new SqlCommand("sp_getcurrentStage", connection);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@PROJECT_NUM", SqlDbType.VarChar, 255, null).Value = file_project_num;
                SqlDataAdapter sqlDa = new SqlDataAdapter(command);
                sqlDa.Fill(dtStage);

            }
            catch (Exception ex)
            {
                objFunc.LogError(ex);
            }
            finally
            {
                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }

            }

            return dtStage;
        }


        /// <summary>
        /// Anantha To get all the project types
        /// </summary>
        /// <returns></returns>
        public DataTable GetAllProjectType()
        {
            DataTable dtallprojecttypes = new DataTable();
            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
            try
            {
                connection.Open();
                SqlCommand command = new SqlCommand("getprojecttypelist", connection);
                command.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter sqlDa = new SqlDataAdapter(command);
                sqlDa.Fill(dtallprojecttypes);

            }
            catch (Exception ex)
            {

            }
            finally
            {
                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }

            }

            return dtallprojecttypes;

        }

    }
}