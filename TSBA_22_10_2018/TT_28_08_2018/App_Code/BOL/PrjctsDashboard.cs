﻿
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Configuration;
namespace TaegutecSalesBudget
{
    public class PrjctsDashboard
    {
        CommonFunctions comfunc=new CommonFunctions();
        public DataTable getProjectsProgress(string sales_engnr_id, string branch_code,string customerClass,string cter=null)
        {
            DataTable dt = new DataTable();
            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
            try
            {
                conn.Open();
                SqlCommand command = new SqlCommand("getTrgtdCmpltnPrjctsInDays", conn);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@sales_engineer_id", SqlDbType.VarChar, 50).Value = sales_engnr_id;
                command.Parameters.Add("@branch_code", SqlDbType.VarChar, 50).Value = branch_code;
                command.Parameters.Add("@customer_class", SqlDbType.VarChar, 50).Value = customerClass;
                command.Parameters.Add("@cter", SqlDbType.VarChar, 50).Value = cter;
                SqlDataAdapter sqlda = new SqlDataAdapter(command);
                sqlda.Fill(dt);
            }
            catch (Exception ex)
            {
                comfunc.LogError(ex);
            }
            finally
            {
                if (conn.State == ConnectionState.Open)
                {
                    conn.Close();
                }
            }
            return dt;
        }

        public DataTable getPendingStages(string sales_engnr_id, string branch_code, string customerClass, string cter = null)
        {
            DataTable dt = new DataTable();
            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
            try
            {
                conn.Open();
                SqlCommand command = new SqlCommand("getPndngStagesStatus", conn);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@sales_engineer_id", SqlDbType.VarChar, 50).Value = sales_engnr_id;
                command.Parameters.Add("@branch_code", SqlDbType.VarChar, 50).Value = branch_code;
                command.Parameters.Add("@customer_class", SqlDbType.VarChar, 50).Value = customerClass;
                command.Parameters.Add("@cter", SqlDbType.VarChar, 50).Value = cter;
                SqlDataAdapter sqlda = new SqlDataAdapter(command);
                sqlda.Fill(dt);
            }
            catch (Exception ex)
            {
                comfunc.LogError(ex);
            }
            finally
            {
                if (conn.State == ConnectionState.Open)
                {
                    conn.Close();
                }
            }
            return dt;
        }
        
        public DataTable getOverdueStages(string sales_engnr_id, string branch_code, string customerClass, string cter = null)
        {
            DataTable dt = new DataTable();
            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
            try
            {
                conn.Open();
                SqlCommand command = new SqlCommand("getOverdueStagesStatus", conn);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@sales_engineer_id", SqlDbType.VarChar, 50).Value = sales_engnr_id;
                command.Parameters.Add("@branch_code", SqlDbType.VarChar, 50).Value = branch_code;
                command.Parameters.Add("@customer_class", SqlDbType.VarChar, 50).Value = customerClass;
                command.Parameters.Add("@cter", SqlDbType.VarChar, 50).Value = cter;
                SqlDataAdapter sqlda = new SqlDataAdapter(command);
                sqlda.Fill(dt);
            }
            catch (Exception ex)
            {
                comfunc.LogError(ex);
            }
            finally
            {
                if (conn.State == ConnectionState.Open)
                {
                    conn.Close();
                }
            }
            return dt;
        }
        
        public decimal[] getCompletedProjectVal(string sales_engnr_id, string branch_code, string customerClass, string cter = null)
        {
            string cmpltdProjectval = null, businessPotential = null, prcnt = null;
            decimal[] vals = new decimal[2];
            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
            try
            {
                conn.Open();
                SqlCommand command = new SqlCommand("getCompletdProjectVal", conn);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@sales_engineer_id", SqlDbType.VarChar, 50).Value = sales_engnr_id;
                command.Parameters.Add("@branch_code", SqlDbType.VarChar, 50).Value = branch_code;
                command.Parameters.Add("@customer_class", SqlDbType.VarChar, 50).Value = customerClass;
                command.Parameters.Add("@cter", SqlDbType.VarChar, 50).Value = cter;
                command.Parameters.Add("@cmpltd_prjct_val", SqlDbType.VarChar, 50).Direction = ParameterDirection.Output;
                command.Parameters.Add("@business_potential", SqlDbType.VarChar, 50).Direction = ParameterDirection.Output;
                command.ExecuteNonQuery();
                cmpltdProjectval = (command.Parameters["@cmpltd_prjct_val"].Value.ToString());
                businessPotential = (command.Parameters["@business_potential"].Value.ToString());
                if (cmpltdProjectval == ""|| cmpltdProjectval =="0")
                {
                    cmpltdProjectval = "0.00";
                }
                vals[0] = Convert.ToDecimal(cmpltdProjectval);
                vals[1] = Convert.ToDecimal(businessPotential);
                //prcnt = businessPotential == 0 ? 0 : (cmpltdProjectval / businessPotential) ;
            }
            catch (Exception ex)
            {
                comfunc.LogError(ex);
            }
            finally
            {
                if (conn.State == ConnectionState.Open)
                {
                    conn.Close();
                }
            }
            return vals;
        }
        
        public DataTable getIndustryBusinessVal(string sales_engnr_id, string branch_code, string customerClass, string cter = null)
        {
            DataTable dt = new DataTable();
            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
            try
            {
                conn.Open();
                SqlCommand command = new SqlCommand("getIndustryProjectsStatus", conn);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@sales_engineer_id", SqlDbType.VarChar, 50).Value = sales_engnr_id;
                command.Parameters.Add("@branch_code", SqlDbType.VarChar, 50).Value = branch_code;
                command.Parameters.Add("@customer_class", SqlDbType.VarChar, 50).Value = customerClass;
                command.Parameters.Add("@cter", SqlDbType.VarChar, 50).Value = cter;
                SqlDataAdapter sqlda = new SqlDataAdapter(command);
                sqlda.Fill(dt);
            }
            catch (Exception ex)
            {
                comfunc.LogError(ex);
            }
            finally
            {
                if (conn.State == ConnectionState.Open)
                {
                    conn.Close();
                }
            }
            return dt;
        }

        public DataTable getCnsldtdIndustryBusinessVal(string sales_engnr_id, string branch_code, string customerClass, string cter = null)
        {
            DataTable dt = new DataTable();
            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
            try
            {
                conn.Open();
                SqlCommand command = new SqlCommand("getConsldtdIndustryProjectsVal", conn);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@sales_engineer_id", SqlDbType.VarChar, 50).Value = sales_engnr_id;
                command.Parameters.Add("@branch_code", SqlDbType.VarChar, 50).Value = branch_code;
                command.Parameters.Add("@customer_class", SqlDbType.VarChar, 50).Value = customerClass;
                command.Parameters.Add("@cter", SqlDbType.VarChar, 50).Value = cter;
                SqlDataAdapter sqlda = new SqlDataAdapter(command);
                sqlda.Fill(dt);
            }
            catch (Exception ex)
            {
                comfunc.LogError(ex);
            }
            finally
            {
                if (conn.State == ConnectionState.Open)
                {
                    conn.Close();
                }
            }
            return dt;
        }

        /// <summary>
        /// Author : Anamika
        /// Date : Dec 6, 2016
        /// Desc : Fetch the data based on project type from database
        /// </summary>
        /// <param name="ddSalesEngineer"></param>
        /// <param name="ddBranch"></param>
        /// <param name="ddCustomerClass"></param>
        /// <returns></returns>
        public DataTable getProjectTypePotential(string ddSalesEngineer, string ddBranch, string ddCustomerClass)
        {
            DataTable dt = new DataTable();
            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
            try
            {
                conn.Open();
                SqlCommand command = new SqlCommand("getPotentialByProjectType", conn);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@sales_engineer_id", SqlDbType.VarChar, 50).Value = ddSalesEngineer;
                command.Parameters.Add("@branch_code", SqlDbType.VarChar, 50).Value = ddBranch;
                command.Parameters.Add("@customer_class", SqlDbType.VarChar, 50).Value = ddCustomerClass;
                SqlDataAdapter sqlda = new SqlDataAdapter(command);
                sqlda.Fill(dt);
            }
            catch (Exception ex)
            {
                comfunc.LogError(ex);
            }
            finally
            {
                if (conn.State == ConnectionState.Open)
                {
                    conn.Close();
                }
            }
            return dt;
        }
        
    }
}







