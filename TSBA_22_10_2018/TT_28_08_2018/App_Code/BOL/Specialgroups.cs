﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Configuration;
using System.Data.SqlClient;


namespace TaegutecSalesBudget
{
    public class Specialgroups
    {

       
        public DataTable getspclgroups(string family_id=null,string sub_family_id=null)
        {
            DataTable dtspclgroups = new DataTable();
            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
            try
            {
                connection.Open();
                SqlCommand command = new SqlCommand("specialgroups", connection);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@item_family_id", SqlDbType.VarChar, 50, null).Value = family_id;
                command.Parameters.Add("@item_sub_family_id", SqlDbType.VarChar, 50, null).Value = sub_family_id;
                

                SqlDataAdapter sqlDa = new SqlDataAdapter(command);
                sqlDa.Fill(dtspclgroups);

            }
            catch (Exception ex)
            {

            }
            finally
            {
                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }

            }

            return dtspclgroups;
        }
        public string update_specialgrps(int year,string item_code, string gold_flag = null, string top_flag = null, string five_years_flag = null, string bb_flag = null, string SPC_flag = null, string Ten_Years_flag=null)
        {
            string Message="Successful";
            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
           
            try
            {
               
                connection.Open();
                SqlCommand command = new SqlCommand("update_specialgroups", connection);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@item_code", SqlDbType.VarChar, 50).Value = item_code;
                command.Parameters.Add("@gold_flag", SqlDbType.VarChar, 10).Value = gold_flag;
                command.Parameters.Add("@top_flag", SqlDbType.VarChar, 10).Value = top_flag;
                command.Parameters.Add("@five_years_flag", SqlDbType.VarChar, 10).Value = five_years_flag;
                command.Parameters.Add("@bb_flag", SqlDbType.VarChar, 10).Value = bb_flag;
                command.Parameters.Add("@SPC_flag", SqlDbType.VarChar, 10).Value = SPC_flag;
                command.Parameters.Add("@ten_flag", SqlDbType.VarChar, 10).Value = Ten_Years_flag;
                command.Parameters.Add("@budget_year", SqlDbType.Int).Value = year;


                command.ExecuteNonQuery();

            }
            catch (Exception ex)
            {
                Message="Failed to update";
            }
            finally
            {
                connection.Close();
            }
            return Message;
        }
    }
}