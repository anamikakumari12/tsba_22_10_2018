﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace TaegutecSalesBudget
{
    
    public class Visit
    {
        public string branchcode, seid, customer_type, customer_number, cter, creationdate_from, creationdate_to, industryid, subindustryid;
        CommonFunctions comfunc = new CommonFunctions();
        public DataTable getVisitorReports(Visit obj) 
        {
            DataTable dtReports = new DataTable();
            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
            try
            {
               
                conn.Open();
                SqlCommand command = new SqlCommand("VisitorReports", conn);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@branchcode", SqlDbType.VarChar, 50).Value = obj.branchcode;
                command.Parameters.Add("@seid", SqlDbType.VarChar, 50).Value = obj.seid;
                command.Parameters.Add("@customer_type", SqlDbType.VarChar, 50).Value = obj.customer_type;
                command.Parameters.Add("@customer_number", SqlDbType.VarChar, 50).Value = obj.customer_number;
                command.Parameters.Add("@cter", SqlDbType.VarChar, 50).Value = obj.cter;
                command.Parameters.Add("@industryid", SqlDbType.VarChar, 50).Value = obj.industryid;
                command.Parameters.Add("@subindustryid", SqlDbType.VarChar, 50).Value = obj.subindustryid;                
                command.Parameters.Add("@creation_date_from", SqlDbType.VarChar, 50).Value = obj.creationdate_from;
                command.Parameters.Add("@creation_date_to", SqlDbType.VarChar, 50).Value = obj.creationdate_to;

                SqlDataAdapter sqlda = new SqlDataAdapter(command);
                sqlda.Fill(dtReports);
            }
            catch (Exception ex)
            {

            }
            finally
            {
                if (conn.State == ConnectionState.Open)
                {
                    conn.Close();
                }
            }
            return dtReports;
        }

        public DataTable getProjectDetails (string customerNumber){
            DataTable dtProjects = new DataTable();
            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
            try
            {
                conn.Open();
                SqlCommand command = new SqlCommand("getProjectDetails", conn);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@customerNumber", SqlDbType.NVarChar, 50).Value = customerNumber;
               
                SqlDataAdapter sqlda = new SqlDataAdapter(command);
                sqlda.Fill(dtProjects);
            }
            catch (Exception ex)
            {

            }
            finally
            {
                if (conn.State == ConnectionState.Open)
                {
                    conn.Close();
                }
            }
            return dtProjects;
        }

        public DataTable getAgenda()
        {
            DataTable dtAgenda = new DataTable();
            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
            try
            {
                conn.Open();
                SqlCommand command = new SqlCommand("Getagendas", conn);
                command.CommandType = CommandType.StoredProcedure;
                
                SqlDataAdapter sqlda = new SqlDataAdapter(command);
                sqlda.Fill(dtAgenda);
            }
            catch (Exception ex)
            {

            }
            finally
            {
                if (conn.State == ConnectionState.Open)
                {
                    conn.Close();
                }
            }
            return dtAgenda;
        }

        public DataTable getCustomersbyIndustry(Visit obj) 
        {
            DataTable dtReports = new DataTable();
            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
            try
            {

                conn.Open();
                SqlCommand command = new SqlCommand("getCustomerDetails_Industry", conn);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@industryid", SqlDbType.VarChar, 50).Value = obj.industryid;
                command.Parameters.Add("@branch", SqlDbType.VarChar, 50).Value = obj.branchcode;
                command.Parameters.Add("@seid", SqlDbType.VarChar, 50).Value = obj.seid;
                command.Parameters.Add("@customertype", SqlDbType.VarChar, 50).Value = obj.customer_type;
                 
                SqlDataAdapter sqlda = new SqlDataAdapter(command);
                sqlda.Fill(dtReports);
            }
            catch (Exception ex)
            {

            }
            finally
            {
                if (conn.State == ConnectionState.Open)
                {
                    conn.Close();
                }
            }
            return dtReports;
        }



        //internal object getVisitDetails(string customerId, string startdate, string enddate)
        //{
        //    DataTable dtReports = new DataTable();
        //    SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
        //    try
        //    {

        //        conn.Open();
        //        SqlCommand command = new SqlCommand("sp_getVisitDetails", conn);
        //        command.CommandType = CommandType.StoredProcedure;
        //        command.Parameters.Add("@CUSTOMERID", SqlDbType.VarChar, 50).Value = customerId;
        //        command.Parameters.Add("@STARTDATE", SqlDbType.VarChar, 50).Value = startdate;
        //        command.Parameters.Add("@ENDDATE", SqlDbType.VarChar, 50).Value = enddate;

        //        SqlDataAdapter sqlda = new SqlDataAdapter(command);
        //        sqlda.Fill(dtReports);
        //    }
        //    catch (Exception ex)
        //    {
        //        comfunc.LogError(ex);
        //    }
        //    finally
        //    {
        //        if (conn.State == ConnectionState.Open)
        //        {
        //            conn.Close();
        //        }
        //    }
        //    return dtReports;
        //}


        internal object getVisitDetails(string customerId, Visit obj, string engineerId)
        {
            DataTable dtReports = new DataTable();
            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
            try
            {

                conn.Open();
                SqlCommand command = new SqlCommand("sp_getVisitDetails", conn);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@CUSTOMERID", SqlDbType.VarChar, 50).Value = customerId;
                command.Parameters.Add("@branchcode", SqlDbType.VarChar, 50).Value = obj.branchcode;
                command.Parameters.Add("@seid", SqlDbType.VarChar, 50).Value = obj.seid;
                command.Parameters.Add("@customer_type", SqlDbType.VarChar, 50).Value = obj.customer_type;
                command.Parameters.Add("@cter", SqlDbType.VarChar, 50).Value = obj.cter;
                command.Parameters.Add("@industryid", SqlDbType.VarChar, 50).Value = obj.industryid;
                command.Parameters.Add("@subindustryid", SqlDbType.VarChar, 50).Value = obj.subindustryid;
                command.Parameters.Add("@creation_date_from", SqlDbType.VarChar, 50).Value = obj.creationdate_from;
                command.Parameters.Add("@creation_date_to", SqlDbType.VarChar, 50).Value = obj.creationdate_to;
                command.Parameters.Add("@VengineerId", SqlDbType.VarChar, 50).Value = engineerId;

                SqlDataAdapter sqlda = new SqlDataAdapter(command);
                sqlda.Fill(dtReports);
            }
            catch (Exception ex)
            {

            }
            finally
            {
                if (conn.State == ConnectionState.Open)
                {
                    conn.Close();
                }
            }
            return dtReports;
        }
    }
}