﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Configuration;
using System.Data.SqlClient;

namespace TaegutecSalesBudget
{
    public class projecttypecs
    {
        public int Add_projecttype(string projecttype)
        {
            int output = 0;
            DataTable dt = new DataTable();
            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
            try
            {
                connection.Open();
                SqlCommand command = new SqlCommand("insertprojecttype", connection);
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add("@projecttype", SqlDbType.VarChar, 200).Value = projecttype;
                SqlDataAdapter sqlda = new SqlDataAdapter(command);

                sqlda.Fill(dt);
                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        output = Convert.ToInt32(dt.Rows[0][0]);
                    }
                }

            }
            catch (Exception e)
            {

            }
            finally
            {
                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }
            }
            return output;
               

            //    command.ExecuteNonQuery();

            //}
            //catch (Exception ex)
            //{

            //}
            //finally
            //{
            //    connection.Close();
            //}

        }

        public DataTable getdataprojecttype()
        {
            DataTable dtallprojecttypes = new DataTable();
            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
            try
            {
                connection.Open();
                SqlCommand command = new SqlCommand("getprojecttypelist", connection);
                command.CommandType = CommandType.StoredProcedure;              
                SqlDataAdapter sqlDa = new SqlDataAdapter(command);
                sqlDa.Fill(dtallprojecttypes);

            }
            catch (Exception ex)
            {

            }
            finally
            {
                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }

            }

            return dtallprojecttypes;
        }



        public void Deleteprojecttype(string projecttype)
        {
            DataTable dt = new DataTable();
            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
            try
            {
                conn.Open();
                SqlCommand cmnd = new SqlCommand("DeleteProjectType", conn);
                cmnd.CommandType = CommandType.StoredProcedure;
                cmnd.Parameters.Add("@Projecttype", SqlDbType.VarChar, 50).Value = projecttype;
                cmnd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {

            }
            finally
            {
                if (conn.State == ConnectionState.Open)
                {
                    conn.Close();
                }
            }
        }


    }
}
