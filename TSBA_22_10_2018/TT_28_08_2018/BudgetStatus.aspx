﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="BudgetStatus.aspx.cs" Inherits="TaegutecSalesBudget.BudgetStatus" MasterPageFile="~/Site.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <script src="js/jquery.dataTables.min.js"></script>
    <link href="css/jquery.dataTables.min.css" rel="stylesheet" />
    <link href="css/buttons.dataTables.min.css" rel="stylesheet" />

    <script type="text/javascript" class="init">
        $(document).ready(function () {
            $('#collapsebtn').unbind('click').bind('click', function (e) {
                debugger;
                var attr = $('#product_image').attr('src');
                $("#dfltreportform").slideToggle();
                if (attr == "images/up_arrow.png") {
                    $("#product_image").attr("src", "images/down_arrow.png");
                } else {
                    $("#product_image").attr("src", "images/up_arrow.png");
                }
            });
        });

        function triggerPostGridLodedActions() {
            bindGridView();
            $('#collapsebtn').unbind('click').bind('click', function (e) {
                debugger;
                var attr = $('#product_image').attr('src');
                $("#dfltreportform").slideToggle();
                if (attr == "images/up_arrow.png") {
                    $("#product_image").attr("src", "images/down_arrow.png");
                } else {
                    $("#product_image").attr("src", "images/up_arrow.png");
                }
            });
        }

        function bindGridView() {
            debugger;
            var head_content = $('#MainContent_budgetstatusvalues tr:first').html();
            $('#MainContent_budgetstatusvalues').prepend('<thead></thead>')
            $('#MainContent_budgetstatusvalues thead').html('<tr>' + head_content + '</tr>');
            $('#MainContent_budgetstatusvalues tbody tr:first').hide();
            $('#MainContent_budgetstatusvalues').DataTable(
               {
                   "info": false,
               });
        }

    </script>
    <style>
        .control_dropdown {
            width: 180px;
            height: 30px;
            border-radius: 4px!important;
            position: relative;
        }

        .label {
            padding-top: 9px;
            width: 100%;
            color: black;
        }

        .control {
            padding-top: 2px;
        }

        .btn.green {
            /*color: #fff;*/
            margin-left: auto;
            margin-right: auto;
            overflow: auto;
            cursor: pointer;
            top: -9px;
            position: relative;
            width: 100px;
            font-weight: 600;
            margin-top: -1px;
        }

        .mn_popup {
            width: 60%;
            /*align-content: center;*/
            margin: 15%;
        }

        body {
            font-family: 90%/1.45em "Helvetica Neue", HelveticaNeue, Helvetica, Arial, sans-serif;
        }

        tfoot {
            border: solid 1px #ebeef5;
            padding: 3px 10px;
        }

            tfoot tr th {
                text-align: right;
                padding: 3px!important;
            }

        tbody {
            border-color: #fff;
        }

        .mn_margin {
            margin: 10px 0 0 0;
        }

        .result {
            margin-left: 45%;
            font-weight: bold;
        }

        #pnlData {
            width: 70%;
        }

        thead {
            border: solid 1px #ebeef5;
        }

        .crumbs .breadcrumb li {
            float: left;
            font-size: 12px;
            padding-top: 2px;
            padding-right: 50px;
        }
    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:ScriptManager ID="ScriptManager2" runat="server" AsyncPostBackTimeout="360" EnableHistory="true" EnablePartialRendering="true" EnablePageMethods="true"></asp:ScriptManager>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div class="crumbs">
                <!-- Start : Breadcrumbs -->
                <ul id="Ul2" class="breadcrumb">
                    <li>
                        <i class="fa fa-home"></i>
                        <a>Budget</a>
                    </li>

                    <li>Budget Status</li>
                    <div>
                        <ul>
                            <li class="title_bedcrum" style="list-style: none;">BUDGET STATUS</li>
                        </ul>
                    </div>
                </ul>
            </div>
            <div id="collapsebtn" class="row">
                <img id="product_image" src="images/up_arrow.png" style="margin-left: 46%;" />
            </div>

            <div id="dfltreportform" class="row filter_panel">
                <div class="col-md-12" runat="server" id="cterDiv">
                    <ul id="divCter" runat="server" class="btn-info rbtn_panel">
                        <li><span style="margin-right: 4px; vertical-align: text-bottom;">TAEGUTEC</span>
                            <asp:RadioButton ID="rdBtnTaegutec" AutoPostBack="true" Checked="true" OnCheckedChanged="rdBtnTaegutec_CheckedChanged" GroupName="byCmpnyCodeInradiobtn" runat="server" />
                            <span style="margin-right: 4px; margin-left: 4px; vertical-align: text-bottom;">DURACARB</span>
                            <asp:RadioButton ID="rdBtnDuraCab" AutoPostBack="true" OnCheckedChanged="rdBtnTaegutec_CheckedChanged" GroupName="byCmpnyCodeInradiobtn" runat="server" />
                        </li>
                    </ul>
                </div> 
                
                <div class="col-md-2 control" runat="server" id="divBranch">
                    <label class="label">BRANCH</label>
                    <asp:DropDownList ID="ddlBranchList" runat="server" OnSelectedIndexChanged="ddlBranchList_SelectedIndexChanged" CssClass="control_dropdown" AutoPostBack="true"></asp:DropDownList>
                </div>

             
                <div class="col-md-2 control" runat="server" id="divyear">
                    <label class="label">CUSTOMER NUMBER</label>
                    <asp:DropDownList ID="ddlCustomerNumber" class="control_dropdown" runat="server" OnSelectedIndexChanged="ddlCustomerNumber_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                </div>

                <div class="col-md-2 control" runat="server" >
                    <label class="label">CUSTOMER NAME</label>
                    <asp:DropDownList ID="ddlCustomerName" runat="server" CssClass="control_dropdown" OnSelectedIndexChanged="ddlCustomerName_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                </div>

                   <div class="col-md-2 control" runat="server" id="divSE">
                    <label class="label">SE NAME</label>
                    <asp:DropDownList ID="ddlSalesEngineerName" runat="server" OnSelectedIndexChanged="SalesEngList_SelectedIndexChanged"  CssClass="control_dropdown" AutoPostBack="true" ViewStateMode="Enabled"></asp:DropDownList>
                </div>

                <div class="col-md-4">
                    <div class="form-group">
                        <asp:Label ID="lblError" runat="server"></asp:Label>
                        <br />
                        <asp:Button ID="reports" runat="server" Text="FILTER" OnClick="status_click" CssClass="btn green" />
                    </div>
                </div>

            </div>
            </br>
            <asp:Label runat="server" ID="lblResult"></asp:Label>
             <asp:GridView ID="budgetstatusvalues" Width="98%" Style="text-align: center;" runat="server" AutoGenerateColumns="False" ShowHeaderWhenEmpty="True">
                <Columns>
                      <asp:TemplateField HeaderText="BRANCH" HeaderStyle-CssClass="HeadergridAll" Visible="true" ItemStyle-Width="75px" ItemStyle-CssClass="align-left">
                        <ItemTemplate>
                            <asp:Label Style="text-align: left;" ID="Labelbranch" runat="server" Text='<%# Eval("region_description") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="CUSTOMER NUMBER" HeaderStyle-CssClass="HeadergridAll" Visible="true" ItemStyle-Width="125px" ItemStyle-CssClass="align-center">
                        <ItemTemplate>
                            <asp:Label Style="text-align: left;" ID="Labelcustno" runat="server" Text='<%# Eval("customer_number") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="CUSTOMER NAME" HeaderStyle-CssClass="HeadergridAll" Visible="true" ItemStyle-Width="135px" ItemStyle-CssClass="align-left">
                        <ItemTemplate>
                            <asp:Label Style="text-align: left;" ID="Labelcustname" runat="server" Text='<%# Eval("customer_name") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>                  
                  
                    <asp:TemplateField HeaderText="SE NAME" HeaderStyle-CssClass="HeadergridAll" Visible="true" ItemStyle-Width="125px" ItemStyle-CssClass="align-left">
                        <ItemTemplate>
                            <asp:Label Style="text-align: right;" ID="LabelSEName" runat="server" Text='<%# Eval("EngineerName") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                  
                    <asp:TemplateField HeaderText="STATUS AT SE" HeaderStyle-CssClass="HeadergridAll" Visible="true" ItemStyle-Width="100px" ItemStyle-CssClass="align-left">
                        <ItemTemplate>
                            <asp:Label Style="text-align: right;" ID="LabelstatusatSE" runat="server" Text='<%# Eval("status_sales_engineer") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>

                         <asp:TemplateField HeaderText="STATUS AT BM" HeaderStyle-CssClass="HeadergridAll" Visible="true" ItemStyle-Width="100px" ItemStyle-CssClass="align-left">
                        <ItemTemplate>
                            <asp:Label Style="text-align: right;" ID="LabelstatusatBM" runat="server" Text='<%# Eval("status_branch_manager") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>

                     <asp:TemplateField HeaderText="STATUS AT HO" HeaderStyle-CssClass="HeadergridAll" Visible="true" ItemStyle-Width="100px" ItemStyle-CssClass="align-left">
                        <ItemTemplate>
                            <asp:Label Style="text-align: right;" ID="LabelstatusatHO" runat="server" Text='<%# Eval("status_ho") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
            </div>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="reports" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="rdBtnTaegutec" EventName="CheckedChanged" />
            <asp:AsyncPostBackTrigger ControlID="rdBtnDuraCab" EventName="CheckedChanged" />
        </Triggers>
    </asp:UpdatePanel>
    <asp:UpdateProgress ID="updateProgress" runat="server">
         <ProgressTemplate>
            <div style="position: fixed; text-align: left; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.7;">
                <span style="border-width: 0px; position: fixed; padding: 50px; font-size: 30px; left: 40%; top: 40%; color: #fff">Please wait</span>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>

</asp:Content>

