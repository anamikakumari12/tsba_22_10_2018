﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Dashboard.aspx.cs" Inherits="TaegutecSalesBudget.Dashboard" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
     <link rel="stylesheet" href="http://cdn.kendostatic.com/2014.3.1316/styles/kendo.common.min.css" />
    <link rel="stylesheet" type="text/css" href="css/kendo.default.min.css" />
    <link rel="stylesheet" href="http://cdn.kendostatic.com/2014.3.1316/styles/kendo.dataviz.min.css" />
    <link rel="stylesheet" href="http://cdn.kendostatic.com/2014.3.1316/styles/kendo.dataviz.default.min.css" />

    
  
     <link href="GridviewScroll.css" rel="stylesheet" />
       
    <style type="text/css">
         .gauge-container {
                 text-align: center;
				 /*margin: 0px auto;*/ margin: 2px 5px 0px 4px;
                }

                .gauge {
                    width: 330px;
                    height: 330px;
                    margin: 0 auto 0;
                }
        td {
            height: 30px !important;
            padding-left:10px;
            text-align:left;
        }        
        .noclose .ui-dialog-titlebar-close {
            display: none;
        }
        .row {
            margin-left: 10px;
            margin-right: -15px;
        }
        .noTitleStuff .ui-dialog-titlebar {display:none}

        .panelBack {
            background-color:#fff !Important; 
        }
        #dropid label,#Div1 label,#Div2 label,#Div3 label,#Div4 label  {
            top: -2px;
            position: relative;
            margin-left: 5px;
        }
        #chart_cust{
            height:275px;
        }
        #se_charts{
           display:none;
           border: 1px solid #e1e1e1;
           padding: 10px;
           background: #fff;
           margin-bottom:20px;
        }

        .panel {
            margin-bottom:0px !important;
        }
        #MainContent_bm_chart {
            display:none;
            border: 1px solid #e1e1e1;
           padding: 10px;
           background: #fff;
           /*margin-bottom:20px;*/
        }
         #MainContent_ho_chart {
            display:none;
           border: 1px solid #e1e1e1;
           padding: 10px;
           background: #fff;
           margin-bottom:20px;
        }
       #MainContent_divcnsldt {
           display:none;
           
          
        }

       #bargraphdiv,#bmbargraphdiv {
           
           border: 1px solid #e1e1e1;
           padding: 10px;
           background: #fff;
           margin-bottom:20px;
        }
        
        .panel-group .panel {
        border-radius: 4px;
        margin-bottom: 0;
        overflow: hidden;
        margin-top: 20px;
        }

        .hyprlnk,.hpprlnk:hover {
            color: #0066FF;
            text-decoration: underline;
            float: right;
            font-weight: 600;
            font-size: 13px;
            margin-top: -21px;
            margin-right: 15px;
            cursor:pointer;
        }
        .gauge-label {
             color: #232BBD;
            
            float: left;
            font-weight: 600;
            font-size: 13px;
            margin-top: -21px;
            margin-left: 8px;
           
        }
        .bdgt-label {
            color: #232BBD;
            float: none;
            font-weight: 600;
            font-size: 13px;
        }
        .lnk_btn,.lnk_btn:hover {
             color: #232BBD;
            float: none;
            font-weight: 600;
            font-size: 13px;
            cursor:pointer;
        }
        .stsval {
            color: #0066FF;
            float: left;
            font-weight: 600;
            font-size: 13px;
            margin-top: -21px;
            margin-left: 80px;
        }

        #MainContent_grdviewAllValues td:last-child {
            text-align : right !important;
        }

        #MainContent_grdviewAllValues td {
            text-align : left;
	    color: #000;

        }

      .panel-heading1 { background:#333; color:#29AAe1 !important; padding: 15px; color:#fff; font-size: 16px; font-weight: bolder; text-transform:uppercase;}
      .panel-heading2 { background:#999; color:#333 !important; padding: 15px;  color:#fff; font-size: 16px; font-weight: bolder; text-transform:uppercase;}
      .panel-heading3 { background:#333; color:#8ac340 !important;  padding:  15px;  color:#fff; font-size: 16px; font-weight: bolder; text-transform:uppercase;}
      .panel-heading4 { background:#999; color:#91298E !important; padding:  15px;  color:#fff; font-size: 16px; font-weight: bolder; text-transform:uppercase;}
      .panel-heading5 { background:#333; color:#F05A26   !important; padding:  15px;  color:#fff; font-size: 16px; font-weight: bolder; text-transform:uppercase;}
      .panel-heading6 { background:#999; color:#002238  !important; padding:  15px;  color:#fff; font-size: 16px; font-weight: bolder; text-transform:uppercase;}
      .panel-heading7 { background:#333; color:#fff  !important; padding:  15px;  color:#fff; font-size: 16px; font-weight: bolder; text-transform:uppercase;}
   </style>
    <script type="text/javascript">

        function visibleLink() {
            document.getElementById("lnkbtnBulkapproval").style.visibility = "visible";
        }
        function disableLink() {
            document.getElementById("lnkbtnBulkapproval").style.visibility = "hidden";
        }
        function collapse(container, img_id) {

            container = "#" + container;
            img_id = "#" + img_id;
            var attr = $(img_id).attr('src');
            $(container).slideToggle();
            if (attr == "images/button_minus.gif") {
                $(img_id).attr("src", "images/button_plus.gif");
                //gridviewScrollTrigger();
            } else {
                // gridviewScrollTrigger();
                $(img_id).attr("src", "images/button_minus.gif");
            }
            setTimeout(function () {
                jQuery(".ui-dialog.ui-widget.ui-widget-content").each(function () {
                    var ht = 0;
                    if (jQuery(this).css("display") == "block") {
                        $('#MainContent_panelHOStatus').dialog('close');
                        $('#MainContent_panelBMStatus').dialog('close');
                        $('#MainContent_panelCustomerType').dialog('close');
                        $('#MainContent_panelRegion').dialog('close');
                        $('#MainContent_panelSEStatus').dialog('close');

                    }
                });
            }, 10);



        }

        function modalht() {

        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<%-- <asp:ScriptManager ID="ScriptManager1" runat="server" AsyncPostBackTimeout="360" EnableHistory="true" EnablePartialRendering="true" EnablePageMethods="true"></asp:ScriptManager>
     
 <asp:UpdatePanel ID="UpdatePanel1" runat="server">
      <ContentTemplate>--%>

    <div class="crumbs">
            <!-- Start : Breadcrumbs -->
            <ul id="breadcrumbs" class="breadcrumb">
               <li>
                  <i class="fa fa-home"></i>
                  <a>Budget</a>
               </li>
               <li>Dashboard</li>
                 <div>
                <ul>
               <li class="title_bedcrum"  style="list-style:none;">DASHBOARD</li>
                </ul>
                     </div>

                


               <div>
                  <ul   style="float:right;list-style:none;   margin-top: -4px; width: 247px; margin-right: -5px; "  class="alert alert-danger fade in">
                     <li>
                         <span style="margin-right:4px;vertical-align:text-bottom;  ">Val In '000</span>
                          <asp:RadioButton id="rbtn_Thousand"  OnCheckedChanged="rbtn_Thousand_CheckedChanged"  Autopostback="true" GroupName="customer" runat="server" Checked="True" />
                         <span style="margin-right:4px; margin-left:4px;vertical-align:text-bottom;">Val In Lakhs</span>
                         <asp:RadioButton id="rbtn_Lakhs" OnCheckedChanged="rbtn_Lakhs_CheckedChanged" Autopostback="true" GroupName="customer" runat="server"/>
              </li>
                  </ul>
               </div>
            </ul>
         </div>  <!-- End : Breadcrumbs -->
    <div class="row" style=" margin-top: 5px;">
        
                  <ul runat="server" id="cterDiv" visible="true"  class="btn-info rbtn_panel"  style="margin-left:5px">
                      <li> <span style="margin-right:4px;vertical-align:text-bottom;  ">TAEGUTEC</span>
                       
                        <asp:RadioButton ID="rdBtnTaegutec"  AutoPostBack="true"  Checked="true"  OnCheckedChanged="rdBtnTaegutec_CheckedChanged"  GroupName="byCmpnyCodeInradiobtn" runat="server" />
                         <span style="margin-right:4px; margin-left:4px;vertical-align:text-bottom;">DURACARB</span>
                        <asp:RadioButton ID="rdBtnDuraCab"  AutoPostBack="true" OnCheckedChanged="rdBtnTaegutec_CheckedChanged"   GroupName="byCmpnyCodeInradiobtn" runat="server" />
                     </li>
                  </ul>
          
<%--        <div style="float:right;padding-right: 10px; padding-left: 10px; width: 100%; margin-top:4px;height:20px;">--%>
        <asp:LinkButton runat="server" ID="lnkbtnBulkapproval" Text="BRANCH WISE APPROVAL" ForeColor="#0066FF"
                          Font-Underline="True" Style="float:right;font-weight: 600;font-size: 13px;padding-right: 10px; padding-left: 10px;  height:20px;margin-top:-22px" Visible="true"></asp:LinkButton>
    </div>
<%--    </div>--%>
   <%-- Gauges---start--%>
    <div class="row" style="margin-top:4px;">   <!-- Inner Page Row 2 Starts Here -->
                    
                    <div class="col-md-4 col-sm-6">
                        <div class="portlet box blue">
                            <div class="portlet-title">
                                <div class="caption">SALES ENGINEER</div>                                 
                            </div>
                            <div class="portlet-body">
                               
                                <div id="example" class="k-content">
            <div id="gauge-container2" class="gauge-container" style="height:220px" >
                <div id="gauge" class="gauge" style="width: 300px; height: 200px;"></div>
            </div>
                                    <asp:Label ID="lblsestatus" CssClass="gauge-label" runat="server" ></asp:Label>
                 <asp:HyperLink ID="HyperLink_se" CssClass="hyprlnk" style="color: #0066FF;"  runat="server">View More..</asp:HyperLink>                    
        </div>
                            </div>
                        </div>
                    </div>
                    
                    <div class="col-md-4 col-sm-6">
                        <div class="portlet box yellow">
                            <div class="portlet-title">
                                <div class="caption">BRANCH MANAGER</div>                                 
                            </div>
                            <div class="portlet-body col-md-12">
                                <div id="Div5" class="k-content">

            <div id="gauge-container" class="gauge-container" style="height:220px">
                <div id="gauge1" class="gauge" style="width: 300px; height: 200px;"></div>
            </div>
                                     <asp:Label ID="lblbmstatus" CssClass="gauge-label" runat="server" ></asp:Label>
            <asp:HyperLink ID="HyperLink_bm" CssClass="hyprlnk" style="color: #0066FF;" runat="server">View More..</asp:HyperLink>
        </div>
                            </div>
                        </div>
                    </div>
                    
                    <div class="col-md-4 col-sm-6">
                        <div class="portlet box blue">
                            <div class="portlet-title">
                                <div class="caption" ><span style="text-align:center">HO</span></div>                               
                            </div>
                            <div class="portlet-body col-md-12">
                                <div id="Div6" class="k-content">

            <div id="gauge-container1" class="gauge-container" style="height:220px">
                <div id="gauge2" class="gauge" style="width: 300px; height: 200px;"></div>
            </div>
                                      <asp:Label ID="lblhostatus" CssClass="gauge-label" runat="server" ></asp:Label>
             <asp:HyperLink ID="HyperLink_ho" style="color: #0066FF;" CssClass="hyprlnk" runat="server">View More..</asp:HyperLink>
        </div>
                            </div>
                          
                        </div>
                         
                    </div>
                    
                </div>
   <%-- Gauges---End--%>
 
   <%-- PIECHARTS-----STARTS--%>
   
    <div class="row panel panel-default ">
        <div id="se_collapsebtn" class=" panel-heading1 " onclick="collapse('se_charts','se_charts_image')">
        <h4 class="panel-title">
            <img id="se_charts_image" src="images/button_plus.gif" align="left">  &nbsp;&nbsp;&nbsp;&nbsp; SALES ENGINEER STATUS-COUNT
        </h4>
        </div>
        </div>
        
   <div class="row" id="se_charts" >
        
       <div class="col-md-4 col-sm-6">
      <div class="portlet box blue">
         <div class="portlet-title">
            <div class="caption">CUSTOMERS</div>
         </div>
         <div class="portlet-body">
            <div id="Div7" class="k-content">
               <div id="Div8" class="gauge-container" style="width:200px;" >
                  <asp:Chart ID="chart_se_cust" EnableViewState="true" runat="server">
                     <Series>
                        <asp:Series Name="Series1" IsValueShownAsLabel="true" ChartType="Pie"></asp:Series>
                     </Series>
                     <ChartAreas>
                        <asp:ChartArea Name="ChartArea1"></asp:ChartArea>
                     </ChartAreas>
                     <Legends>
                        <asp:Legend></asp:Legend>
                     </Legends>    
                  </asp:Chart>
                     <asp:Label  ID="lbl_sec_bdgt_val" CssClass="hyprlnk" style="float:none;text-decoration:none;cursor:auto;" runat="server"  ></asp:Label>
                  
               </div>
                
            </div>
            
         </div>
      </div>
   </div>
      <div class="col-md-4 col-sm-6">
      <div class="portlet box blue">
         <div class="portlet-title">
            <div class="caption">CHANNEL PARTNERS </div>
         </div>
         <div class="portlet-body">
            <div id="Div9" class="k-content">
               <div id="Div10" class="gauge-container" style="width:200px;" >
                  <asp:Chart ID="chart_se_dstrbtr" EnableViewState="true" runat="server">
                     <Series>
                        <asp:Series Name="Series1" IsValueShownAsLabel="true"  ChartType="Pie"></asp:Series>
                     </Series>
                     <ChartAreas>
                        <asp:ChartArea Name="ChartArea1"></asp:ChartArea>
                     </ChartAreas>
                     <Legends>
                        <asp:Legend></asp:Legend>
                     </Legends>
                  </asp:Chart>
                  <asp:Label ID="lbl_sed_bdgt_val" CssClass="hyprlnk" style="float:none;text-decoration:none;cursor:auto;" runat="server" ></asp:Label>   

               </div>
            </div>
         </div>
      </div>
   </div>
           <div class="col-md-4 col-sm-6">
      <div class="portlet box blue">
         <div class="portlet-title">
            <div class="caption">TOTALS</div>
         </div>
         <div class="portlet-body">
            <div id="Div11" class="k-content">
               <div id="Div12" class="gauge-container" style="width:200px;"  >
                  <asp:Chart ID="chart_se_total" EnableViewState="true" runat="server">
                     <Series>
                        <asp:Series Name="Series1" IsValueShownAsLabel="true" ChartType="Pie"></asp:Series>
                     </Series>
                     <ChartAreas>
                        <asp:ChartArea Name="ChartArea1"></asp:ChartArea>
                     </ChartAreas>
                     <Legends>
                        <asp:Legend></asp:Legend>
                     </Legends>
                  </asp:Chart>
                  <asp:Label ID="lbl_set_bdgt_val" CssClass="hyprlnk" style="float:none;text-decoration:none;cursor:auto;" runat="server" ></asp:Label>   
               </div>
            </div>
         </div>
      </div>
   </div>
            </div>
<%--</div>
</div>--%>
    <%--BRANCH MANGER PIE CHART--%>
      <div style="margin-top:20px;" class="row panel panel-default ">
        <div id="bm_collapsebtn" class=" panel-heading2 " onclick="collapse('MainContent_bm_chart','MainContent_bm_charts_image')">
        <h4 class="panel-title">
            <img id="bm_charts_image" runat="server" src="images/button_plus.gif" align="left">  &nbsp;&nbsp;&nbsp;&nbsp; BRANCH MANAGER STATUS-COUNT
        </h4>
        </div>
        </div>
          <div class="row" id="bm_chart" runat="server">
   <div class="col-md-4 col-sm-6">
      <div class="portlet box blue">
         <div class="portlet-title">
            <div class="caption">CUSTOMERS</div>
         </div>
         <div class="portlet-body">
            <div id="Div14" class="k-content">
               <div id="Div15" class="gauge-container" style="width:200px;">
                  <asp:Chart ID="chart_bm_cust" EnableViewState="true" runat="server">
                     <Series>
                        <asp:Series Name="Series1" IsValueShownAsLabel="true" ChartType="Pie"></asp:Series>
                     </Series>
                     <ChartAreas>
                        <asp:ChartArea Name="ChartArea1"></asp:ChartArea>
                     </ChartAreas>
                     <Legends>
                        <asp:Legend></asp:Legend>
                     </Legends>
                  </asp:Chart>
                  <asp:LinkButton ID="lbl_bmc_bdgt_val"  CssClass="hyprlnk" style="color: #0066FF;float:none;margin-right:0px;"  runat="server" OnClick="bm_bdgt_val_Click" ></asp:LinkButton>   

               </div>
            </div>
         </div>
      </div>
   </div>
      <div class="col-md-4 col-sm-6">
      <div class="portlet box blue">
         <div class="portlet-title">
            <div class="caption">CHANNEL PARTNERS </div>
         </div>
         <div class="portlet-body">
            <div id="Div16" class="k-content">
               <div id="Div17" class="gauge-container" style="width:200px;" >
                  <asp:Chart ID="chart_bm_dstrbtr" EnableViewState="true" runat="server">
                     <Series>
                        <asp:Series Name="Series1" IsValueShownAsLabel="true"  ChartType="Pie"></asp:Series>
                     </Series>
                     <ChartAreas>
                        <asp:ChartArea Name="ChartArea1"></asp:ChartArea>
                     </ChartAreas>
                     <Legends>
                        <asp:Legend></asp:Legend>
                     </Legends>
                  </asp:Chart>
                  <asp:LinkButton ID="lbl_bmd_bdgt_val" CssClass="hyprlnk" style="color: #0066FF;float:none;margin-right:0px;" runat="server"  OnClick="bm_bdgt_val_Click" ></asp:LinkButton>   

               </div>
            </div>
         </div>
      </div>
   </div>
           <div class="col-md-4 col-sm-6">
      <div class="portlet box blue">
         <div class="portlet-title">
            <div class="caption">TOTALS</div>
         </div>
         <div class="portlet-body">
            <div id="Div18" class="k-content">
               <div id="Div19" class="gauge-container" style="width:200px;">
                  <asp:Chart ID="chart_bm_total" EnableViewState="true" runat="server">
                     <Series>
                        <asp:Series Name="Series1" IsValueShownAsLabel="true" ChartType="Pie"></asp:Series>
                     </Series>
                     <ChartAreas>
                        <asp:ChartArea Name="ChartArea1"></asp:ChartArea>
                     </ChartAreas>
                     <Legends>
                        <asp:Legend></asp:Legend>
                     </Legends>
                  </asp:Chart>
                  <asp:LinkButton ID="lbl_bmt_bdgt_val" CssClass="hyprlnk" style="color: #0066FF;float:none;margin-right:0px;" runat="server"  OnClick="bm_bdgt_val_Click" ></asp:LinkButton>   

               </div>
            </div>
         </div>
      </div>
   </div>
</div>

    <%--BRANCH MANAGER PIE CHART END --%>
   <%-- BRANCH MANGER BAR GRAPH START--%>
     <%-- //BAR GRAPHS START--%>
     <div id="divbmbargraph" runat="server" >
        <div style="margin-top:20px;" class="row panel panel-default ">
        <div id="Div32" class=" panel-heading2 "  onclick="collapse('bmbargraphdiv','MainContent_bmbar_graphs_image')">
        <h4 class="panel-title">
            <img id="bmbar_graphs_image" runat="server" src="images/button_plus.gif" align="left">  &nbsp;&nbsp;&nbsp;&nbsp;BRANCH MANAGER STATUS-VALUE
        </h4>
        </div>
        </div>
   <div class="row" id="bmbargraphdiv">
       <div class="col-md-4 col-sm-6">
      <div class="portlet box blue">
         <div class="portlet-title">
            <div class="caption">CUSTOMERS</div>
         </div>
         <div class="portlet-body">
            <div id="Div33" class="k-content" >
               <div id="Div34" >
                  <asp:Chart ID="cbmbdgt_bargraph" EnableViewState="true" runat="server">

                     <Series>
                        <asp:Series Name="Series1" IsVisibleInLegend="true" IsValueShownAsLabel="true" ChartType="Column"></asp:Series>
                     </Series>
                     <ChartAreas>
                        <asp:ChartArea Name="ChartArea1" BackColor="SkyBlue"><AxisX LabelAutoFitStyle="LabelsAngleStep45,LabelsAngleStep90,WordWrap">
              <MajorGrid LineWidth="0"  /><LabelStyle Font="Verdana, 8.25pt" />
         </AxisX><AxisY>
              <MajorGrid LineWidth="0" />
         </AxisY></asp:ChartArea>
                     </ChartAreas>
                    <%-- <Legends>
                        <asp:Legend></asp:Legend>
                     </Legends>--%>
                  </asp:Chart>
                 
               </div>
            </div>
         </div>
      </div>
   </div>

        <%-- //DISTRIBUTORS_START--%>
         <div class="col-md-4 col-sm-6">
      <div class="portlet box blue">
         <div class="portlet-title">
            <div class="caption">CHANNEL PARTNERS </div>
         </div>
         <div class="portlet-body">
            <div id="Div35" class="k-content" >
               <div id="Div36">
                  <asp:Chart ID="dbmbdgt_bargraph" EnableViewState="true" runat="server">

                     <Series>
                        <asp:Series Name="Series1" IsVisibleInLegend="true" IsValueShownAsLabel="true" ChartType="Column"></asp:Series>
                     </Series>
                     <ChartAreas>
                        <asp:ChartArea Name="ChartArea1" BackColor="SkyBlue"><AxisX LabelAutoFitStyle="LabelsAngleStep45,LabelsAngleStep90,WordWrap">
              <MajorGrid LineWidth="0" /><LabelStyle Font="Verdana, 8.25pt" />
         </AxisX><AxisY>
              <MajorGrid LineWidth="0" />
         </AxisY></asp:ChartArea>
                     </ChartAreas>
                   <%--  <Legends>
                        <asp:Legend></asp:Legend>
                     </Legends>--%>
                  </asp:Chart>
                 
               </div>
            </div>
         </div>
      </div>
   </div>
        <%-- Distributors-end--%>
             <div class="col-md-4 col-sm-6">
      <div class="portlet box blue">
         <div class="portlet-title">
            <div class="caption">TOTALS</div>
         </div>
         <div class="portlet-body">
            <div id="Div37" class="k-content" >
               <div id="Div38">
                  <asp:Chart ID="tbmbdgt_bargraph" EnableViewState="true" runat="server">
                     <Series>
                        <asp:Series Name="Series1" IsVisibleInLegend="true" IsValueShownAsLabel="true" ChartType="Column"></asp:Series>
                     </Series>
                     <ChartAreas>
                        <asp:ChartArea Name="ChartArea1" BackColor="SkyBlue"><AxisX LabelAutoFitStyle="LabelsAngleStep45,LabelsAngleStep90,WordWrap">
              <MajorGrid LineWidth="0" /><LabelStyle Font="Verdana, 8.25pt" />
         </AxisX><AxisY>
              <MajorGrid LineWidth="0" />
         </AxisY></asp:ChartArea>
                     </ChartAreas>
                     <%--<Legends>
                        <asp:Legend></asp:Legend>
                     </Legends>--%>
                  </asp:Chart>
                 
               </div>
            </div>
         </div>
      </div>
   </div>
        </div>
         </div>
   <%-- BAR GRAPHS END--%>
   <%-- BRANCH MANAGER BAR GRAPH END--%>

   <%-- HO PIE CHART---START--%>
      <div style="margin-top:20px;" class="row panel panel-default ">
        <div id="ho_collapsebtn" class=" panel-heading3 " onclick="collapse('MainContent_ho_chart','MainContent_ho_charts_image')">
        <h4 class="panel-title">
            <img id="ho_charts_image" runat="server" src="images/button_plus.gif" align="left">  &nbsp;&nbsp;&nbsp;&nbsp;HO STATUS-COUNT
        </h4>
        </div>
        </div>
        <div class="row" id="ho_chart" runat="server">
   <div class="col-md-4 col-sm-6">
      <div class="portlet box blue">
         <div class="portlet-title">
            <div class="caption">CUSTOMERS</div>
         </div>
         <div class="portlet-body">
            <div id="Div20" class="k-content">
               <div id="Div21" class="gauge-container" style="width:200px;">
                  <asp:Chart ID="chart_ho_cust" EnableViewState="true" runat="server">
                     <Series>
                        <asp:Series Name="Series1" IsValueShownAsLabel="true" ChartType="Pie"></asp:Series>
                     </Series>
                     <ChartAreas>
                        <asp:ChartArea Name="ChartArea1"></asp:ChartArea>
                     </ChartAreas>
                     <Legends>
                        <asp:Legend></asp:Legend>
                     </Legends>
                  </asp:Chart>
                  <asp:LinkButton ID="lbl_hoc_bdgt_val" CssClass="hyprlnk" style="color: #0066FF;float:none;margin-right:0px;" runat="server"  OnClick="ho_bdgt_val_Click" ></asp:LinkButton>   

               </div>
            </div>
         </div>
      </div>
   </div>
      <div class="col-md-4 col-sm-6">
      <div class="portlet box blue">
         <div class="portlet-title">
            <div class="caption">CHANNEL PARTNERS </div>
         </div>
         <div class="portlet-body">
            <div id="Div22" class="k-content">
               <div id="Div23" class="gauge-container" style="width:200px;">
                  <asp:Chart ID="chart_ho_dstrbtr" EnableViewState="true" runat="server">
                     <Series>
                        <asp:Series Name="Series1" IsValueShownAsLabel="true"  ChartType="Pie"></asp:Series>
                     </Series>
                     <ChartAreas>
                        <asp:ChartArea Name="ChartArea1"></asp:ChartArea>
                     </ChartAreas>
                     <Legends>
                        <asp:Legend></asp:Legend>
                     </Legends>
                  </asp:Chart>
                  <asp:LinkButton ID="lbl_hod_bdgt_val" CssClass="hyprlnk" style="color: #0066FF;float:none;margin-right:0px;" runat="server" OnClick="ho_bdgt_val_Click"  ></asp:LinkButton>   

               </div>
            </div>
         </div>
      </div>
   </div>
           <div class="col-md-4 col-sm-6">
      <div class="portlet box blue">
         <div class="portlet-title">
            <div class="caption">TOTALS</div>
         </div>
         <div class="portlet-body">
            <div id="Div24" class="k-content">
               <div id="Div25" class="gauge-container" style="width:200px;">
                  <asp:Chart ID="chart_ho_total" EnableViewState="true" runat="server">
                     <Series>
                        <asp:Series Name="Series1" IsVisibleInLegend="true" IsValueShownAsLabel="true" ChartType="Pie"></asp:Series>
                         
                     </Series>
                     <ChartAreas>
                        <asp:ChartArea Name="ChartArea1"></asp:ChartArea>
                     </ChartAreas>
                     <Legends>
                        <asp:Legend></asp:Legend>
                     </Legends>
                  </asp:Chart>
                  <asp:LinkButton ID="lbl_hot_bdgt_val" CssClass="hyprlnk" style="color: #0066FF;float:none;margin-right:0px;" runat="server" OnClick="ho_bdgt_val_Click" ></asp:LinkButton>   

               </div>
            </div>
         </div>
      </div>
   </div>
</div>
   <%-- HO PIE CHART----END--%>
   <%-- PIECHARTS-----ENDS--%>
    <%-- //BAR GRAPHS START--%>
     <div id="divbargraph" runat="server" >
        <div style="margin-top:20px;" class="row panel panel-default ">
        <div id="Div27" class=" panel-heading3 "  onclick="collapse('bargraphdiv','MainContent_bar_graphs_image')">
        <h4 class="panel-title">
            <img id="bar_graphs_image" runat="server" src="images/button_plus.gif" align="left">  &nbsp;&nbsp;&nbsp;&nbsp;<asp:Label ID="lbl_bdgt_text" runat="server" Text="BUDGET DETAILS"></asp:Label>
        </h4>
        </div>
        </div>
   <div class="row" id="bargraphdiv">
       <div class="col-md-4 col-sm-6">
      <div class="portlet box blue">
         <div class="portlet-title">
            <div class="caption">CUSTOMERS</div>
         </div>
         <div class="portlet-body">
            <div id="Div13" class="k-content" >
               <div id="Div26" >
                  <asp:Chart ID="cbdgt_bargraph" EnableViewState="true" runat="server">

                     <Series>
                        <asp:Series Name="Series1" IsVisibleInLegend="true" IsValueShownAsLabel="true" ChartType="Column"></asp:Series>
                     </Series>
                     <ChartAreas>
                        <asp:ChartArea Name="ChartArea1" BackColor="SkyBlue"><AxisX LabelAutoFitStyle="LabelsAngleStep45,LabelsAngleStep90,WordWrap">
              <MajorGrid LineWidth="0" /><LabelStyle Font="Verdana, 8.25pt" />
         </AxisX><AxisY>
              <MajorGrid LineWidth="0" />
         </AxisY></asp:ChartArea>
                     </ChartAreas>
                    <%-- <Legends>
                        <asp:Legend></asp:Legend>
                     </Legends>--%>
                  </asp:Chart>
                 
               </div>
            </div>
         </div>
      </div>
   </div>

        <%-- //DISTRIBUTORS_START--%>
         <div class="col-md-4 col-sm-6">
      <div class="portlet box blue">
         <div class="portlet-title">
            <div class="caption">CHANNEL PARTNERS </div>
         </div>
         <div class="portlet-body">
            <div id="Div28" class="k-content" >
               <div id="Div29">
                  <asp:Chart ID="dbdgt_bargraph" EnableViewState="true" runat="server">

                     <Series>
                        <asp:Series Name="Series1" IsVisibleInLegend="true" IsValueShownAsLabel="true" ChartType="Column"></asp:Series>
                     </Series>
                     <ChartAreas>
                        <asp:ChartArea Name="ChartArea1" BackColor="SkyBlue"><AxisX LabelAutoFitStyle="LabelsAngleStep45,LabelsAngleStep90,WordWrap">
              <MajorGrid LineWidth="0" /><LabelStyle Font="Verdana, 8.25pt" />
         </AxisX><AxisY>
              <MajorGrid LineWidth="0" />
         </AxisY></asp:ChartArea>
                     </ChartAreas>
                   <%--  <Legends>
                        <asp:Legend></asp:Legend>
                     </Legends>--%>
                  </asp:Chart>
                 
               </div>
            </div>
         </div>
      </div>
   </div>
        <%-- Distributors-end--%>
             <div class="col-md-4 col-sm-6">
      <div class="portlet box blue">
         <div class="portlet-title">
            <div class="caption">TOTALS</div>
         </div>
         <div class="portlet-body">
            <div id="Div30" class="k-content" >
               <div id="Div31">
                  <asp:Chart ID="tbdgt_bargraph" EnableViewState="true" runat="server">
                     <Series>
                        <asp:Series Name="Series1" IsVisibleInLegend="true" IsValueShownAsLabel="true" ChartType="Column"></asp:Series>
                     </Series>
                     <ChartAreas>
                        <asp:ChartArea Name="ChartArea1" BackColor="SkyBlue"><AxisX LabelAutoFitStyle="LabelsAngleStep45,LabelsAngleStep90,WordWrap">
              <MajorGrid LineWidth="0" /><LabelStyle Font="Verdana, 8.25pt" />
         </AxisX><AxisY>
              <MajorGrid LineWidth="0" />
         </AxisY></asp:ChartArea>
                     </ChartAreas>
                     <%--<Legends>
                        <asp:Legend></asp:Legend>
                     </Legends>--%>
                  </asp:Chart>
                 
               </div>
            </div>
         </div>
      </div>
   </div>
        </div>
         </div>
   <%-- BAR GRAPHS END--%>
    <div style="margin-top:20px;" class="row panel panel-default ">
        <div id="gridtitle" class=" panel-heading4 "  >
        <h4 class="panel-title">
            <img id="cnsldt_img" runat="server" src="images/button_plus.gif" align="left">  &nbsp;&nbsp;&nbsp;&nbsp; CONSOLIDATED STATUS
        </h4>
        </div>
        </div>
      <div id="divcnsldt" runat="server" style="float: left; padding-right: 10px; padding-left: 10px; width: 100%;"><%-- margin-top: -25px;--%>
          <asp:HiddenField runat="server" ID="hdn_CustomerNum" />
          <asp:HiddenField ID="hdn_se_statuscnt" runat="server" />
          <asp:HiddenField ID="hdn_bm_statuscnt" runat="server" />
          <asp:HiddenField ID="hdn_ho_statuscnt" runat="server" />
          <asp:HiddenField ID="hdn_total_cnt" runat="server" />
          <asp:HiddenField ID="hdn_se_cust_cnt" runat="server" />
          <br />
             <asp:GridView ID="grdviewAllValues" runat="server" AutoGenerateColumns="False" ViewStateMode="Enabled" ShowHeader="False" Width="100%" Style="text-align: center;">
                  <Columns>
                      <asp:TemplateField HeaderText="Customer Number">
                          <ItemTemplate>
                              <asp:Label runat="server" ID="lblcustomernumber" Text='<%# Eval("customer_number") %>'></asp:Label>
                              <asp:HiddenField runat="server" ID="hdn_customernumber" Value='<%# Eval("customer_number") %>'/>
                          </ItemTemplate>
                      </asp:TemplateField>
                      <asp:TemplateField HeaderText="Customer Name">
                          <ItemTemplate>
                              <asp:LinkButton runat="server" ID="lnkbtn_Customer_name" Text='<%# Eval("customer_name")%>' Visible='<%# Eval("flag").ToString() =="customer" %>' OnClick="lnkbtn_Vustomer_name_Click" Style="color: #2a6496;text-decoration: underline;"></asp:LinkButton>
                              <asp:Label runat="server" ID="lblcustomer_name" Text='<%# Eval("customer_name") %>' Visible='<%# Eval("flag").ToString() !="customer" %>'></asp:Label>
                             
                          </ItemTemplate>
                      </asp:TemplateField>

                       <asp:TemplateField HeaderText="Customer Type">
                          <ItemTemplate>
                              <asp:Label runat="server" ID="lbl_customer_type" Text='<%# Eval("customer_type") %>'></asp:Label>
                              <asp:Image runat="server" ID="ibtnCtype" ImageUrl="~/images/arrow_down_popup.png" Visible='<%# Eval("flag").ToString() =="Heading" %>' 
                                  Style="padding-left: 10px;"/>
                          </ItemTemplate>
                      </asp:TemplateField>

                      <asp:TemplateField HeaderText="Customer Region">
                          <ItemTemplate>
                              <asp:Label runat="server" ID="lbl_Customer_region" Text='<%# Eval("Customer_region") %>'></asp:Label>
                              <asp:Image runat="server" ID="ibtnRegion" ImageUrl="~/images/arrow_down_popup.png" Visible='<%# Eval("flag").ToString() =="Heading" %>' 
                                  Style="padding-left: 10px;"/>
                          
                          </ItemTemplate>
                      </asp:TemplateField>

                      <asp:TemplateField HeaderText="Sales Engineer Status">
                          <ItemTemplate>
                              <asp:Label runat="server" ID="lbl_status_sales_engineer" Text='<%# Eval("status_sales_engineer") %>'></asp:Label>
                              <asp:Image runat="server" ID="ibtnSEStatus" ImageUrl="~/images/arrow_down_popup.png" Visible='<%# Eval("flag").ToString() =="Heading" %>' Style="padding-left: 10px;" />
                          
                          </ItemTemplate>
                      </asp:TemplateField>

                      <asp:TemplateField HeaderText="Branch Manager Status">
                          <ItemTemplate>
                              <asp:Label runat="server" ID="lbl_status_branch_manager" Text='<%# Eval("status_branch_manager") %>'></asp:Label>
                              <asp:Image runat="server" ID="ibtnBMStatus" ImageUrl="~/images/arrow_down_popup.png" Visible='<%# Eval("flag").ToString() =="Heading" %>' Style="padding-left: 10px;" />
                          
                          </ItemTemplate>
                      </asp:TemplateField>

                      <asp:TemplateField HeaderText="HO Satus">
                          <ItemTemplate>
                              <asp:Label runat="server" ID="lbl_status_ho" Text='<%# Eval("status_ho") %>'></asp:Label>
                              <asp:Image runat="server" ID="ibtnHOStatus" ImageUrl="~/images/arrow_down_popup.png" Visible='<%# Eval("flag").ToString() =="Heading" %>' Style="padding-left: 10px;" />
                          
                          </ItemTemplate>
                      </asp:TemplateField>

                      <asp:TemplateField HeaderText="Value" >
                          <ItemTemplate>
                              <asp:Label runat="server" ID="lbl_estimate_value_next_year" Text='<%# Eval("estimate_value_next_year") %>'></asp:Label>
                              <asp:Label runat="server" ID="lblFlag" Text='<%# Eval("flag") %>' Style="display:none"></asp:Label>
                          </ItemTemplate>
                      </asp:TemplateField>

                  </Columns>
            </asp:GridView>
      </div>


   <div class="col-md-2" id="footer" runat="server" style="height:20px;">
       <asp:HiddenField runat="server" ID="hdnRole" />
     </div>
       
        
    <asp:Panel runat="server" ID="panelCustomerType" CssClass="panelBack" style="display:none;">
        <div  id="dropid" >
            <asp:CheckBox runat="server" ID="CheckBox1"  Text="ALL"  /><br />
            <asp:CheckBox runat="server" ID="cbCTCustomer" CssClass="chkCT" Text="CUSTOMER" /><br />
            <asp:CheckBox runat="server" ID="cbCTDistributor" CssClass="chkCT" Text="CHANNEL PARTNER" /><br />
            <asp:Button runat="server" ID="btnCTOk" Text="Ok" OnClick="btnCTOk_Click" CssClass="btn green" /><br />
        </div>
    </asp:Panel>

    <asp:Panel runat="server" ID="panelRegion" CssClass="panelBack" style="display:none; width:245px !important">
        <div  id="Div1" style="width:230px;" >           
            <asp:Panel ID="Panel1" runat="server" ScrollBars="Vertical" Style="max-height: 150px !important;">
                <asp:CheckBox runat="server" ID="cbAllRegion"  Text="ALL" Style="margin-left:10px;"  /><br />
                <asp:CheckBoxList ID="cblRegion" runat="server" Width="90%" >
                                                    <%--<asp:ListItem>BGL</asp:ListItem>
                                                    <asp:ListItem>NIC</asp:ListItem>
                                                    <asp:ListItem>PUD</asp:ListItem>
                                                    <asp:ListItem>PUR</asp:ListItem>
                                                    <asp:ListItem>MUM</asp:ListItem>
                                                    <asp:ListItem>GUR</asp:ListItem>--%>
                                                </asp:CheckBoxList>
             </asp:Panel>
            <asp:Button runat="server" ID="btnRegionOk" Text="Ok" OnClick="btnRegionOk_Click" style="top:0px" CssClass="btn green"/><br />
        </div>
    </asp:Panel>

    <asp:Panel runat="server" ID="panelSEStatus" CssClass="panelBack" style="display:none;">
        <div  id="Div2" style="float: left;">
            <asp:CheckBox runat="server" ID="CheckBox2"  Text="ALL"  /><br />
            <asp:CheckBox runat="server" ID="cb_se_NI"  CssClass="chkse" Text="Not Initiated" /><br />
            <asp:CheckBox runat="server" ID="cb_se_PS" CssClass="chkse" Text="Pending Submission" /><br />
            <asp:CheckBox runat="server" ID="cb_se_Sub" CssClass="chkse" Text="Submitted" /><br />
            <asp:Button runat="server" ID="btn_se_s_ok" Text="Ok" OnClick="btn_se_s_ok_Click" CssClass="btn green" /><br />
        </div>
    </asp:Panel>

    <asp:Panel runat="server" ID="panelBMStatus" CssClass="panelBack" style="display:none;">
        <div  id="Div3" >
             <asp:CheckBox runat="server" ID="CheckBox3" Text="ALL"  /><br />
             <asp:CheckBox runat="server" ID="cb_bm_NI" CssClass="chkbm" Text="Not Initiated" /><br />
             <asp:CheckBox runat="server" ID="cb_bm_PS" CssClass="chkbm" Text="Pending Submission" /><br />
             <asp:CheckBox runat="server" ID="cb_bm_Sub" CssClass="chkbm" Text="Submitted" /><br />


            <asp:CheckBox runat="server" ID="cb_bm_tr" CssClass="chkbm" Text="To be Reviewed" /><br />
            <asp:CheckBox runat="server" ID="cb_bm_sr" CssClass="chkbm" Text="Sent for Review" /><br />
            <asp:CheckBox runat="server" ID="cb_bm_app" CssClass="chkbm" Text="Approved" /><br />
            <asp:Button runat="server" ID="btn_bm_s_ok" Text="Ok" OnClick="btn_bm_s_ok_Click" CssClass="btn green" /><br />
        </div>
    </asp:Panel>

    <asp:Panel runat="server" ID="panelHOStatus" CssClass="panelBack" style="display:none;">
        <div  id="Div4"  >
            <asp:CheckBox runat="server" ID="CheckBox4" Text="ALL"  /><br />
            <asp:CheckBox runat="server" ID="cb_ho_tr"  CssClass="chkho" Text="To be Reviewed" /><br />
            <asp:CheckBox runat="server" ID="cb_ho_app"  CssClass="chkho" Text="Approved" /><br />
<%--            <asp:CheckBox runat="server" ID="ch_ho_null" Text="" /><br />--%>
            <asp:Button runat="server" ID="btn_ho_s_ok" Text="Ok" OnClick="btn_ho_s_ok_Click" CssClass="btn green" /><br />
        </div>
    </asp:Panel>
    <div id="DivBranchApprove" style="width:100%; display:none; text-align: center !important; height:auto !important">           
            <label class="col-md-4 ">BRANCH</label>
            <asp:DropDownList ID="ddlBranchList" runat="server" CssClass="form-control" ViewStateMode="Enabled" Width="68%"></asp:DropDownList>
               <br />
            <asp:Button runat="server" ID="btnBranchApproval" OnClientClick=" javascript:closePopUpApprove();" OnClick="btnBranchApproval_Click"  CssClass="btn green" Style="width: 130px; font-weight: bolder;margin-left:20px;" Text="APPROVE"/>

       </div>
   

     <%-- </ContentTemplate>
      <Triggers>
          <asp:AsyncPostBackTrigger ControlID="btnBranchApproval" EventName="Click" />

         <asp:AsyncPostBackTrigger ControlID="lbl_bmc_bdgt_val" EventName="Click" />         
         <asp:AsyncPostBackTrigger ControlID="lbl_bmd_bdgt_val" EventName="Click" />
         <asp:AsyncPostBackTrigger ControlID="lbl_bmt_bdgt_val" EventName="Click" />

         <asp:AsyncPostBackTrigger ControlID="lbl_hoc_bdgt_val" EventName="Click" />
         <asp:AsyncPostBackTrigger ControlID="lbl_hod_bdgt_val" EventName="Click" />
         <asp:AsyncPostBackTrigger ControlID="lbl_hot_bdgt_val" EventName="Click" />

         <asp:AsyncPostBackTrigger ControlID="btnCTOk" EventName="Click" />
         <asp:AsyncPostBackTrigger ControlID="btnRegionOk" EventName="Click" />
         <asp:AsyncPostBackTrigger ControlID="btn_se_s_ok" EventName="Click" />          
         <asp:AsyncPostBackTrigger ControlID="btn_bm_s_ok" EventName="Click" />
         <asp:AsyncPostBackTrigger ControlID="btn_ho_s_ok" EventName="Click" />
        
         <asp:AsyncPostBackTrigger ControlID="rdBtnTaegutec" EventName="CheckedChanged" />
         <asp:AsyncPostBackTrigger ControlID="rdBtnDuraCab" EventName="CheckedChanged" />
      </Triggers>
   </asp:UpdatePanel>
   <asp:UpdateProgress id="updateProgress" runat="server">
      <ProgressTemplate>
         <div style="position: fixed; text-align: left; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.7;">
            <span style="border-width: 0px; position: fixed; padding: 50px; font-size: 30px; left: 40%; top: 40%; color:#fff" >Please wait</span>
         </div>
      </ProgressTemplate>
   </asp:UpdateProgress>--%>

    <script src="http://cdn.kendostatic.com/2014.3.1316/js/jquery.min.js"></script>
    <script src="http://cdn.kendostatic.com/2014.3.1316/js/kendo.all.min.js"></script>
    <script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.2/jquery-ui.min.js"></script>
 
    <script type="text/javascript" src="gridscroll.js"></script>
    <script type="text/javascript" src="js/app.js"></script>
      
    <script type="text/javascript">
        $(window).resize(function () {

            gridviewScrollTrigger();


        });

        $(document).ready(function () {
           

            
            var se_cnt = $('#MainContent_hdn_se_statuscnt').val();
            var bm_cnt = $('#MainContent_hdn_bm_statuscnt').val();
            var ho_cnt = $('#MainContent_hdn_ho_statuscnt').val();
            var total_cnt = $('#MainContent_hdn_total_cnt').val() / 3;
            var se_total_cnt = $('#MainContent_hdn_se_cust_cnt').val() / 3;
            var range1 = total_cnt;
            var range2 = total_cnt + total_cnt;
            var range3 = total_cnt + total_cnt + total_cnt;

            //for se gauge
            var srange1 = se_total_cnt;
            var srange2 = se_total_cnt + se_total_cnt;
            var srange3 = se_total_cnt + se_total_cnt + se_total_cnt;
            createGauge("", se_cnt, srange1, srange2, srange3);
            createGauge1("", bm_cnt, range1, range2, range3);
            createGauge2("", ho_cnt, range1, range2, range3);


            $(document).bind("kendo:skinChange", function (e) {
                createGauge();
                createGauge1();
                createGauge2();
            });
            $('#gridtitle').click(function () {

                var attr = $('#MainContent_cnsldt_img').attr('src');
                //var imgsrc=images/button_plus.gif;
                $("#MainContent_divcnsldt").slideToggle();
                if (attr == "images/button_minus.gif") {

                    $("#MainContent_cnsldt_img").attr("src", "images/button_plus.gif");
                } else {
                    $(window).resize();
                    $("#MainContent_cnsldt_img").attr("src", "images/button_minus.gif");
                }

                jQuery(".ui-dialog.ui-widget").each(function () {

                    if (jQuery(this).css("display") == "block") {

                        jQuery(this).css("display", "none")
                    }
                });

            });

            $("#MainContent_HyperLink_se").click(function () {
                $("#MainContent_bm_chart").css("display", "none");
                $("#MainContent_bm_charts_image").attr("src", "images/button_plus.gif");
                $("#MainContent_ho_chart").css("display", "none");
                $("#MainContent_ho_charts_image").attr("src", "images/button_plus.gif");
                if ($('#se_charts').css('display') == 'none') {
                    $("#se_charts").css("display", "block");
                    $("#se_charts_image").attr("src", "images/button_minus.gif");
                }
                else if ($('#se_charts').css('display') == 'block') {
                    $("#se_charts").css("display", "block");
                    $("#se_charts_image").attr("src", "images/button_minus.gif");
                }
                else {
                    $("#se_charts").css("display", "none");
                    $("#se_charts_image").attr("src", "images/button_plus.gif");
                }
            });

            $("#MainContent_HyperLink_bm").click(function () {
                $("#se_charts").css("display", "none");
                $("#se_charts_image").attr("src", "images/button_plus.gif");
                $("#MainContent_ho_chart").css("display", "none");
                $("#MainContent_ho_charts_image").attr("src", "images/button_plus.gif");
                $("#bargraphdiv").css("display", "none");
                $("#MainContent_bar_graphs_image").attr("src", "images/button_plus.gif");
                if ($('#MainContent_bm_chart').css('display') == 'none') {
                    $("#MainContent_bm_chart").css("display", "block");
                    $("#MainContent_bm_charts_image").attr("src", "images/button_minus.gif");
                }
                else if ($('#MainContent_bm_chart').css('display') == 'block') {
                    $("#MainContent_bm_chart").css("display", "block");
                    $("#MainContent_bm_charts_image").attr("src", "images/button_minus.gif");
                }
                else {
                    $("#MainContent_bm_chart").css("display", "none");
                    $("#MainContent_bm_charts_image").attr("src", "images/button_plus.gif");
                }
            });

            $("#MainContent_HyperLink_ho").click(function () {
                $("#bmbargraphdiv").css("display", "none");
                $("#MainContent_bmbar_graphs_image").attr("src", "images/button_plus.gif");
                $("#MainContent_bm_chart").css("display", "none");
                $("#MainContent_bm_charts_image").attr("src", "images/button_plus.gif");
                $("#se_charts").css("display", "none");
                $("#se_charts_image").attr("src", "images/button_plus.gif");
                if ($('#MainContent_ho_chart').css('display') == 'none') {
                    $("#MainContent_ho_chart").css("display", "block");
                    $("#MainContent_ho_charts_image").attr("src", "images/button_minus.gif");
                }
                else if ($('#MainContent_ho_chart').css('display') == 'block') {
                    $("#MainContent_ho_chart").css("display", "block");
                    $("#MainContent_ho_charts_image").attr("src", "images/button_mius.gif");
                }
                else {
                    $("#MainContent_ho_chart").css("display", "none");
                    $("#MainContent_ho_charts_image").attr("src", "images/button_plus.gif");
                }
            });
            function createGauge(labelPosition, se_cnt, range1, range2, range3) {

                $("#gauge").kendoRadialGauge({
                    // renderAs: "canvas",
                    pointer: [{
                        color: "#736F6E",
                        value: se_cnt
                    }],

                    scale: {
                        //majorUnit: 2,
                        //minorUnit: .25,
                        // minorUnit: 25,
                        startAngle: 0,
                        endAngle: 180,
                        //minorUnit: 5,

                        max: range3,
                        labels: {
                            position: "outside",

                        },

                        //majorTicks: {
                        //    size: 10
                        //},
                        ranges: [
                             {
                                 from: 0,
                                 to: range1,
                                 color: "#830300"
                             }, {
                                 from: range1,
                                 to: range2,
                                 color: "yellow"
                             }, {
                                 from: range2,
                                 to: range3,
                                 color: "#006400"
                             }
                        ],
                        rangeSize: 40,
                        rangeDistance: -10,
                    }
                });
            }
            function createGauge1(labelPosition, bm_cnt, range1, range2, range3) {
                $("#gauge1").kendoRadialGauge({

                    pointer: [{
                        color: "#736F6E",
                        value: bm_cnt
                    }],

                    scale: {
                        //minorUnit: 5,
                        //startAngle: -30,
                        //endAngle: 210,
                        startAngle: 0,
                        endAngle: 180,


                        max: range3,
                        labels: {
                            position: "outside",

                        },
                        //majorTicks: {
                        //    size: 10
                        //},
                        ranges: [
                              {
                                  from: 0,
                                  to: range1,
                                  color: "#830300"
                              }, {
                                  from: range1,
                                  to: range2,
                                  color: "yellow"
                              }, {
                                  from: range2,
                                  to: range3,
                                  color: "#006400"
                              }
                        ],
                        rangeSize: 40,
                        rangeDistance: -10,
                    }
                });
            }
            function createGauge2(labelPosition, ho_cnt, range1, range2, range3) {
                $("#gauge2").kendoRadialGauge({

                    pointer: [{
                        color: "#736F6E",
                        value: ho_cnt
                    }],

                    scale: {
                        //minorUnit: 5,
                        //startAngle: -30,
                        //endAngle: 210,
                        startAngle: 0,
                        endAngle: 180,


                        max: range3,
                        labels: {
                            position: "outside",

                        },
                        //majorTicks: {
                        //    size: 10
                        //},
                        ranges: [
                              {
                                  from: 0,
                                  to: range1,
                                  color: "#830300"
                              }, {
                                  from: range1,
                                  to: range2,
                                  color: "yellow"
                              }, {
                                  from: range2,
                                  to: range3,
                                  color: "#006400"
                              }
                        ],
                        rangeSize: 40,
                        rangeDistance: -10,
                    }
                });
            }


            $('#MainContent_lnkbtnBulkapproval').bind("click", function (e) {
                dclg = $("#DivBranchApprove").dialog(
                            {
                                resizable: false,
                                draggable: true,
                                modal: false,
                                title: "BRANCH WISE APPROVAL",
                                width: "350",
                                height: "180",
                                open: function () {
                                    $(".ui-widget-overlay").css("display", 'none');
                                },
                                close: function () {
                                    $("#overlay").removeClass("ui-widget-overlay");
                                }
                            });
                //divPopUpBranchApproval = dclg;
                dclg.parent().appendTo(jQuery("form:first"));
                e.preventDefault();
            });


            $("#Div4 input[type='submit']").attr("disabled", true);
            $("#Div3 input[type='submit']").attr("disabled", true);
            $("#Div2 input[type='submit']").attr("disabled", true);
            $("#Div1 input[type='submit']").attr("disabled", true);
            $("#dropid input[type='submit']").attr("disabled", true);

            $("#Div4 INPUT[type='checkbox']").change(function () {

                var checkboxes = $("#Div4 input[type='checkbox']")
                var submitButt = $("#Div4 input[type='submit']");
                if ($(this).next('label').text() == "ALL") {
                    $("#Div4 INPUT[type='checkbox']").not(this).prop('checked', this.checked);
                    submitButt.attr("disabled", !checkboxes.is(":checked"));
                }
                else
                    submitButt.attr("disabled", !checkboxes.is(":checked"));
            });
            $(".chkho").click(function () {

                $("#MainContent_CheckBox4").prop('checked', ($(".chkho input[type='checkbox']:checked").length == $(".chkho input[type='checkbox']").length) ? true : false);
            });
            $("#Div3 INPUT[type='checkbox']").change(function () {

                var checkboxes = $("#Div3 input[type='checkbox']")
                var submitButt = $("#Div3 input[type='submit']");
                if ($(this).next('label').text() == "ALL") {
                    $("#Div3 INPUT[type='checkbox']").not(this).prop('checked', this.checked);
                    submitButt.attr("disabled", !checkboxes.is(":checked"));
                }
                else
                    submitButt.attr("disabled", !checkboxes.is(":checked"));
            });
            $(".chkbm").click(function () {

                $("#MainContent_CheckBox3").prop('checked', ($(".chkbm input[type='checkbox']:checked").length == $(".chkbm input[type='checkbox']").length) ? true : false);
            });
            $("#Div2 INPUT[type='checkbox']").change(function () {

                var checkboxes = $("#Div2 input[type='checkbox']")
                var submitButt = $("#Div2 input[type='submit']");
                if ($(this).next('label').text() == "ALL") {
                    $("#Div2 INPUT[type='checkbox']").not(this).prop('checked', this.checked);
                    submitButt.attr("disabled", !checkboxes.is(":checked"));
                }
                else
                    submitButt.attr("disabled", !checkboxes.is(":checked"));
            });
            $(".chkse").click(function () {

                $("#MainContent_CheckBox2").prop('checked', ($(".chkse input[type='checkbox']:checked").length == $(".chkse input[type='checkbox']").length) ? true : false);
            });
            $("#dropid INPUT[type='checkbox']").change(function () {

                var checkboxes = $(" #dropid input[type='checkbox']")
                var submitButt = $("#dropid input[type='submit']");
                console.log($(checkboxes[0]).next("label").text());
                if ($(this).next('label').text() == "ALL") {
                    $("#dropid INPUT[type='checkbox']").not(this).prop('checked', this.checked);
                    submitButt.attr("disabled", !checkboxes.is(":checked"));
                }
                else
                    submitButt.attr("disabled", !checkboxes.is(":checked"));

            });
            $(".chkCT").click(function () {

                $("#MainContent_CheckBox1").prop('checked', ($(".chkCT input[type='checkbox']:checked").length == $(".chkCT input[type='checkbox']").length) ? true : false);
            });
            $("#Div1 INPUT[type='checkbox']").change(function () {

                var checkboxes = $(" #Div1 input[type='checkbox']")
                var submitButt = $("#Div1 input[type='submit']");
                if ($(this).next('label').text() == "ALL") {
                    $("#Div1 INPUT[type='checkbox']").not(this).prop('checked', this.checked);
                    submitButt.attr("disabled", !checkboxes.is(":checked"));
                }
                else
                    submitButt.attr("disabled", !checkboxes.is(":checked"));
            });
            
            $("#MainContent_cbAllRegion").click(function () {
                debugger;
                disableChk();
                if ($('#MainContent_cbAllRegion').is(':checked')) {
                    $('input:checkbox[name$=cblRegion]').each(
                        function () {
                            $(this).attr('checked', 'checked');
                        });
                }
                else {
                    $('input:checkbox[name$=cblRegion]').each(
                        function () {
                            $(this).removeAttr('checked');
                        });
                }              
                //$("#MainContent_cbAllRegion").prop('checked', ($("#MainContent_cblRegion input[type='checkbox']:checked").length == $("#MainContent_cblRegion input[type='checkbox']").length) ? true : false);
            });
            $('input:checkbox[name$=cblRegion]').click(function () {
                debugger;
                    // if any of the checkbox is unchecked
                    // check all checkbox should be cleared
                    if (!$(this).is(':checked')) {
                        $('#MainContent_cbAllRegion').removeAttr('checked');
                    }
                    else {
                        // if all of the checkbox is checked
                        // check all checkbox should be checked
                        var count = $('input:checkbox[name$=cblRegion]').length  
                        if (count  == ($('input:checkbox[name$=cblRegion]:checked').length + 1)) {
                            $('#MainContent_cbAllRegion').attr('checked', 'checked');
                        }
                    }
                });

            $('#MainContent_lnkbtnBulkapproval').bind("click", function (e) {
                dclg = $("#DivBranchApprove").dialog(
                            {
                                resizable: false,
                                draggable: true,
                                modal: false,
                                title: "BRANCH WISE APPROVAL",
                                width: "350",
                                height: "180",
                                open: function () {
                                    $(".ui-widget-overlay").css("display", 'none');
                                },
                                close: function () {
                                    $("#overlay").removeClass("ui-widget-overlay");
                                }
                            });
                //divPopUpBranchApproval = dclg;
                dclg.parent().appendTo(jQuery("form:first"));
                e.preventDefault();
            });





        });

        function disableChk() {
            if ($("#MainContent_hdnRole").val() == "BM" || $("#MainContent_hdnRole").val() == "SE") {
                $("#MainContent_cbAllRegion").removeAttr('checked');
            } 
        }
        $(function () {
            $("[id*=lnkbtn_Customer_name]").bind("click", function () {
                var td = $("td", $(this).closest("tr"));
                var CustomerNum = $("[id*=hdn_customernumber]", td).val();
                $("#MainContent_hdn_CustomerNum").val(CustomerNum);
            });

        });

        var gridView1;
        function gridviewScrollTrigger() {
            if (gridView1 != undefined)
                return;
            gridView1 = $('#MainContent_grdviewAllValues').gridviewScroll({
                width: $(window).width() - 60,
                height: 500,
                railcolor: "#F0F0F0",
                barcolor: "#606060",
                barhovercolor: "#606060",
                bgcolor: "#F0F0F0",
                freezesize: 0,
                arrowsize: 30,
                varrowtopimg: "Images/arrowvt.png",
                varrowbottomimg: "Images/arrowvb.png",
                harrowleftimg: "Images/arrowhl.png",
                harrowrightimg: "Images/arrowhr.png",
                headerrowcount: 1,
                railsize: 16,
                barsize: 14,
                verticalbar: "auto",
                horizontalbar: "auto",
                wheelstep: 1,
            });
            jQuery(".ui-dialog.ui-widget").each(function () {
                if (jQuery(this).css("display") == "block")
                    jQuery(this).css("top", $('#MainContent_grdviewAllValues').offset().top)
            });
            bindPopup();

        }
        function modalht() {

            jQuery(".ui-dialog.ui-widget").each(function () {

                if (jQuery(this).css("display") == "block") {

                    jQuery(this).css("top", $('#MainContent_grdviewAllValues').offset().top)
                }
            });
        }

        var closedialog_panelHOStatus;
        var bindPopupBinded = false;
        function bindPopup() {
            if (bindPopupBinded) return;
            /*  Display or close pop up HO Status Start*/
            var dlg_panelHOStatus = $('#MainContent_panelHOStatus').dialog({

                autoOpen: false,
                dialogClass: 'noTitleStuff',
                minHeight: 100,
                width: 160,
                position: { my: "left top", at: "left bottom", of: $('#MainContent_grdviewAllValues_ibtnHOStatus_0').closest("td") },
                draggable: true,
                resizable: false,
                modal: false,
                closeText: 'Close',
                open: function () {
                    $(this).dialog('open');

                    closedialog_panelHOStatus = 1;
                },
                focus: function () {
                    closedialog_panelHOStatus = 0;
                },
                close: function () {
                    $(this).dialog('close');
                    closedialog_panelHOStatus = 0;

                }
            });
            dlg_panelHOStatus.parent().appendTo(jQuery("form:first"));

            $('#MainContent_grdviewAllValues_ibtnHOStatus_0').click(function () {

                if (closedialog_panelHOStatus == 1) {
                    $('#MainContent_panelHOStatus').dialog('close');
                } else {
                    $('#MainContent_panelHOStatus').dialog('open');
                    closedialog_panelHOStatus = 1;
                }
                //gridviewScrollTrigger();
            });
            /*   Display or close pop up HO Status END*/


            /*  Display or close pop up BM Status Start*/
            var dlg_panelBMStatus = $('#MainContent_panelBMStatus').dialog({
                autoOpen: false,
                dialogClass: 'noTitleStuff',
                minHeight: 100,
                width: 200,
                position: { my: "left top", at: "left bottom", of: $('#MainContent_grdviewAllValues_ibtnBMStatus_0').closest("td") },
                draggable: true,
                resizable: false,
                modal: false,
                closeText: 'Close',
                open: function () {
                    $(this).dialog('open');
                    closedialog_panelBMStatus = 1;

                },
                focus: function () {
                    closedialog_panelBMStatus = 0;
                },
                close: function () {

                    $(this).dialog('close');
                    closedialog_panelBMStatus = 0;
                }
            });
            dlg_panelBMStatus.parent().appendTo(jQuery("form:first"));

            $('#MainContent_grdviewAllValues_ibtnBMStatus_0').click(function () {
                if (closedialog_panelBMStatus == 1) {
                    $('#MainContent_panelBMStatus').dialog('close');
                } else {
                    $('#MainContent_panelBMStatus').dialog('open');
                    closedialog_panelBMStatus = 1;
                }
            });
            var closedialog_panelBMStatus;

            /*   Display or close pop up BM Status END*/


            /*  Display or close pop up SE Status Start*/
            var dlg_panelSEStatus = $('#MainContent_panelSEStatus').dialog({
                autoOpen: false,
                dialogClass: 'noTitleStuff',
                minHeight: 100,
                width: 200,
                position: { my: "left top", at: "left bottom", of: $('#MainContent_grdviewAllValues_ibtnSEStatus_0').closest("td") },
                draggable: true,
                resizable: false,
                modal: false,
                closeText: 'Close',
                open: function () {
                    $(this).dialog('open');
                    closedialog_panelSEStatus = 1;

                },
                focus: function () {
                    closedialog_panelSEStatus = 0;
                },
                close: function () {
                    $(this).dialog('close');
                    closedialog_panelSEStatus = 0;

                }
            });
            dlg_panelSEStatus.parent().appendTo(jQuery("form:first"));

            $('#MainContent_grdviewAllValues_ibtnSEStatus_0').click(function () {

                if (closedialog_panelSEStatus == 1) {
                    $('#MainContent_panelSEStatus').dialog('close');
                } else {
                    $('#MainContent_panelSEStatus').dialog('open');
                    closedialog_panelSEStatus = 1;
                }
            });
            var closedialog_panelSEStatus;

            /*   Display or close pop up SE Status END*/

            /*  Display or close pop up customer REGION Start*/
            var dlg_panelRegion = $('#MainContent_panelRegion').dialog({
                autoOpen: false,
                dialogClass: 'noTitleStuff',
                minHeight: 100,
                width: 250,
                position: { my: "left top", at: "left bottom", of: $('#MainContent_grdviewAllValues_ibtnRegion_0').closest("td") },
                draggable: true,
                resizable: false,
                modal: false,
                closeText: 'Close',
                open: function () {
                    $(this).dialog('open');
                    closedialog_panelRegion = 1;

                },
                focus: function () {
                    closedialog_panelRegion = 0;
                },
                close: function () {
                    $(this).dialog('close');
                    closedialog_panelRegion = 0;

                }
            });
            dlg_panelRegion.parent().appendTo(jQuery("form:first"));

            $('#MainContent_grdviewAllValues_ibtnRegion_0').click(function () {

                if (closedialog_panelRegion == 1) {
                    $('#MainContent_panelRegion').dialog('close');
                } else {
                    $('#MainContent_panelRegion').dialog('open');
                    closedialog_panelRegion = 1;
                }
            });
            var closedialog_panelRegion;

            /*  Display or close pop up customer REGION END*/

            /*   Display or close pop up customer type Start*/
            var dlg_panelCustomerType = $('#MainContent_panelCustomerType').dialog({
                autoOpen: false,
                dialogClass: 'noTitleStuff',
                minHeight: 100,
                width: 190,

                position: { my: "left top", at: "left bottom", of: $('#MainContent_grdviewAllValues_ibtnCtype_0').closest("td") },
                draggable: true,
                resizable: false,
                modal: false,
                closeText: 'Close',
                open: function () {
                    $(this).dialog('open');
                    closedialog = 1;
                    // $(document).bind('click', overlayclickclose);
                },
                focus: function () {
                    closedialog = 0;
                },
                close: function () {
                    $(this).dialog('close');
                    closedialog = 0;
                    //$(document).unbind('click');
                }
            });
            dlg_panelCustomerType.parent().appendTo(jQuery("form:first"));

            $('#MainContent_grdviewAllValues_ibtnCtype_0').click(function () {

                if (closedialog == 1) {
                    $('#MainContent_panelCustomerType').dialog('close');
                } else {
                    $('#MainContent_panelCustomerType').dialog('open');
                    closedialog = 1;
                }
            });
            var closedialog;

            /*   Display or close pop up customer type END*/



        }


    </script>


</asp:Content>