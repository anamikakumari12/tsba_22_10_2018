﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="TaegutecSalesBudget.Login" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
 <head runat="server">
    <meta http-equiv="X-UA-Compatible" content="IE=8, IE=9, IE=edge, IE=11"/>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0" />
    <title>Login |  Sales-Budget & Performance Monitoring</title>

    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css" />
    <link rel="stylesheet" type="text/css" href="css/jquery-ui-1.10.3.custom.css" />
    <link rel="stylesheet" type="text/css" href="fonts/font-awesome/css/font-awesome.css"/>
    <link rel="stylesheet" type="text/css" href="fonts/fsquere/style.css"/>
    <link rel='stylesheet' type='text/css' href="fonts/open-sans/open-sans.css" />
    <link href="css/main.css" rel="stylesheet" type="text/css"/>
    <link href="css/responsive.css" rel="stylesheet" type="text/css"/>
    <link rel="icon" type="image/png" href="#" />
   
</head>
   
<body class="login" style="background-color: #54364a; background-size: cover; background-image: url(images/d.jpg);">
    
    <div class="logo"><!-- BEGIN LOGO -->
        <img src="images/logo.png" alt="logo" />
    </div>  <!-- END LOGO -->
    
     <div class="content">   <!-- BEGIN LOGIN -->
        
        <form  id="loginform" class="form-vertical login-form" runat="server" >
            
           
            <div class="alert alert-danger hide">
                <asp:Button class="close" data-dismiss="alert" ID="Button1" runat="server" />
                <%--<button class="close" data-dismiss="alert"></button>--%>
                <span>Enter any username and passowrd.</span>
            </div>
            
            <div class="control-group">
                <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
                <label class="control-label">Username</label>
                <div class="controls">
                    <div class="input-icon left">
                        <i class="fs-user-2"></i>
                        <asp:TextBox runat="server" class="form-control" type="text" id="user" placeholder="Username" name="username"></asp:TextBox>
<%--                        <input class="form-control" type="text" id="user" placeholder="Username" name="username" runat="server"/>--%>
                    </div>
                </div>
            </div>
            
            <div class="control-group">
                <label class="control-label">Password</label>
                <div class="controls">
                    <div class="input-icon left">
                        <i class="fs-locked"></i>
                        <asp:TextBox runat="server" class="form-control" id="password" placeholder="Password" name="password" type="password"></asp:TextBox>
                        <%--<input class="form-control" type="password" id="password" placeholder="Password" name="password" runat="server"/>--%>
                    </div>
                </div>
            </div>
            
            <div class="form-actions">
                <label class="checkbox">
                    <input id="Checkbox1" type="checkbox" name="remember" value="1" runat="server"/> Remember me
                </label>
                <%--<button type="submit" class="btn green pull-right">--%>
                    <asp:Button class="btn green" ID="loginBtn" runat="server" text="Login" OnClick="loginBtn_Click" />
                    <%--<i class="fs-checkmark-2"></i> Login--%>
                <%--</button>   --%>         
            </div>
            
            <div class="forget-password">
                <a href="Recoverpassword.aspx" class="" id="forget-password">Forgot your password ?</a>
            </div>
            
        </form> <!-- END LOGIN FORM --> 
               
        
        <form class="form-vertical forget-form" > <!-- BEGIN FORGOT PASSWORD FORM -->
          <%--  <h3 class="">Forget Password ?</h3>
            <p>Enter your e-mail address below to reset your password.</p>
            --%>
            <div class="control-group">
                <div class="controls">
                    <div class="input-icon left">
                        <i class="fa fa-envelope-o"></i>
                        <input id="Text1" class="form-control" type="text" placeholder="Email" name="email" runat="server" />
                    </div>
                </div>
            </div>
            
            <div class="form-actions">
                <button type="button" id="back-btn" class="btn">
                    <i class="fs-arrow-left"></i> Back
                </button>
                <button type="submit" class="btn green pull-right">
                    <i class="fs-checkmark-2"></i> Submit
                </button>            
            </div>
        </form> <!-- END FORGOT PASSWORD FORM -->
      
    </div>
    <!-- END LOGIN -->
    <!-- BEGIN COPYRIGHT -->
    <div class="copyright">
        copyright &copy; 2014  Design & Developed by <a href="#">KNS TECHNOLOGIES PVT Ltd</a>... Sales-Budget & Performance Monitoring.    </div>
    <!-- END COPYRIGHT -->

    <script type="text/javascript" src="js/jquery.min.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/respond.min.js"></script>
    <script type="text/javascript" src="js/app.js"></script>
     <script type="text/javascript" src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.13.0/jquery.validate.js"></script> 

   <%-- <script type="text/javascript" src="Scripts/login.js"></script>--%>
    <script type="text/javascript">
        $(document).ready(function () {
            
            App.initLogin();
            function ValidateEmail(email) {
                var expr = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
                return expr.test(email);
            };
            $("#loginBtn").click(function () {
                if (!ValidateEmail($("#user").val())) {
                    alert("Please provide a  valid email address");
                    return false;

                }
                else {
                    return true;
                }
            });
            $("#loginform").validate({
                rules: {
                    user: {
                        required: true,
                        //minlength: 3
                    },
                    password: {
                        required: true,
                        //minlength: 6
                    }
                },
                messages: {
                    user: {
                        required: "Please provide a username",
                        //minlength: "Username must be at least 3 characters long"
                    },
                    password: {
                        required: "Please provide a password",
                        //minlength: "Your password must be at least 5 characters long"
                    }
                }

            });
            $('#password').keypress(function (event) {
                if (event.keyCode == 13) {
                    $('#loginBtn').click();
                }
            })
            //$(function () {
            //    $('#password').keypress(function (event) {
            //        if (event.keyCode == 13) {
            //            $('#loginBtn').click();
            //        }
            //    })
            //})
        });




    </script>
</body>
</html>
