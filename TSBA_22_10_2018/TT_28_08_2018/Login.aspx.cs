﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace TaegutecSalesBudget
{
    public partial class Login : System.Web.UI.Page
    {
        LoginAuthentication authObj = new LoginAuthentication();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["UserName"] != null) 
                {
                    if (Session["RoleId"].ToString() == "Admin") { Response.Redirect("AdminProfile.aspx?Profile"); }
                    else Response.Redirect("ReportsDashboard.aspx?RD");
                }            
            }
        }

        protected void loginBtn_Click(object sender, EventArgs e)
        {
            authObj.LoginMailID = user.Text.ToString();
            authObj.MailPassword = authObj.Encrypt(password.Text.ToString());
            string ErrorMessage = authObj.authLogin(authObj);
            if (authObj.ErrorNum == 0)
            {
                Session["UserName"] = authObj.UserName;
                Session["LoginMailId"] = authObj.LoginMailID;
                Session["RoleId"] = authObj.RoleId;
                Session["UserId"] = authObj.EngineerId;
                Session["BranchCode"] = authObj.BranchCode;
                Session["BranchDesc"] = authObj.BranchDesc;
                Session["Territory"] = authObj.Territory;
                Session["EngineerId"] = authObj.EngineerId;
                Session["Password"] = authObj.MailPassword;
                if (Session["RoleId"] != null)
                {

                    if (Session["RoleId"].ToString() == "Admin") { Response.Redirect("AdminProfile.aspx?Profile"); }
                    else Response.Redirect("ReportsDashboard.aspx?RD");
                   
                }    
            }
            else if (authObj.ErrorNum == 1)
            {
                string scriptString = "<script type='text/javascript'> alert('User name and password does not match');</script>";
                ClientScriptManager script = Page.ClientScript;
                script.RegisterClientScriptBlock(GetType(), "Script", scriptString);
            }
            else if (authObj.ErrorNum == 2)
            {
                string scriptString = "<script type='text/javascript'> alert('Your account has been blocked');</script>";
                ClientScriptManager script = Page.ClientScript;
                script.RegisterClientScriptBlock(GetType(), "Script", scriptString);
            }
          
        }
    }
}