﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Globalization;
using System.IO.Compression;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using iTextSharp.text.html.simpleparser;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.Windows.Forms;
using System.Text;
using System.Xml;


namespace TaegutecSalesBudget
{
    public partial class PenetrationReport : System.Web.UI.Page
    {
        #region Global Declaration
        public static string cter;
        CommonFunctions objFunc = new CommonFunctions();
        Budget objBud = new Budget();
        public static float byValueIn = 1000;
        public static int tablesLoadedStatus;
        DataSet dsReport = new DataSet();
        #endregion

        #region Events
        /// <summary>
        /// Author : Anamika
        /// Date : April 17, 2017
        /// Desc : Loading the page and storeing cter value in session
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }
                Session["Customer_Number"] = null;
                Session["Project_Number"] = null;
                Session["Project"] = null;
                if (!IsPostBack)
                {
                    cter = null;
                    string roleId = Session["RoleId"].ToString();

                    if (roleId == "HO")
                    {
                        if (Session["cter"] == null)
                        {
                            Session["cter"] = "TTA";
                            cter = "TTA";

                        }
                        if (Session["cter"].ToString() == "DUR")
                        {
                            rdBtnDuraCab.Checked = true;
                            rdBtnTaegutec.Checked = false;
                            cter = "DUR";
                        }
                        else
                        {
                            rdBtnTaegutec.Checked = true;
                            rdBtnDuraCab.Checked = false;
                            cter = "TTA";
                        }
                        divCter.Visible = true;
                    }

                }
            }
            catch (Exception ex)
            {
                objFunc.LogError(ex);
            }
        }

        /// <summary>
        /// Author : Anamika
        /// Date : April 17, 2017
        /// Desc : Loading the report based on filter selection
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void reports_Click(object sender, EventArgs e)
        {
            
            DataTable dtReport = new DataTable();
            if (!string.IsNullOrEmpty(txtFromDate.Text) && !string.IsNullOrEmpty(txtToDate.Text) && !string.IsNullOrEmpty(txtContribution.Text) && Convert.ToInt32(txtContribution.Text)<=100)
            {
                string From_date = Convert.ToString(txtFromDate.Text);
                From_date = From_date.Remove(0, 2);
                From_date = From_date.Insert(0, "01");
                //From_date = From_date.Replace('/', '-');
                string To_date = Convert.ToString(txtToDate.Text);
                //To_date = To_date.Replace('/', '-');
                int Contribution = Convert.ToInt32(txtContribution.Text);
                string filter = Convert.ToString(ddlcustomertype.SelectedValue);

                dsReport = objBud.GetPenetrationReport(From_date, To_date, Contribution, filter, cter, byValueIn);
                if (dsReport != null)
                {
                    if (dsReport.Tables[1].Rows.Count > 0)
                    {
                        dtReport = dsReport.Tables[1];
                        LoadGrid(filter, dtReport);
                        lblResult.Text = "All values are in thousands.";
                        if (dsReport.Tables[0].Rows.Count > 0)
                        {
                            lblCount.Text = "TOTAL : ";
                            txtCount.Text =Convert.ToString(dsReport.Tables[0].Rows[0][0]);
                            lblSale.Text = "TOTAL SALE : ";
                            txtSale.Text = Convert.ToString(dsReport.Tables[0].Rows[0][1]);
                            lblContr.Text = "TOTAL CONTRIBUTION : ";
                            txtContr.Text = Convert.ToString(dsReport.Tables[0].Rows[0][2]);
                            Session["Total"] = dsReport.Tables[0];
                        }

                    }
                    
                    else
                    {
                        lblResult.Text = "No data to display. Change the filter and try again.";
                    }
                }
                else
                {

                    lblResult.Text = "No data to display. Change the filter and try again.";
                }
               
            }
            else {
                lblResult.Text = "";
                lblCount.Text = "";
                txtCount.Text = "";
                lblSale.Text = "";
                txtSale.Text = "";
                lblContr.Text = "";
                txtContr.Text = "";
                ClearGrid();
            }
            ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "triggerPostGridLodedActions();", true);
        }

        /// <summary>
        /// Author : Anamika
        /// Date : April 17, 2017
        /// Desc : Clearing the details
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnClear_Click(object sender, EventArgs e)
        {
            txtFromDate.Text = "";
            txtToDate.Text = "";
            txtContribution.Text = "";
            lblResult.Text = "";
            lblCount.Text ="";
            txtCount.Text ="";
            lblSale.Text = "";
            txtSale.Text = "";
            lblContr.Text ="";
            txtContr.Text = "";
            ClearGrid();
            ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "triggerPostGridLodedActions();", true);
        }

        /// <summary>
        /// Author : Anamika
        /// Date : April 24, 2017
        /// Desc : Export to excel
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void exportexlbtn_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(txtFromDate.Text) && !string.IsNullOrEmpty(txtToDate.Text) && !string.IsNullOrEmpty(txtContribution.Text))
            {
                ExportReport();
            }
            //try
            //{
            //if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }
            //Response.AddHeader("content-disposition", string.Format("attachment; filename={0}", "PenetrationReport.xlsx"));
            //Response.ContentType = "application/ms-excel";
            //StringWriter sw = new StringWriter();
            //HtmlTextWriter ht = new HtmlTextWriter(sw);

            //string Fromdate = Convert.ToString(txtFromDate.Text);
            //string Todate = Convert.ToString(txtToDate.Text);
            //string FilterBy = Convert.ToString(ddlcustomertype.SelectedItem.Text);
            //string Contribution = Convert.ToString(txtContribution.Text);
            //string date = DateTime.Now.ToString("dd/MM/yyyy");

            //if (Session["RoleId"].ToString() == "HO")
            //{
            //    string territory;
            //    if (rdBtnTaegutec.Checked)
            //    {
            //        territory = "TAEGUTEC";
            //    }
            //    else
            //    {
            //        territory = "DURACARB";
            //    }
            //    sw.WriteLine("TERRITORY :" + "" + territory + "<br/>");
            //}

            //sw.WriteLine("FILTER BY :" + "" + FilterBy + "<br/>");
            //sw.WriteLine("FROM DATE :" + "" + Fromdate + "<br/>");
            //sw.WriteLine("TO DATE :" + "" + Todate + "<br/>");
            //sw.WriteLine("CONTRIBUTION :" + "" + Contribution + "<br/>");
            //sw.WriteLine("DATE :" + "" + date + "<br/>" + "<br/>");

            //GridView dtPenetrationReport = new GridView();
            //if (FilterBy.Equals("CUSTOMER"))
            //{
            //    dtPenetrationReport.DataSource = (DataTable)Session["dtCustomer"];
            //    dtPenetrationReport.DataBind();
            //    //dtPenetrationReport.HeaderRow.Cells[0].Text = grdCustomer.HeaderRow.Cells[0].Text;
            //    //dtPenetrationReport.HeaderRow.Cells[1].Text = grdCustomer.HeaderRow.Cells[1].Text;
            //    //dtPenetrationReport.HeaderRow.Cells[2].Text = grdCustomer.HeaderRow.Cells[2].Text;
            //    //dtPenetrationReport.HeaderRow.Cells[3].Text = grdCustomer.HeaderRow.Cells[3].Text;
            //}
            //else if (FilterBy.Equals("CHANNEL PARTNER"))
            //{
            //    dtPenetrationReport.DataSource = (DataTable)Session["dtCP"];
            //    dtPenetrationReport.DataBind();
            //    //dtPenetrationReport.HeaderRow.Cells[0].Text = grdChannelPartner.HeaderRow.Cells[0].Text;
            //    //dtPenetrationReport.HeaderRow.Cells[1].Text = grdChannelPartner.HeaderRow.Cells[1].Text;
            //    //dtPenetrationReport.HeaderRow.Cells[2].Text = grdChannelPartner.HeaderRow.Cells[2].Text;
            //    //dtPenetrationReport.HeaderRow.Cells[3].Text = grdChannelPartner.HeaderRow.Cells[3].Text;
            //}
            //else if (FilterBy.Equals("BRANCH"))
            //{

            //    dtPenetrationReport.DataSource = (DataTable)Session["dtBranch"];
            //    dtPenetrationReport.DataBind();
            //    //dtPenetrationReport.HeaderRow.Cells[0].Text = grdBranch.HeaderRow.Cells[0].Text;
            //    //dtPenetrationReport.HeaderRow.Cells[1].Text = grdBranch.HeaderRow.Cells[1].Text;
            //    //dtPenetrationReport.HeaderRow.Cells[2].Text = grdBranch.HeaderRow.Cells[2].Text;
            //}
            //else if (FilterBy.Equals("PRODUCT FAMILY"))
            //{
            //    dtPenetrationReport.DataSource = (DataTable)Session["dtProduct"];
            //    dtPenetrationReport.DataBind();
            //    //dtPenetrationReport.HeaderRow.Cells[0].Text = grdProduct.HeaderRow.Cells[0].Text;
            //    //dtPenetrationReport.HeaderRow.Cells[1].Text = grdProduct.HeaderRow.Cells[1].Text;
            //    //dtPenetrationReport.HeaderRow.Cells[2].Text = grdProduct.HeaderRow.Cells[2].Text;
            //}
            //else if (FilterBy.Equals("SALES ENGINEER"))
            //{
            //    dtPenetrationReport.DataSource = (DataTable)Session["dtSE"];
            //    dtPenetrationReport.DataBind();
            //}
            //dtPenetrationReport.RenderControl(ht);

            //Response.Write(sw.ToString());
            //Response.Flush();

            //Response.End();
            ////HttpContext.Current.ApplicationInstance.CompleteRequest();
            //ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "triggerPostGridLodedActions();", true);
            ////}
            ////catch (Exception ex)
            ////{
            ////    objFunc.LogError(ex);
            ////}
        }

        /// <summary>
        /// Author : Anamika
        /// Date : April 24, 2017
        /// Desc : Export to pdf
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void exportpdfbtn_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(txtFromDate.Text) && !string.IsNullOrEmpty(txtToDate.Text) && !string.IsNullOrEmpty(txtContribution.Text))
            {
                GridView gvPenetrationReport = new GridView();
                reports_Click(null, null);
                DataTable dtTotal = new DataTable();
                dtTotal = (DataTable)Session["Total"];
                Response.ContentType = "application/pdf";
                Response.AddHeader("content-disposition", "attachment;filename=PenetrationReport.pdf");
                Response.Cache.SetCacheability(HttpCacheability.NoCache);
                StringWriter sw = new StringWriter();
                HtmlTextWriter hw = new HtmlTextWriter(sw);

                string Fromdate = Convert.ToString(txtFromDate.Text);
                string Todate = Convert.ToString(txtToDate.Text);
                string FilterBy = Convert.ToString(ddlcustomertype.SelectedItem.Text);
                string Contribution = Convert.ToString(txtContribution.Text);
                string date = DateTime.Now.ToString("dd/MM/yyyy");

                if (Session["RoleId"].ToString() == "HO")
                {
                    string territory;
                    if (rdBtnTaegutec.Checked)
                    {
                        territory = "TAEGUTEC";
                    }
                    else
                    {
                        territory = "DURACARB";
                    }
                    sw.WriteLine("TERRITORY :" + "" + territory + "<br/>");
                }

                sw.WriteLine("FILTER BY :" + "" + FilterBy + "<br/>");
                sw.WriteLine("FROM DATE :" + "" + Fromdate + "<br/>");
                sw.WriteLine("TO DATE :" + "" + Todate + "<br/>");
                sw.WriteLine("CONTRIBUTION :" + "" + Contribution + "<br/>");
                sw.WriteLine("DATE :" + "" + date + "<br/>" + "<br/>");
                sw.WriteLine("<br/>" + "<br/>");
                sw.WriteLine("TOTAL COUNT :" + "" + Convert.ToString(dtTotal.Rows[0][0]) + "<br/>" + "<br/>");
                sw.WriteLine("TOTAL SALE :" + "" + Convert.ToString(dtTotal.Rows[0][1]) + "<br/>" + "<br/>");
                sw.WriteLine("TOTAL CONTRIBUTION :" + "" + Convert.ToString(dtTotal.Rows[0][2]) + "<br/>" + "<br/>");


                DataTable dtPDF = new DataTable();
                if (FilterBy.Equals("CUSTOMER"))
                {
                    //dtPDF. = grdProduct.GridLines;
                    //if (grdCustomer.HeaderRow != null)
                    //{
                    //    dtPDF.Rows.Add(grdProduct.HeaderRow);
                    //}
                    ////  add each of the data rows to the table
                    //foreach (GridViewRow row in grdProduct.Rows)
                    //{
                    //    dtPDF.Rows.Add(row);
                    //}
                    gvPenetrationReport.DataSource = (DataTable)Session["dtCustomer"];
                    gvPenetrationReport.DataBind();
                }
                else if (FilterBy.Equals("CHANNEL PARTNER"))
                {
                    gvPenetrationReport.DataSource = (DataTable)Session["dtCP"];
                    gvPenetrationReport.DataBind();
                }
                else if (FilterBy.Equals("BRANCH"))
                {

                    gvPenetrationReport.DataSource = (DataTable)Session["dtBranch"];
                    gvPenetrationReport.DataBind();
                }
                else if (FilterBy.Equals("PRODUCT FAMILY"))
                {
                    gvPenetrationReport.DataSource = (DataTable)Session["dtProduct"];
                    gvPenetrationReport.DataBind();
                }
                else if (FilterBy.Equals("SALES ENGINEER"))
                {
                    gvPenetrationReport.DataSource = (DataTable)Session["dtSE"];
                    gvPenetrationReport.DataBind();
                }

                gvPenetrationReport.RenderControl(hw);
                StringReader sr = new StringReader(sw.ToString());
                Document pdfDoc = new Document(PageSize.A4, 10f, 10f, 10f, 0f);
                HTMLWorker htmlparser = new HTMLWorker(pdfDoc);
                PdfWriter.GetInstance(pdfDoc, Response.OutputStream);
                pdfDoc.Open();
                htmlparser.Parse(sr);

                pdfDoc.Close();
                Response.Write(pdfDoc);
                //HttpContext.Current.Response.End();
                Response.End();
            }
        }

        /// <summary>
        /// Author : Anamika
        /// Date : April 17, 2017
        /// Desc : Loading the report based on value selected
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void byValueIn_CheckedChanged(Object sender, EventArgs e)
        {
            if (ValInThsnd.Checked)
            {
                byValueIn = 1000;

                if (tablesLoadedStatus == 1)
                {
                    reports_Click(null, null);
                }
            }

            if (ValInLakhs.Checked)
            {
                byValueIn = 100000;

                if (tablesLoadedStatus == 1)
                {
                    reports_Click(null, null);
                }
            }
        }

        /// <summary>
        /// Author : Anamika
        /// Date : April 21, 2017
        /// Desc : Storing the cter vale in session
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void rdBtnTaegutec_CheckedChanged(object sender, EventArgs e)
        {

            if (rdBtnTaegutec.Checked)
            {
                cter = "TTA";
                Session["cter"] = "TTA";

            }
            if (rdBtnDuraCab.Checked)
            {
                cter = "DUR";
                Session["cter"] = "DUR";

            }
            ClearGrid();
        }
        #endregion

        #region Methods
        /// <summary>
        /// Author : Anamika
        /// Date : April 17, 2017
        /// Desc : Clearing all the details
        /// </summary>
        public void ClearGrid()
        {
            grdCustomer.DataSource = null;
            grdCustomer.DataBind();
            grdChannelPartner.DataSource = null;
            grdChannelPartner.DataBind();
            grdBranch.DataSource = null;
            grdBranch.DataBind();
            grdProduct.DataSource = null;
            grdProduct.DataBind();
            grdSE.DataSource = null;
            grdSE.DataBind();
            tablesLoadedStatus = 0;
        }

        /// <summary>
        /// Author : Anamika
        /// Date : April 17, 2017
        /// Desc : Loading gridview with the datatable
        /// </summary>
        /// <param name="filter"></param>
        /// <param name="dtReport"></param>
        public void LoadGrid(string filter, DataTable dtReport)
        {
            if (filter == "Customer")
            {
                ClearGrid();
                Session["dtCustomer"] = dtReport;
                grdCustomer.DataSource = dtReport;
                grdCustomer.DataBind();
                tablesLoadedStatus = 1;
            }
            else if (filter == "CP")
            {
                ClearGrid();
                Session["dtCP"] = dtReport;
                grdChannelPartner.DataSource = dtReport;
                grdChannelPartner.DataBind();
                tablesLoadedStatus = 1;
            }
            else if (filter == "Branches")
            {
                ClearGrid();
                Session["dtBranch"] = dtReport;
                grdBranch.DataSource = dtReport;
                grdBranch.DataBind();
                tablesLoadedStatus = 1;
            }
            else if (filter == "Product")
            {
                ClearGrid();
                Session["dtProduct"] = dtReport;
                grdProduct.DataSource = dtReport;
                grdProduct.DataBind();
                tablesLoadedStatus = 1;
            }
            else if (filter == "SE")
            {
                ClearGrid();
                Session["dtSE"] = dtReport;
                grdSE.DataSource = dtReport;
                grdSE.DataBind();
                tablesLoadedStatus = 1;

            }
        }

        /// <summary>
        /// Author : Anamika
        /// Date : April 24, 2017
        /// Desc : Export to excel method
        /// </summary>
        private void ExportReport()
        {
            try
            {
                if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }
                reports_Click(null, null);
                Response.AddHeader("content-disposition", string.Format("attachment; filename={0}", "PenetrationReport.xls"));
                Response.ContentType = "application/ms-excel";
                string Fromdate = Convert.ToString(txtFromDate.Text);
                string Todate = Convert.ToString(txtToDate.Text);
                string FilterBy = Convert.ToString(ddlcustomertype.SelectedItem.Text);
                string Contribution = Convert.ToString(txtContribution.Text);
                string date = DateTime.Now.ToString("dd/MM/yyyy");



                using (StringWriter sw = new StringWriter())
                {
                    using (HtmlTextWriter htw = new HtmlTextWriter(sw))
                    {
                        //  Create a table to contain the grid
                        Table table = new Table();

                        //  include the gridline settings
                        if (FilterBy.Equals("CUSTOMER"))
                        {
                            table.GridLines = grdCustomer.GridLines;
                            if (grdCustomer.HeaderRow != null)
                            {
                                table.Rows.Add(grdCustomer.HeaderRow);
                            }
                            //  add each of the data rows to the table
                            foreach (GridViewRow row in grdCustomer.Rows)
                            {
                                table.Rows.Add(row);
                            }
                        }
                        else if (FilterBy.Equals("CHANNEL PARTNER"))
                        {
                            table.GridLines = grdChannelPartner.GridLines;
                            if (grdCustomer.HeaderRow != null)
                            {
                                table.Rows.Add(grdChannelPartner.HeaderRow);
                            }
                            //  add each of the data rows to the table
                            foreach (GridViewRow row in grdChannelPartner.Rows)
                            {
                                table.Rows.Add(row);
                            }
                        }
                        else if (FilterBy.Equals("BRANCH"))
                        {
                            table.GridLines = grdBranch.GridLines;
                            if (grdCustomer.HeaderRow != null)
                            {
                                table.Rows.Add(grdBranch.HeaderRow);
                            }
                            //  add each of the data rows to the table
                            foreach (GridViewRow row in grdBranch.Rows)
                            {
                                table.Rows.Add(row);
                            }
                        }
                        else if (FilterBy.Equals("PRODUCT FAMILY"))
                        {
                            table.GridLines = grdProduct.GridLines;
                            if (grdCustomer.HeaderRow != null)
                            {
                                table.Rows.Add(grdProduct.HeaderRow);
                            }
                            //  add each of the data rows to the table
                            foreach (GridViewRow row in grdProduct.Rows)
                            {
                                table.Rows.Add(row);
                            }
                        }
                        else if (FilterBy.Equals("SALES ENGINEER"))
                        {
                            table.GridLines = grdSE.GridLines;
                            if (grdCustomer.HeaderRow != null)
                            {
                                table.Rows.Add(grdSE.HeaderRow);
                            }
                            //  add each of the data rows to the table
                            foreach (GridViewRow row in grdSE.Rows)
                            {
                                table.Rows.Add(row);
                            }
                        }

                        //  add the header row to the table



                        //  render the table into the htmlwriter
                        if (Session["RoleId"].ToString() == "HO")
                        {
                            string territory;
                            if (rdBtnTaegutec.Checked)
                            {
                                territory = "TAEGUTEC";
                            }
                            else
                            {
                                territory = "DURACARB";
                            }
                            sw.WriteLine("TERRITORY :" + "" + territory + "<br/>");
                        }
                        sw.WriteLine("FILTER BY :" + "" + FilterBy + "<br/>");
                        sw.WriteLine("FROM DATE :" + "" + Fromdate + "<br/>");
                        sw.WriteLine("TO DATE :" + "" + Todate + "<br/>");
                        sw.WriteLine("CONTRIBUTION :" + "" + Contribution + "<br/>");
                        sw.WriteLine("DATE :" + "" + date + "<br/>" + "<br/>");
                        sw.WriteLine("<br/>" + "<br/>");
                        sw.WriteLine("TOTAL COUNT :" + "" + Convert.ToString(dsReport.Tables[0].Rows[0][0]) + "<br/>" + "<br/>");
                        sw.WriteLine("TOTAL SALE :" + "" + Convert.ToString(dsReport.Tables[0].Rows[0][1]) + "<br/>" + "<br/>");
                        sw.WriteLine("TOTAL CONTRIBUTION :" + "" + Convert.ToString(dsReport.Tables[0].Rows[0][2]) + "<br/>" + "<br/>");

                        table.RenderControl(htw);
                    }

                    Response.Write(sw.ToString());
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "triggerPostGridLodedActions();", true);

                    Response.End();

                }
            }
            catch (Exception ex)
            {
                objFunc.LogError(ex);
            }

        }
        #endregion

    }
}