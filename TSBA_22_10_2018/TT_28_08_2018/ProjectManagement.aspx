﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="ProjectManagement.aspx.cs" Inherits="TaegutecSalesBudget.ProjectManagement" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <link href="GridviewScroll.css" rel="stylesheet" />
       <script type="text/javascript" src="gridscroll.js"></script>
    <div class="crumbs">    <!-- Start : Breadcrumbs -->
<ul id="breadcrumbs" class="breadcrumb">
<li>
<i class="fa fa-home"></i>
<a>Admin</a>
</li>                       
<li class="current">Project Management</li>
</ul>
  </div>  <!-- End : Breadcrumbs -->
    <asp:ScriptManager ID="ProjectScriptManager" runat="server" AsyncPostBackTimeout="360">
</asp:ScriptManager>
    <br />
  <asp:UpdatePanel ID="ProjectUpdatePanel" runat="server"  >
    <ContentTemplate>
   
         <div  class="col-md-4 " >
             <div class="form-group">
                                           
                <div class="col-md-4">   
                   <asp:Button ID="updatebtn" runat="server" Text="UPDATE"  CssClass="btn green" OnClick="updatebtn_Click"  />          
                </div>
             </div>
         </div>
 <%--   ----GRID VIEW----START------%>
        </br>
        </br>
        </br>
        </br>
         </br>
        
    <div class="row" style="padding-left:15px">
                                    <%--class="col-md-5">--%>
          <asp:HiddenField runat="server" ID="hdn_CustomerNum"  />
          <asp:HiddenField runat="server" ID="hdn_projectnum"  />
<asp:GridView ID="projectList" runat="server" Width="100%" Style="text-align: center;" AutoGenerateColumns="false" ShowHeaderWhenEmpty="true" OnRowDataBound="projectList_RowDataBound" >
    <columns>
        <asp:TemplateField HeaderText="CUSTOMER">
            <HeaderTemplate>
                <asp:Label ID="customer_lbl"  runat="server" Text="CUSTOMER"></asp:Label>
                <asp:ImageButton ToolTip="sorting" Width="15px" runat="server" ImageUrl="~/images/sort_neutral.png" ID="sort_customer" OnClick="sort_customer_Click" />
            </HeaderTemplate>
            <ItemTemplate>
            <asp:Label  ID="customer_lbl" runat="server" Text='<%# Eval("customer_lbl") %>'></asp:Label>
            <asp:HiddenField runat="server" ID="hdn_customernumber" Value='<%# Eval("customer_num") %>'/>
            </ItemTemplate>
        </asp:TemplateField>

        <asp:TemplateField HeaderText="CHANNEL PARTNER" >
            <HeaderTemplate>
                <asp:Label ID="distributor_lbl"  runat="server" Text="CHANNEL PARTNER"></asp:Label>
                <asp:ImageButton ToolTip="sorting" Width="15px" runat="server" ImageUrl="~/images/sort_neutral.png" ID="sort_distributor_num" OnClick="sort_distributor_num_Click" />
            </HeaderTemplate>

            <ItemTemplate>
            <asp:Label  ID="distributor_lbl" runat="server" Text='<%# Eval("distributor_lbl") %>'></asp:Label>
            
            </ItemTemplate>
        </asp:TemplateField>
                                         
        <asp:TemplateField HeaderText="PROJECT TITLE" >
            <HeaderTemplate>
                <asp:Label ID="title_lbl"  runat="server" Text="PROJECT TITLE"></asp:Label>
                <asp:ImageButton ToolTip="sorting" Width="15px" runat="server" ImageUrl="~/images/sort_neutral.png" ID="sort_title" OnClick="sort_title_Click" />
            </HeaderTemplate>

            <ItemTemplate>
            <asp:Label ID="project_name"  runat="server" Text='<%# Eval("project_title") %>'></asp:Label>
            <asp:HiddenField runat="server" ID="hdn_projectnumber" Value='<%# Eval("project_num") %>'/>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="OWNER"  >
            <ItemTemplate>
               <asp:DropDownList runat="server" ID="ddlOwnerList"   ></asp:DropDownList>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="REVIEWER" >
            <ItemTemplate>
           <asp:DropDownList runat="server" ID="ddlReviewerList"  ></asp:DropDownList>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="ESCALATE TO"  >
            <ItemTemplate>
                <asp:DropDownList runat="server" ID="ddlEscalationList" ></asp:DropDownList>
            </ItemTemplate>
        </asp:TemplateField>
       
      
                                         
    </columns>
</asp:GridView>
</div>
        </ContentTemplate>
      <Triggers>
                     
                     <asp:AsyncPostBackTrigger ControlID="updatebtn" EventName="Click"/>
                    
      </Triggers>
  </asp:UpdatePanel>
    <asp:UpdateProgress id="updateProgress" runat="server">
    <ProgressTemplate>
        <div style="position: fixed; text-align: left; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.7;">
           
             <span style="border-width: 0px; position: fixed; padding: 50px; font-size: 30px; left: 40%; top: 40%; color:#fff" >Please wait</span>
        </div>
    </ProgressTemplate>
 </asp:UpdateProgress> 
    <%-- GRID VIEW END--%>
    <style type="text/css">
        td {
            height: 30px !important;
            padding-left: 10px;
            text-align: left;
        }
        th {
            background: #006780;
            color: #fff;
            font-weight: 600;
            font-size: 13px;
            border-color: #fff;
            height: 40px;
            padding-left: 10px;
            text-align: left;
        }
    
        
    </style>
    
    <script type="text/javascript">

        $(document).ready(function () {
            gridviewScrollTrigger();
            $(window).resize(function () {

                gridviewScrollTrigger();
            });
        });
        function gridviewScrollTrigger() {

            gridView1 = $('#MainContent_projectList').gridviewScroll({
                width: $(window).width() - 60,
                height: 500,
                railcolor: "#F0F0F0",
                barcolor: "#606060",
                barhovercolor: "#606060",
                bgcolor: "#F0F0F0",
                freezesize: 0,
                arrowsize: 30,
                varrowtopimg: "Images/arrowvt.png",
                varrowbottomimg: "Images/arrowvb.png",
                harrowleftimg: "Images/arrowhl.png",
                harrowrightimg: "Images/arrowhr.png",
                headerrowcount: 1,
                railsize: 16,
                barsize: 14,
                verticalbar: "auto",
                horizontalbar: "auto",
                wheelstep: 1,
            });
        }


</script>

    <style>
        td {
            height: 30px !important;
            padding-left: 10px;
             /*background: #fff;*/
            /*text-align: right;*/
        }
        th {
            background: #006780;
            color: #fff;
            font-weight: 600;
            font-size: 13px;
            border-color: #fff;
            height: 40px;
            padding-left: 10px;
            text-align: center;
        }
    </style>
</asp:Content>

