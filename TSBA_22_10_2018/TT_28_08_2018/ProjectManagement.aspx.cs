﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace TaegutecSalesBudget
{
    public partial class ProjectManagement : System.Web.UI.Page
    {
        MDP objMDP = new MDP();
        static DataTable dtprojectReport;
        public static bool potSort;
        static DataTable sortedReports = null;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }
            if (!IsPostBack)
            {
                proceed_Click(null, null);
            }
        }




        public void projectList_RowDataBound(object sender, GridViewRowEventArgs e)
        {

            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DropDownList ddlEscalate = (DropDownList)e.Row.FindControl("ddlEscalationList");
                DataTable dtHoDetails = new DataTable();
                dtHoDetails = objMDP.GetAllHO();
                if (dtHoDetails != null)
                {
                    DataTable dtDeatils = new DataTable();
                    dtDeatils.Columns.Add("HO_number", typeof(string));
                    dtDeatils.Columns.Add("HO_name", typeof(string));


                    for (int i = 0; i < dtHoDetails.Rows.Count; i++)
                    {
                        dtDeatils.Rows.Add(dtHoDetails.Rows[i].ItemArray[0].ToString(), dtHoDetails.Rows[i].ItemArray[1].ToString() + " " + "(" + dtHoDetails.Rows[i].ItemArray[0].ToString() + ")");
                    }
                    ddlEscalate.DataSource = dtDeatils;
                    ddlEscalate.DataTextField = "HO_name";
                    ddlEscalate.DataValueField = "HO_number";
                    ddlEscalate.DataBind();
                    ddlEscalate.SelectedValue = dtprojectReport.Rows[e.Row.RowIndex]["escalate_lbl"].ToString();


                }
                DropDownList ddlOwner = (DropDownList)e.Row.FindControl("ddlOwnerList");
                DataTable dtSEDetails = new DataTable();
                dtSEDetails = objMDP.GetAllSE();
                if (dtSEDetails != null)
                {
                    DataTable dtDeatils = new DataTable();
                    dtDeatils.Columns.Add("SE_number", typeof(string));
                    dtDeatils.Columns.Add("SE_name", typeof(string));


                    for (int i = 0; i < dtSEDetails.Rows.Count; i++)
                    {
                        dtDeatils.Rows.Add(dtSEDetails.Rows[i].ItemArray[0].ToString(), dtSEDetails.Rows[i].ItemArray[1].ToString() + " " + "(" + dtSEDetails.Rows[i].ItemArray[0].ToString() + ")");
                    }
                    ddlOwner.DataSource = dtDeatils;
                    ddlOwner.DataTextField = "SE_name";
                    ddlOwner.DataValueField = "SE_number";
                    ddlOwner.DataBind();
                    ddlOwner.SelectedValue = dtprojectReport.Rows[e.Row.RowIndex]["owner_lbl"].ToString();


                }
                DropDownList ddlReviewer = (DropDownList)e.Row.FindControl("ddlReviewerList");
                DataTable dtREDetails = new DataTable();
                dtREDetails = objMDP.GetAllReviewer();
                if (dtREDetails != null)
                {
                    DataTable dtDeatils = new DataTable();
                    dtDeatils.Columns.Add("RE_number", typeof(string));
                    dtDeatils.Columns.Add("RE_name", typeof(string));


                    for (int i = 0; i < dtREDetails.Rows.Count; i++)
                    {
                        dtDeatils.Rows.Add(dtREDetails.Rows[i].ItemArray[0].ToString(), dtREDetails.Rows[i].ItemArray[1].ToString() + " " + "(" + dtREDetails.Rows[i].ItemArray[0].ToString() + ")");
                    }
                    ddlReviewer.DataSource = dtDeatils;
                    ddlReviewer.DataTextField = "RE_name";
                    ddlReviewer.DataValueField = "RE_number";
                    ddlReviewer.DataBind();
                    ddlReviewer.SelectedValue = dtprojectReport.Rows[e.Row.RowIndex]["reviewer_lbl"].ToString();


                }

            }

        }


        protected void proceed_Click(object sender, EventArgs e)
        {
            dtprojectReport = new DataTable();
            string roleId = Session["RoleId"].ToString();
            string owner = "ALL";
            string reviewer = "ALL";
            string escalate = "ALL";
            string industry = "ALL";
            string subindustry = "ALL";
            string customer = "ALL";
            string branch = "ALL";
            string status = "ALL";
            string Outcome = "ALL";
            string from_target_date = "";
            string to_target_date = "";
            string from_created_date = "";
            string to_created_date = "";
            string Engineername = "";
            string customerClass = null;
            string strSearch = null;
            if (owner == "ALL")
            {
                owner = null;
            }
            if (reviewer == "ALL")
            {
                reviewer = null;
            }
            if (escalate == "ALL")
            {
                escalate = null;
            }
            if (industry == "ALL")
            {
                industry = null;
            }
            if (subindustry == "ALL")
            {
                subindustry = null;
            }
            if (customer == "ALL")
            {
                customer = null;
            }
            if (branch == "ALL")
            {
                branch = null;
            }
            if (status == "ALL")
            {
                status = null;
            }
            if (Outcome == "ALL")
            {
                Outcome = null;
            }
            if (from_target_date == "")
            {
                from_target_date = null;
            }
            if (to_target_date == "")
            {
                to_target_date = null;
            }
            if (from_created_date == "")
            {
                from_created_date = null;
            }
            if (to_created_date == "")
            {
                to_created_date = null;
            }
            if (Engineername == "")
            {
                Engineername = null;
            }
            DataTable projectReport = new DataTable();
            DataSet dsprojectReport = objMDP.getProjectReport(owner, reviewer, escalate, industry, subindustry, customer, customerClass, branch, status, from_target_date, to_target_date, from_created_date, to_created_date, strSearch, Engineername);
            if(dsprojectReport.Tables.Count>0)
            {
                projectReport = dsprojectReport.Tables[0];
            }
            dtprojectReport.Columns.Add("customer_num", typeof(string));
            dtprojectReport.Columns.Add("customer_lbl", typeof(string));
            dtprojectReport.Columns.Add("distributor_num", typeof(string));
            dtprojectReport.Columns.Add("distributor_lbl", typeof(string));
            dtprojectReport.Columns.Add("project_title", typeof(string));
            dtprojectReport.Columns.Add("project_num", typeof(string));
            dtprojectReport.Columns.Add("owner_lbl", typeof(string));
            dtprojectReport.Columns.Add("reviewer_lbl", typeof(string));
            dtprojectReport.Columns.Add("escalate_lbl", typeof(string));
            dtprojectReport.Columns.Add("Engineer_Name", typeof(string));


            for (int i = 0; i < projectReport.Rows.Count; i++)
            {
                string customer_num = projectReport.Rows[i].ItemArray[0].ToString();
                string customer_lbl = projectReport.Rows[i].ItemArray[1].ToString() + " (" + projectReport.Rows[i].ItemArray[0].ToString() + " )";
                string distributor_num = projectReport.Rows[i].ItemArray[19].ToString();
                string distributor_lbl = "";
                if (distributor_num != "")
                {
                    distributor_lbl = objMDP.get_distributorname_by_number(distributor_num) + " (" + projectReport.Rows[i].ItemArray[19].ToString() + " )";
                }
                string ptoject_title = projectReport.Rows[i].ItemArray[11].ToString() + " (" + projectReport.Rows[i].ItemArray[12].ToString() + " )";
                string project_num = projectReport.Rows[i].ItemArray[12].ToString();
                string owner_lbl = projectReport.Rows[i].ItemArray[15].ToString();
                string reviewer_lbl = projectReport.Rows[i].ItemArray[16].ToString();
                string escalate_lbl = projectReport.Rows[i].ItemArray[17].ToString();
                dtprojectReport.Rows.Add(customer_num, customer_lbl, distributor_num, distributor_lbl, ptoject_title, project_num, owner_lbl, reviewer_lbl, escalate_lbl);


            }
            projectList.DataSource = dtprojectReport;
            projectList.DataBind();
            bindgridColor();
            sortedReports = dtprojectReport;
        }
        protected void bindgridColor()
        {
            if (projectList.Rows.Count != 0)
            {
                int color = 0;

                foreach (GridViewRow row in projectList.Rows)
                {

                    color++;
                    if (color == 1) { row.CssClass = "color_Product1 "; }
                    else if (color == 2)
                    {
                        row.CssClass = "color_Product2 ";
                        color = 0;
                    }

                }
            }
        }

        protected void updatebtn_Click(object sender, EventArgs e)
        {
            if (Session["UserName"] == null) { Response.Redirect("Login.aspx?Login"); return; }

            foreach (GridViewRow row in projectList.Rows)
            {
                var cust_num = row.FindControl("hdn_customernumber") as HiddenField;
                var project_number = row.FindControl("hdn_projectnumber") as HiddenField;
                var ddlOwner = row.FindControl("ddlOwnerList") as DropDownList;
                var ddlReviewer = row.FindControl("ddlReviewerList") as DropDownList;
                var ddlEscalation = row.FindControl("ddlEscalationList") as DropDownList;

                var txtRemarks = row.FindControl("txt_remarks") as TextBox;
                if (row.RowIndex < projectList.Rows.Count)
                {
                    objMDP.admin_cust_num = cust_num.Value;
                    objMDP.admin_proj_num = project_number.Value;
                    objMDP.admin_owner = ddlOwner.SelectedItem.Value;
                    objMDP.admin_escalate = ddlEscalation.SelectedItem.Value;
                    objMDP.admin_reviewer = ddlReviewer.SelectedItem.Value;
                    string Messege = objMDP.update_project_authority(objMDP);
                }

            }
            proceed_Click(null, null);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "alert('Sucessfully Updated');", true);
        }

        #region Sorting
        protected void ReportGridView_Sorting(object sender, GridViewSortEventArgs e)
        {

            //Retrieve the table from the session object.
            DataTable dt = sortedReports.Copy();
            EnumerableRowCollection<DataRow> dr1 = null;
            //dt.Rows[dt.Rows.Count - 1].Delete();

            if (dt != null)
            {

                //Sort the data.

                if (GetSortDirection(e.SortDirection) == "ASC")
                {
                    dr1 = (from row in dt.AsEnumerable()

                           orderby row[e.SortExpression] ascending

                           select row);
                }
                else
                {
                    dr1 = (from row in dt.AsEnumerable()

                           orderby row[e.SortExpression] descending

                           select row);
                }
                DataTable dx = dr1.AsDataView().ToTable();
                //dx.Rows.Add(sortedReports.Rows[sortedReports.Rows.Count - 1].ItemArray);
                projectList.DataSource = dx;
                projectList.DataBind();


                bindgridColor();
            }

        }

        private string GetSortDirection(SortDirection sortDirection)
        {
            string newSortDirection = String.Empty;
            switch (sortDirection)
            {
                case SortDirection.Ascending:
                    newSortDirection = "ASC";
                    break;
                case SortDirection.Descending:
                    newSortDirection = "DESC";
                    break;
            }

            return newSortDirection;
        }

        protected void sort_title_Click(object sender, ImageClickEventArgs e)
        {

            if (potSort == false)
            {
                ReportGridView_Sorting(null, new GridViewSortEventArgs("project_title", SortDirection.Ascending));
                potSort = true;
            }
            else
            {
                ReportGridView_Sorting(null, new GridViewSortEventArgs("project_title", SortDirection.Descending));
                potSort = false;
            }

        }

        protected void sort_customer_Click(object sender, ImageClickEventArgs e)
        {

            if (potSort == false)
            {
                ReportGridView_Sorting(null, new GridViewSortEventArgs("customer_lbl", SortDirection.Ascending));
                potSort = true;
            }
            else
            {
                ReportGridView_Sorting(null, new GridViewSortEventArgs("customer_lbl", SortDirection.Descending));
                potSort = false;
            }

        }

        protected void sort_customer_num_Click(object sender, ImageClickEventArgs e)
        {

            if (potSort == false)
            {
                ReportGridView_Sorting(null, new GridViewSortEventArgs("customer_num", SortDirection.Ascending));
                potSort = true;
            }
            else
            {
                ReportGridView_Sorting(null, new GridViewSortEventArgs("customer_num", SortDirection.Descending));
                potSort = false;
            }

        }

        protected void sort_distributor_num_Click(object sender, ImageClickEventArgs e)
        {

            if (potSort == false)
            {
                ReportGridView_Sorting(null, new GridViewSortEventArgs("distributor_lbl", SortDirection.Ascending));
                potSort = true;
            }
            else
            {
                ReportGridView_Sorting(null, new GridViewSortEventArgs("distributor_lbl", SortDirection.Descending));
                potSort = false;
            }

        }









        #endregion
    }
}
