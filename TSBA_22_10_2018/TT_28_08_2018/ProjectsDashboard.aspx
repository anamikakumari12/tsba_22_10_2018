﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ProjectsDashboard.aspx.cs" Inherits="TaegutecSalesBudget.ProjectsDashboard" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server"></asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
     <asp:ScriptManager ID="ScriptManager1" runat="server" AsyncPostBackTimeout="360" EnableHistory="true" EnablePartialRendering="true" EnablePageMethods="true"></asp:ScriptManager>
   
   <asp:UpdatePanel ID="UpdatePanel1" runat="server">
      <ContentTemplate>
   <link href="GridviewScroll.css" rel="stylesheet" />
   <script type="text/javascript" src="gridscroll.js"></script>
    <link rel="stylesheet" href="http://cdn.kendostatic.com/2014.3.1316/styles/kendo.common.min.css" />
    <link rel="stylesheet" type="text/css" href="css/kendo.default.min.css" />
    <link rel="stylesheet" href="http://cdn.kendostatic.com/2014.3.1316/styles/kendo.dataviz.min.css" />
    <link rel="stylesheet" href="http://cdn.kendostatic.com/2014.3.1316/styles/kendo.dataviz.default.min.css" />
   
   <div class="crumbs" >
      <!-- Start : Breadcrumbs -->
      <ul id="breadcrumbs" class="breadcrumb">
         <li>
            <i class="fa fa-home"></i>
            <a>Project</a>
         </li>
         <li class="current">Dashboard</li>
         
      </ul>
   </div>
           <div id="collapsebtn" class="row">
                                <img id="product_image" src="images/up_arrow.png" align="left" style="margin-left: 46%;"/> 
                                                               
          </div>
      <div id="divSelections">
     <div id="divCter" style="margin-left:5px" runat="server" visible="false">
                  <ul  class="btn-info rbtn_panel">
                      <li ><span style="margin-right:4px;vertical-align:text-bottom;  ">TAEGUTEC</span>
                       
                        <asp:RadioButton ID="rdBtnTaegutec"  AutoPostBack="true"  Checked="true" OnCheckedChanged="rdBtnTaegutec_CheckedChanged"   GroupName="byCmpnyCodeInradiobtn" runat="server" />
                         <span style="margin-right:4px; margin-left:4px;vertical-align:text-bottom;">DURACARB</span>
                        <asp:RadioButton ID="rdBtnDuraCab"  AutoPostBack="true" OnCheckedChanged="rdBtnTaegutec_CheckedChanged"   GroupName="byCmpnyCodeInradiobtn" runat="server" />
                     </li>
                  </ul>
         
    </div>
          <div class="row" style="margin-left:2px">
               <div id="divBranch" runat="server" class="col-md-4">
               <div class="form-group">
                  <label class="col-md-4 control-label">BRANCH</label>
                  <div class="col-md-4">
                     <asp:DropDownList ID="ddlBranchList" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlBranchList_SelectedIndexChanged" 
                        CssClass="form-control select2" Width="230px">
                     </asp:DropDownList>
                  </div>
               </div>
            </div>
            <div id="divSalesEngnr" runat="server"  class="col-md-4">
               <div class="form-group">
                  <label class="col-md-4 control-label">SALES ENGINEER </label>
                  <div class="col-md-4" >
                     <asp:DropDownList ID="ddlSalesEngineerList" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlSalesEngineerList_SelectedIndexChanged"
                        CssClass="form-control" Width="230px" >
                        <asp:ListItem>--SELECT SALES ENGINEER--</asp:ListItem>
                     </asp:DropDownList>
                  </div>
               </div>
            </div>
               <div  class="col-md-4">
               <div class="form-group">
                  <label class="col-md-4 control-label">CUSTOMER CLASS </label>
                  <div class="col-md-4" >
                     <asp:DropDownList ID="ddlCustomerClass" runat="server" 
                        CssClass="form-control" Width="230px" >
                           <asp:ListItem Value="ALL">ALL</asp:ListItem>
                        <asp:ListItem Value="KA">KEY FOCUSED CUSTOMER</asp:ListItem>
                        <asp:ListItem Value="KF">‘A’ CLASS CUSTOMER</asp:ListItem>
                        <asp:ListItem Value="OT"> OTHERS</asp:ListItem>
                     </asp:DropDownList>
                  </div>
               </div>
            </div>
               <div  class="col-md-4 " >
               <div class="form-group">
                  <div >
                     <asp:Button ID="filter" runat="server" Text="FILTER" OnClick="filter_Click" CssClass="btn green" />
                  </div>
               </div>
            </div>
          </div>
          </div>
<div class="col-md-12" style="margin-top:12px">
    <div class="col-md-6"><asp:Label runat="server" id="Label13" style="float:right;margin-top:-15px" Text="Val In Lakhs"></asp:Label></div>
    <div class="col-md-6">        
      <asp:Label runat="server" id="vallbl" style="float:right;margin-top:-15px" Text="Val In Lakhs"></asp:Label></div>
    
    </div>
   <div class="col-md-6">
      <div class="portlet box grey">
         <div class="portlet-title">
            <div class="caption">PROJECT PERFORMANCE</div>
         </div>
        <div class="portlet-body shadow">
               <div class="portlet-body shadow">
                   <div class="col-md-4" style="margin-top:15px;">
                   <asp:Table ID="Table1" runat="server" CellPadding="5" class="table"  align="Center" rules="rows" border="1"
                    GridLines="horizontal" HorizontalAlign="Center">
                    <asp:TableRow>
                        <asp:TableCell style="background: #006780;color: #fff; font-weight:bold;">Total Project Potential Value</asp:TableCell>
                        <asp:TableCell  ID="lblprjcttotal">2</asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell style="background: #006780;color: #fff; font-weight:bold;">Completed Project Potential Value</asp:TableCell>
                        <asp:TableCell ID="lblcmpltdval">4</asp:TableCell>
                    </asp:TableRow>
                    </asp:Table>
                   </div>
                   <div class="col-md-8">
                      <asp:HiddenField ID="hdnfieldprojectval" runat="server" />
                        <div id="example" class="k-content">
                        <div id="gauge-container2" class="gauge-container" >
                            <div id="gauge" class="gauge" style="width: 300px; height: 180px;"></div>
                         <%--    <asp:Label style="float:left" runat="server" ></asp:Label>
                       <asp:Label  style="float:right" runat="server" ></asp:Label>--%>
                        </div>
                       
                        </div>
                       </div>
                      
                   <div >
                     <asp:Label ID="Label1" CssClass="" style="text-align:center;clear:both;padding-left:199px;font-size:14px;font-weight:600" runat="server" Text="PROJECTS VALUE COMPLETED"></asp:Label>
                   </div>
               </div>
           
    
         <%-- CONSOLIDATED BAR GRAPH START--%>  
               <div class="portlet-body shadow">
                   <asp:Chart ID="bargraphconsldtd" runat="server"  EnableViewState="true" Width="500px" Style="padding-top:10px;max-width:100%" >

                       <Series>
                           <asp:Series Name="Customer Potential" LabelForeColor="White"   IsValueShownAsLabel="true" LabelAngle="-90" CustomProperties="LabelStyle= Bottom, DrawingStyle=Cylinder" Color="#C73B0B" IsVisibleInLegend="true" ToolTip="Customer Potential : #VALY"  ChartType="Column"  ></asp:Series>
                       </Series>
                        <Series>
                           <asp:Series Name="Project Potential" LabelForeColor="White" IsValueShownAsLabel="true" LabelAngle="-90"  CustomProperties="LabelStyle= Bottom, DrawingStyle=Cylinder" Color="#F2C545" IsVisibleInLegend="true" ToolTip="Project Potential : #VALY"  ChartType="Column"  ></asp:Series>
                       </Series>
                        <Series>
                           <asp:Series Name="Business Expected" LabelForeColor="White" IsValueShownAsLabel="true" LabelAngle="-90"  CustomProperties="LabelStyle= Bottom, DrawingStyle=Cylinder" Color="#978E43" IsVisibleInLegend="true" ToolTip="Business Expected : #VALY"  ChartType="Column"  ></asp:Series>
                       </Series>
                        <Series>
                           <asp:Series Name="Monthly Business Expected" LabelForeColor="White" IsValueShownAsLabel="true" LabelAngle="-90"  CustomProperties="LabelStyle= Bottom, DrawingStyle=Cylinder" Color="#350608" IsVisibleInLegend="true" ToolTip="Monthly Business Expected : #VALY"  ChartType="Column"  ></asp:Series>
                       </Series>
                       <ChartAreas>
                           <asp:ChartArea Name="ChartArea1">
                               <AxisX  LabelAutoFitStyle="LabelsAngleStep45,LabelsAngleStep90,WordWrap" TitleFont="14">
                                   <LabelStyle Font="Serif" />
                                   <MajorGrid LineWidth="0" />
                               </AxisX>
                               <AxisY>
                                   <MajorGrid LineWidth="0" />
                               </AxisY>
                           </asp:ChartArea>
                       </ChartAreas>
                       <Legends>
                           <asp:Legend></asp:Legend>
                       </Legends>
                      <%-- <BorderSkin  
                        SkinStyle="Emboss" />--%>
                   </asp:Chart>
                   <asp:Label ID="Label11" CssClass="progresslbl" runat="server" style="padding-left:215px" Text="CONSOLIDATED VIEW"></asp:Label>
               </div>
             


          <%--CONSOLIDATED BAR GRAPH END--%>
          <%--BAR GRAPH START--%>
        <%-- <div class="background" style="width: 300px; height: 300px; white-space: nowrap; overflow-x: scroll; border: 0; padding: 10px;">--%>

               <div class="portlet-body shadow" >
                   <div class="noborder" style="overflow: auto;  height: 300px;z-index:9999">
                   <div class="noborder" style="max-width: 100%;">
                   <asp:Chart ID="bargraphIndustry" runat="server"   EnableViewState="true" style="padding-top:10px;margin-left:-65px" Width="2217px"  >
                      <Series>
                           <asp:Series Name="Customer Potential" ToolTip="Customer Potential : #VALY" LabelForeColor="White" IsValueShownAsLabel="true" LabelAngle="-90" CustomProperties="LabelStyle= Bottom, DrawingStyle=Cylinder" Color="#533419"    IsVisibleInLegend="true" ChartType="Column"  ></asp:Series>
                       </Series>
                        <Series>
                           <asp:Series Name="Project Potential" ToolTip="Project Potential : #VALY" LabelForeColor="White" IsValueShownAsLabel="true" LabelAngle="-90" CustomProperties="LabelStyle= Bottom, DrawingStyle=Cylinder" Color="#739240" IsVisibleInLegend="true" ChartType="Column"  ></asp:Series>
                       </Series>
                        <Series>
                           <asp:Series Name="Business Expected" ToolTip="Business Expected : #VALY" LabelForeColor="White" IsValueShownAsLabel="true" LabelAngle="-90" CustomProperties="LabelStyle= Bottom, DrawingStyle=Cylinder" Color="#C86000" IsVisibleInLegend="true"   ChartType="Column"  ></asp:Series>
                       </Series>
                        <Series>
                           <asp:Series Name="Monthly Business Expected" ToolTip="Monthly Business Expected : #VALY" LabelForeColor="White" IsValueShownAsLabel="true" LabelAngle="-90" CustomProperties="LabelStyle= Bottom, DrawingStyle=Cylinder" Color="#425227" IsVisibleInLegend="true"   ChartType="Column"  ></asp:Series>
                       </Series>
                       <ChartAreas>
                           <asp:ChartArea Name="ChartArea1">
                               <AxisX Interval="1" LabelAutoFitStyle="LabelsAngleStep45,LabelsAngleStep90,WordWrap" TitleFont="14" >
                                    <LabelStyle Angle="-90" Font="Serif" />
                                   <MajorGrid LineWidth="0" />
                               </AxisX>
                               <AxisY>
                                   <MajorGrid LineWidth="0" />
                               </AxisY>
                           </asp:ChartArea>
                       </ChartAreas>
                       <Legends>
                           <asp:Legend></asp:Legend>
                       </Legends>
                      <%-- <BorderSkin  
                        SkinStyle="Emboss" />--%>
                   </asp:Chart>
     </div>
                       </div>
                   <asp:Label ID="Label12" CssClass="progresslbl" runat="server" style="padding-left:225px;" Text="INDUSTRY VIEW"></asp:Label>
               </div>
           <%--  </div>   --%> 
            <div class="portlet-body shadow" >
                   <div class="noborder" style="overflow: auto;  height: 300px;z-index:9999">
                   <div class="noborder" style="max-width: 100%;">
                   <asp:Chart ID="bargraphProjectType" runat="server"   EnableViewState="true" style="padding-top:10px;margin-left:-65px" Width="2217px"  >
                      <Series>
                           <asp:Series Name="Potential For Closed Projects" ToolTip="Potential For Closed Projects : #VALY" LabelForeColor="White" IsValueShownAsLabel="true" LabelAngle="-90" CustomProperties="LabelStyle= Bottom, DrawingStyle=Cylinder" Color="#533419"    IsVisibleInLegend="true" ChartType="Column"  ></asp:Series>
                       </Series>
                        <Series>
                           <asp:Series Name="Potential For All Projects" ToolTip="Potential For All Projects : #VALY" LabelForeColor="White" IsValueShownAsLabel="true" LabelAngle="-90" CustomProperties="LabelStyle= Bottom, DrawingStyle=Cylinder" Color="#739240" IsVisibleInLegend="true" ChartType="Column"  ></asp:Series>
                       </Series>
                       <ChartAreas>
                           <asp:ChartArea Name="ChartArea1">
                               <AxisX Interval="1" LabelAutoFitStyle="LabelsAngleStep45,LabelsAngleStep90,WordWrap" TitleFont="14" >
                                    <LabelStyle Angle="-90" Font="Serif" />
                                   <MajorGrid LineWidth="0" />
                               </AxisX>
                               <AxisY>
                                   <MajorGrid LineWidth="0" />
                               </AxisY>
                           </asp:ChartArea>
                       </ChartAreas>
                       <Legends>
                           <asp:Legend></asp:Legend>
                       </Legends>
                      <%-- <BorderSkin  
                        SkinStyle="Emboss" />--%>
                   </asp:Chart>
     </div>
                       </div>
                   <asp:Label ID="Label14" CssClass="progresslbl" runat="server" style="padding-left:225px;" Text="PROJECT TYPE VIEW"></asp:Label>
               </div>
     
            </div>
       
          <%--BAR GRAPH END--%>
      </div>
   </div>
   <div class="col-md-6">
      <div class="portlet box grey" style="">
         <div class="portlet-title">
            <div class="caption">PROGRESS</div>
         </div>
         <div class="portlet-body shadow" >
            <div class="shadow" style="margin-top:3px;" >
           
               <div id="Div7"  style="width:100%;clear:both"   class="k-content">
                  <div id="Div8" class="gauge-container" style="width:50%; float:left;" >
                     <asp:Chart ID="chart_target" EnableViewState="true" runat="server" ViewStateMode="Enabled" Height="180px" Width="180px">
                        <Series>
                           <asp:Series Name="Series1" LabelForeColor="White" IsValueShownAsLabel="True"  ChartType="Pie" ChartArea="ChartArea1" Legend="Legend1"  ></asp:Series>
                        </Series>
                        <ChartAreas>
                           <asp:ChartArea Name="ChartArea1">
                              <Area3DStyle Enable3D="True" />
                           </asp:ChartArea>
                        </ChartAreas>
                       <%-- <Legends>
                           <asp:Legend Name="Legend1"></asp:Legend>
                        </Legends>--%>

                     </asp:Chart>
                  </div>

                  <div style="width:50%;float:right;padding-top:11px;" class="table" >
                     <asp:GridView ID="gridtrgtcmpltnprjcts" runat="server" AutoGenerateColumns="false" >
                        <columns>
                             <asp:TemplateField HeaderText=" " ItemStyle-Width="30">
                                 <ItemTemplate>
                                     
                                 <asp:Label ID="Image1" runat="server" Height = "10" CssClass="square" style="background:#399BFF;"  Visible = '<%# Eval("Target").ToString() == " <30 Days"? true : false %>' />
                                 <asp:Label ID="Label2" runat="server" Height = "10" CssClass="square" style="background:#26A65B;"  Visible = '<%# Eval("Target").ToString() == "31-60" ? true : false %>' />
                                 <asp:Label ID="Label3" runat="server" Height = "10" CssClass="square" style="background:#EF4836 ;"  Visible = '<%# Eval("Target").ToString() == "61-90" ? true : false %>' />
                                 <asp:Label ID="Label4" runat="server" Height = "10" CssClass="square" style="background:#F39C12 ;"  Visible = '<%# Eval("Target").ToString() == "91-180" ? true : false %>' />
                                 <asp:Label ID="Label5" runat="server" Height = "10" CssClass="square" style="background:rgb(203, 81, 58) ;"  Visible = '<%# Eval("Target").ToString() == ">180" ? true : false %>' />
                                
                                 </ItemTemplate>
                              </asp:TemplateField>
                           <asp:TemplateField HeaderText="Target" ItemStyle-Width="90"  ControlStyle-CssClass="hdrtxt"  >
                              <ItemTemplate >
                                 <asp:Label  ID="lbl1" runat="server" Text='<%# Eval("Target") %>' ></asp:Label>
                              </ItemTemplate>
                           </asp:TemplateField>
                           <asp:TemplateField HeaderText="Project Potential" ItemStyle-Width="90" ItemStyle-CssClass="valalign">
                              <ItemTemplate>
                                 <asp:Label  ID="lbl1" runat="server" Text='<%# Eval("Count") %>'></asp:Label>
                              </ItemTemplate>
                           </asp:TemplateField>
                           <asp:TemplateField HeaderText="%" ItemStyle-Width="90" ItemStyle-CssClass="valalign">
                              <ItemTemplate>
                                 <asp:Label  ID="lbl1" runat="server" Text='<%# Eval("Value") %>'></asp:Label>
                              </ItemTemplate>
                           </asp:TemplateField>
                        </columns>
                     </asp:GridView>
                  </div>
               </div>
               <div class="clearfix"></div>
                                <asp:Label ID="lbltrgtcmpltn" CssClass="progresslbl" runat="server" Text="POTENTIAL VALUE BY TARGETTED  DAYS"></asp:Label>
                                 </div>
               <%--STAGE PENDING PROJECTS START--%>
              <div class="portlet-body shadow" style="margin-top:3px;" >
                  <div id="Div1"  style="width:100%;clear:both"   class="k-content">
                     <div id="Div2" class="gauge-container" style="width:50%;float:left ;" >
                        <asp:Chart ID="chartPdngPrjcts" EnableViewState="true" runat="server" ViewStateMode="Enabled" Height="180px" Width="180px">
                           <Series>
                              <asp:Series Name="Series1" LabelForeColor="White" IsValueShownAsLabel="True"  ChartType="Pie" ChartArea="ChartArea1" Legend="Legend1"  ></asp:Series>
                           </Series>
                           <ChartAreas>
                              <asp:ChartArea Name="ChartArea1">
                                 <Area3DStyle Enable3D="True" />
                              </asp:ChartArea>
                           </ChartAreas>
                         <%--  <Legends>
                              <asp:Legend Name="Legend1"></asp:Legend>
                           </Legends>--%>
                        </asp:Chart>
                     </div>
                     <div style="width:50%;float:right;padding-top:11px;" class="table">
                        <asp:GridView ID="GridPdngPrjcts" runat="server" AutoGenerateColumns="false">
                           <columns>
                                <asp:TemplateField HeaderText=" " ItemStyle-Width="30">
                                 <ItemTemplate>
                                 <asp:Label ID="Image1" runat="server" Height = "10" CssClass="square" style="background:#399BFF;"  Visible = '<%# Eval("Target").ToString() == "Stage 1"? true : false %>' />
                                 <asp:Label ID="Label2" runat="server" Height = "10" CssClass="square" style="background:#26A65B;"  Visible = '<%# Eval("Target").ToString() == "Stage 2" ? true : false %>' />
                                 <asp:Label ID="Label3" runat="server" Height = "10" CssClass="square" style="background:#EF4836 ;"  Visible = '<%# Eval("Target").ToString() == "Stage 3" ? true : false %>' />
                                 <asp:Label ID="Label4" runat="server" Height = "10" CssClass="square" style="background:#F39C12 ;"  Visible = '<%# Eval("Target").ToString() == "Stage 4" ? true : false %>' />
                                 <asp:Label ID="Label5" runat="server" Height = "10" CssClass="square" style="background:#CB513A ;"  Visible = '<%# Eval("Target").ToString() == "Stage 5" ? true : false %>' />
                                 <asp:Label ID="Label6" runat="server" Height = "10" CssClass="square" style="background:#C7BAA7 ;"  Visible = '<%# Eval("Target").ToString() == "Stage 6" ? true : false %>' />
                                 <asp:Label ID="Label7" runat="server" Height = "10" CssClass="square" style="background:#FAA43A ;"  Visible = '<%# Eval("Target").ToString() == "Stage 7" ? true : false %>' />
                                 <asp:Label ID="Label8" runat="server" Height = "10" CssClass="square" style="background:#5DA5DA ;"  Visible = '<%# Eval("Target").ToString() == "Stage 8" ? true : false %>' />
                                 <asp:Label ID="Label9" runat="server" Height = "10" CssClass="square" style="background:#4D4D4D ;"  Visible = '<%# Eval("Target").ToString() == "Stage 9" ? true : false %>' />
                                 <asp:Label ID="Label10" runat="server" Height = "10" CssClass="square" style="background:#FF00FF;"  Visible = '<%# Eval("Target").ToString() == "Stage 10" ? true : false %>' />

                                 </ItemTemplate>
                              </asp:TemplateField>
                              <asp:TemplateField HeaderText="Stage" ItemStyle-Width="90">
                                 <ItemTemplate>
                                    <asp:Label  ID="lbl1"  runat="server" Text='<%# Eval("Target") %>'></asp:Label>
                                 </ItemTemplate>
                              </asp:TemplateField>
                              <asp:TemplateField HeaderText="Project Potential"  ItemStyle-Width="90" ItemStyle-CssClass="valalign">
                                 <ItemTemplate>
                                    <asp:Label  ID="lbl1" runat="server" Text='<%# Eval("Count") %>'></asp:Label>
                                 </ItemTemplate>
                              </asp:TemplateField>
                              <asp:TemplateField HeaderText="%" ItemStyle-Width="90" ItemStyle-CssClass="valalign">
                                 <ItemTemplate>
                                    <asp:Label  ID="lbl1" runat="server" Text='<%# Eval("Value") %>'></asp:Label>
                                 </ItemTemplate>
                              </asp:TemplateField>
                           </columns>
                        </asp:GridView>
                     </div>
                  </div>

              <div class="clearfix"></div>
                                                <asp:Label ID="lblPdngPrjcts" CssClass="progresslbl" runat="server" Text="STAGE-WISE STATUS OF PENDING PROJECTS"></asp:Label>

               </div>
               <%--   STAGE COMPLETION GRID END--%>
               <%--OVERDUE VALUE---START--%>
                           <div class="portlet-body shadow"  style="margin-top:3px;" >
                  <div id="Div3"  style="width:100%;clear:both;"   class="k-content">
                     <div id="Div4" class="gauge-container" style="width:50%;float:left;" >
                                                  <asp:Label ID="lblnodata" runat="server"  Visible="false" Text="No data to display"></asp:Label>

                        <asp:Chart ID="chartOverdue" EnableViewState="true" runat="server" ViewStateMode="Enabled" Height="180px" Width="180px">
                           <Series>
                              <asp:Series Name="Series1" LabelForeColor="White" IsValueShownAsLabel="True"  ChartType="Pie" ChartArea="ChartArea1" Legend="Legend1"  ></asp:Series>
                           </Series>
                           <ChartAreas>
                              <asp:ChartArea Name="ChartArea1">
                                 <Area3DStyle Enable3D="True" />
                              </asp:ChartArea>
                           </ChartAreas>
                          <%-- <Legends>
                              <asp:Legend Name="Legend1"></asp:Legend>
                           </Legends>--%>
                        </asp:Chart>
                     </div>
                     <div style="width:50%;float:right;padding-top:11px" class="table">
                        <asp:GridView ID="GridViewOverdue" runat="server" AutoGenerateColumns="false">
                           <columns>
                               <asp:TemplateField HeaderText=" " ItemStyle-Width="30">
                                 <ItemTemplate>
                                 <asp:Label ID="Image1" runat="server" Height = "10" CssClass="square" style="background:#399BFF;"  Visible = '<%# Eval("Target").ToString() == "Stage 1" ? true : false %>' />
                                 <asp:Label ID="Label2" runat="server" Height = "10" CssClass="square" style="background:#26A65B;"  Visible = '<%# Eval("Target").ToString() == "Stage 2" ? true : false %>' />
                                 <asp:Label ID="Label3" runat="server" Height = "10" CssClass="square" style="background:#EF4836 ;"  Visible = '<%# Eval("Target").ToString() == "Stage 3" ? true : false %>' />
                                 <asp:Label ID="Label4" runat="server" Height = "10" CssClass="square" style="background:#F39C12 ;"  Visible = '<%# Eval("Target").ToString() == "Stage 4" ? true : false %>' />
                                 <asp:Label ID="Label5" runat="server" Height = "10" CssClass="square" style="background:#CB513A;"  Visible = '<%# Eval("Target").ToString() == "Stage 5" ? true : false %>' />
                                 <asp:Label ID="Label6" runat="server" Height = "10" CssClass="square" style="background:#C7BAA7 ;"  Visible = '<%# Eval("Target").ToString() == "Stage 6" ? true : false %>' />
                                 <asp:Label ID="Label7" runat="server" Height = "10" CssClass="square" style="background:#FAA43A ;"  Visible = '<%# Eval("Target").ToString() == "Stage 7" ? true : false %>' />
                                 <asp:Label ID="Label8" runat="server" Height = "10" CssClass="square" style="background:#5DA5DA ;"  Visible = '<%# Eval("Target").ToString() == "Stage 8" ? true : false %>' />
                                 <asp:Label ID="Label9" runat="server" Height = "10" CssClass="square" style="background:#4D4D4D ;"  Visible = '<%# Eval("Target").ToString() == "Stage 9" ? true : false %>' />
                                 <asp:Label ID="Label10" runat="server" Height = "10" CssClass="square" style="background:#FF00FF;"  Visible = '<%# Eval("Target").ToString() == "Stage 10" ? true : false %>' />

                                 </ItemTemplate>
                              </asp:TemplateField>
                              <asp:TemplateField HeaderText="Stage" ItemStyle-Width="90">
                                 <ItemTemplate>
                                    <asp:Label  ID="lbl1" style="text-align:right" runat="server" Text='<%# Eval("Target") %>'></asp:Label>
                                 </ItemTemplate>
                              </asp:TemplateField>
                              <asp:TemplateField HeaderText="Project Potential"  ItemStyle-Width="90" ItemStyle-CssClass="valalign">
                                 <ItemTemplate>
                                    <asp:Label  ID="lbl1" style="text-align:right" runat="server" Text='<%# Eval("Count") %>'></asp:Label>
                                 </ItemTemplate>
                              </asp:TemplateField>
                              <asp:TemplateField HeaderText="%" ItemStyle-Width="90" ItemStyle-CssClass="valalign">
                                 <ItemTemplate>
                                    <asp:Label  ID="lbl1" style="text-align:right" runat="server" Text='<%# Eval("Value") %>'></asp:Label>
                                 </ItemTemplate>
                              </asp:TemplateField>
                           </columns>
                        </asp:GridView>
                     </div>
                  </div>

                 <div class="clearfix"></div>
                 <asp:Label ID="lbloverdueval" CssClass="progresslbl" runat="server" Text="STAGE-WISE OVERDUE VALUE OF PROJECTS"></asp:Label>
                               </div>
                 
               <%--   OVERDUE VALUE---END--%>
            </div>
         </div>
      </div>
   </div> </ContentTemplate>
      <Triggers>
         <asp:AsyncPostBackTrigger ControlID="rdBtnTaegutec" EventName="CheckedChanged" />
         <asp:AsyncPostBackTrigger ControlID="rdBtnDuraCab" EventName="CheckedChanged" />
          <asp:AsyncPostBackTrigger ControlID="ddlBranchList" EventName="SelectedIndexChanged" />
         <asp:AsyncPostBackTrigger ControlID="ddlSalesEngineerList" EventName="SelectedIndexChanged" />
         <asp:AsyncPostBackTrigger ControlID="filter" EventName="Click" />

      </Triggers>
   </asp:UpdatePanel>
 <div class="clearfix"></div>
   <div class="col-md-12">
      <div class="col-md-5"></div>
      <div class="col-md-2">
         <asp:HyperLink ID="HyperLink1" NavigateUrl="~/MDPEntry.aspx?MDPEntry" style="  width: 150px; margin-left: 15px; border-radius: 5px !important;" class="btn green"  runat="server">NEXT</asp:HyperLink>

      </div>
       
      <div class="col-md-5"></div>
   </div>
      
   <asp:UpdateProgress id="updateProgress" runat="server">
      <ProgressTemplate>
         <div style="position: fixed; text-align: left; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.7;">
            <span style="border-width: 0px; position: fixed; padding: 50px; font-size: 30px; left: 40%; top: 40%; color:#fff" >Please wait</span>
         </div>
      </ProgressTemplate>
   </asp:UpdateProgress>
   <style type="text/css">
      @media (min-width: 768px) and (max-width: 900px) {
      th, td {
      font-size: 9px;
      }
      }
      .table thead > tr > th, .table tbody > tr > th, .table tfoot > tr > th, .table thead > tr > td, .table tbody > tr > td, .table tfoot > tr > td 
      {
      border-top: 1px solid #ddd;
      line-height: 1.42857;
      padding: 2px !important;
      }
       td {
           height:0px !important;
           padding:0px !important;
       }
      .square {
	    width: 10px;
	    height: 10px;
	   margin-left: 10px;
    }
       .valalign  {
           text-align:right;
       }
      #MainContent_lblprjcttotal    {
           text-align:right;
       }
      #MainContent_lblcmpltdval  {
           text-align:right;
       }
       .th {
            text-align:center;
       }
       .gauge-container {
                 
				 margin: 0px auto;
         }

        .gauge {
                    width: 330px;
                    height: 330px;
                    margin: 0 auto 0;
                }

    .portlet.box > .portlet-body {
    background-color: #fff;
    padding: 12px !important;
    }
      .portlet > .portlet-title > .caption {
      display: inherit !important;
      float: none !important;
      font-size: 18px !important;
      margin: 0 0 4px !important;
      padding: 0 !important;
      text-align: center !important;
      }
    #header_fixed {
    display:none;
    position:fixed;
    top:50px;
    background-color:#006780;
    margin-left:15px;
    color:#fff;
    }
      th,td {
      border: 1px solid #ddd;
      }
       th {
           background:#006780;
           color:#fff;
           text-align:center;
       }
       .progresslbl {
           text-align:center;
           padding-left:185px;
           font-size:14px;
           font-weight:600;
       }
      #MainContent_gridtrgtcmpltnprjcts tr:nth-child(odd) {
    background: rgba(227,227,227,1);
}
            #MainContent_gridtrgtcmpltnprjcts tr:nth-child(even) {
    background: #f1f1f1;
}
  
            #MainContent_GridPdngPrjcts tr:nth-child(odd) {
    background: rgba(227,227,227,1);
}
            #MainContent_GridPdngPrjcts tr:nth-child(even) {
    background: #f1f1f1;
}
                  #MainContent_GridViewOverdue tr:nth-child(odd) {
    background: rgba(227,227,227,1);
}
                   #MainContent_GridViewOverdue tr:nth-child(even) {
    background:#f1f1f1;
}

       .shadow {
-webkit-box-shadow: 1px 2px 3px 2px rgba(227,227,227,1);
-moz-box-shadow: 1px 2px 3px 2px rgba(227,227,227,1);
box-shadow: 1px 2px 3px 2px rgba(227,227,227,1);
       }
      /*th {
      padding:3px;
      text-align:center;
      }
      td {
      padding:3px;
      }*/
      .HeadergridAll {
      text-align: center;
      background:#006780; color:#fff;
      }
      #MainContent_trgtdcmpltnprjcts th{
      padding:14px;
      }
      #MainContent_trgtdcmpltnprjcts  tr:last-child, #MainContent_stgcmpltnstatus  tr:last-child, #MainContent_GridOverdue  tr:last-child,
      #MainContent_stgcmpltnstatusFreeze tr:last-child,#MainContent_GridOverdueFreeze tr:last-child  {
      /*font-size: 14px;*/
      background-color: #fdf8e4;
      color: #666666;
      }
      .portlet.box > .portlet-body {
      border-radius:0px !important;
      }
       #MainContent_Table1 td {
           border:1px solid #999;
       }
       .caption1 { border-bottom: 1px solid #ccc;}
       /*.table thead > tr > th, .table tbody > tr > th, .table tfoot > tr > th, .table thead > tr > td, .table tbody > tr > td, .table tfoot > tr > td {
        border-top: 1px solid #ddd;
        line-height: 1.42857;
        padding:2px !important; 
        }*/
   </style>
    <script src="http://cdn.kendostatic.com/2014.3.1316/js/jquery.min.js"></script>
    <script src="http://cdn.kendostatic.com/2014.3.1316/js/kendo.all.min.js"></script>
    <script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.2/jquery-ui.min.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
        loadScript();
        $('#product_image').unbind('click').bind('click', function (e) {
            var attr = $('#product_image').attr('src');
            $("#divSelections").slideToggle();
            if (attr == "images/up_arrow.png") {
                $("#product_image").attr("src", "images/down_arrow.png");
            } else {
                $("#product_image").attr("src", "images/up_arrow.png");
            }
        });
    });
    function loadScript() {
        var pval = $('#MainContent_hdnfieldprojectval').val();
        $("#gauge").kendoRadialGauge({
            pointer: [{
                // Current value
                color: "#736F6E",
                value: pval,
            }],
            scale: {
                // Start and End angle of the Gauge
                startAngle: 0,
                endAngle: 180,



                // Configure major and minor unit
                //minorUnit: 0.2,
                //majorUnit: 0.2,
                // Make major ticks same size than minor ticks
                //majorTicks: {
                //    size: 10
                //},

                // Define min and max (0% - 100%)
                min: 0,
                max: 1,

                // Labels outside the range and number as percentage with no decimals
                labels: {
                    position: "outside",
                    format: "p0"
                },

                // Color ranges
                ranges: [
                    {
                        from: 0,
                        to: 0.33,
                        color: "#830300"
                    },
                    {
                        from: 0.33,
                        to: 0.66,
                        color: "yellow"
                    },
                    {
                        from: 0.66,
                        to: 1.00,
                        color: "green"
                    }
                ],
                rangeSize: 40,
                rangeDistance: -10,
            }
        });
        $('#product_image').unbind('click').bind('click', function (e) {
            var attr = $('#product_image').attr('src');
            $("#divSelections").slideToggle();
            if (attr == "images/up_arrow.png") {
                $("#product_image").attr("src", "images/down_arrow.png");
            } else {
                $("#product_image").attr("src", "images/up_arrow.png");
            }
        });
    }
</script>
</asp:Content>  