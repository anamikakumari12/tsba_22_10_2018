﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net;
using System.Net.Mail;
namespace TaegutecSalesBudget
{
    public partial class Recoverpassword : System.Web.UI.Page
    {
        string ErrorMessege = "failed in sending email";
        EmailPassword pwdemail = new EmailPassword();
        LoginAuthentication logauth = new LoginAuthentication();
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void resetbtn_Click(object sender, EventArgs e)
        {
            pwdemail.LoginMailID = uemail.Text.ToString();
            string ErrorMessage = pwdemail.sendmail(pwdemail);
            if (pwdemail.ErrorNum == 0)
            {
                try
                {
                    MailMessage email = new MailMessage();
                    string pwd = logauth.Decrypt(pwdemail.Password);
                    if (string.IsNullOrEmpty(pwd) && pwdemail.LoginMailID != null)
                    {
                        //if the password is null,create a new password
                        string  generatedPassword = PasswordSecurityGenerate.Generate(8, 8);
                        generatedPassword = logauth.Encrypt(generatedPassword);
                        string result = logauth.setPassword(uemail.Text.ToString(), generatedPassword);
                        if (result != "Success")
                        {
                            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Failed to  create password for the new user');", true);
                        }
                        else
                        {
                            pwd = logauth.Decrypt(generatedPassword);
                        }
                    }
                   
                    email.To.Add(new MailAddress(uemail.Text.ToString())); //Destination Recipient e-mail address.
                    email.Subject = "Sales-Budget & Performance Monitoring Password Reset";//Subject for your request
                    email.Body = " <br/><br/>Your Username: " + pwdemail.LoginMailID + "<br/><br/>Your Password: " +pwd + "<br/><br/>" + "From" + "<br/>" + "IT Team - TaeguTec"  ;
                    email.IsBodyHtml = true;
                    //SMTP SERVER DETAILS
                    SmtpClient smtpc = new SmtpClient();
                    smtpc.Send(email);

                    //string scriptString = "<script type='text/javascript'> alert('Login Details was sent to your email successfully');window.location='Login.aspx';</script>";
                    //ClientScriptManager script = Page.ClientScript;
                    //script.RegisterClientScriptBlock(GetType(), "Script", scriptString);
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect",
                    "alert('Login Details was sent to your email successfully'); window.location=' Login.aspx';", true);
                }
                catch (Exception ex)
                {
                    ErrorMessege = ex.Message;
                    ScriptManager.RegisterStartupScript(this, GetType(), "alert", "alert('Failed to send an email');", true);

                }
                finally
                {

                }

            }
            else if (pwdemail.ErrorNum == 1 || pwdemail.ErrorNum == 3)
            {
                //string scriptString = "<script type='text/javascript'> alert('Please provide your  registered email');</script>";
                //ClientScriptManager script = Page.ClientScript;
                //script.RegisterClientScriptBlock(GetType(), "Script", scriptString);
                ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Please provide your  registered email');", true);

            }
            else if (pwdemail.ErrorNum == 2)
            {
                //string scriptString = "<script type='text/javascript'> alert('Your account has been blocked');</script>";
                //ClientScriptManager script = Page.ClientScript;
                //script.RegisterClientScriptBlock(GetType(), "Script", scriptString);
                ScriptManager.RegisterStartupScript(this, GetType(), "showalerts", "alert('Your account has been blocked');", true);

            }
          
        }

        protected void btnBack_Click(object sender, EventArgs e)
        {
            Response.Redirect("Login.aspx");
        }

    }
}