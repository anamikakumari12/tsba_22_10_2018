﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Text;
using System.Web.UI.WebControls;
using System.Globalization;
using System.Web.UI.HtmlControls;
using System.IO;
using System.IO.Compression;
using System.Web.Services;
using Ionic.Zip;
using System.Drawing;

namespace TaegutecSalesBudget
{
    public partial class ReviewWeekly : System.Web.UI.Page
    {
        Reports objReports = new Reports();
        Budget objBudgets = new Budget();
        CommonFunctions objFunc = new CommonFunctions();
        AdminConfiguration objConfig = new AdminConfiguration();
        public static DataTable dtsalesbycust, dtTempCust, dtTempChanelP, dtTempCustTot, dtTempChanelPTot, dtGrandTotal;
        public static DataTable dtTotals, dtfamilyname, dtsalesfamily, dtfamilytotals, dtvalue;
        public static string salesengineernumber, customernumber, customertype, RoleID, strUserId, branchcode, cter;
        public static float byValueIn, budgetval, ytdval, arate;
        public static int tablesLoadedStatus, actual_mnth;
        public static bool cvflagsort;
        Review objRSum = new Review();
        public static int valuein;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }
            if (!IsPostBack)
            {
                hdnsearch.Value = "";
                string strUserId = Session["UserId"].ToString();
                string roleId = Session["RoleId"].ToString();
                LoadMonth();
                Thousand.Checked = true;
                export.Visible = false;
                datepicker.Text = System.DateTime.Now.ToString("dd/MM/yyyy");
                cter = null;
                valuein = 1000;
                if (Session["RoleId"].ToString() == "HO" || Session["RoleId"].ToString() == "TM")
                {
                    if (Session["RoleId"].ToString() == "HO")
                    {
                        if (Session["cter"] == null && roleId == "HO")
                        {
                            Session["cter"] = "TTA";
                            cter = "TTA";

                        }

                        if (Session["cter"].ToString() == "DUR")
                        {
                            rdBtnDuraCab.Checked = true;
                            rdBtnTaegutec.Checked = false;
                            cter = "DUR";
                        }
                        else
                        {
                            rdBtnTaegutec.Checked = true;
                            rdBtnDuraCab.Checked = false;
                            cter = "TTA";
                        }
                        cterDiv.Visible = true;
                    }
                    LoadBranches();
                    ddlBranchList_SelectedIndexChanged(null, null);
                }
                else if (Session["RoleId"].ToString() == "BM")
                {
                    string username = Session["UserName"].ToString();
                    string branchcode = Session["BranchCode"].ToString();
                    string branchDec = Session["BranchDesc"].ToString();
                    //bind branch
                    //ddlBranchList.Items.Insert(0, branchcode);
                    ddlBranchList.Items.Add(new ListItem(branchDec, branchcode));
                    divBranch.Visible = false;
                    // sales engineers loading
                    DataTable dtSalesEngDetails = objReports.LoadUserInfo(null, branchcode);
                    if (dtSalesEngDetails != null)
                    {
                        ddlSalesEngineerList.DataSource = dtSalesEngDetails;
                        ddlSalesEngineerList.DataTextField = "EngineerName";
                        ddlSalesEngineerList.DataValueField = "EngineerId";
                        ddlSalesEngineerList.DataBind();
                        //ddlSalesEngineerList.Items.Insert(0, "SELECT SALES ENGINEER");
                        ddlSalesEngineerList.Items.Insert(0, "ALL");
                    }
                }
                else if (Session["RoleId"].ToString() == "SE")
                {
                    string username = Session["UserName"].ToString();
                    string branchcode = Session["BranchCode"].ToString();
                    string branchDec = Session["BranchDesc"].ToString();
                    //bind branch
                    //ddlBranchList.Items.Insert(0, branchcode);
                    ddlBranchList.Items.Add(new ListItem(branchDec, branchcode));
                    divBranch.Visible = false;
                    //bind Sales engineer
                    //ddlSalesEngineerList.Items.Insert(0, strUserId);
                    ddlSalesEngineerList.Items.Add(new ListItem(username, strUserId));
                    divSE.Visible = false;
                }
            }
        }

       
         
        #region Filter drop downs loading
        protected void LoadMonth()
        {
            String sMonth = DateTime.Now.ToString("MM");
            //int DateNumber = Convert.ToInt32(sMonth.Substring(0, 2)) - 1;
            int DateNumber = DateTime.Now.Month;// objConfig.getActualMonth();
            if (DateNumber == 0)
            {
                DateNumber = 12;
            }

            for (int month = 1; month <= DateNumber; month++)
            {
                string monthName = DateTimeFormatInfo.CurrentInfo.GetMonthName(month);
                ddlMonth.Items.Add(new ListItem(monthName, month.ToString().PadLeft(2, '0')));
            }

            ddlMonth.SelectedValue = DateTime.Now.Month.ToString().PadLeft(2, '0');
            //ddlMonth.Items.Insert(0, "SELECT MONTH");
            //string message = "DropDown1: " + ddlMonth.SelectedItem.Text + " " + ddlMonth.SelectedItem.Value + "\\n";
        }

        protected void LoadBranches()
        {
            if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }
            string roleId = Session["RoleId"].ToString();
            string userId = Session["UserId"].ToString();
            string branchcode = Session["BranchCode"].ToString();
            DataTable dtData = new DataTable();
            objRSum.BranchCode = userId; // passing here territory Engineer Id  as branch code IF role is TM 
            objRSum.roleId = roleId;
            objRSum.flag = "Branch";
            objRSum.cter = cter;
            dtData = objRSum.getFilterAreaValue(objRSum);
            ddlBranchList.DataSource = dtData;
            ddlBranchList.DataTextField = "BranchDesc";
            ddlBranchList.DataValueField = "BranchCode";
            ddlBranchList.DataBind();
            ddlBranchList.Items.Insert(0, "ALL");
            ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "triggerPostGridLodedActions();", true);
        }

        protected void ddlBranchList_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }
            hdnsearch.Value = "";
            string roleId = Session["RoleId"].ToString();
            string userId = Session["UserId"].ToString();
            string branchcode = Session["BranchCode"].ToString();
            objRSum.BranchCode = roleId == "TM" && ddlBranchList.SelectedItem.Value == "ALL" ? userId : ddlBranchList.SelectedItem.Value;
            objRSum.roleId = roleId;
            objRSum.flag = "SalesEngineer";
            objRSum.cter = cter;
            DataTable dtData = objRSum.getFilterAreaValue(objRSum);

            if (dtData.Rows.Count != 0)
            {
                ddlSalesEngineerList.DataSource = dtData;
                ddlSalesEngineerList.DataTextField = "EngineerName";
                ddlSalesEngineerList.DataValueField = "EngineerId";
                ddlSalesEngineerList.DataBind();
                ddlSalesEngineerList.Items.Insert(0, "ALL");
            }
            else
            {
                ddlSalesEngineerList.DataSource = dtData;
                ddlSalesEngineerList.DataTextField = "EngineerName";
                ddlSalesEngineerList.DataValueField = "EngineerId";
                ddlSalesEngineerList.DataBind();
                ddlSalesEngineerList.Items.Insert(0, "NO SALES ENGINEER");
            }
            Session["targetreport"] = null;
            salesbycustomer.DataSource = null;
            salesbycustomer.DataBind();
            ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "triggerPostGridLodedActions();", true);
        }

        #endregion

        protected void reports_Click(object sender, EventArgs e)
        {
            hdnsearch.Value = "";
            LoadTargets();
        }              

        #region Export
        private static void PrepareControlForExport(Control control)
        {
            for (int i = 0; i < control.Controls.Count; i++)
            {
                Control current = control.Controls[i];
                if (current is LinkButton)
                {
                    control.Controls.Remove(current);
                    control.Controls.AddAt(i, new LiteralControl((current as LinkButton).Text));
                }
                else if (current is ImageButton)
                {
                    control.Controls.Remove(current);
                    control.Controls.AddAt(i, new LiteralControl((current as ImageButton).AlternateText));
                }
                else if (current is HyperLink)
                {
                    control.Controls.Remove(current);
                    control.Controls.AddAt(i, new LiteralControl((current as HyperLink).Text));
                }
                else if (current is DropDownList)
                {
                    control.Controls.Remove(current);
                    //control.Controls.AddAt(i, new LiteralControl((current as DropDownList).SelectedItem.Text));
                }
                else if (current is HiddenField)
                {
                    control.Controls.Remove(current);
                }
                else if (current is CheckBox)
                {
                    control.Controls.Remove(current);
                    // control.Controls.AddAt(i, new LiteralControl((current as CheckBox).Checked ? "True" : "False"));
                }
                if (current.HasControls())
                {
                    PrepareControlForExport(current);
                }
            }
        }

        private void ExportReport(DataTable dtNew, DataTable dtExport)
        {
            //review_overall();
            try
            {
                Response.AddHeader("content-disposition", string.Format("attachment; filename={0}", "ReviewWeekly.xls"));
                Response.ContentType = "application/ms-excel";
                //Response.ContentType = "application/vnd.ms-powerpoint";
                string branch = ddlBranchList.SelectedItem.Text;
                string salesenggname = ddlSalesEngineerList.SelectedItem.Text;
                string month = ddlMonth.SelectedItem.Text;
               // string date = datepicker.Text;
                string date = DateTime.Now.ToString("dd/MM/yyyy");
               
                using (StringWriter sw = new StringWriter())
                {
                    using (HtmlTextWriter htw = new HtmlTextWriter(sw))
                    {
                        //  Create a table to contain the grid
                        Table table = new Table();

                        //  include the gridline settings
                        table.GridLines = salesbycustomer.GridLines;
                        //  add the header row to the table
                        if (salesbycustomer.HeaderRow != null)
                        {
                            PrepareControlForExport(salesbycustomer.HeaderRow);
                            table.Rows.Add(salesbycustomer.HeaderRow);
                            salesbycustomer.DataSource = dtNew;
                        }
                        else
                        {
                            salesbycustomer.DataSource = null;
                        }
                        
                        salesbycustomer.DataBind();
                        
                        //  add each of the data rows to the table
                        foreach (GridViewRow row in salesbycustomer.Rows)
                        {
                            PrepareControlForExport(row);
                            table.Rows.Add(row);
                        }
                        table.Rows[0].Height = 40;
                        table.Rows[0].BackColor = Color.LightSeaGreen;
                        table.Rows[0].Font.Bold = true;

                        sw.WriteLine("<table><tr><td></td><td></td><td></td><td></td><td></td><td colspan=3 style='font-weight: bold; font-size:20px; '>Weekly Review report</td></table>");
                        sw.WriteLine("<table style='margin-left: 200px;'>");

                        if (Session["RoleId"].ToString() == "HO")
                        {
                            string territory;
                            if (rdBtnTaegutec.Checked)
                            {
                                territory = "TAEGUTEC";
                            }
                            else
                            {
                                territory = "DURACARB";
                            }
                            //  sw.WriteLine("TERRITORY :" + "" + territory + "<br/>");
                            sw.WriteLine("<tr><td></td><td></td><td></td><td></td><td></td><td style='font-weight: bold;'>TERRITORY:" + "</td><td colspan=8 style='font-style: italic;'>" + territory + "</td></tr>");
                        }
                        //  sw.WriteLine("BRANCH :" + "" + branch + "<br/>");
                        sw.WriteLine("<tr><td></td><td></td><td></td><td></td><td></td><td border='1px' style='font-weight:bold;'> BRANCH NAME :" + "</td><td colspan=8 style='font-style: italic;'>" + branch + "</td></tr>");
                        // sw.WriteLine("SALES ENGINEER NAME :" + "" + salesenggname + "<br/>");
                        sw.WriteLine("<tr><td></td><td></td><td></td><td></td><td></td><td border='1px' style='font-weight:bold;'> SALES ENGINEER NAME :" + "</td><td colspan=8 style='font-style: italic;'>" + salesenggname + "</td></tr>");

                        //sw.WriteLine("REVIEW MONTH :" + "" + month + "<br/>");
                        sw.WriteLine("<tr><td></td><td></td><td></td><td></td><td></td><td border='1px' style='font-weight:bold;'> REVIEW MONTH :" + "</td><td colspan=8 style='font-style: italic;'>" + month + "</td></tr>");

                        //  sw.WriteLine("DATE :" + "" + date + "<br/>");
                        //sw.WriteLine("<tr><td></td><td></td><td><td border='1px' style='font-weight:bold;'>  DATE :" + "</td><td colspan=8 style='font-style: italic;'>" + date + "</td></tr>");
                       // sw.WriteLine("</table><br/>");
                        if (!String.IsNullOrEmpty(hdnsearch.Value))
                        {
                            sw.WriteLine("<tr><td></td><td></td><td><td></td><td></td><td border='1px' style='font-weight:bold;'>SEARCH TEXT:" + "</td><td colspan=8 style='font-style:italic; text-align: left;'>" + hdnsearch.Value + "</td></tr>");
                        }
                            sw.WriteLine("</table><br/>");
                        
                        //if (Thousand.Checked)
                        //{
                        //    //sw.WriteLine("VALUE IN :" + "" + "THOUSANDS " + "<br/>");
                        //    sw.WriteLine("<tr><td></td><td></td><td><td border='1px' style='font-weight:bold;'>  VALUE IN :" + "</td><td colspan=8 style='font-style: italic;'>" + "" + "</td></tr>");
                        //}
                        //else
                        //{
                        //   // sw.WriteLine("VALUE IN :" + "" + "LAKHS " + "<br/>");
                        //    sw.WriteLine("<tr><td></td><td></td><td><td border='1px' style='font-weight:bold;'>  VALUE IN :" + "</td><td colspan=8 style='font-style: italic;'>" + "" + "</td></tr>");
                        //}
                       
                        table.RenderControl(htw);
                    }
                    Response.Write(sw.ToString());
                    DataRow dr = dtExport.AsEnumerable().SingleOrDefault(r => r.Field<string>("cred_code") == "CUSTOMER TOTAL : ");
                    if (dr != null)
                    {
                        dr.Delete();
                        dtExport.AcceptChanges();
                    }
                    dr = dtExport.AsEnumerable().SingleOrDefault(r => r.Field<string>("cred_code") == "CHANNEL PARTNER TOTAL : ");
                    if (dr != null)
                    {
                        dr.Delete();
                        dtExport.AcceptChanges();
                    }
                    dr = dtExport.AsEnumerable().SingleOrDefault(r => r.Field<string>("cred_code") == "GRAND TOTAL : ");
                    if (dr != null)
                    {
                        dr.Delete();
                        dtExport.AcceptChanges();
                    }
                   
                    salesbycustomer.DataSource = dtExport;
                    salesbycustomer.DataBind();
                    Response.End();
                }
            }
            catch (Exception ex)
            {
                objFunc.LogError(ex);
            }
        }
        public override void VerifyRenderingInServerForm(Control control)
        {
        }
        protected void export_Click(object sender, EventArgs e)
        {
            try
            {
                DataTable dt = (DataTable)Session["targetreport"];
                //Session["targetreport"] = null;
                //DataTable dt = new DataTable();
                DataTable dtNew = new DataTable();
                //DataRow dr;
                //dt.Columns.Add(new System.Data.DataColumn("cust_number", typeof(String)));
                //dt.Columns.Add(new System.Data.DataColumn("cust_name", typeof(String)));
                //dt.Columns.Add(new System.Data.DataColumn("customer_type", typeof(String)));
                //dt.Columns.Add(new System.Data.DataColumn("cred_code", typeof(String)));
                //dt.Columns.Add(new System.Data.DataColumn("sales_current_year", typeof(String)));
                //dt.Columns.Add(new System.Data.DataColumn("open_order", typeof(Int32)));
                //dt.Columns.Add(new System.Data.DataColumn("can_supply_now", typeof(Int32)));
                //dt.Columns.Add(new System.Data.DataColumn("targetval", typeof(Int32)));
                //dt.Columns.Add(new System.Data.DataColumn("ActualValue", typeof(Int32)));
                //dt.Columns.Add(new System.Data.DataColumn("variance", typeof(Int32)));
                //dt.Columns.Add(new System.Data.DataColumn("prorata", typeof(Int32)));

                //foreach (GridViewRow row in salesbycustomer.Rows)
                //{
                //    Label custnum = (Label)row.FindControl("Label1");
                //    Label custname = (Label)row.FindControl("Labelcust_name");
                //    string str1 = custname.Text;
                //    HiddenField custtype = (HiddenField)row.FindControl("hdn_custtype");
                //    string str = custtype.Value;
                //    Label creditcode = (Label)row.FindControl("Label9");
                //    Label salescurrentyear = (Label)row.FindControl("Label8");
                //    Label openorder = (Label)row.FindControl("Label6");
                //    Label cansupplynow = (Label)row.FindControl("Label7");
                //    Label targetmonth = (Label)row.FindControl("lbl_Targetvalue");
                //    Label actualmonth = (Label)row.FindControl("lbl_Actualsale");
                //    Label variance = (Label)row.FindControl("Label3");
                //    Label prorate = (Label)row.FindControl("Labe5");
                //    int var1 = 0;

                //    if (variance.Text.Contains("-"))
                //    {
                //        string var = variance.Text.Replace("-", "");
                //        var1 = Convert.ToInt32(var.Replace(",", ""))* -1;
                //    }
                //    else {
                //        var1 = Convert.ToInt32(variance.Text.Replace(",",""));
                //    }
                    

                //    dr = dt.NewRow();
                //    dr[0] = custnum.Text;
                //    dr[1] = custname.Text;
                //    dr[2] = custtype.Value;
                //    dr[3] = creditcode.Text;
                //    dr[4] = salescurrentyear.Text;
                //    dr[5] = Convert.ToInt32(openorder.Text.Replace(",", ""));
                //    dr[6] = Convert.ToInt32(cansupplynow.Text.Replace(",", ""));
                //    dr[7] = Convert.ToInt32(targetmonth.Text.Replace(",", ""));
                //    dr[8] = Convert.ToInt32(actualmonth.Text.Replace(",", ""));
                //    dr[9] = var1;
                //    dr[10] = Convert.ToInt32(prorate.Text);
                //    dt.Rows.Add(dr);
               //    }

                    //ExportReport(dtNew,dt);


                //dt.Rows[(dt.Rows.Count) - 1].Delete();
                //dt.AcceptChanges();
                //dt.Rows[(dt.Rows.Count) - 1].Delete();
                //dt.AcceptChanges();
                //DataRow dr = dt.AsEnumerable().SingleOrDefault(r => r.Field<string>("cust_name") == "CUSTOMER TOTAL");
                //if (dr != null)
                //{
                //    dr.Delete();
                //    dt.AcceptChanges();
                //}

                    DataTable dtcheck = CheckforSearchedData(dt);
                   // setsessions(dt);
                    dtNew = addGrandTotalToExcelTable(dtcheck);
                    ExportReport(dtNew,dt);
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "triggerPostGridLodedActions();", true);
                }
            
            catch (Exception ex)
            {
                objFunc.LogError(ex);
            }
        }
        private DataTable CheckforSearchedData(DataTable dt)
        {
            
            string search = hdnsearch.Value;
            List<string> searchList = new List<string>();
            DataRow[] filteredRows;
            try
            {
                if (!string.IsNullOrEmpty(search))
                {
                    if (search.Contains(' '))
                    {
                        searchList = search.Split(' ').ToList();
                    }
                    else
                    {
                        searchList.Add(search);
                    }

                    for (int i = 0; i < searchList.Count; i++)
                    {
                        DataTable dtOutput = new DataTable();
                        //dtOutput.Columns.Add("customer_type"); 
                        //dtOutput.Columns.Add("branch");
                        //dtOutput.Columns.Add("sales_engg");
                        dtOutput.Columns.Add("cust_number");
                        dtOutput.Columns.Add("cust_name");
                        dtOutput.Columns.Add("branch_code");
                        dtOutput.Columns.Add("salesengineer_name");
                        dtOutput.Columns.Add("customer_type");
                        dtOutput.Columns.Add("cred_code");
                        dtOutput.Columns.Add("sales_current_year");
                        dtOutput.Columns.Add("open_order");
                        dtOutput.Columns.Add("can_supply_now");
                        dtOutput.Columns.Add("targetval");
                        dtOutput.Columns.Add("ActualValue");
                        dtOutput.Columns.Add("variance");
                        dtOutput.Columns.Add("prorata");


                        filteredRows = dt.Select("CONVERT(customer_type, System.String) LIKE '%" + searchList[i].Trim()
                              + "%' OR CONVERT(cust_number, System.String) LIKE'%" + searchList[i].Trim()
                              + "%' OR CONVERT(branch_code, System.String) LIKE'%" + searchList[i].Trim()
                              + "%' OR CONVERT(salesengineer_name, System.String) LIKE'%" + searchList[i].Trim()
                              + "%' OR CONVERT(cust_name, System.String) LIKE'%" + searchList[i].Trim()
                              + "%' OR CONVERT(cred_code, System.String) LIKE'%" + searchList[i].Trim()
                              + "%' OR CONVERT(sales_current_year, System.String) LIKE'%" + searchList[i].Trim()
                              + "%' OR CONVERT(open_order, System.String) LIKE'%" + searchList[i].Trim()
                              + "%' OR CONVERT(can_supply_now, System.String) LIKE'%" + searchList[i].Trim()
                              + "%' OR CONVERT(targetval, System.String) LIKE '%" + searchList[i].Trim()
                              + "%' OR CONVERT(ActualValue, System.String)  LIKE '%" + searchList[i].Trim()
                              + "%' OR CONVERT(variance, System.String)  LIKE '%" + searchList[i].Trim()
                              + "%' OR CONVERT(prorata, System.String)  LIKE '%" + searchList[i].Trim()
                              + "%'");
                        foreach (DataRow dr in filteredRows)
                        {
                            //  dtOutput.ImportRow(dr);
                            dtOutput.Rows.Add(dr.ItemArray);
                        }
                        dt = dtOutput;
                    }

                }
                //else
                //{
                //    dtOutput = dt;
                //}
            }
            catch (Exception ex)
            {
                objFunc.LogError(ex);
            }

            return dt;
        }
        private DataTable addGrandTotalToExcelTable(DataTable dtAdd)
        {
            string search = hdnsearch.Value;
            try
             {
                 DataRow dr = dtAdd.NewRow();
                 DataRow dr1 = dtAdd.NewRow();
                 DataRow dr2 = dtAdd.NewRow();
               
                dr["cred_code"] = "CUSTOMER TOTAL : ";
                dr1["cred_code"] = "CHANNEL PARTNER TOTAL : ";
                dr2["cred_code"] = "GRAND TOTAL : ";

                if (!string.IsNullOrEmpty(search))
                {
                    if (dtAdd.AsEnumerable().Where(y => y.Field<string>("customer_type").Equals("C")) != null)
                    {

                        dr["sales_current_year"] = Convert.ToInt32(dtAdd.AsEnumerable().Where(y => y.Field<string>("customer_type").Equals("C")).Sum(x => Convert.ToDecimal(x["sales_current_year"])).ToString());
                        dr["open_order"] = Convert.ToInt32(dtAdd.AsEnumerable().Where(y => y.Field<string>("customer_type").Equals("C")).Sum(x => Convert.ToDecimal(x["open_order"])).ToString());
                        dr["can_supply_now"] = Convert.ToInt32(dtAdd.AsEnumerable().Where(y => y.Field<string>("customer_type").Equals("C")).Sum(x => Convert.ToDecimal(x["can_supply_now"])).ToString());
                        dr["targetval"] = Convert.ToInt32(dtAdd.AsEnumerable().Where(y => y.Field<string>("customer_type").Equals("C")).Sum(x => Convert.ToDecimal(x["targetval"])).ToString());
                        dr["ActualValue"] = Convert.ToInt32(dtAdd.AsEnumerable().Where(y => y.Field<string>("customer_type").Equals("C")).Sum(x => Convert.ToDecimal(x["ActualValue"])).ToString());
                        dr["variance"] = Convert.ToInt32(dtAdd.AsEnumerable().Where(y => y.Field<string>("customer_type").Equals("C")).Sum(x => Convert.ToDecimal(x["variance"])).ToString());
                        if (Convert.ToDecimal(dr["targetval"]) != 0)
                        dr["prorata"] = Math.Round(Convert.ToDecimal(dr["ActualValue"]) / Convert.ToDecimal(dr["targetval"]) * 100, 0);
                        else
                            dr["prorata"] = 0;
                    }
                    else {
                        dr["sales_current_year"] = 0;
                        dr["open_order"] = 0;
                        dr["can_supply_now"] = 0;
                        dr["targetval"] = 0;
                        dr["ActualValue"] = 0;
                        dr["variance"] = 0;
                        dr["prorata"] = 0;
                    }
                    if (dtAdd.AsEnumerable().Where(y => y.Field<string>("customer_type").Equals("D")) != null)
                    {
                        dr1["sales_current_year"] = Convert.ToInt32(dtAdd.AsEnumerable().Where(y => y.Field<string>("customer_type").Equals("D")).Sum(x => Convert.ToDecimal(x["sales_current_year"])).ToString());
                        dr1["open_order"] = Convert.ToInt32(dtAdd.AsEnumerable().Where(y => y.Field<string>("customer_type").Equals("D")).Sum(x => Convert.ToDecimal(x["open_order"])).ToString());
                        dr1["can_supply_now"] = Convert.ToInt32(dtAdd.AsEnumerable().Where(y => y.Field<string>("customer_type").Equals("D")).Sum(x => Convert.ToDecimal(x["can_supply_now"])).ToString());
                        dr1["targetval"] = Convert.ToInt32(dtAdd.AsEnumerable().Where(y => y.Field<string>("customer_type").Equals("D")).Sum(x => Convert.ToDecimal(x["targetval"])).ToString());
                        dr1["ActualValue"] = Convert.ToInt32(dtAdd.AsEnumerable().Where(y => y.Field<string>("customer_type").Equals("D")).Sum(x => Convert.ToDecimal(x["ActualValue"])).ToString());
                        dr1["variance"] = Convert.ToInt32(dtAdd.AsEnumerable().Where(y => y.Field<string>("customer_type").Equals("D")).Sum(x => Convert.ToDecimal(x["variance"])).ToString());
                        if (Convert.ToDecimal(dr1["targetval"]) != 0)
                        dr1["prorata"] = Math.Round(Convert.ToDecimal(dr1["ActualValue"]) / Convert.ToDecimal(dr1["targetval"]) * 100, 0);
                        else
                            dr1["prorata"] = 0;                           
                    }
                    else
                    {
                        dr1["sales_current_year"] = 0;
                        dr1["open_order"] = 0;
                        dr1["can_supply_now"] = 0;
                        dr1["targetval"] = 0;
                        dr1["ActualValue"] = 0;
                        dr1["variance"] = 0;
                        dr1["prorata"] = 0;
                    }
                    if (dtAdd.Rows.Count != null)
                    {
                        dr2["sales_current_year"] = Convert.ToInt32(dtAdd.AsEnumerable().Sum(x => Convert.ToDecimal(x["sales_current_year"])).ToString());
                        dr2["open_order"] = Convert.ToInt32(dtAdd.AsEnumerable().Sum(x => Convert.ToDecimal(x["open_order"])).ToString());
                        dr2["can_supply_now"] = Convert.ToInt32(dtAdd.AsEnumerable().Sum(x => Convert.ToDecimal(x["can_supply_now"])).ToString());
                        dr2["targetval"] = Convert.ToInt32(dtAdd.AsEnumerable().Sum(x => Convert.ToDecimal(x["targetval"])).ToString());
                        dr2["ActualValue"] = Convert.ToInt32(dtAdd.AsEnumerable().Sum(x => Convert.ToDecimal(x["ActualValue"])).ToString());
                        dr2["variance"] = Convert.ToInt32(dtAdd.AsEnumerable().Sum(x => Convert.ToDecimal(x["variance"])).ToString());
                        if (Convert.ToDecimal(dr2["targetval"]) != 0)
                            dr2["prorata"] = Math.Round(Convert.ToDecimal(dr2["ActualValue"]) / Convert.ToDecimal(dr2["targetval"]) * 100, 0);
                        else
                            dr2["prorata"] = 0;                      
                    }
                    else {
                        dr2["sales_current_year"] = 0;
                        dr2["open_order"] = 0;
                        dr2["can_supply_now"] = 0;
                        dr2["targetval"] = 0;
                        dr2["ActualValue"] = 0;
                        dr2["variance"] = 0;
                        dr2["prorata"] = 0;
                    }       
                }

                else
                {

                    dr["sales_current_year"] = Convert.ToDecimal(Session["sales_current_year"]);
                    dr1["sales_current_year"] = Convert.ToDecimal(Session["sales_current_year_cp"]);
                    dr2["sales_current_year"] = Convert.ToDecimal(Session["sales_current_year_total"]);

                    dr["open_order"] = Convert.ToDecimal(Session["open_order"]);
                    dr1["open_order"] = Convert.ToDecimal(Session["open_order_cp"]);
                    dr2["open_order"] =  Convert.ToDecimal(Session["open_order_total"]);

                    dr["can_supply_now"] = Convert.ToDecimal(Session["supply_now"]);
                    dr1["can_supply_now"] = Convert.ToDecimal(Session["supply_now_cp"]);
                    dr2["can_supply_now"] = Convert.ToDecimal(Session["supply_now_total"]);

                    dr["targetval"] = Convert.ToDecimal(Session["target_month"]);
                    dr1["targetval"] = Convert.ToDecimal(Session["target_month_cp"]);
                    dr2["targetval"] = Convert.ToDecimal(Session["target_month_total"]);

                    dr["ActualValue"] = Convert.ToDecimal(Session["actual_month"]);
                    dr1["ActualValue"] = Convert.ToDecimal(Session["actual_month_cp"]);
                    dr2["ActualValue"] = Convert.ToDecimal(Session["actual_month_total"]);

                    dr["variance"] = Convert.ToDecimal(Session["variance"]);
                    dr1["variance"] = Convert.ToDecimal(Session["variance_cp"]);
                    dr2["variance"] = Convert.ToDecimal(Session["variance_total"]);

                    dr["prorata"] = Convert.ToDecimal(Session["pro_rate"]);
                    dr1["prorata"] = Convert.ToDecimal(Session["pro_rate_cp"]);
                    dr2["prorata"] = Convert.ToDecimal(Session["pro_rate_total"]);

                }

                //dr["prorata"] =  ((Convert.ToDecimal(Session["ActualValue"]) / Convert.ToDecimal(Session["targetval"])) * 100);
                //dr1["prorata"] = ((Convert.ToDecimal(Session["ActualValue"]) / Convert.ToDecimal(Session["targetval"])) * 100);
                //dr2["prorata"] =((Convert.ToDecimal(Session["ActualValue"]) / Convert.ToDecimal(Session["targetval"])) * 100);
               
               //decimal targetval = 0;
               //if (Convert.ToDecimal("targetval") != 0)
               // dr["prorata"] = Convert.ToDecimal(dr["ActualValue"]) / Convert.ToDecimal(dr["targetval"]) * 100;

               // if (Convert.ToDecimal("targetval") != 0)              
               // dr1["prorata"] = Convert.ToDecimal(dr1["ActualValue"]) / Convert.ToDecimal(dr1["targetval"]) * 100 ;

               // if (Convert.ToDecimal("targetval") != 0)              
               // dr2["prorata"] = Convert.ToDecimal(dr2["ActualValue"]) / Convert.ToDecimal(dr2["targetval"]) * 100;


             //   decimal prorata = 0;
              //  if (Convert.ToDecimal("targetval") != 0)              
                 //  dr["prorata"] = Math.Round(Convert.ToDecimal(dr["ActualValue"]) / Convert.ToDecimal(dr["targetval"]) * 100, 0);     
               // if (Convert.ToDecimal("targetval") != 0)           
                 //  dr1["prorata"] = Math.Round(Convert.ToDecimal(dr1["ActualValue"]) / Convert.ToDecimal(dr1["targetval"]) * 100, 0);
               // if (Convert.ToDecimal("targetval") != 0)                       
                 //  dr2["prorata"] = Math.Round(Convert.ToDecimal(dr2["ActualValue"]) / Convert.ToDecimal(dr2["targetval"]) * 100, 0);          
                dtAdd.Rows.Add(dr);
                dtAdd.Rows.Add(dr1);
                dtAdd.Rows.Add(dr2);
                

                //dr["open_order"] = Convert.ToString(Session["open_order"]);
                //dr["can_supply_now"] = Convert.ToString(Session["supply_now"]);
                //dr["targetval"] = Convert.ToString(Session["target_month"]);
                //dr["ActualValue"] = Convert.ToString(Session["actual_month"]);
                //dr["variance"] = Convert.ToString(Session["variance"]);
                //dr["prorata"] = Convert.ToString(Session["pro_rate"]);

               // DataRow dr1 = dt.NewRow();
                //dr1["sales_current_year"] = "CHANNEL PARTNER TOTAL : ";
                //dr1["open_order"] = Convert.ToInt32(dt.AsEnumerable().Where(y => y.Field<string>("customer_type").Equals("D")).Sum(x => Convert.ToDecimal(x["open_order"])).ToString());
                //dr1["can_supply_now"] = Convert.ToInt32(dt.AsEnumerable().Where(y => y.Field<string>("customer_type").Equals("D")).Sum(x => Convert.ToDecimal(x["can_supply_now"])).ToString());
                //dr1["targetval"] = Convert.ToInt32(dt.AsEnumerable().Where(y => y.Field<string>("customer_type").Equals("D")).Sum(x => Convert.ToDecimal(x["targetval"])).ToString());
                //dr1["ActualValue"] = Convert.ToInt32(dt.AsEnumerable().Where(y => y.Field<string>("customer_type").Equals("D")).Sum(x => Convert.ToDecimal(x["ActualValue"])).ToString());
                //dr1["variance"] = Convert.ToInt32(dt.AsEnumerable().Where(y => y.Field<string>("customer_type").Equals("D")).Sum(x => Convert.ToDecimal(x["variance"])).ToString());
                //dr1["prorata"] = Convert.ToInt32(dt.AsEnumerable().Where(y => y.Field<string>("customer_type").Equals("D")).Sum(x => Convert.ToDecimal(x["prorata"])).ToString());
                //dt.Rows.Add(dr1);

                //DataRow dr2 = dt.NewRow();
                //dr2["sales_current_year"] = "CHANNEL PARTNER TOTAL : ";
                //dr2["open_order"] = Convert.ToInt32(dt.AsEnumerable().Sum(x => Convert.ToDecimal(x["open_order"])).ToString());
                //dr2["can_supply_now"] = Convert.ToInt32(dt.AsEnumerable().Sum(x => Convert.ToDecimal(x["can_supply_now"])).ToString());
                //dr2["targetval"] = Convert.ToInt32(dt.AsEnumerable().Sum(x => Convert.ToDecimal(x["targetval"])).ToString());
                //dr2["ActualValue"] = Convert.ToInt32(dt.AsEnumerable().Sum(x => Convert.ToDecimal(x["ActualValue"])).ToString());
                //dr2["variance"] = Convert.ToInt32(dt.AsEnumerable().Sum(x => Convert.ToDecimal(x["variance"])).ToString());
                //dr2["prorata"] = Convert.ToInt32(dt.AsEnumerable().Sum(x => Convert.ToDecimal(x["prorata"])).ToString());
                //dt.Rows.Add(dr2);

            }

            catch (Exception ex)
            {
                objFunc.LogError(ex);
            }
            return dtAdd;
        }

        protected override void SavePageStateToPersistenceMedium(Object pViewState)
        {
            Pair pair1;
            System.Web.UI.PageStatePersister pageStatePersister1 = this.PageStatePersister;
            Object ViewState;
            if (pViewState is Pair)
            {
                pair1 = ((Pair)pViewState);
                pageStatePersister1.ControlState = pair1.First;
                ViewState = pair1.Second;
            }
            else
            {
                ViewState = pViewState;
            }

            LosFormatter mFormat = new LosFormatter();
            StringWriter mWriter = new StringWriter();
            mFormat.Serialize(mWriter, ViewState);
            String mViewStateStr = mWriter.ToString();
            byte[] pBytes = System.Convert.FromBase64String(mViewStateStr);
            pBytes = Compress(pBytes);
            String vStateStr = System.Convert.ToBase64String(pBytes);
            pageStatePersister1.ViewState = vStateStr;
            pageStatePersister1.Save();
        }

        #endregion

        #region Compression
        private byte[] Compress(byte[] b)
        {
            MemoryStream ms = new MemoryStream();
            GZipStream zs = new GZipStream(ms, CompressionMode.Compress, true);
            zs.Write(b, 0, b.Length);
            zs.Close();
            return ms.ToArray();
        }

        /// This method takes the compressed byte stream as parameter
        /// and return a decompressed bytestream.

        private byte[] Decompress(byte[] b)
        {
            MemoryStream ms = new MemoryStream();
            GZipStream zs = new GZipStream(new MemoryStream(b),
                                           CompressionMode.Decompress, true);
            byte[] buffer = new byte[4096];
            int size;
            while (true)
            {
                size = zs.Read(buffer, 0, buffer.Length);
                if (size > 0)
                    ms.Write(buffer, 0, size);
                else break;
            }
            zs.Close();
            return ms.ToArray();
        }

        protected override object LoadPageStateFromPersistenceMedium()
        {
            System.Web.UI.PageStatePersister pageStatePersister1 = this.PageStatePersister;
            pageStatePersister1.Load();
            String vState = pageStatePersister1.ViewState.ToString();
            byte[] pBytes = System.Convert.FromBase64String(vState);
            pBytes = Decompress(pBytes);
            LosFormatter mFormat = new LosFormatter();
            Object ViewState = mFormat.Deserialize(System.Convert.ToBase64String(pBytes));
            return new Pair(pageStatePersister1.ControlState, ViewState);
        }
        #endregion

        #region valuesIn 000'/00000
        protected void Thousand_CheckedChanged(object sender, EventArgs e)
        {
            valuein = 1000;
            hdnsearch.Value = "";
            LoadTargets();
            int BudgetYear = objConfig.getBudgetYear();
            ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "triggerPostGridLodedActions();", true);
        }

        protected void Lakhs_CheckedChanged(object sender, EventArgs e)
        {
            valuein = 100000;
            hdnsearch.Value = "";
            LoadTargets();
            int BudgetYear = objConfig.getBudgetYear();

            ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "triggerPostGridLodedActions();", true);
        }
        #endregion

        #region Grid Events
        protected void salesbycustomer_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                var lblTargetvalue = e.Row.FindControl("lbl_Targetvalue") as Label;
                var lblActualsale = e.Row.FindControl("lbl_Actualsale") as Label;
                var lbl_customername = e.Row.FindControl("Labelcust_name") as Label;
                if (lbl_customername != null)
                    if (!lbl_customername.Text.ToString().Contains("TOTAL"))
                        if (lblTargetvalue != null)
                        {
                            if (lblTargetvalue.Text == "0" && (lblActualsale.Text != "0"))
                            {
                                e.Row.Cells[7].CssClass = "tdHighlight";
                            }
                        }
            }
        }
        //commented by Neha
        //protected void ibtn_ProRataPerSort_Click(object sender, ImageClickEventArgs e)
        //{

        //    if (cvflagsort == false)
        //    {
        //        salesbycustomer_Sorting(null, new GridViewSortEventArgs("prorata", SortDirection.Ascending));
        //        cvflagsort = true;
        //    }
        //    else
        //    {
        //        salesbycustomer_Sorting(null, new GridViewSortEventArgs("prorata", SortDirection.Descending));
        //        cvflagsort = false;
        //    }
        //    ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "triggerPostGridLodedActions();", true);
        //}

        protected void salesbycustomer_Sorting(object sender, GridViewSortEventArgs e)
        {
            //dtTempCust, dtTempChanelP, dtTempCustTot, dtTempChanelPTot;
            DataTable dt = new DataTable();
            if (dtTempCust != null)
            {
                DataView dataView = new DataView(dtTempCust);
                dataView.Sort = e.SortExpression + " " + ConvertSortDirection(e.SortDirection);
                dt = dataView.ToTable();
                dt.Merge(dtTempCustTot);

            }
            if (dtTempChanelP != null)
            {
                DataTable dtdlr = new DataTable();
                DataView dataView = new DataView(dtTempChanelP);
                dataView.Sort = e.SortExpression + " " + ConvertSortDirection(e.SortDirection);
                dtdlr = dataView.ToTable();
                dtdlr.Merge(dtTempChanelPTot);
                dt.Merge(dtdlr);
            }

            dt.Merge(dtGrandTotal);
            salesbycustomer.DataSource = dt;
            salesbycustomer.DataBind();
            ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "triggerPostGridLodedActions();", true);

        }

        private string ConvertSortDirection(SortDirection sortDirection)
        {
            string newSortDirection = String.Empty;
            switch (sortDirection)
            {
                case SortDirection.Ascending:
                    newSortDirection = "ASC";
                    break;
                case SortDirection.Descending:
                    newSortDirection = "DESC";
                    break;
            }

            return newSortDirection;
        }

        #endregion

        #region Taegutec/Duracarb
        protected void rdBtnTaegutec_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "triggerPostGridLodedActions();$('#alertmsg').show().delay(5000).fadeOut();", true);
                hdnsearch.Value = "";
                if (rdBtnTaegutec.Checked)
                {
                    Session["cter"] = "TTA";
                    cter = "TTA";
                }
                if (rdBtnDuraCab.Checked)
                {
                    Session["cter"] = "DUR";
                    cter = "DUR";
                }
                LoadBranches();
                ddlBranchList_SelectedIndexChanged(null, null);
                Session["targetreport"] = null;
                salesbycustomer.DataSource = null;
                salesbycustomer.DataBind();
                //salesbycustomer_total.DataSource = null;
                //salesbycustomer_total.DataBind();
                export.Visible = false;
            }
            catch (Exception ex)
            {
                objFunc.LogError(ex);
            }
        }

        #endregion

        #region Targets Loading

        public void LoadTargets()
        {
            try
            {
                string months = ddlMonth.SelectedItem.Value;
                DateTime currentyear = DateTime.Now;
                string year = currentyear.Year.ToString();
                string yearmonth = year + months;
                int month = Convert.ToInt32(yearmonth);
                DataTable dtcustomer = new DataTable();

                string roleId = Session["RoleId"].ToString();
                string userId = Session["UserId"].ToString();
                string branchcode = Session["BranchCode"].ToString();

                objRSum.BranchCode = (roleId == "TM" && ddlBranchList.SelectedItem.Value == "ALL" ? userId : ddlBranchList.SelectedItem.Value);
                if (objRSum.BranchCode == "ALL") { objRSum.BranchCode = null; }

                objRSum.salesengineer_id = (roleId == "TM" && ddlSalesEngineerList.SelectedItem.Value == "ALL" ? userId : ddlSalesEngineerList.SelectedItem.Value);
                if (objRSum.salesengineer_id == "ALL") { objRSum.salesengineer_id = null; }

                objRSum.customer_type = "C";
                objRSum.valuein = valuein;
                objRSum.cter = cter;
                int target_year_month = month;

                //Customer Data
                dtcustomer = objRSum.getTargets(objRSum, target_year_month);
               //dtcustomer = addCulterInfo(dtcustomer);
                dtTempCust = dtcustomer;

                //customer total
                DataTable dtCustToatal = new DataTable();
                dtCustToatal = objRSum.getTargets_total(objRSum, target_year_month);
                // dtCustToatal = addCulterInfo(dtCustToatal);
                dtTempCustTot = dtCustToatal;

                //Chanel partner Data
                DataTable dtChanelpartner = new DataTable();
                objRSum.customer_type = "D";
                dtChanelpartner = objRSum.getTargets(objRSum, target_year_month);
               // dtChanelpartner = addCulterInfo(dtChanelpartner);
                dtTempChanelP = dtChanelpartner;

                //Channel partner total6
                DataTable dtCPTotal = new DataTable();
                dtCPTotal = objRSum.getTargets_total(objRSum, target_year_month);
             //   dtCPTotal = addCulterInfo(dtCPTotal);
                dtTempChanelPTot = dtCPTotal;

                //Overall Total
                DataTable dttotal = new DataTable();
                objRSum.customer_type = null;
                dttotal = objRSum.getTargets_total(objRSum, target_year_month);
           //   dttotal = addCulterInfo(dttotal);
                dtGrandTotal = dttotal;

                DataTable dtTargets = new DataTable();
                DataSet dsprojectReport = new DataSet();
                dtTargets = dtcustomer.Clone();
                // add D and C here through column
               //dtChanelpartner.Columns.Add("C");

                dtTargets.Merge(dtcustomer);
                dtTargets.Merge(dtCustToatal);
                dtTargets.Merge(dtChanelpartner);
                dtTargets.Merge(dtCPTotal);
                dtTargets.Merge(dttotal);

                DataTable dtAllTotal = new DataTable();
                dtAllTotal = dtCPTotal.Clone();
                dtAllTotal.Merge(dtCustToatal);
                dtAllTotal.Merge(dtCPTotal);
                dtAllTotal.Merge(dttotal);

                #region Culture
                //dtTargets = addCulterInfo(dtTargets);
                //dtAllTotal = addCulterInfo(dtAllTotal);
                #endregion

                if (dtcustomer.Rows.Count != 0 || dtChanelpartner.Rows.Count != 0)
                {
                    //// 3 exterarowsdeleted here
                    if (dtTargets.Rows.Count > 0)
                    {
                        dtTargets.Rows[(dtTargets.Rows.Count) - 1].Delete();
                        dtTargets.AcceptChanges();
                        dtTargets.Rows[(dtTargets.Rows.Count) - 1].Delete();
                        dtTargets.AcceptChanges();
                        DataRow dr = dtTargets.AsEnumerable().SingleOrDefault(r => r.Field<string>("cust_name") == "CUSTOMER TOTAL");
                        if (dr != null)
                        {
                            dr.Delete();
                            dtTargets.AcceptChanges();
                        }

                        //DataRow dr1 = dtcustomer.AsEnumerable().SingleOrDefault(r => r.Field<string>("cust_name") == "GRAND TOTAL");
                        //if (dr1 != null)
                        //{
                        //    dr1.Delete();
                        //    dtTargets.AcceptChanges();
                        //}
                        //DataRow dr2 = dtcustomer.AsEnumerable().SingleOrDefault(r => r.Field<string>("cust_name") == "CHANNEL PARTNER TOTAL");
                        //if (dr2 != null)
                        //{
                        //    dr2.Delete();
                        //    dtTargets.AcceptChanges();
                        //}
                        Session["targetreport"] = dtTargets;
                        salesbycustomer.DataSource = dtTargets;
                        salesbycustomer.DataBind();
                        setsessions(dtCustToatal, dtCPTotal, dttotal);

                        // salesbycustomer_total.DataSource = dtAllTotal;
                       //salesbycustomer_total.DataBind();
                        export.Visible = true;
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "triggerPostGridLodedActions();", true);
                    }
                }
                    else
                    {
                        Session["targetreport"] = null;
                        salesbycustomer.DataSource = null;
                        salesbycustomer.DataBind();
                        //salesbycustomer_total.DataSource = null;
                        //salesbycustomer_total.DataBind();
                        export.Visible = false;
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "triggerPostGridLodedActions();alert('No data found');", true);
                    }                
            }

            catch (Exception ex)
            {
                objFunc.LogError(ex);
            }
        }

        private void setsessions(DataTable dtCustToatal,DataTable dtCPTotal,DataTable dttotal)
        {
            try
            {
                Session["sales_current_year"] = 0;
                Session["open_order"] = 0;
                Session["supply_now"] = 0;
                Session["target_month"] = 0;
                Session["actual_month"] = 0;
                Session["variance"] = 0;
                Session["pro_rate"] = 0;

                Session["sales_current_year_cp"] = 0;
                Session["open_order_cp"] = 0;
                Session["supply_now_cp"] = 0;
                Session["target_month_cp"] = 0;
                Session["actual_month_cp"] = 0;
                Session["variance_cp"] = 0;
                Session["pro_rate_cp"] = 0;

                Session["sales_current_year_total"] = 0;
                Session["open_order_total"] = 0;
                Session["supply_now_total"] = 0;
                Session["target_month_total"] = 0;
                Session["actual_month_total"] = 0;
                Session["variance_total"] = 0;
                Session["pro_rate_total"] = 0;


                Session["sales_current_year_1"] = 0;
                Session["open_order_1"] = 0;
                Session["supply_now_1"] = 0;
                Session["target_month_1"] = 0;
                Session["actual_month_1"] = 0;
                Session["variance_1"] = 0;
                Session["pro_rate_1"] = 0;

                if (dtCustToatal.Rows.Count > 0)
                {
                    if (dtCustToatal.Rows[0]["sales_current_year"] != DBNull.Value)
                    {
                        Session["sales_current_year"] = Convert.ToString(dtCustToatal.Rows[0]["sales_current_year"]);
                    }
                    if (dtCustToatal.Rows[0]["open_order"] != DBNull.Value)
                    {
                        Session["open_order"] = Convert.ToString(dtCustToatal.Rows[0]["open_order"]);
                    }
                    if (dtCustToatal.Rows[0]["can_supply_now"] != DBNull.Value)
                    {
                        Session["supply_now"] = Convert.ToString(dtCustToatal.Rows[0]["can_supply_now"]);
                    }
                    if (dtCustToatal.Rows[0]["targetval"] != DBNull.Value)
                    {
                        Session["target_month"] = Convert.ToString(dtCustToatal.Rows[0]["targetval"]);
                    }
                    if (dtCustToatal.Rows[0]["ActualValue"] != DBNull.Value)
                    {
                        Session["actual_month"] = Convert.ToString(dtCustToatal.Rows[0]["ActualValue"]);
                    }
                    if (dtCustToatal.Rows[0]["variance"] != DBNull.Value)
                    {
                        Session["variance"] = Convert.ToString(dtCustToatal.Rows[0]["variance"]);
                    }
                    if (dtCustToatal.Rows[0]["prorata"] != DBNull.Value)
                    {
                        Session["pro_rate"] = Convert.ToString(dtCustToatal.Rows[0]["prorata"]);
                    }
                }

              //  else
              //  {
                //    Session["sales_current_year"] = 0;
                 //   Session["open_order"] = 0;
                 //   Session["supply_now"] = 0;
                 //   Session["target_month"] = 0;
                 //   Session["actual_month"] = 0;
                 //   Session["variance"] = 0;
                 //   Session["pro_rate"] = 0;
              //  }
               // String txt = dtCPTotal.Rows[0]["open_order"].ToString();
                if (dtCPTotal.Rows.Count > 0)
                {
                    if (dtCPTotal.Rows[0]["sales_current_year"] != DBNull.Value)
                    {
                        Session["sales_current_year_cp"] = Convert.ToString(dtCPTotal.Rows[0]["sales_current_year"]);
                    }
                    if (dtCPTotal.Rows[0]["open_order"] != DBNull.Value)
                    {
                        Session["open_order_cp"] = Convert.ToString(dtCPTotal.Rows[0]["open_order"]);
                    }
                    if (dtCPTotal.Rows[0]["can_supply_now"] != DBNull.Value)
                    {
                        Session["supply_now_cp"] = Convert.ToString(dtCPTotal.Rows[0]["can_supply_now"]);
                    }
                    if (dtCPTotal.Rows[0]["targetval"] != DBNull.Value)
                    {
                        Session["target_month_cp"] = Convert.ToString(dtCPTotal.Rows[0]["targetval"]);
                    }
                    if (dtCPTotal.Rows[0]["ActualValue"] != DBNull.Value)
                    {
                    Session["actual_month_cp"] = Convert.ToString(dtCPTotal.Rows[0]["ActualValue"]);
                    }
                    if (dtCPTotal.Rows[0]["variance"] != DBNull.Value)
                    {
                    Session["variance_cp"] = Convert.ToString(dtCPTotal.Rows[0]["variance"]);
                    }
                    if (dtCPTotal.Rows[0]["prorata"] != DBNull.Value)
                    {
                        Session["pro_rate_cp"] = Convert.ToString(dtCPTotal.Rows[0]["prorata"]);
                    }
                }
               // else
               // {
               //     Session["sales_current_year_cp"] = 0;
                //    Session["open_order_cp"] = 0;
                //    Session["supply_now_cp"] = 0;
                 //   Session["target_month_cp"] = 0;
                 //   Session["actual_month_cp"] = 0;
                 //   Session["variance_cp"] = 0;
                 //   Session["pro_rate_cp"] = 0;
               // }

                if (dttotal.Rows.Count > 0)
                {
                    if (dttotal.Rows[0]["sales_current_year"] != DBNull.Value)
                    {
                        Session["sales_current_year_total"] = Convert.ToString(dttotal.Rows[0]["sales_current_year"]);
                    }
                    if (dttotal.Rows[0]["open_order"] != DBNull.Value)
                    {
                        Session["open_order_total"] = Convert.ToString(dttotal.Rows[0]["open_order"]);
                    }
                    if (dttotal.Rows[0]["can_supply_now"] != DBNull.Value)
                    {
                        Session["supply_now_total"] = Convert.ToString(dttotal.Rows[0]["can_supply_now"]);
                    }
                    if (dttotal.Rows[0]["targetval"] != DBNull.Value)
                    {
                        Session["target_month_total"] = Convert.ToString(dttotal.Rows[0]["targetval"]);
                    }
                    if (dttotal.Rows[0]["ActualValue"] != DBNull.Value)
                    {
                        Session["actual_month_total"] = Convert.ToString(dttotal.Rows[0]["ActualValue"]);
                    }
                    if (dttotal.Rows[0]["variance"] != DBNull.Value)
                    {
                        Session["variance_total"] = Convert.ToString(dttotal.Rows[0]["variance"]);
                    }
                    if (dttotal.Rows[0]["prorata"] != DBNull.Value)
                    {
                        Session["pro_rate_total"] = Convert.ToString(dttotal.Rows[0]["prorata"]);
                    }
                }

              //  else
           //    {
                //    Session["sales_current_year_total"] = 0;
                 //   Session["open_order_total"] = 0;
                 //   Session["supply_now_total"] = 0;
                 //   Session["target_month_total"] = 0;
                 //   Session["actual_month_total"] = 0;
                //    Session["variance_total"] = 0;
                 //   Session["pro_rate_total"] = 0;
               // }
                    //Session["open_order"] = dtCustToatal.AsEnumerable().Sum(x => Convert.ToDecimal(x["open_order"])).ToString();
                    //Session["supply_now"] = dtCustToatal.AsEnumerable().Sum(x => Convert.ToDecimal(x["can_supply_now"])).ToString();
                    //Session["target_month"] = dtCustToatal.AsEnumerable().Sum(x => Convert.ToDecimal(x["targetval"])).ToString();
                    //Session["actual_month"] = dtCustToatal.AsEnumerable().Sum(x => Convert.ToDecimal(x["ActualValue"])).ToString();
                    //Session["variance"] = dtCustToatal.AsEnumerable().Sum(x => Convert.ToDecimal(x["variance"])).ToString();
                    //Session["pro_rate"] = dtCustToatal.AsEnumerable().Sum(x => Convert.ToDecimal(x["prorata"])).ToString();


                    //Session["open_order_cp"] = dtCPTotal.AsEnumerable().Sum(x => Convert.ToDecimal(x["open_order"])).ToString();
                    //Session["supply_now_cp"] = dtCPTotal.AsEnumerable().Sum(x => Convert.ToDecimal(x["can_supply_now"])).ToString();
                    //Session["target_month_cp"] = dtCPTotal.AsEnumerable().Sum(x => Convert.ToDecimal(x["targetval"])).ToString();
                    //Session["actual_month_cp"] = dtCPTotal.AsEnumerable().Sum(x => Convert.ToDecimal(x["ActualValue"])).ToString();
                    //Session["variance_cp"] = dtCPTotal.AsEnumerable().Sum(x => Convert.ToDecimal(x["variance"])).ToString();
                    //Session["pro_rate_cp"] = dtCPTotal.AsEnumerable().Sum(x => Convert.ToDecimal(x["prorata"])).ToString();


                   //Session["open_order"] = dttotal.AsEnumerable().Sum(x => Convert.ToDecimal(x["open_order"])).ToString();
                   //Session["supply_now"] = dttotal.AsEnumerable().Sum(x => Convert.ToDecimal(x["can_supply_now"])).ToString();
                   //Session["target_month"] = dttotal.AsEnumerable().Sum(x => Convert.ToDecimal(x["targetval"])).ToString();
                   //Session["actual_month"] = dttotal.AsEnumerable().Sum(x => Convert.ToDecimal(x["ActualValue"])).ToString();
                   //Session["variance"] = dttotal.AsEnumerable().Sum(x => Convert.ToDecimal(x["variance"])).ToString();
                   //Session["pro_rate"] = dttotal.AsEnumerable().Sum(x => Convert.ToDecimal(x["prorata"])).ToString();

                hdnsales_current_year.Value = Convert.ToString(Session["sales_current_year"]);
                hdnopen_order.Value = Convert.ToString(Session["open_order"]);      
                hdnsupply_now.Value = Convert.ToString(Session["supply_now"]);
                hdntarget_month.Value = Convert.ToString(Session["target_month"]);
                hdnactual_month.Value = Convert.ToString(Session["actual_month"]);
                hdnvariance.Value = Convert.ToString(Session["variance"]);
                hdnpro_rate.Value = Convert.ToString(Session["pro_rate"]);

                hdnsales_current_year_cp.Value = Convert.ToString(Session["sales_current_year_cp"]);
                hdnopen_order_cp.Value = Convert.ToString(Session["open_order_cp"]);      
                hdnsupply_now_cp.Value = Convert.ToString(Session["supply_now_cp"]);
                hdntarget_month_cp.Value = Convert.ToString(Session["target_month_cp"]);
                hdnactual_month_cp.Value = Convert.ToString(Session["actual_month_cp"]);
                hdnvariance_cp.Value = Convert.ToString(Session["variance_cp"]);
                hdnpro_rate_cp.Value = Convert.ToString(Session["pro_rate_cp"]);

                hdnsales_current_year_total.Value = Convert.ToString(Session["sales_current_year_total"]);
                hdnopen_order_total.Value = Convert.ToString(Session["open_order_total"]);
                hdnsupply_now_total.Value = Convert.ToString(Session["supply_now_total"]);
                hdntarget_month_total.Value = Convert.ToString(Session["target_month_total"]);
                hdnactual_month_total.Value = Convert.ToString(Session["actual_month_total"]);
                hdnvariance_total.Value = Convert.ToString(Session["variance_total"]);
                hdnpro_rate_total.Value = Convert.ToString(Session["pro_rate_total"]); 
            }

            catch(Exception ex)
            {
                objFunc.LogError(ex);
            }
        }

        public DataTable addCulterInfo(DataTable dtTargets)
        {
            DataTable dtTempTargets = new DataTable();
            dtTempTargets.Columns.Add("cust_number", typeof(string));
            dtTempTargets.Columns.Add("cust_name");
            dtTempTargets.Columns.Add("customer_type");
            dtTempTargets.Columns.Add("cred_code");
            dtTempTargets.Columns.Add("sales_current_year");
            dtTempTargets.Columns.Add("open_order");
            dtTempTargets.Columns.Add("can_supply_now");
            dtTempTargets.Columns.Add("targetval");
            dtTempTargets.Columns.Add("ActualValue");
            dtTempTargets.Columns.Add("variance");
            dtTempTargets.Columns.Add("prorata", typeof(int));

            var culture = new CultureInfo("en-us", true)
            {
                NumberFormat =
                {
                    NumberGroupSizes = new int[] { 2, 2 }
                }
            };

            //adding culture to numbers
            for (int i = 0; i < dtTargets.Rows.Count; i++)
            {
                string cust_number = dtTargets.Rows[i].ItemArray[0].ToString();
                string cust_name = dtTargets.Rows[i].ItemArray[1].ToString();
                string cust_type = dtTargets.Rows[i].ItemArray[2].ToString();
                string cred_code = dtTargets.Rows[i].ItemArray[3].ToString();

                decimal sales_current_year = dtTargets.Rows[i].ItemArray[4].ToString() == "" || dtTargets.Rows[i].ItemArray[4].ToString() == null ? 0
                                    : Convert.ToDecimal(dtTargets.Rows[i].ItemArray[4].ToString());

                decimal open_order = dtTargets.Rows[i].ItemArray[5].ToString() == "" || dtTargets.Rows[i].ItemArray[5].ToString() == null ? 0
                                   : Convert.ToDecimal(dtTargets.Rows[i].ItemArray[5].ToString());

                decimal can_supply_now = dtTargets.Rows[i].ItemArray[6].ToString() == "" || dtTargets.Rows[i].ItemArray[6].ToString() == null ? 0
                                     : Convert.ToDecimal(dtTargets.Rows[i].ItemArray[6].ToString());

                decimal targetva = dtTargets.Rows[i].ItemArray[7].ToString() == "" || dtTargets.Rows[i].ItemArray[7].ToString() == null ? 0
                                   : Convert.ToDecimal(dtTargets.Rows[i].ItemArray[7].ToString());

                decimal actlva = dtTargets.Rows[i].ItemArray[8].ToString() == "" || dtTargets.Rows[i].ItemArray[8].ToString() == null ? 0
                                    : Convert.ToDecimal(dtTargets.Rows[i].ItemArray[8].ToString());

                decimal varianc = dtTargets.Rows[i].ItemArray[9].ToString() == "" || dtTargets.Rows[i].ItemArray[9].ToString() == null ? 0
                                    : Convert.ToDecimal(dtTargets.Rows[i].ItemArray[9].ToString());

                decimal prorata = dtTargets.Rows[i].ItemArray[10].ToString() == "" || dtTargets.Rows[i].ItemArray[10].ToString() == null ? 0
                                     : Convert.ToDecimal(dtTargets.Rows[i].ItemArray[10].ToString());



                if (targetva != 0 || actlva != 0 || varianc != 0 || prorata != 0)
                {
                    if (sales_current_year != 0)
                        dtTempTargets.Rows.Add(cust_number,
                                               cust_name,
                                               cust_type,
                                               cred_code,
                                               Convert.ToString(Math.Round(sales_current_year).ToString("NO", culture)),
                                               Convert.ToString(Math.Round(open_order).ToString("N0", culture)),
                                               Convert.ToString(Math.Round(can_supply_now).ToString("N0", culture)),
                                               Convert.ToString(Math.Round(targetva).ToString("N0", culture)),
                                               Convert.ToString(Math.Round(actlva).ToString("N0", culture)),
                                               Convert.ToString(Math.Round(varianc).ToString("N0", culture)),
                                               (Math.Round(prorata))

                                       );
                    else
                        dtTempTargets.Rows.Add(cust_number,
                                          cust_name,
                                          cust_type,
                                          cred_code,
                                          Convert.ToString(Math.Round(sales_current_year)),
                                          Convert.ToString(Math.Round(open_order).ToString("N0", culture)),
                                          Convert.ToString(Math.Round(can_supply_now).ToString("N0", culture)),
                                          Convert.ToString(Math.Round(targetva).ToString("N0", culture)),
                                          Convert.ToString(Math.Round(actlva).ToString("N0", culture)),
                                          Convert.ToString(Math.Round(varianc).ToString("N0", culture)),
                                          (Math.Round(prorata))

                                  );
                }
            }

            return dtTempTargets;
        }

        [WebMethod]
        public static List<string> setGrandtotal(string search)
        {
            DataTable dt = new DataTable();
            DataTable dtOutput = new DataTable();
            List<string> hdndata = new List<string>();
            List<string> searchList = new List<string>();
            DataRow[] filteredRows;
            dt = (DataTable)HttpContext.Current.Session["targetreport"];
            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {
                    dtOutput = new DataTable();
                    dtOutput.Columns.Add("cust_number");
                    dtOutput.Columns.Add("cust_name");
                    dtOutput.Columns.Add("branch_code");
                    dtOutput.Columns.Add("salesengineer_name");
                    dtOutput.Columns.Add("customer_type");
                    //dtOutput.Columns.Add("customer_name");
                    dtOutput.Columns.Add("cred_code");
                    dtOutput.Columns.Add("sales_current_year");
                    dtOutput.Columns.Add("open_order");
                    dtOutput.Columns.Add("can_supply_now");
                    dtOutput.Columns.Add("targetval");
                    dtOutput.Columns.Add("ActualValue");
                    dtOutput.Columns.Add("variance");
                    //dtOutput.Columns.Add("actlva");
                    //dtOutput.Columns.Add("varianc");
                    dtOutput.Columns.Add("prorata");

                    if (!string.IsNullOrEmpty(search))
                    {
                        if (search.Contains(' '))
                        {
                            searchList = search.Split(' ').ToList();
                        }
                        else
                        {
                            searchList.Add(search);
                        }
                        for (int i = 0; i < searchList.Count; i++)
                        {
                            //dtOutput = new DataTable();

                            ////dtOutput.Columns.Add("branch");
                            ////dtOutput.Columns.Add("sales_engg");

                            //dtOutput.Columns.Add("cust_number");
                            //dtOutput.Columns.Add("cust_name");
                            //dtOutput.Columns.Add("customer_type");
                            ////dtOutput.Columns.Add("customer_name");
                            //dtOutput.Columns.Add("cred_code");
                            //dtOutput.Columns.Add("sales_current_year");
                            //dtOutput.Columns.Add("open_order");
                            //dtOutput.Columns.Add("can_supply_now");
                            //dtOutput.Columns.Add("targetval");
                            //dtOutput.Columns.Add("ActualValue");
                            //dtOutput.Columns.Add("variance");                          
                            ////dtOutput.Columns.Add("actlva");
                            ////dtOutput.Columns.Add("varianc");
                            //dtOutput.Columns.Add("prorata");

                            dtOutput = new DataTable();
                            dtOutput.Columns.Add("cust_number");
                            dtOutput.Columns.Add("cust_name");
                            dtOutput.Columns.Add("branch_code");
                            dtOutput.Columns.Add("salesengineer_name");
                            dtOutput.Columns.Add("customer_type");
                            //dtOutput.Columns.Add("customer_name");
                            dtOutput.Columns.Add("cred_code");
                            dtOutput.Columns.Add("sales_current_year");
                            dtOutput.Columns.Add("open_order");
                            dtOutput.Columns.Add("can_supply_now");
                            dtOutput.Columns.Add("targetval");
                            dtOutput.Columns.Add("ActualValue");
                            dtOutput.Columns.Add("variance");
                            //dtOutput.Columns.Add("actlva");
                            //dtOutput.Columns.Add("varianc");
                            dtOutput.Columns.Add("prorata");

                            filteredRows = dt.Select("CONVERT(customer_type, System.String) LIKE '%" + searchList[i].Trim()
                             // + "%' OR CONVERT(branch, System.String) LIKE'%" + search
                             // + "%' OR CONVERT(sales_engg, System.String) LIKE'%" + search
                              + "%' OR CONVERT(cust_number, System.String) LIKE'%" + searchList[i].Trim()
                              + "%' OR CONVERT(cust_name, System.String) LIKE'%" + searchList[i].Trim()
                              + "%' OR CONVERT(branch_code, System.String) LIKE'%" + searchList[i].Trim()
                              + "%' OR CONVERT(salesengineer_name, System.String) LIKE'%" + searchList[i].Trim()

                              //+ "%' OR CONVERT(customer_type, System.String) LIKE'%" + search
                              + "%' OR CONVERT(cred_code, System.String) LIKE'%" + searchList[i].Trim()
                              + "%' OR CONVERT(sales_current_year, System.String) LIKE'%" + searchList[i].Trim()
                              + "%' OR CONVERT(open_order, System.String) LIKE'%" + searchList[i].Trim()
                              + "%' OR CONVERT(can_supply_now, System.String) LIKE'%" + searchList[i].Trim()
                              + "%' OR CONVERT(targetval, System.String) LIKE '%" + searchList[i].Trim()
                              + "%' OR CONVERT(ActualValue, System.String)  LIKE '%" + searchList[i].Trim()
                              + "%' OR CONVERT(variance, System.String)  LIKE '%" + searchList[i].Trim()
                              + "%' OR CONVERT(prorata, System.String)  LIKE '%" + searchList[i].Trim()
                              + "%'");
                

                            //filteredRows = dt.Select("CONVERT(cust_number , System.String) LIKE '%" + searchList[i].Trim()
                            //     + "%' OR CONVERT(cust_name, System.String) LIKE '%" + searchList[i].Trim()
                            //     //+ "%' OR CONVERT(customer_name, System.String) LIKE '%" + searchList[i].Trim()
                            //     + "%' OR CONVERT(sales_current_year, System.String)  LIKE '%" + searchList[i].Trim()
                            //     + "%' OR CONVERT(open_order, System.String)  LIKE '%" + searchList[i].Trim()
                            //     + "%' OR CONVERT(can_supply_now, System.String)  LIKE '%" + searchList[i].Trim()
                            //     //+ "%' OR CONVERT(targetva, System.String)  LIKE '%" + searchList[i].Trim()
                            //     + "%' OR CONVERT(actlva, System.String)  LIKE '%" + searchList[i].Trim()
                            //     + "%' OR CONVERT(varianc, System.String)  LIKE '%" + searchList[i].Trim()
                            //     + "%'");


                            foreach (DataRow dr in filteredRows)
                            {
                                dtOutput.Rows.Add(dr.ItemArray);
                            }
                            dt = dtOutput;
                        }
                    }
                    //else{

                    //     dtOutput = dt;
                    //}
                    string sales_current_year = "0";
                    string open_order = "0";
                    string target_month = "0";
                    string can_supply_now = "0";
                    string ActualValue = "0";
                    string variance = "0";
                    decimal pro_rate = 0;

                    string sales_current_year_cp = "0";
                    string open_order_cp = "0";
                    string target_month_cp = "0";
                    string can_supply_now_cp = "0";
                    string ActualValue_cp = "0";
                    string variance_cp = "0";
                    decimal pro_rate_cp = 0;

                    string sales_current_year_total = "0";
                    string open_order_total = "0";
                    string target_month_total = "0";
                    string can_supply_now_total = "0";
                    string ActualValue_total = "0";
                    string variance_total = "0";
                    decimal pro_rate_total = 0;

                    if (dt.AsEnumerable().Where(y => y.Field<string>("customer_type").Equals("C")) != null)
                    {
                         sales_current_year = Convert.ToString(dt.AsEnumerable().Where(y => y.Field<string>("customer_type").Equals("C")).Sum(x => Convert.ToDecimal(x["sales_current_year"])).ToString());
                         open_order = Convert.ToString(dt.AsEnumerable().Where(y => y.Field<string>("customer_type").Equals("C")).Sum(x => Convert.ToDecimal(x["open_order"])).ToString());
                        //string actual_month = Convert.ToString(dt.AsEnumerable().Where(y => y.Field<string>("customer_type").Equals("C")).Sum(x => Convert.ToDecimal(x["actlva"])).ToString());
                         target_month = Convert.ToString(dt.AsEnumerable().Where(y => y.Field<string>("customer_type").Equals("C")).Sum(x => Convert.ToDecimal(x["targetval"])).ToString());
                        // string targetval = Convert.ToString(dt.AsEnumerable().Where(y => y.Field<string>("customer_type").Equals("C")).Sum(x => Convert.ToDecimal(x["targetval"])).ToString());
                         can_supply_now = Convert.ToString(dt.AsEnumerable().Where(y => y.Field<string>("customer_type").Equals("C")).Sum(x => Convert.ToDecimal(x["can_supply_now"])).ToString());
                         ActualValue = Convert.ToString(dt.AsEnumerable().Where(y => y.Field<string>("customer_type").Equals("C")).Sum(x => Convert.ToDecimal(x["ActualValue"])).ToString());
                         variance = Convert.ToString(dt.AsEnumerable().Where(y => y.Field<string>("customer_type").Equals("C")).Sum(x => Convert.ToDecimal(x["variance"])).ToString());
                        //string prorata = Convert.ToString(dt.AsEnumerable().Where(y => y.Field<string>("customer_type").Equals("C")).Sum(x => Convert.ToDecimal(x["prorata"])).ToString());

                         if (Convert.ToDecimal(target_month) != 0)
                             pro_rate = Math.Round((Convert.ToDecimal(ActualValue) / Convert.ToDecimal(target_month)) * 100, 0);
                      
                    }
                    //else {
                    //    string sales_current_year = "0";
                    //    string open_order = "0";
                    //    string target_month = "0";
                    //    string can_supply_now = "0";
                    //    string ActualValue = "0";
                    //    string variance = "0";
                    //    decimal pro_rate = 0;
                    //}
                    if (dt.AsEnumerable().Where(y => y.Field<string>("customer_type").Equals("D")) != null)
                    {
                         sales_current_year_cp = Convert.ToString(dt.AsEnumerable().Where(y => y.Field<string>("customer_type").Equals("D")).Sum(x => Convert.ToDecimal(x["sales_current_year"])).ToString());
                         open_order_cp = Convert.ToString(dt.AsEnumerable().Where(y => y.Field<string>("customer_type").Equals("D")).Sum(x => Convert.ToDecimal(x["open_order"])).ToString());
                        //string actual_month_cp = Convert.ToString(dt.AsEnumerable().Where(y => y.Field<string>("customer_type").Equals("D")).Sum(x => Convert.ToDecimal(x["actlva"])).ToString());
                         target_month_cp = Convert.ToString(dt.AsEnumerable().Where(y => y.Field<string>("customer_type").Equals("D")).Sum(x => Convert.ToDecimal(x["targetval"])).ToString());
                         can_supply_now_cp = Convert.ToString(dt.AsEnumerable().Where(y => y.Field<string>("customer_type").Equals("D")).Sum(x => Convert.ToDecimal(x["can_supply_now"])).ToString());
                        // string targetval_cp = Convert.ToString(dt.AsEnumerable().Where(y => y.Field<string>("customer_type").Equals("D")).Sum(x => Convert.ToDecimal(x["targetval"])).ToString());
                         ActualValue_cp = Convert.ToString(dt.AsEnumerable().Where(y => y.Field<string>("customer_type").Equals("D")).Sum(x => Convert.ToDecimal(x["ActualValue"])).ToString());
                         variance_cp = Convert.ToString(dt.AsEnumerable().Where(y => y.Field<string>("customer_type").Equals("D")).Sum(x => Convert.ToDecimal(x["variance"])).ToString());
                        //   string prorata_cp = Convert.ToString(dt.AsEnumerable().Where(y => y.Field<string>("customer_type").Equals("D")).Sum(x => Convert.ToDecimal(x["prorata"])).ToString());
                         if (Convert.ToDecimal(target_month_cp) != 0)
                            pro_rate_cp = Math.Round((Convert.ToDecimal(ActualValue_cp) / Convert.ToDecimal(target_month_cp)) * 100, 0);
                    }
                   
                    if (dt.Rows.Count != null)
                    {
                         sales_current_year_total = dt.AsEnumerable().Sum(x => Convert.ToDecimal(x["sales_current_year"])).ToString();
                         open_order_total = dt.AsEnumerable().Sum(x => Convert.ToDecimal(x["open_order"])).ToString();
                         can_supply_now_total = dt.AsEnumerable().Sum(x => Convert.ToDecimal(x["can_supply_now"])).ToString();
                         target_month_total = dt.AsEnumerable().Sum(x => Convert.ToDecimal(x["targetval"])).ToString();
                        // string actual_month = dtOutput.AsEnumerable().Sum(x => Convert.ToDecimal(x["actlva"])).ToString();
                         ActualValue_total = dt.AsEnumerable().Sum(x => Convert.ToDecimal(x["ActualValue"])).ToString();
                        // string variance = dtOutput.AsEnumerable().Sum(x => Convert.ToDecimal(x["varianc"])).ToString();
                         variance_total = dt.AsEnumerable().Sum(x => Convert.ToDecimal(x["variance"])).ToString();
                        //  string pro_rate = dtOutput.AsEnumerable().Sum(x => Convert.ToDecimal(x["prorata"])).ToString();
                        
                        if (Convert.ToDecimal(target_month_total) != 0)
                            pro_rate_total = Math.Round((Convert.ToDecimal(ActualValue_total) / Convert.ToDecimal(target_month_total)) * 100, 0);
                    }

                   
                 // string sales_current_year_total= Convert.ToString(dt.AsEnumerable().Sum(x => Convert.ToDecimal(x["sales_current_year"])).ToString());        
                 // string  open_order_total = Convert.ToString(dt.AsEnumerable().Sum(x => Convert.ToDecimal(x["open_order"])).ToString());
                 // string actual_month_total = Convert.ToString(dt.AsEnumerable().Where(y => y.Field<string>("customer_type").Equals("C")).Sum(x => Convert.ToDecimal(x["actlva"])).ToString());
                 // string target_month_total = Convert.ToString(dt.AsEnumerable().Where(y => y.Field<string>("customer_type").Equals("C")).Sum(x => Convert.ToDecimal(x["targetval"])).ToString());
                 // string can_supply_now_total = Convert.ToString(dt.AsEnumerable().Sum(x => Convert.ToDecimal(x["can_supply_now"])).ToString());
                 //// string targetval_total = Convert.ToString(dt.AsEnumerable().Sum(x => Convert.ToDecimal(x["targetval"])).ToString());
                 // string ActualValue_total = Convert.ToString(dt.AsEnumerable().Sum(x => Convert.ToDecimal(x["ActualValue"])).ToString());
                 // string variance_total = Convert.ToString(dt.AsEnumerable().Sum(x => Convert.ToDecimal(x["variance"])).ToString());
                 //// string prorata_total = Convert.ToString(dt.AsEnumerable().Sum(x => Convert.ToDecimal(x["prorata"])).ToString());
                 // decimal pro_rate_total = 0;
                 // if (Convert.ToDecimal(target_month) != 0)
                 //     pro_rate_total = Math.Round((Convert.ToDecimal(actual_month) / Convert.ToDecimal(target_month)) * 100, 0);

                    hdndata.Add(Convert.ToString(sales_current_year));
                    hdndata.Add(Convert.ToString(open_order));
                    hdndata.Add(Convert.ToString(can_supply_now));
                    hdndata.Add(Convert.ToString(target_month));
                    hdndata.Add(Convert.ToString(ActualValue));
                    hdndata.Add(Convert.ToString(variance));
                    hdndata.Add(Convert.ToString(pro_rate));

                    hdndata.Add(Convert.ToString(sales_current_year_cp));
                    hdndata.Add(Convert.ToString(open_order_cp));
                    hdndata.Add(Convert.ToString(can_supply_now_cp));
                    hdndata.Add(Convert.ToString(target_month_cp));
                    hdndata.Add(Convert.ToString(ActualValue_cp));
                    hdndata.Add(Convert.ToString(variance_cp));
                    hdndata.Add(Convert.ToString(pro_rate_cp));

                    hdndata.Add(Convert.ToString(sales_current_year_total));
                    hdndata.Add(Convert.ToString(open_order_total));
                    hdndata.Add(Convert.ToString(can_supply_now_total));
                    hdndata.Add(Convert.ToString(target_month_total));
                    hdndata.Add(Convert.ToString(ActualValue_total));
                    hdndata.Add(Convert.ToString(variance_total));
                    hdndata.Add(Convert.ToString(pro_rate_total));

                }
            }
            return hdndata;
        }
        #endregion
    }
}