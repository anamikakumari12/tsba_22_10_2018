﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace TaegutecSalesBudget
{
    public partial class SpecialGroups : System.Web.UI.Page
    {
        AdminConfiguration objAdmin = new AdminConfiguration();
        Budget objBudget = new Budget();
        Review objRSum = new Review();
        Specialgroups objSpclgroups = new Specialgroups();
       
        List<string> cssList = new List<string>();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }
            if (!IsPostBack)
            {
                LoadProductFamliy();
                ddlProductFamliy_SelectedIndexChanged(null, null);
                LoadSpecialGroups();
                proceed_Click(null, null);
               string BudgetYear_B = objAdmin.GetProfile("B_BUDGET_YEAR");
               string B_BUDGET_YEAR = BudgetYear_B;
                List<string> years = new List<string>();
                years.Add(B_BUDGET_YEAR);
                string By = Convert.ToString(Convert.ToInt32(B_BUDGET_YEAR) + 1);
                years.Add(By);
                B_BUDGETYEAR.DataSource = years;
                B_BUDGETYEAR.DataBind();
            }
        }
        protected void LoadProductFamliy()
        {
            DataTable dtProductFamilyList = new DataTable();
            dtProductFamilyList = objBudget.LoadFamilyId();
            ddlProductFamliy.DataSource = dtProductFamilyList;
            ddlProductFamliy.DataTextField = "item_family_name";
            ddlProductFamliy.DataValueField = "item_family_id";

            ddlProductFamliy.DataBind();
            ddlProductFamliy.Items.Insert(0, "ALL");
        }
        protected void ddlProductFamliy_SelectedIndexChanged(object sender, EventArgs e)
        {
            string famId = ddlProductFamliy.SelectedItem.Value == "ALL" ? "0" : ddlProductFamliy.SelectedItem.Value;
            int Id = Convert.ToInt32(famId);
            DataTable dtPL = new DataTable();
            dtPL = objRSum.getProductLine(Id);
            ddlProductLine.DataSource = dtPL;
            ddlProductLine.DataValueField = "item_sub_family_id";
            ddlProductLine.DataTextField = "item_sub_family_name";
            ddlProductLine.DataBind();
            ddlProductLine.Items.Insert(0, "ALL");
            ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "gridviewScrollTrigger();", true);


        }
        protected void LoadSpecialGroups()
        {
            DataTable spclgroups = objSpclgroups.getspclgroups("ALL", "ALL");
            DataTable dtspclgroups = new DataTable();
            dtspclgroups.Columns.Add("item_code", typeof(string));
            dtspclgroups.Columns.Add("item_short_name", typeof(string));
            dtspclgroups.Columns.Add("gold_flag", typeof(string));
            dtspclgroups.Columns.Add("top_flag", typeof(string));
            dtspclgroups.Columns.Add("five_years_flag", typeof(string));
            dtspclgroups.Columns.Add("bb_flag", typeof(string));
            dtspclgroups.Columns.Add("SPC_flag", typeof(string));
            dtspclgroups.Columns.Add("ten_years_flag", typeof(string));


            for (int i = 0; i < spclgroups.Rows.Count; i++)
            {
                string item_code = spclgroups.Rows[i].ItemArray[0].ToString();
                string item_name = spclgroups.Rows[i].ItemArray[1].ToString();
                string gold_flag = spclgroups.Rows[i].ItemArray[2].ToString();
                string top_flag = spclgroups.Rows[i].ItemArray[3].ToString();
                string five_years_flag = spclgroups.Rows[i].ItemArray[4].ToString();
                string bb_flag = spclgroups.Rows[i].ItemArray[5].ToString();
                string SPC_flag = spclgroups.Rows[i].ItemArray[6].ToString();
               string Ten_Years_flag = spclgroups.Rows[i].ItemArray[7].ToString();


                dtspclgroups.Rows.Add(item_code, item_name, gold_flag, top_flag, five_years_flag, bb_flag, SPC_flag,Ten_Years_flag);
            }
            spclgroupsgrid.DataSource = dtspclgroups;
            spclgroupsgrid.DataBind();

        }
        protected void proceed_Click(object sender, EventArgs e)
        {
            string family_id = ddlProductFamliy.SelectedItem.Value;
            string sub_family_id = ddlProductLine.SelectedItem.Value;
            DataTable spclgroups = objSpclgroups.getspclgroups(family_id, sub_family_id);
            DataTable dtspclgroups = new DataTable();
            dtspclgroups.Columns.Add("item_code", typeof(string));
            dtspclgroups.Columns.Add("item_short_name", typeof(string));
            dtspclgroups.Columns.Add("gold_flag", typeof(string));
            dtspclgroups.Columns.Add("top_flag", typeof(string));
            dtspclgroups.Columns.Add("five_years_flag", typeof(string));
            dtspclgroups.Columns.Add("bb_flag", typeof(string));
            dtspclgroups.Columns.Add("SPC_flag", typeof(string));
            dtspclgroups.Columns.Add("ten_years_flag", typeof(string));
            for (int i = 0; i < spclgroups.Rows.Count; i++)
            {
                string item_code = spclgroups.Rows[i].ItemArray[0].ToString();
                string item_name = spclgroups.Rows[i].ItemArray[1].ToString();
                string gold_flag = spclgroups.Rows[i].ItemArray[2].ToString();
                string top_flag = spclgroups.Rows[i].ItemArray[3].ToString();
                string five_years_flag = spclgroups.Rows[i].ItemArray[4].ToString();
                string bb_flag = spclgroups.Rows[i].ItemArray[5].ToString();
                string SPC_flag = spclgroups.Rows[i].ItemArray[6].ToString();
                string Ten_Years_flag = spclgroups.Rows[i].ItemArray[7].ToString();

                dtspclgroups.Rows.Add(item_code, item_name, gold_flag, top_flag, five_years_flag, bb_flag, SPC_flag,Ten_Years_flag);
            }
            spclgroupsgrid.DataSource = dtspclgroups;
            spclgroupsgrid.DataBind();
            bindgridColor();
            ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "gridviewScrollTrigger();", true);

        }
        protected void update_Click(object sender, EventArgs e)
        {
            foreach (GridViewRow row in spclgroupsgrid.Rows)
            {
                string item_code, gold_flag = null, top_flag = null, five_years_flag = null, bb_flag = null, SPC_flag = null, Ten_Years_flag = null;
                int year;
                year = Convert.ToInt32(B_BUDGETYEAR.SelectedValue);
                var lblitemCode = row.FindControl("item_code_lbl") as Label;
                var gold_flag_cb = row.FindControl("gold_flag") as CheckBox;
                var top_flag_cb = row.FindControl("top_flag") as CheckBox;
                var five_years_flag_cb = row.FindControl("five_years_flag") as CheckBox;
                var bb_flag_cb = row.FindControl("bb_flag") as CheckBox;
                var SPC_flag_cb = row.FindControl("SPC_flag") as CheckBox;
                var ten_years_flag_cb = row.FindControl("Ten_years_flag") as CheckBox;
                if (gold_flag_cb.Checked == true)
                {
                    gold_flag = "Y";
                }
                if (top_flag_cb.Checked == true)
                {
                    top_flag = "Y";
                }
                if (five_years_flag_cb.Checked == true)
                {
                    five_years_flag = "Y";
                }
                if (bb_flag_cb.Checked == true)
                {
                    bb_flag = "Y";
                }
                if (SPC_flag_cb.Checked == true)
                {
                    SPC_flag = "Y";
                }
                if (ten_years_flag_cb.Checked == true)
                {
                    Ten_Years_flag = "Y";
                }
                item_code = lblitemCode.Text.ToString();
                string msg = objSpclgroups.update_specialgrps(year,item_code, gold_flag, top_flag, five_years_flag, bb_flag, SPC_flag, Ten_Years_flag);
                if (msg == "Successful")
                {

                    //string scriptString = "<script type='text/javascript'> alert('Successfully updated');</script>";
                    //ClientScriptManager script = Page.ClientScript;
                    //script.RegisterClientScriptBlock(GetType(), "Script", scriptString);
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "alert('Updated Successfully'); triggerScript();", true);

                }
                bindgridColor();

            }


        }
        protected void bindgridColor()
        {
            if (spclgroupsgrid.Rows.Count != 0)
            {
                int color = 0;

                foreach (GridViewRow row in spclgroupsgrid.Rows)
                {

                    color++;
                    if (color == 1) { row.CssClass = "color_Product1 "; }
                    else if (color == 2)
                    {
                        row.CssClass = "color_Product2 ";
                        color = 0;
                    }

                }
            }
        }
        protected void LoadCSS()
        {
            cssList.Add("color_3");
            cssList.Add("color_4");
            cssList.Add("color_3");


        }
        protected string GetCSS(int colorIndex)
        {
            string index = Convert.ToString(colorIndex);
            string cIndex = index[index.Length - 1].ToString();

            if (cIndex.Contains("1"))
            { return cssList.ElementAt(1); }
            else if (cIndex.Contains("2"))
            { return cssList.ElementAt(2); }
            else { return cssList.ElementAt(2); }

        }

        private byte[] Compress(byte[] b)
        {
            MemoryStream ms = new MemoryStream();
            GZipStream zs = new GZipStream(ms, CompressionMode.Compress, true);
            zs.Write(b, 0, b.Length);
            zs.Close();
            return ms.ToArray();
        }

        /// This method takes the compressed byte stream as parameter
        /// and return a decompressed bytestream.

        private byte[] Decompress(byte[] b)
        {
            MemoryStream ms = new MemoryStream();
            GZipStream zs = new GZipStream(new MemoryStream(b),
                                           CompressionMode.Decompress, true);
            byte[] buffer = new byte[4096];
            int size;
            while (true)
            {
                size = zs.Read(buffer, 0, buffer.Length);
                if (size > 0)
                    ms.Write(buffer, 0, size);
                else break;
            }
            zs.Close();
            return ms.ToArray();
        }

        protected override object LoadPageStateFromPersistenceMedium()
        {
            System.Web.UI.PageStatePersister pageStatePersister1 = this.PageStatePersister;
            pageStatePersister1.Load();
            String vState = pageStatePersister1.ViewState.ToString();
            byte[] pBytes = System.Convert.FromBase64String(vState);
            pBytes = Decompress(pBytes);
            LosFormatter mFormat = new LosFormatter();
            Object ViewState = mFormat.Deserialize(System.Convert.ToBase64String(pBytes));
            return new Pair(pageStatePersister1.ControlState, ViewState);
        }

        protected override void SavePageStateToPersistenceMedium(Object pViewState)
        {
            Pair pair1;
            System.Web.UI.PageStatePersister pageStatePersister1 = this.PageStatePersister;
            Object ViewState;
            if (pViewState is Pair)
            {
                pair1 = ((Pair)pViewState);
                pageStatePersister1.ControlState = pair1.First;
                ViewState = pair1.Second;
            }
            else
            {
                ViewState = pViewState;
            }
            LosFormatter mFormat = new LosFormatter();
            StringWriter mWriter = new StringWriter();
            mFormat.Serialize(mWriter, ViewState);
            String mViewStateStr = mWriter.ToString();
            byte[] pBytes = System.Convert.FromBase64String(mViewStateStr);
            pBytes = Compress(pBytes);
            String vStateStr = System.Convert.ToBase64String(pBytes);
            pageStatePersister1.ViewState = vStateStr;
            pageStatePersister1.Save();
        }


    }
}