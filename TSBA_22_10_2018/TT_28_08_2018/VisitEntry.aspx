﻿<%@ Page Language="C#" EnableEventValidation="false" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="VisitEntry.aspx.cs" Inherits="TaegutecSalesBudget.VisitEntryNew" %>


<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">

   <%-- <link href="VisitsTheme/fullcalendar/fullcalendar.css" rel="stylesheet" type="text/css" />--%>
    <link href="VisitsTheme/css/jquery.qtip-2.2.0.css" rel="stylesheet" type="text/css" />

    <link href="css/visit_plan.css" rel="stylesheet" />

    <link href="media/kns_calendar/kns_calendar.css" rel="stylesheet" />
    <link href="media/kns_calendar/fSelect.css" rel="stylesheet" />
    <link href="media/kns_calendar/kns_modal.css" rel="stylesheet" />
    <script src="VisitsTheme/jquery/moment-2.8.1.min.js" type="text/javascript"></script>
    <script src="VisitsTheme/jquery/jquery-ui-1.11.1.js" type="text/javascript"></script>

    <script src="VisitsTheme/jquery/jquery.qtip-2.2.0.js" type="text/javascript"></script>

    <link href="media/css/themes/south/jquery-ui.theme.min.css" rel="stylesheet" type="text/css" charset="utf-8" />
    <%--<script src="media/kns_calendar/kns_calendar.min.js"></script>--%>
    <script src="js/fullcalendar.js"></script>



    <style type='text/css'>
       

        body {
            margin-top: 40px;
            text-align: center;
            font-size: 14px;
            font-family: "Lucida Grande",Helvetica,Arial,Verdana,sans-serif;
        }

        #calendar {
            width: 900px;
            margin: 0 auto;
        }
        /* css for timepicker */
        .ui-timepicker-div dl {
            text-align: left;
        }

        .control-label2 {
            font-weight: normal;
            padding-top: 9px;
        }

        input#MainContent_btnFilter {
            margin-top: 20px;
        }

        .ui-timepicker-div dl dt {
            height: 25px;
        }

        .ui-timepicker-div dl dd {
            margin: -25px 0 10px 65px;
        }

        .style1 {
            width: 100%;
        }

        input[type=submit]:disabled{
            cursor: not-allowed;
            background-color: #f9f9f9;
            color: #adadad;
            width: 18% !important;
            background-color: #419641;
            color: #333333;
            font-family: "Segoe UI", Helvetica, Arial, sans-serif;
        }

        .fc th, .fc td {
            border-style: 1px solid !important;
        }

        /* table fields alignment*/
        .multiple {
            padding: 6px 6px !important;
            height: 0px !important;
            font-size: 12px;
            border: none !important;
        }

        #calendar {
            margin-top: 24px;
        }

        .fs-search:before {
            position: absolute;
            right: 0px;
            margin-right: 15px;
            top: 12px;
        }

        .aspNetDisabled {
            width: 70.4% !important;
            height: auto;
            padding: 5px 5px 5px 5px;
            margin: 5px 1px 3px 0px;
        }

        .ui-datepicker-trigger {
            padding-top: 15px;
        }

        .col-sm-3 {
            width: 29% !important;
        }

        .rbtn_panel {
            list-style: none;
            width: 240px !important;
            padding: 0px 2px 0px 9px;
            margin-bottom: 0px;
        }
        .fc-event-container {
            width:60%;
        }
        /*
        .fc-event-container,.fc-draggable {
            width:50%;
            float:right;
        }*/
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableHistory="true" EnablePartialRendering="true" EnablePageMethods="true" AsyncPostBackTimeout="360">
    </asp:ScriptManager>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div class="contentpanel">
                <!--\\\\\\\ contentpanel start\\\\\\-->
                <div class="container clear_both padding_fix">
                    <div class="row table_display table_type">
                        <div class="col-md-12">
                            <div class='block-web' style='float: left; width: 100%'>
                                <div class="header">
                                    <div class="crumbs">
                                        <!-- Start : Breadcrumbs -->
                                        <ul id="breadcrumbs" class="breadcrumb">
                                            <li>
                                                <i class="fa fa-home"></i>
                                                <a>Visit</a>
                                            </li>
                                            <li class="current">Visit Made Entry</li>

                                        </ul>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div class='list_of_sales col-sm-3'>
                            <div id="MainContent_dfltreportform" runat="server" class="row filter_panel">
                                <div runat="server" id="cterDiv" visible="false">
                                    <ul class="btn-info rbtn_panel">
                                        <li><span style="margin-right: 4px; vertical-align: text-bottom;">TAEGUTEC</span>

                                            <asp:RadioButton ID="rdBtnTaegutec" AutoPostBack="true" Checked="true" OnCheckedChanged="rdBtnTaegutec_CheckedChanged" GroupName="byCmpnyCodeInradiobtn" runat="server" />
                                            <span style="margin-right: 4px; margin-left: 4px; vertical-align: text-bottom;">DURACARB</span>
                                            <asp:RadioButton ID="rdBtnDuraCab" AutoPostBack="true" OnCheckedChanged="rdBtnTaegutec_CheckedChanged" GroupName="byCmpnyCodeInradiobtn" runat="server" />
                                        </li>
                                    </ul>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="col-md-4 control-label2">BRANCH </label>
                                        <div class="col-md-4">
                                            <asp:DropDownList ID="fltrddlBranch" AutoPostBack="true" OnSelectedIndexChanged="fltrddlBranch_SelectedIndexChanged" runat="server" class="form-control" Style="width: 200px;"></asp:DropDownList>


                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="col-md-4 control-label2">SALES ENGINEER</label>
                                        <div class="col-md-4">
                                            <asp:DropDownList ID="fltrddlSalesEng" runat="server" class="form-control" Style="width: 200px;"></asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="col-md-4 " style="margin-top: 2px;">
                                    <div class="form-group">
                                        <%--          <asp:Button runat="server" ID ="btnFilter" CssClass="btn green" OnClick="btnFilter_Click" Text="FILTER" />--%>
                                        <input type="button" value="Filter" class="btn green" id="btnFilter" />
                                    </div>

                                </div>

                            </div>

                        </div>

                        <div id='calendar' class="col-sm-9"></div>
                        <div id="fullCalModal" class="modal fade">
                            <div class="modal-dialog1">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h4 id="modalTitle" class="modal-title">Visit Plan</h4>
                                    </div>
                                    <div id="modalBody" class="modal-body modalBody"></div>
                                    <div class="modal-footer">
                                        <input type="button" class="add_report btn btn-success" value="Edit Report">
                                        <input type="button" class="save_report btn btn-success" style="display: none" value="Save Report">
                                        <input type="button" class="close_report btn btn-success" data-dismiss="modal" value="Close">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <%--Create a new Visit or Edit--%>
                        <%--  <cc1:ModalPopupExtender ID="mp1" runat="server" PopupControlID="Panel1" TargetControlID="btnShow"
    CancelControlID="btnClose" BackgroundCssClass="modalBackground">
</cc1:ModalPopupExtender>
<asp:Panel ID="Panel1" runat="server" CssClass="modalPopup" align="center" Style="display: none">--%>
                        <div id="addDialog" class="modal fade">
                            <div class="modal-dialog1">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h4 id="H1" class="modal-title">Visit Entry</h4>
                                    </div>
                                    <div id='Div2'></div>
                                    <div id="Div1" class="modal-body">
                                        <form id='plan_entry'>
                                            <table width='100%' cellspacing='2' cellpadding='3' border='0' bgcolor='#efefef'>
                                                <tr>
                                                    <td>
                                                        <table width='100%' cellspacing='3' cellpadding='1' border='0' bgcolor='#ffffff'>
                                                            <tr>
                                                                <td width='20%' style='width: 420px;' class='table_holder'>
                                                                    <table width='100%' border='0' cellpadding='1' cellspacing='1' bgcolor='#ffffff'>
                                                                        <td>
                                                                            <asp:HiddenField ID="hdnvalue" runat="server" />
                                                                        </td>
                                                                        <td>
                                                                            <asp:HiddenField ID="hdnValueSubmitFlag" runat="server" />
                                                                        </td>
                                                                        <td>
                                                                            <asp:HiddenField ID="hdnValueId" runat="server" />
                                                                        </td>
                                                                        <td>
                                                                            <asp:HiddenField ID="hdnValDistCustomer" runat="server" />
                                                                        </td>
                                                                        <td>
                                                                            <asp:HiddenField ID="hdnSEname" runat="server" />
                                                                        </td>
                                                                        <tr>
                                                                            <td class='text_data_darker'>Engineer Name</td>

                                                                            <td class='text_data'>
                                                                                <asp:TextBox runat="server" ID="txtEngineername" ReadOnly="true" class='remark kns_inputText kns_inputSelectWidth' size='25'></asp:TextBox>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <asp:HiddenField ID="eventId" runat="server" />
                                                                            <td class='text_data_darker'>Agenda<span class='star'>*</span></td>

                                                                            <td class='text_data'>
                                                                                <asp:ListBox runat="server" onChange="checkFotCustomer()" ID="lbxAgenda" OnSelectedIndexChanged="visit_type_SelectedIndexChanged" SelectionMode="multiple" size='1' class='plan_select_box kns_inputText kns_inputSelectWidth'></asp:ListBox>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class='text_data_darker'>Customer  type </td>
                                                                            <td class='text_data'>
                                                                                <asp:DropDownList ID="ddlCustomer" AutoPostBack="true" runat="server" OnSelectedIndexChanged="ddlCustomer_SelectedIndexChanged" size='1' class='plan_select_box kns_inputText kns_inputSelectWidth'>
                                                                                    <asp:ListItem Text="CUSTOMER" Value="C" />
                                                                                    <asp:ListItem Text="CHANNEL PARTNER" Value="D" />
                                                                                </asp:DropDownList>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class='text_data_darker'>Channel Partner  Name<span id="cpstar" class='star'>*</span></td>
                                                                            <td class='text_data'>
                                                                                <asp:DropDownList ID="ddlDistributorList" AutoPostBack="true" OnSelectedIndexChanged="ddlDistributorList_SelectedIndexChanged" runat="server" size='1' class='plan_select_box kns_inputText kns_inputSelectWidth'></asp:DropDownList>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class='text_data_darker'>Customer  Name <span id="cnstar" class='star'>*</span></td>
                                                                            <td class='text_data'>
                                                                                <asp:DropDownList ID="ddlCustomerList" AutoPostBack="true" OnSelectedIndexChanged="ddlCustomerList_SelectedIndexChanged" runat="server" class='remark kns_inputText kns_inputSelectWidth'></asp:DropDownList>
                                                                            </td>
                                                                        </tr>
                                                                    
                                                                        <tr>
                                                                            <td  class='text_data_darker'>Customer Classification <span id="ccstar" class='star'>*</span></td>
                                                                            <td class='text_data'>
                                                                                <%--                                                <input type='text' name='contact_number' id='Text2' class='remark kns_inputText kns_inputSelectWidth' size='25'>--%>
                                                                                <asp:TextBox runat="server" ID="txtBxCustomerClass" ReadOnly="true" class='remark kns_inputText kns_inputSelectWidth' size='25'></asp:TextBox>
                                                                            </td>
                                                                        </tr>
                                                                      
                                                                        <tr>
                                                                            <td class='text_data_darker'>Remarks<span class='star'>*</span> </td>
                                                                            <td class='text_data'>
                                                                                <asp:TextBox ID="txtRemarks" TextMode="multiline" Columns="50" Rows="5" runat="server" class='purpose kns_inputText kns_inputSelectWidth' size='25' />
                                                                            </td>
                                                                        </tr>
                                                                        <tr id="trProject">
                                                                            <td class='text_data_darker'>Project</td>
                                                                            <td class='text_data '>
                                                                                <%--                                              <input type='text' name='contact_number' id='Text4' class='remark kns_inputText kns_inputSelectWidth' size='25'>--%>
                                                                                <asp:DropDownList ID="ddlProject" AutoPostBack="true" runat="server" OnSelectedIndexChanged="ddlProject_SelectedIndexChanged" class='remark kns_inputText kns_inputSelectWidth' size='1'></asp:DropDownList>
                                                                            </td>
                                                                        </tr>
                                                                        <tr id="trPrjctmember">
                                                                            <td class='text_data_darker'>Project Member</td>
                                                                            <td class='text_data time_picker'>
                                                                                <%--                                              <input type='text' name='contact_number' id='Text5' class='remark kns_inputText kns_inputSelectWidth' size='25'>--%>
                                                                                <asp:TextBox runat="server" ReadOnly="true" ID="txtBxProjectMember" CssClass="remark kns_inputText kns_inputSelectWidth" size='25'></asp:TextBox>

                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class='text_data_darker'>Visit Start:</td>
                                                                            <td class="text_data">
                                                                                <%--                                                        <asp:Label runat="server" id="lblEventStartDate"><span  runat="server" ></span></asp:Label>--%>
                                                                                <asp:TextBox ID="addEventStartDate" EnableViewState="true" ReadOnly="true" runat="server" class='remark kns_inputText kns_inputSelectWidth' size='25'></asp:TextBox>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="text_data_darker">Visit End:</td>
                                                                            <td class="text_data">
                                                                                <%--                                                        <asp:Label runat="server" id="lblEventEndDate"><span  runat="server" ></span></asp:Label>--%>
                                                                                <asp:TextBox ID="addEventEndDate" ReadOnly="true" runat="server" class='remark kns_inputText kns_inputSelectWidth' size='25'></asp:TextBox>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class='text_data_darker'>No of Hours</td>
                                                                            <td class='text_data'>
                                                                                <input type='text' readonly="true" name='contact_number' id='txtTotalHrs' class='remark kns_inputText kns_inputSelectWidth' size='25'></td>
                                                                        </tr>
                                                                    </table>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                            </table>
                                        </form>
                                    </div>
                                    <div class="modal-footer">
                                        <input type="button" id="btnclose" class="close_visit_entry btn btn-success" value="Close" data-dismiss="modal">
                                        <asp:Button ID="btnSave" OnClick="btnSave_Click" Text="Save &  Exit" class="btn btn-success" runat="server" />
                                        <asp:Button ID="btnSubmit"  OnClick="btnSubmit_Click" Text="Submit" class="btn btn-success" runat="server" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <%--    </asp:Panel>--%>
                        <%-- Completed New visit or edit--%>
                    </div>
                    <div id="display_quote" class="modal fade">
                        <div class="modal-dialog1">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h4 id="H2" class="modal-title">QUOTE INFORMATION</h4>
                                </div>
                                <div class="display_quote_modalBody" class="modal-body">
                                </div>
                                <div class="modal-footer">
                                    <input type="button" class="close_visit_entry" value="Close" data-dismiss="modal">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--\\\\\\\ content panel end \\\\\\-->
            </div>
            <!--\\\\\\\ inner end\\\\\\-->
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="ddlCustomer" EventName="SelectedIndexChanged" />
            <asp:AsyncPostBackTrigger ControlID="ddlDistributorList" EventName="SelectedIndexChanged" />

            <asp:AsyncPostBackTrigger ControlID="ddlProject" EventName="SelectedIndexChanged" />
            <asp:AsyncPostBackTrigger ControlID="lbxAgenda" EventName="SelectedIndexChanged" />
            <asp:AsyncPostBackTrigger ControlID="fltrddlBranch" EventName="SelectedIndexChanged" />

            <asp:AsyncPostBackTrigger ControlID="rdBtnTaegutec" EventName="CheckedChanged" />
            <asp:AsyncPostBackTrigger ControlID="rdBtnDuraCab" EventName="CheckedChanged" />

            <asp:AsyncPostBackTrigger ControlID="btnSubmit" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="btnSave" EventName="Click" />

        </Triggers>
    </asp:UpdatePanel>
    <asp:UpdateProgress ID="updateProgress" runat="server">
        <ProgressTemplate>
            <div style="position: fixed; text-align: left; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.7;">

                <span style="border-width: 0px; position: fixed; padding: 50px; font-size: 30px; left: 40%; top: 40%; color: #fff">Please wait</span>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>


    <script type="text/javascript">
        var currentUpdateEvent;
        var addStartDate;
        var addEndDate;
        var globalAllDay;
        var tempStartDate;
        var tempEnddate;
        var projectID;
        var totalhrs;
        function updateEvent(event, element, start, end) {
            //alert(event.start.format("DD-MM-YYYY hh:mm:ss"));

            //var date = new Date(event.start);
            //var newstartdt = date.format("MM-dd-yyyy hh:mm:ss tt");
            //alert(date.toString());
            if ($(this).data("qtip")) $(this).qtip("destroy");
            $("#MainContent_txtEngineername").val("");
            currentUpdateEvent = event;

            $('#addDialog').modal();
            //alert(event.id);

            $('#MainContent_ddlCustomerList').change(function (e) {
                // put code here
            }).change();
            $('#MainContent_ddlDistributorList').change(function (e) {
                // put code here
            }).change();

            projectID = event.project_id;

            $("#MainContent_addEventStartDate").val("" + event.start.format("MM-DD-YYYY hh:mm:ss T"));
            $("#MainContent_addEventEndDate").val("" + event.end.format("MM-DD-YYYY hh:mm:ss T"));
            $("#MainContent_txtBxCustomerClass").val("" + event.customer_class);
            $("MainContent_txtEngineername").val("" + event.visited_salesengineer_name);

            $("#MainContent_txtRemarks").text("" + event.remarks);
            var agendaselected = event.agenda;
            var splitValue = agendaselected.split(',');
            console.log(splitValue);
            $.each(splitValue, function (index, val) {
                if (val != "") {
                    $("#MainContent_lbxAgenda option[value='" + val + "']").attr("selected", "true");
                }
            });
            $("#MainContent_ddlProject").val(event.project_id);
            $('#MainContent_hdnvalue').val(projectID);
            $('#MainContent_hdnValueId').val(event.id);
            $('#MainContent_hdnValueSubmitFlag').val();
            $('#MainContent_hdnSEname').val(event.visited_salesengineer_name);


            addStartDate = event.start;
            addEndDate = event.end;
            var dt1 = (event.start.format("MM-DD-YYYY hh:mm:ss T")) + "M";
            var dt2 = (event.end.format("MM-DD-YYYY hh:mm:ss T")) + "M";
            var from = new Date(Date.parse(dt1));
            var to = new Date(Date.parse(dt2));
            totalhrs = (parseInt(((to - from) / 1000 / 3600) * 100, 10)) / 100;
            $('#txtTotalHrs').val(totalhrs);

            if (event.distributor_number != null) {

                $('#MainContent_ddlCustomer').val('D');
                $('#MainContent_hdnValDistCustomer').val(event.customerNumber);
                $("#MainContent_ddlDistributorList").val(event.distributor_number);
            }
            if (event.customerNumber != null) {

                $("#MainContent_ddlCustomerList").val(event.customerNumber);
            }

            var flag = event.isSubmit;
            $('#MainContent_hdnValueSubmitFlag').val(flag);
            if (event.isSubmit == "1") {
                $('#addDialog').find('input, textarea, button, select').attr('disabled', 'disabled');
            }
        }

        function updateSuccess(updateResult) {
            //alert(updateResult);
        }

        function deleteSuccess(deleteResult) {
            //alert(deleteResult);
        }

        function addSuccess(addResult) {
            // if addresult is -1, means event was not added
            //    alert("added key: " + addResult);

            if (addResult != -1) {
                $('#calendar').fullCalendar('renderEvent',
                                {
                                    title: $("#addEventName").val(),
                                    start: addStartDate,
                                    end: addEndDate,
                                    id: addResult,
                                    description: $("#addEventDesc").val(),
                                    allDay: globalAllDay
                                },
                                true // make the event "stick"
                            );


                $('#calendar').fullCalendar('unselect');
            }
        }

        function UpdateTimeSuccess(updateResult) {
            alert(updateResult);
        }

        function selectDate(start, end, allDay) {
            var newstartdt = start.format("MM-DD-YYYY HH:MM:ss");
            var newenddt = end.format("MM-DD-YYYY HH:MM:ss");
            var today = new Date();
            var DD = today.getDate();
            var MM = today.getMonth() + 1; //January is 0!
            var YYYY = today.getFullYear();
            if (DD < 10) {
                DD = '0' + DD
            }
            if (MM < 10) {
                MM = '0' + MM
            }
            var hh = today.getHours();
            var mm = today.getMinutes();
            var ss = today.getSeconds();          

            var todaydate = MM + '-' + DD + '-' + YYYY + ' ' + hh + ':' + mm + ':' + ss;
            var newstartdate = new Date(newstartdt);
            var today_date = new Date(todaydate);
            var newenddate = new Date(newenddt);
            if (today_date.getTime() - newstartdate.getTime() < 0 || today_date.getTime() - newenddate.getTime() < 0){
            //if (newstartdt > todaydate || newenddt > todaydate) {
                alert("Please Select Valid Date");
                location.reload();
            }
            else {

                $('#addDialog').modal({
                    buttons: {
                        "Add": function () {
                            //alert("sent:" + addStartDate.format("dd-MM-yyyy hh:mm:ss tt") + "==" + addStartDate.toLocaleString());
                            var eventToAdd = {
                                title: $("#addEventName").val(),
                                description: $("#addEventDesc").val(),
                                start: addStartDate.toJSON(),
                                end: addEndDate.toJSON(),

                                allDay: isAllDay(addStartDate, addEndDate)
                            };

                            if (checkForSpecialChars(eventToAdd.title) || checkForSpecialChars(eventToAdd.description)) {
                                alert("please enter characters: A to Z, a to z, 0 to 9, spaces");
                            }
                            else {
                                //alert("sending " + eventToAdd.title);

                                PageMethods.addEvent(eventToAdd, addSuccess);
                                $(this).dialog("close");
                            }
                        }
                    }
                });
            }
            var newstartdt = start.format("MM-DD-YYYY hh:mm:ss T");
            //alert(newstartdt);
            //var dateEnd = new Date(end);
            var newEnddt = end.format("MM-DD-YYYY hh:mm:ss T")

            $("#MainContent_addEventStartDate").val("" + newstartdt);
            $("#MainContent_addEventEndDate").val("" + newEnddt);
            tempStartDate = "" + start.toLocaleString();
            tempEnddate = "" + end.toLocaleString();
            addStartDate = start;
            addEndDate = end;
            globalAllDay = allDay;

            var dt1 = (newstartdt) + "M";
            var dt2 = (newEnddt) + "M";
            var from = new Date(Date.parse(dt1));
            var to = new Date(Date.parse(dt2));
            totalhrs = (parseInt(((to - from) / 1000 / 3600) * 100, 10)) / 100;
            $('#txtTotalHrs').val(totalhrs);

            //alert(allDay);

        }

        function updateEventOnDropResize(event, allDay) {

            //alert("allday: " + allDay);
            var eventToUpdate = {
                id: event.id,
                start: event.start
            };

            // FullCalendar 1.x
            //if (allDay) {
            //    eventToUpdate.start.setHours(0, 0, 0);
            //}

            if (event.end === null) {
                eventToUpdate.end = eventToUpdate.start;
            }
            else {
                eventToUpdate.end = event.end;

                // FullCalendar 1.x
                //if (allDay) {
                //    eventToUpdate.end.setHours(0, 0, 0);
                //}
            }

            // FullCalendar 1.x
            //eventToUpdate.start = eventToUpdate.start.format("dd-MM-yyyy hh:mm:ss tt");
            //eventToUpdate.end = eventToUpdate.end.format("dd-MM-yyyy hh:mm:ss tt");

            // FullCalendar 2.x
            var endDate;
            if (!event.allDay) {
                endDate = new Date(eventToUpdate.end + 60 * 60000);
                endDate = endDate.toJSON();
            }
            else {
                endDate = eventToUpdate.end.toJSON();
            }

            eventToUpdate.start = eventToUpdate.start.toJSON();
            eventToUpdate.end = eventToUpdate.end.toJSON(); //endDate;
            eventToUpdate.allDay = event.allDay;

            PageMethods.UpdateEventTime(eventToUpdate, UpdateTimeSuccess);
        }

        function eventDropped(event, delta, revertFunc) {

            if ($(this).data("qtip")) $(this).qtip("destroy");
            if (event.isSubmit == 0) {
                // FullCalendar 2.x
                updateEventOnDropResize(event);


            } else {
                $(this).qtip("destroy");
                revertFunc();
            }
        }

        function eventResized(event, delta, revertFunc) {

            if ($(this).data("qtip")) $(this).qtip("destroy");
            if (event.isSubmit == 0) {
                // FullCalendar 2.x
                updateEventOnDropResize(event);

            }
            else {
                revertFunc();
            }
        }

        function checkForSpecialChars(stringToCheck) {
            var pattern = /[^A-Za-z0-9 ]/;
            return pattern.test(stringToCheck);
        }

        function isAllDay(startDate, endDate) {
            var allDay;

            if (startDate.format("HH:mm:ss") == "00:00:00" && endDate.format("HH:mm:ss") == "00:00:00") {
                allDay = true;
                globalAllDay = true;
            }
            else {
                allDay = false;
                globalAllDay = false;
            }

            return allDay;
        }

        function qTipText(start, end, description) {
            addStartDate = start;
            addEndDate = end;
            globalAllDay = allDay;
            //alert(allDay);
        }

        function updateEventOnDropResize(event, allDay) {

            //alert("allday: " + allDay);
            var eventToUpdate = {
                id: event.id,
                start: event.start
            };

            // FullCalendar 1.x
            //if (allDay) {
            //    eventToUpdate.start.setHours(0, 0, 0);
            //}

            if (event.end === null) {
                eventToUpdate.end = eventToUpdate.start;
            }
            else {
                eventToUpdate.end = event.end;

                // FullCalendar 1.x
                //if (allDay) {
                //    eventToUpdate.end.setHours(0, 0, 0);
                //}
            }

            // FullCalendar 1.x
            //eventToUpdate.start = eventToUpdate.start.format("dd-MM-yyyy hh:mm:ss tt");
            //eventToUpdate.end = eventToUpdate.end.format("dd-MM-yyyy hh:mm:ss tt");

            // FullCalendar 2.x
            var endDate;
            if (!event.allDay) {
                endDate = new Date(eventToUpdate.end + 60 * 60000);
                endDate = endDate.toJSON();
            }
            else {
                endDate = eventToUpdate.end.toJSON();
            }

            eventToUpdate.start = eventToUpdate.start.toJSON();
            eventToUpdate.end = eventToUpdate.end.toJSON(); //endDate;
            eventToUpdate.allDay = event.allDay;

            PageMethods.UpdateEventTime(eventToUpdate, UpdateTimeSuccess);
        }



        function checkForSpecialChars(stringToCheck) {
            var pattern = /[^A-Za-z0-9 ]/;
            return pattern.test(stringToCheck);
        }

        function isAllDay(startDate, endDate) {
            var allDay;

            if (startDate.format("HH:mm:ss") == "00:00:00" && endDate.format("HH:mm:ss") == "00:00:00") {
                allDay = true;
                globalAllDay = true;
            }
            else {
                allDay = false;
                globalAllDay = false;
            }

            return allDay;
        }

        //Edited by Aswini. 
        //Pop up display on mouse over of event
        function qTipText(start, end, customer_number, customer_short_name, customer_class, visited_salesengineer_name, remarks) {
            var text;

            if (end !== null)
                text = "<strong>Start:</strong> " + start.format("MM/DD/YYYY hh:mm T") +
                       "<br/><strong>End:</strong> " + end.format("MM/DD/YYYY hh:mm T") +
                       "<br/><br/><strong>Visited Eng. Name: </strong>" + visited_salesengineer_name +
                       "<br/><br/><strong>Customer Number: </strong>" + customer_number +
                       "<br/><strong>Customer Name: </strong>" + customer_short_name +
                       "<br/><strong>Customer Class: </strong>" + customer_class +
                       "<br/><strong>Remarks: </strong>" + remarks;
            else
                text = "<strong>Start:</strong> " + start.format("MM/DD/YYYY hh:mm T") +
                        "<br/><br/><strong>Visited Eng. Name: </strong>" + visited_salesengineer_name +
                        "<br/><br/><strong>Customer Number: </strong>" + customer_number +
                        "<br/><strong>Customer Name: </strong>" + customer_short_name +
                        "<br/><strong>Customer Class: </strong>" + customer_class +
                        "<br/><strong>Remarks: </strong>" + remarks;

            return text;
        }


        function eventDragStop(event, dayDelta, minuteDelta, allDay, revertFunc) {

            if (event.isSubmit == 0) {
                if ($(this).data("qtip")) $(this).qtip("destroy");

                return true;
            }
            else {
                if ($(this).data("qtip")) $(this).qtip("destroy");
                //$('.fc-bg').re
                return false;
            }
        }

        $(document).ready(function (events) {

            triggerScript();
            //events.preventDefault();
            //triggerClick();

        });
        $(window).load(function () {
            triggerClick();
        });
        function showPopUp() {
            $('#addDialog').modal();

        }
        function triggerScript() {
            $('#MainContent_rdBtnTaegutec').change(function () {
                $("#btnFilter").trigger("click");
                //triggerClick();
            });
            $('#MainContent_rdBtnDuraCab').change(function () {
                $("#btnFilter").trigger("click");
            });
            $('#btnFilter').click(function (events) {

                var branch = $('#MainContent_fltrddlBranch').val();
                var sp = $('#MainContent_fltrddlSalesEng').val();
                //alert(branch + '----' + sp);
                if (branch != 'ALL' && sp != 'ALL') {


                    var searchIndex = '.sale_person_' + sp;
                    var visitor_searchIndex = '.visited_person_' + sp;
                    $('#calendar .fc-event').css('visibility', 'hidden');
                    $('#calendar').find(searchIndex).css('visibility', 'visible');
                    $('#calendar').find(visitor_searchIndex).css('visibility', 'visible');
                } else if (branch != 'ALL' && sp == 'ALL') {


                    $('#calendar .fc-event').css('visibility', 'hidden');
                    $('#MainContent_fltrddlSalesEng option').each(function () {
                        var spIndex = $(this).val();
                        if (spIndex != 'ALL') {
                            var searchIndex = '.sale_person_' + spIndex;
                            var visitor_searchIndex = '.visited_person_' + spIndex;
                            $('#calendar').find(searchIndex).css('visibility', 'visible');
                            $('#calendar').find(visitor_searchIndex).css('visibility', 'visible');
                        }
                    });
                }
                else if (branch == 'ALL' && sp != 'ALL') {
                    $('#calendar .fc-event').css('visibility', 'hidden');
                    var searchIndex = '.sale_person_' + sp;
                    var visitor_searchIndex = '.visited_person_' + sp;
                    $('#calendar .fc-event').css('visibility', 'hidden');
                    $('#calendar').find(searchIndex).css('visibility', 'visible');
                    $('#calendar').find(visitor_searchIndex).css('visibility', 'visible');
                }
                else if (branch == 'ALL' && sp == 'ALL') {
                    $('#calendar .fc-event').css('visibility', 'hidden');
                    $('#MainContent_fltrddlSalesEng option').each(function () {
                        var spIndex = $(this).val();
                        if (spIndex != 'ALL') {
                            var searchIndexs = '.sale_person_' + spIndex;
                            var visitor_searchIndex = '.visited_person_' + spIndex;

                            $('#calendar').find(searchIndex).css('visibility', 'visible');
                            $('#calendar').find(visitor_searchIndex).css('visibility', 'visible');
                        }
                    });
                }

            });
            //alert(projectID);
            if (projectID != null) {
                $("#MainContent_ddlProject").val(projectID);
                projectID = null;
            }
            agenda();

            var date = new Date(tempStartDate);
            var newstartdt = date.format("MM-dd-yyyy hh:mm:ss tt");
            var dateEnd = new Date(tempEnddate);
            var newEnddt = dateEnd.format("MM-dd-yyyy hh:mm:ss tt");
            var date = new Date();
            var d = date.getDate();
            var m = date.getMonth();
            var y = date.getFullYear();
            var options = {
                weekday: "long", year: "numeric", month: "short",
                day: "numeric", hour: "2-digit", minute: "2-digit"
            };


            //Display calender events, 
            // Edited by aswini_06_jan_2016
            $('#calendar').fullCalendar({

                theme: true,
                header: {
                    left: 'prev,next today',
                    center: 'title',
                    right: 'month,agendaWeek,agendaDay'
                },
                defaultView: 'agendaWeek',
                eventClick: updateEvent,
                selectable: true,
                selectHelper: true,
                select: selectDate,
                editable: true,
                events: "CalendarJsonResponse.ashx",
                eventDrop: eventDropped,
                eventResize: eventResized,
                eventLimit: true,
                eventRender: function (event, element) {
                    element.addClass('sale_person_' + event.salesEngineer);
                    element.addClass('visited_person_' + event.visited_salesengineer_id);
                    //alert(event.visited_salesengineer_id);//
                    //alert(event.salesEngineer)
                    element.find('.fc-content').append("<strong>" + event.customer_short_name + "(" + event.customerNumber + ")" + "</strong>"); //+ "(" + event.salesEngineer + ")</strong>");
                    //alert(event.customer_short_name);//
                    element.qtip({
                        content: {
                            text: qTipText(event.start, event.end, event.customerNumber, event.customer_short_name, event.customer_class, event.visited_salesengineer_name, event.remarks),
                            title: '<strong>' + event.customer_short_name + "( " + event.customerNumber + " )" + '</strong>'
                        },
                        position: {
                            my: 'bottom left',
                            at: 'top right'
                        },
                        style: { classes: 'qtip-shadow qtip-rounded' }
                    });

                },


            });

            //YNR START
            $('#MainContent_lbxAgenda').change(function (changeEvent) {
                
                var val = $(this).val();
                $('#trProject').show();
                $('#trPrjctmember').show();
            });

            $('#txtTotalHrs').val(totalhrs);
            $('#btnclose').click(function () {
                location.reload();
            });
            function agenda() {
                var agendaselected = [];
                $('#MainContent_lbxAgenda :selected').each(function (i, selected) {
                    agendaselected.push($(selected).text());

                });
                $('#trProject').show();
                $('#trPrjctmember').show();
            }
        }
        function closePopUp() {
            //$('.modal-backdrop').hide();
            //$('#addDialog').hide();
            //alert('Sucessfully submitted');
            //alert("yes");
            //$('.modal-backdrop').hide();
            location.reload();
        }
        function triggerClick() {


            var branch = $('#MainContent_fltrddlBranch').val();
            var sp = $('#MainContent_fltrddlSalesEng').val();
            //alert(branch + '----' + sp);
            if (branch != 'ALL' && sp != 'ALL') {
                var searchIndex = '.sale_person_' + sp;
                var visitor_searchIndex = '.visited_person_' + sp;
                $('#calendar .fc-event').css('visibility', 'hidden');
                $('#calendar').find(searchIndex).css('visibility', 'visible');
                $('#calendar').find(visitor_searchIndex).css('visibility', 'visible');
            } else if (branch != 'ALL' && sp == 'ALL') {

                $('#calendar .fc-event').css('visibility', 'hidden');
                $('#MainContent_fltrddlSalesEng option').each(function () {
                    var spIndex = $(this).val();
                    if (spIndex != 'ALL') {

                        var searchIndex = '.sale_person_' + spIndex;
                        var visitor_searchIndex = '.visited_person_' + spIndex;// alert(visitor_searchIndex);
                        $('#calendar').find(searchIndex).css('visibility', 'visible');
                        $('#calendar').find(visitor_searchIndex).css('visibility', 'visible');
                    }
                });
            }
            else if (branch == 'ALL' && sp != 'ALL') {
                $('#calendar .fc-event').css('visibility', 'hidden');
                var searchIndex = '.sale_person_' + sp;
                var visitor_searchIndex = '.visited_person_' + sp;
                $('#calendar .fc-event').css('visibility', 'hidden');
                $('#calendar').find(searchIndex).css('visibility', 'visible');
                $('#calendar').find(visitor_searchIndex).css('visibility', 'visible');
            }
            else if (branch == 'ALL' && sp == 'ALL') {
                $('#calendar .fc-event').css('visibility', 'hidden');
                $('#MainContent_fltrddlSalesEng option').each(function () {
                    var spIndex = $(this).val();
                    if (spIndex != 'ALL') {
                        var searchIndexs = '.sale_person_' + spIndex;
                        var visitor_searchIndex = '.visited_person_' + spIndex;

                        $('#calendar').find(searchIndex).css('visibility', 'visible');
                        $('#calendar').find(visitor_searchIndex).css('visibility', 'visible');
                    }
                });
            }
        }
        function checkFotCustomer()
        {
            debugger;
            //var lbxAgenda = $('#lbxAgenda');
            var flag = 0;
            var lbxAgenda = document.getElementById("<%= lbxAgenda.ClientID%>");
            for (var i = 0; i < lbxAgenda.options.length; i++) {
                if (lbxAgenda.options[i].selected) {
                    if (lbxAgenda.options[i].innerHTML == "Internal Review" || lbxAgenda.options[i].innerHTML == "Monthly Meeting" || lbxAgenda.options[i].innerHTML == "Review with channel partners" || lbxAgenda.options[i].innerHTML == "Review with HO Persons" || lbxAgenda.options[i].innerHTML == "Visit to HO") {
                        flag = 1;
                        break;
                    }
                }
            }
            if (flag == 0) {
                $('#ccstar').show();
                $('#cpstar').show();
                $('#cnstar').show();
            }
            else {
                $('#ccstar').hide();
                $('#cpstar').hide();
                $('#cnstar').hide();
            }
        }
        
    </script>
</asp:Content>

