﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Globalization;
using System.Text.RegularExpressions;
using TaegutecSalesBudget.App_Code.BOL;
using System.Data;
using System.Text;
using System.IO;

namespace TaegutecSalesBudget
{
    public partial class VisitEntryNew : System.Web.UI.Page
    {
        #region Instance creation / Variable defining
        Budget objBudget = new Budget();
        Review objRSum = new Review();
        MDP objMDP = new MDP();
        string cter;
        Visit objVisitEntry = new Visit();
        public static DataTable dtProjectDetails;
        public static DataTable dtCutomerDetails;

        #endregion

        #region web methods : Jquery calling

        //this method only updates start and end time
        //this is called when a event is dragged or resized in the calendar
        [System.Web.Services.WebMethod(true)]
        public static string UpdateEventTime(ImproperCalendarEvent improperEvent)
        {
            List<int> idList = (List<int>)System.Web.HttpContext.Current.Session["idList"];
            if (idList != null && idList.Contains(improperEvent.id))
            {

                // FullCalendar 2.x
                EventDAO.updateEventTime(improperEvent.id,
                                         Convert.ToDateTime(improperEvent.start).ToUniversalTime(),
                                         Convert.ToDateTime(improperEvent.end).ToUniversalTime()
                                         );  //allDay parameter added for FullCalendar 2.x

                return "updated Customer Visit " + " update start to: " + improperEvent.start +
                    " update end to: " + improperEvent.end;
            }

            return "unable to update event with id: " + improperEvent.id;
        }

        [System.Web.Services.WebMethod(true)]
        public static string EditEvent(ImproperCalendarEvent improperEvent)
        {
            List<int> idList = (List<int>)System.Web.HttpContext.Current.Session["idList"];
            if (idList != null && idList.Contains(improperEvent.id))
            {

                // FullCalendar 2.x
                EventDAO.updateEventTime(improperEvent.id,
                                         Convert.ToDateTime(improperEvent.start).ToUniversalTime(),
                                         Convert.ToDateTime(improperEvent.end).ToUniversalTime()
                                         );  //allDay parameter added for FullCalendar 2.x

                return "updated Customer Visit " + " update start to: " + improperEvent.start +
                    " update end to: " + improperEvent.end;



            }

            return "unable to update event with id: " + improperEvent.id;
        }

        [System.Web.Services.WebMethod]
        public void GetCurrentTime(string name)
        {
            string sr = name;

        }
        #endregion

        private static bool CheckAlphaNumeric(string str)
        {
            return Regex.IsMatch(str, @"^[a-zA-Z0-9 ]*$");
        }

        #region Page Load
        protected void Page_Load(object sender, EventArgs e)
        {
            string str = Convert.ToString(Request.Form[addEventStartDate.UniqueID]);
            ddlDistributorList.Attributes.Add("Class", "plan_select_box kns_inputText kns_inputSelectWidth");
            if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }
            string customerType = ddlCustomer.SelectedItem.Value;
            string strUserId = Session["UserId"].ToString();
            string roleId = Session["RoleId"].ToString();
            string salesEngName = Session["UserName"].ToString();
            string branchCode = Session["BranchCode"].ToString();
            if (!IsPostBack)
            {
                #region Roles data Loading
                if (roleId == "SE")
                {
                    LoadCustomerDetails_SE(strUserId);
                    if (ddlCustomer.SelectedItem.Value != "D")
                    {
                        ddlDistributorList.Enabled = false;
                    }
                    LoadDistributor_SE(strUserId);
                }
                if (roleId == "BM")
                {
                    LoadCustomerDetails_BM(strUserId);
                    if (ddlCustomer.SelectedItem.Value != "D")
                    {
                        ddlDistributorList.Enabled = false;
                    }
                    LoadDistributor_BM(strUserId);
                }
                if (roleId == "HO")
                {
                    cterDiv.Visible = true;
                    if (Session["cter"] == null)
                    {
                        Session["cter"] = "TTA";
                        cter = "TTA";
                    }
                    if (Session["cter"].ToString() == "DUR")
                    {
                        rdBtnDuraCab.Checked = true;
                        rdBtnTaegutec.Checked = false;
                        cter = "DUR";
                    }
                    else
                    {
                        rdBtnTaegutec.Checked = true;
                        rdBtnDuraCab.Checked = false;
                        cter = "TTA";
                    }
                    LoadCustomerDetails_HO();
                    if (ddlCustomer.SelectedItem.Value != "D")
                    {
                        ddlDistributorList.Enabled = false;
                    }
                    LoadAllDistributors();

                }
                if (roleId == "TM")
                {
                    //DisableForm();
                    LoadDistributor_TM(strUserId);
                    if (ddlCustomer.SelectedItem.Value != "D")
                    {
                        ddlDistributorList.Enabled = false;
                    }
                    ddlCustomerList.Items.Clear();
                    LoadCustomerDetails_TM(strUserId);


                }
                #endregion
                LoadBranches();
                loadAgendas();
                txtEngineername.Text = Session["UserName"].ToString();

            }

        }
        #endregion

        #region Drop downs changing
        protected void ddlCustomer_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                setDates();
                if (ddlCustomer.SelectedItem.Value != "D")
                {
                    ddlDistributorList.Enabled = false;
                }
                ddlDistributorList.Attributes.Add("Class", "plan_select_box kns_inputText kns_inputSelectWidth");
                if (!String.IsNullOrEmpty(hdnValDistCustomer.Value))
                {
                    return;
                }
                string customerType;
                string strUserId = Session["UserId"].ToString();
                string roleId = Session["RoleId"].ToString();
                string salesEngName = Session["UserName"].ToString();
                string branchCode = Session["BranchCode"].ToString();
                customerType = ddlCustomer.SelectedItem.Value;
                if (roleId == "SE")
                {
                    if (customerType == "C")
                    {
                        LoadCustomerDetails_SE(strUserId);
                    }
                    else
                    {
                        ddlDistributorList.Enabled = true;
                        ddlCustomerList.Items.Clear();
                        LoadDistributor_SE(strUserId);
                    }

                }
                if (roleId == "BM")
                {
                    //LoadCustomerDetails_BM(branchCode);
                    //LoadDistributor_BM(branchCode);
                    if (customerType == "C")
                    {
                        LoadCustomerDetails_BM(strUserId);
                    }
                    else
                    {
                        ddlDistributorList.Enabled = true;
                        ddlCustomerList.Items.Clear();
                        LoadDistributor_BM(strUserId);
                    }

                }
                if (roleId == "HO")
                {
                    //DisableForm();
                    if (customerType == "C")
                    {
                        LoadCustomerDetails_HO();
                    }
                    else
                    {
                        ddlDistributorList.Enabled = true;
                        ddlCustomerList.Items.Clear();
                        LoadAllDistributors();
                    }
                }
                if (roleId == "TM")
                {
                    //DisableForm();
                    if (customerType == "C")
                    {
                        LoadDistributor_TM(strUserId);
                    }
                    else
                    {
                        ddlDistributorList.Enabled = true;
                        ddlCustomerList.Items.Clear();
                        LoadCustomerDetails_TM(strUserId);
                    }

                }
                if (IsPostBack)
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "triggerScript(),showPopUp();", true);
                }

            }
            catch (Exception ex)
            {
                LogFile("ddlDistributorList_SelectedIndexChanged", ex.Message.ToString(), "", "");
                ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "triggerScript(),showPopUp();", true);

            }
        }

        protected void ddlDistributorList_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                setDates();
                string roleId = Session["RoleId"].ToString();
                DataTable dtnewCustomer = new DataTable();
                if (!string.IsNullOrEmpty(hdnSEname.Value))
                {
                    txtEngineername.Text = hdnSEname.Value.ToString();
                }
                //txtdistCustomerName.Enabled = true;
                if (ddlDistributorList.SelectedItem != null)
                {
                    string distributor = ddlDistributorList.SelectedItem.Value;
                    if (distributor != "-- SELECT CHANNEL PARTNER --")
                    {
                        //objMDP.distributor = distributor;
                        // dtnewCustomer = objMDP.LoadCustomers();
                        LoadDistributorCustomer(ddlDistributorList.SelectedItem.Value);
                        if (!string.IsNullOrEmpty(hdnValDistCustomer.Value))
                        {
                            ddlCustomerList.SelectedValue = hdnValDistCustomer.Value;
                        }
                    }


                }
                if (hdnValueSubmitFlag.Value == "1")
                {
                    DisableForm();
                }
                else
                {
                    EnableForm();
                }
                ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "triggerScript(),showPopUp();", true);
            }
            catch (Exception ex)
            {
                LogFile("ddlDistributorList_SelectedIndexChanged", ex.Message.ToString(), "", ""); ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "triggerScript(),showPopUp();", true);

            }

        }

        protected void ddlCustomerList_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                setDates();
                if (hdnValueSubmitFlag.Value == "1")
                {
                    DisableForm();

                }
                else
                {
                    EnableForm();
                }
                if (!string.IsNullOrEmpty(hdnSEname.Value))
                {
                    txtEngineername.Text = hdnSEname.Value.ToString();
                }
                //string lblid = lblEventID.Text;
                if (ddlCustomerList.SelectedItem != null)
                {
                    if (!string.IsNullOrEmpty(hdnValDistCustomer.Value))
                    {
                        //string strUserId = Session["UserId"].ToString();
                        //LoadCustomerDetails_BM(strUserId);
                        LoadDistributorCustomer(ddlDistributorList.SelectedItem.Value);
                        ddlCustomerList.SelectedValue = hdnValDistCustomer.Value;
                        //ddlCustomerList.SelectedItem.Value = hdnValDistCustomer.Value;

                    }
                    string customerNumber = ddlCustomerList.SelectedItem.Value;
                    string customerClass;
                    if (dtCutomerDetails.Rows.Count > 1)
                    {
                        DataRow dr = dtCutomerDetails.AsEnumerable()
                       .SingleOrDefault(r => r.Field<string>("customer_number") == customerNumber);
                        customerClass = dr != null ? dr.Field<string>("customer_class") : "";
                        if (!string.IsNullOrEmpty(customerClass))
                        {
                            if (customerClass.Contains("KA"))
                            {
                                customerClass = "KEY FOCUSED CUSTOMER";
                            }
                            else if (customerClass.Contains("KF"))
                            {
                                customerClass = "‘A’ CLASS CUSTOMER";
                            }
                            else if (customerClass.Contains("OT"))
                            {
                                customerClass = "OTHERS";
                            }

                            txtBxCustomerClass.Text = customerClass;
                        }
                    }

                    StringBuilder sAgenda = new StringBuilder();
                    foreach (ListItem li in lbxAgenda.Items)
                    {
                        if (li.Selected)
                        {
                            sAgenda.Append(li);
                        }
                    }
                    string visitType = Convert.ToString(sAgenda);
                    // ddlVisitType.SelectedItem.Value;
                    if (!string.IsNullOrEmpty(visitType))
                    {
                        //if (visitType.Contains("Project Discussions"))
                        //{
                            DataTable dtprojects = loadProjects(customerNumber);
                            ddlProject.DataSource = loadProjects(customerNumber);
                            ddlProject.DataTextField = "Project_Title";
                            ddlProject.DataValueField = "Project_Number";

                            ddlProject.DataBind();
                            ddlProject.Items.Insert(0, "None");
                            ddlProject_SelectedIndexChanged(null, null);
                        //}
                    }
                }
            }
            catch (Exception ex)
            {
                LogFile("ddlCustomerList_SelectedIndexChanged", ex.Message.ToString(), "", "");
            }
            ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "triggerScript(),showPopUp();", true);

        }

        protected void visit_type_SelectedIndexChanged(object sender, EventArgs e)
        {
            //string str=lblEventID.Text;

            StringBuilder sAgenda = new StringBuilder();
            foreach (ListItem li in lbxAgenda.Items)
            {
                if (li.Selected)
                {
                    sAgenda.Append(li);
                }
            }
            string visitType = Convert.ToString(sAgenda);
            if (!string.IsNullOrEmpty(visitType))
            {

                //if (visitType.Contains("Project Discussions"))
                //{
                    //DataTable dtprojects = loadProjects(customerNumber);
                    string customerNumber = ddlCustomerList.SelectedValue;
                    if (!(string.IsNullOrEmpty(customerNumber) || customerNumber == "-- SELECT CUSTOMER --"))
                    {
                        ddlProject.DataSource = loadProjects(ddlCustomerList.SelectedItem.Value);
                        ddlProject.DataTextField = "Project_Title";
                        ddlProject.DataValueField = "Project_Number";

                        ddlProject.DataBind();
                        ddlProject.Items.Insert(0, "None");
                        string projectID = Convert.ToString(Request.Form[hdnvalue.UniqueID]);
                        if (!string.IsNullOrEmpty(projectID))
                        {
                            ddlProject.SelectedItem.Value = projectID;
                        }
                        ddlProject_SelectedIndexChanged(null, null);
                    }
                //}

            }
            setDates();
            ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "triggerScript(),showPopUp();", true);
        }

        protected void ddlProject_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                setDates();
                if (ddlProject.SelectedIndex != -1)
                {
                    string projectNumber = ddlProject.SelectedItem.Value;
                    if (projectNumber != "None" && projectNumber != "-- SELECT PROJECT --")
                    {
                        DataRow dr = dtProjectDetails.AsEnumerable()
                           .SingleOrDefault(r => r.Field<string>("Project_Number") == projectNumber);
                        txtBxProjectMember.Text = dr != null ? dr.Field<string>("EngineerName") : "";
                    }
                }
                else
                {
                    txtBxProjectMember.Text = "";
                }
                setDates();
                if (hdnValueSubmitFlag.Value == "1")
                {
                    DisableForm();

                }
                else
                {
                    EnableForm();
                }
                ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "triggerScript(),showPopUp();", true);
            }
            catch (Exception ex)
            {
                LogFile("ddlProject_SelectedIndexChanged ", ex.Message.ToString(), "", "");
                ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "triggerScript(); alert('Oops something went wrong. please refresh your page');", true);

            }
        }

        protected void fltrddlBranch_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }

            string roleId = Session["RoleId"].ToString();
            string userId = Session["UserId"].ToString();
            string branchcode = Session["BranchCode"].ToString();
            objRSum.BranchCode = roleId == "TM" && fltrddlBranch.SelectedItem.Value == "ALL" ? userId : fltrddlBranch.SelectedItem.Value;
            objRSum.roleId = roleId;
            objRSum.flag = "SalesEngineer";
            objRSum.cter = cter;
            DataTable dtData = objRSum.getFilterAreaValue(objRSum);

            if (dtData.Rows.Count != 0)
            {
                fltrddlSalesEng.DataSource = dtData;
                fltrddlSalesEng.DataTextField = "EngineerName";
                fltrddlSalesEng.DataValueField = "EngineerId";
                fltrddlSalesEng.DataBind();
                fltrddlSalesEng.Items.Insert(0, "ALL");
            }
            else
            {
                fltrddlSalesEng.DataSource = dtData;
                fltrddlSalesEng.DataTextField = "EngineerName";
                fltrddlSalesEng.DataValueField = "EngineerId";
                fltrddlSalesEng.DataBind();
                fltrddlSalesEng.Items.Insert(0, "NO SALES ENGINEER");
            }
            if (roleId == "SE")
            {
                fltrddlSalesEng.Items.Clear();
                fltrddlSalesEng.DataTextField = "EngineerName";
                fltrddlSalesEng.DataValueField = "EngineerId";
                fltrddlSalesEng.Items.Insert(0, new ListItem(Convert.ToString(Session["UserName"]), Convert.ToString(Session["UserId"])));
            }
            ScriptManager.RegisterStartupScript(this, this.GetType(), "fltrSe", "triggerScript();", true);


        }

        protected void rdBtnTaegutec_CheckedChanged(object sender, EventArgs e)
        {
            if (rdBtnTaegutec.Checked)
            {
                Session["cter"] = "TTA";
                cter = "TTA";
                LoadBranches();
            }
            if (rdBtnDuraCab.Checked)
            {
                Session["cter"] = "DUR";
                cter = "DUR";
                LoadBranches();
            }
            ScriptManager.RegisterStartupScript(this, this.GetType(), "rdbtn", "triggerClick();", true);
            return;

        }


        #endregion

        #region Button Save/Submit/Filter
        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }

            try
            {
                string seventId = Convert.ToString(Request.Form[hdnValueId.UniqueID]);
                int eventId = 0;
                if (!string.IsNullOrEmpty(seventId))
                {
                    eventId = Convert.ToInt32(seventId);
                }
                CalendarEvent cevent = new CalendarEvent();
                StringBuilder sAgenda = new StringBuilder();
                foreach (ListItem li in lbxAgenda.Items)
                {
                    if (li.Selected)
                    {
                        sAgenda.Append(li + ",");
                    }
                }
                cevent.agenda = Convert.ToString(sAgenda);
                if (ddlProject.SelectedItem != null)
                {
                    cevent.project_id = ddlProject.SelectedItem.Value;
                }
                cevent.id = eventId;
                if (ddlCustomer.SelectedItem.Value == "CUSTOMER")
                    cevent.customerNumber = ddlCustomerList.SelectedItem.Value;
                else
                    cevent.customerNumber = ddlCustomerList.SelectedValue;
                string customerName;
                DataRow dr = dtCutomerDetails.AsEnumerable()
               .SingleOrDefault(r => r.Field<string>("customer_number") == cevent.customerNumber);
                customerName = dr != null ? dr.Field<string>("customer_short_name") : null;
                cevent.customer_short_name = customerName;
                cevent.modified_date = Convert.ToString(System.DateTime.Now);
                cevent.customer_class = getCustomerClassCode(txtBxCustomerClass.Text);
                cevent.remarks = txtRemarks.Text;
                if (ddlCustomer.SelectedItem.Value == "D")
                {
                    cevent.distributor_number = ddlDistributorList.SelectedItem.Value;
                }

                string startdt = Convert.ToString(Request.Form[addEventStartDate.UniqueID]);
                string enddt = Convert.ToString(Request.Form[addEventEndDate.UniqueID]);

                cevent.isSubmit = 1;
                cevent.visit_date_start = (startdt + "M");
                cevent.visit_date_end = enddt + "M";

                cevent.visited_salesengineer_id = Session["UserId"].ToString();
                cevent.visited_salesengineer_name = Session["UserName"].ToString();
                cevent.created_date = Convert.ToString(System.DateTime.Now);

                bool result = IsValidated(cevent);
                setDates();
                if (result == true)
                {
                    int resultsave = EventDAO.addVisit(cevent);
                    if (resultsave == 1)
                    {
                        DisableForm();
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup3", "alert('SUCESSFULLY SUBMITTED');closePopUp();", true);
                    }
                    else ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup3", "alert('FAILED TO SUBMIT');closePopUp();", true);

                }

                //ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "triggerScript();", true);
            }
            catch (Exception ex) { LogFile("Visit Submit", ex.Message.ToString(), "", ""); }
            ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "triggerScript();", true);
        }

        protected void setDates()
        {
            try
            {
                addEventStartDate.Text = Convert.ToString(Request.Form[addEventStartDate.UniqueID]);
                addEventEndDate.Text = Convert.ToString(Request.Form[addEventEndDate.UniqueID]);
            }
            catch (Exception ex)
            {
                LogFile("setDates() : error", ex.Message.ToString(), "", "");

            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }

            try
            {
                string seventId = Convert.ToString(Request.Form[hdnValueId.UniqueID]);
                int eventId = 0;
                if (!string.IsNullOrEmpty(seventId))
                {
                    eventId = Convert.ToInt32(seventId);
                }
                CalendarEvent cevent = new CalendarEvent();
                StringBuilder sAgenda = new StringBuilder();
                foreach (ListItem li in lbxAgenda.Items)
                {
                    if (li.Selected)
                    {
                        sAgenda.Append(li + ",");
                    }
                }
                cevent.agenda = Convert.ToString(sAgenda);
                if (ddlProject.SelectedItem != null)
                {
                    cevent.project_id = ddlProject.SelectedItem.Value;
                }
                cevent.id = eventId;
                if (ddlCustomer.SelectedItem.Value == "CUSTOMER")
                    cevent.customerNumber = ddlCustomerList.SelectedItem.Value;
                else
                    cevent.customerNumber = ddlCustomerList.SelectedValue;
                string customerName;
                DataRow dr = dtCutomerDetails.AsEnumerable()
               .SingleOrDefault(r => r.Field<string>("customer_number") == cevent.customerNumber);
                customerName = dr != null ? dr.Field<string>("customer_short_name") : null;
                cevent.customer_short_name = customerName;
                if (ddlCustomer.SelectedItem.Value == "D")
                {
                    cevent.distributor_number = ddlDistributorList.SelectedItem.Value;
                }

                cevent.modified_date = Convert.ToString(System.DateTime.Now);
                cevent.customer_class = getCustomerClassCode(txtBxCustomerClass.Text);
                cevent.remarks = txtRemarks.Text;

                string startdt = Convert.ToString(Request.Form[addEventStartDate.UniqueID]);
                string enddt = Convert.ToString(Request.Form[addEventEndDate.UniqueID]);
                cevent.isSubmit = 0;

                cevent.visit_date_start = (startdt + "M");
                cevent.visit_date_end = enddt + "M";

                cevent.visited_salesengineer_id = Session["UserId"].ToString();
                cevent.visited_salesengineer_name = Session["UserName"].ToString();
               
                cevent.created_date = Convert.ToString(System.DateTime.Now);
                bool result = saveValidate(cevent);
                if (result == true)
                {
                    int resultsave = EventDAO.addVisit(cevent);
                    if (resultsave == 1)
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup3", "alert('SUCESSFULLY SAVED');closePopUp();", true);
                    }
                    else ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup3", "alert('FAILED TO SAVE');closePopUp();", true);

                }
            }
            catch (Exception ex) { LogFile("Visit Save", ex.Message.ToString(), "", ""); }
            ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "triggerScript();", true);
        }

        /// <summary>
        /// Not in use 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnFilter_Click(object sender, EventArgs e)
        {
            string branch = fltrddlBranch.SelectedItem.Value == "ALL" ? null : fltrddlBranch.SelectedItem.Value;
            string seid = fltrddlSalesEng.SelectedItem.Value == "ALL" ? null : fltrddlSalesEng.SelectedItem.Value;
            string temp = "CalendarJsonResponse.ashx?branch=" + branch + "&seid=" + seid;
            Session["Cal_Branch"] = branch;
            Session["Cal_SE"] = seid;
            Session["Cal_Filter"] = 1;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopups5", "triggerScript();", true);

        }

        #endregion

        #region Validate
        protected bool saveValidate(CalendarEvent cevent)
        {
            try
            {
                bool result = true;
                bool checkAgenda = false;
                StringBuilder sAgenda = new StringBuilder();
                foreach (ListItem li in lbxAgenda.Items)
                {
                    if (li.Selected)
                    {
                        if (li.Text == "Internal Review" || li.Text == "Monthly Meeting" || li.Text == "Review with channel partners" || li.Text == "Review with HO Persons" || li.Text == "Visit to HO")
                        {
                            checkAgenda = true;
                        }
                    }
                }
                if (checkAgenda == false)
                {
                    if (ddlCustomer.SelectedItem.Value == "D")
                    {

                        if (ddlDistributorList.SelectedItem.Value == "-- SELECT CHANNEL PARTNER --")
                        {
                            result = false;
                            ddlDistributorList.Enabled = true;
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopups3", "triggerScript(),showPopUp();alert('Please  select Channel Partner');", true);
                            setDates();
                            return result;
                        }
                        if (string.IsNullOrEmpty(cevent.customerNumber) || cevent.customerNumber == "-- SELECT CUSTOMER --")
                        {
                            result = false;
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopups4", "triggerScript(),showPopUp();alert('Please select a Customer');", true);
                            setDates();
                            return result;
                        }
                        else
                        {
                            result = true;
                        }

                    }
                    if (string.IsNullOrEmpty(cevent.customerNumber) || cevent.customerNumber == "-- SELECT CUSTOMER --")
                    {
                        result = false;
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopups1", "triggerScript(),showPopUp();alert('Please select a Customer');", true);
                        setDates();
                        return result;
                    }

                }
                else{

                        if (ddlDistributorList.SelectedItem.Value == "-- SELECT CHANNEL PARTNER --")
                        {
                            cevent.distributor_number = null;
                        }
                        if (cevent.customerNumber == "-- SELECT CUSTOMER --")
                        {
                            cevent.customerNumber = null;
                        }
                    result = true;
                }
                if (string.IsNullOrEmpty(cevent.agenda))
                {
                    result = false;
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopups5", "triggerScript(),showPopUp();alert('Please  select agenda');", true);
                    setDates();
                    return result;
                }
                setDates();
                    return result;
                
            }
            catch (Exception ex)
            {
                LogFile("Save validation", ex.Message.ToString(), "", "");
                ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "triggerScript(); alert('Oops something went wrong. please refresh your page');", true);
                return false;
            }

        }
        private bool IsValidated(CalendarEvent cevent)
        {
            try
            {
                bool result = true;
                bool checkAgenda = false;
                StringBuilder sAgenda = new StringBuilder();
                foreach (ListItem li in lbxAgenda.Items)
                {
                    if (li.Selected)
                    {
                        if (li.Text == "Internal Review" || li.Text == "Monthly Meeting" || li.Text == "Review with channel partners" || li.Text == "Review with HO Persons" || li.Text == "Visit to HO")
                        {
                            checkAgenda = true;
                        }
                    }
                }
                if (checkAgenda == false)
                {
                    if (ddlCustomer.SelectedItem.Value == "D")
                    {
                        if (ddlDistributorList.SelectedItem.Value == "-- SELECT CHANNEL PARTNER --")
                        {
                            result = false;
                            ddlDistributorList.Enabled = true;
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup3", "triggerScript(),showPopUp();alert('Please  select Channel Partner');", true);
                            setDates();
                            return result;
                        }
                        if (string.IsNullOrEmpty(cevent.customerNumber) || cevent.customerNumber == "-- SELECT CUSTOMER --")
                        {
                            result = false;
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup4", "triggerScript(),showPopUp();alert('Please select a Customer');", true);
                            setDates();
                            return result;
                        }
                        else
                        {
                            result = true;
                        }
                    }
                    if (string.IsNullOrEmpty(cevent.customerNumber) || cevent.customerNumber == "-- SELECT CUSTOMER --")
                    {
                        result = false;
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup1", "triggerScript(),showPopUp();alert('Please select a Customer');", true);
                        setDates();
                        return result;
                    }
                    
                }
                else
                {
                    if (ddlDistributorList.SelectedItem.Value == "-- SELECT CHANNEL PARTNER --")
                    {
                        cevent.distributor_number = null;
                    }
                    if (cevent.customerNumber == "-- SELECT CUSTOMER --")
                    {
                        cevent.customerNumber = null;
                    }
                    result = true;
                }
                if (string.IsNullOrEmpty(cevent.remarks))
                {
                    result = false;
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup2", "triggerScript(),showPopUp();alert('Please  fill remarks');", true);
                    setDates();
                    return result;
                }
                if (string.IsNullOrEmpty(cevent.agenda))
                {
                    result = false;
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup5", "triggerScript(),showPopUp();alert('Please  select agenda');", true);
                    setDates();
                    return result;
                }
                setDates();
                return result;
            }
            catch (Exception ex)
            {
                LogFile("submit validation", ex.Message.ToString(), "", "");
                ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "triggerScript(); alert('Oops something went wrong. please refresh your page');", true);
                return false;
            }
        }


        #endregion

        #region Channel Partner Loading
        protected void LoadDistributor_TM(string strUserId)
        {
            //dtCutomerDetails = new DataTable();
            //dtCutomerDetails = objMDP.LoadDistributor_TM(strUserId);
            DataTable dtDistributor = new DataTable();
            dtDistributor = objBudget.LoadCustomerDetails("ALL", "TM", strUserId, null, "D");
            if (dtDistributor != null)
            {
                DataTable dtDeatils = new DataTable();
                dtDeatils.Columns.Add("customer_number", typeof(string));
                dtDeatils.Columns.Add("customer_name", typeof(string));


                for (int i = 0; i < dtDistributor.Rows.Count; i++)
                {
                    //dtDeatils.Rows.Add(dtCutomerDetails.Rows[i].ItemArray[0].ToString(), dtCutomerDetails.Rows[i].ItemArray[1].ToString() + " " + "(" + dtCutomerDetails.Rows[i].ItemArray[0].ToString() + ")");
                    dtDeatils.Rows.Add(dtDistributor.Rows[i].ItemArray[1].ToString(), dtDistributor.Rows[i].ItemArray[2].ToString() + " " + "(" + dtDistributor.Rows[i].ItemArray[1].ToString() + ")");
                }
                ddlDistributorList.DataSource = dtDeatils;
                ddlDistributorList.DataTextField = "customer_name";
                ddlDistributorList.DataValueField = "customer_number";
                ddlDistributorList.DataBind();
                ddlDistributorList.Items.Insert(0, "-- SELECT CHANNEL PARTNER --");



            }
        }
        protected void LoadDistributorCustomer(string distributor)
        {
            dtCutomerDetails = new DataTable();
            dtCutomerDetails = objMDP.loadDistributors_customers_v(distributor);
            if (dtCutomerDetails != null)
            {
                DataTable dtDeatils = new DataTable();
                dtDeatils.Columns.Add("customer_number", typeof(string));
                dtDeatils.Columns.Add("customer_name", typeof(string));


                for (int i = 0; i < dtCutomerDetails.Rows.Count; i++)
                {
                    dtDeatils.Rows.Add(dtCutomerDetails.Rows[i].ItemArray[1].ToString(), dtCutomerDetails.Rows[i].ItemArray[2].ToString() + " " + "(" + dtCutomerDetails.Rows[i].ItemArray[1].ToString() + ")");
                }
                ddlCustomerList.DataSource = dtDeatils;
                ddlCustomerList.DataTextField = "customer_name";
                ddlCustomerList.DataValueField = "customer_number";
                ddlCustomerList.DataBind();
                ddlCustomerList.Items.Insert(0, "-- SELECT CUSTOMER --");

            }

        }
        protected void LoadDistributor_SE(string strUserId)
        {
            //dtCutomerDetails = new DataTable();
            //dtCutomerDetails = objMDP.LoadDistributor_SE(strUserId);
            DataTable dtDistributor = new DataTable();
            dtDistributor = objBudget.LoadCustomerDetails(strUserId, "SE", null, null, "D");
            if (dtDistributor != null)
            {
                DataTable dtDeatils = new DataTable();
                dtDeatils.Columns.Add("customer_number", typeof(string));
                dtDeatils.Columns.Add("customer_name", typeof(string));


                for (int i = 0; i < dtDistributor.Rows.Count; i++)
                {
                    //dtDeatils.Rows.Add(dtCutomerDetails.Rows[i].ItemArray[0].ToString(), dtCutomerDetails.Rows[i].ItemArray[1].ToString() + " " + "(" + dtCutomerDetails.Rows[i].ItemArray[0].ToString() + ")");
                    dtDeatils.Rows.Add(dtDistributor.Rows[i].ItemArray[1].ToString(), dtDistributor.Rows[i].ItemArray[2].ToString() + "(" + dtDistributor.Rows[i].ItemArray[1].ToString() + ")");
                }
                ddlDistributorList.DataSource = dtDeatils;
                ddlDistributorList.DataTextField = "customer_name";
                ddlDistributorList.DataValueField = "customer_number";
                ddlDistributorList.DataBind();
                ddlDistributorList.Items.Insert(0, "-- SELECT CHANNEL PARTNER --");
            }
        }
        protected void LoadDistributor_BM(string strUserId)
        {
            //dtCutomerDetails = new DataTable();
            //dtCutomerDetails = objMDP.LoadDistributor_BM(branchCode);
            DataTable dtDistributor = new DataTable();

            dtDistributor = objBudget.LoadCustomerDetails(strUserId, "BM", null, null, "D");
            if (dtDistributor != null)
            {
                DataTable dtDeatils = new DataTable();
                dtDeatils.Columns.Add("customer_number", typeof(string));
                dtDeatils.Columns.Add("customer_name", typeof(string));


                for (int i = 0; i < dtDistributor.Rows.Count; i++)
                {
                    //dtDeatils.Rows.Add(dtCutomerDetails.Rows[i].ItemArray[0].ToString(), dtCutomerDetails.Rows[i].ItemArray[1].ToString() + " " + "(" + dtCutomerDetails.Rows[i].ItemArray[0].ToString() + ")");
                    dtDeatils.Rows.Add(dtDistributor.Rows[i].ItemArray[0].ToString(), dtDistributor.Rows[i].ItemArray[1].ToString() + "(" + dtDistributor.Rows[i].ItemArray[0].ToString() + ")");
                }
                ddlDistributorList.DataSource = dtDeatils;
                ddlDistributorList.DataTextField = "customer_name";
                ddlDistributorList.DataValueField = "customer_number";
                ddlDistributorList.DataBind();
                ddlDistributorList.Items.Insert(0, "-- SELECT CHANNEL PARTNER --");
            }
        }

        protected void LoadAllDistributors()
        {
            DataTable dtDistributor = new DataTable();
            //  dtDistributor = objMDP.LoadAllDistributors();
            dtDistributor = objBudget.LoadCustomerDetails(null, "HO", null, cter, "D");
            if (dtDistributor != null)
            {
                DataTable dtDst = new DataTable();
                //dtDeatils.Columns.Add("distributor_number", typeof(string));
                //dtDeatils.Columns.Add("distributor_name", typeof(string));
                dtDst.Columns.Add("customer_number", typeof(string));
                dtDst.Columns.Add("customer_name", typeof(string));

                for (int i = 0; i < dtDistributor.Rows.Count; i++)
                {
                    // dtDeatils.Rows.Add(dtDistributor.Rows[i].ItemArray[0].ToString(), dtDistributor.Rows[i].ItemArray[1].ToString() + " " + "(" + dtDistributor.Rows[i].ItemArray[0].ToString() + ")");
                    dtDst.Rows.Add(dtDistributor.Rows[i].ItemArray[1].ToString(), dtDistributor.Rows[i].ItemArray[2].ToString() + " " + "(" + dtDistributor.Rows[i].ItemArray[1].ToString() + ")");

                }
                ddlDistributorList.DataSource = dtDst;
                //ddlDistributorList.DataTextField = "distributor_name";
                //ddlDistributorList.DataValueField = "distributor_number";
                ddlDistributorList.DataTextField = "customer_name";
                ddlDistributorList.DataValueField = "customer_number";
                ddlDistributorList.DataBind();
                ddlDistributorList.Items.Insert(0, "-- SELECT CHANNEL PARTNER --");



            }


        }

        #endregion

        #region Customer Loading
        protected void LoadCustomerDetails_TM(string strUserId)
        {
            dtCutomerDetails = new DataTable();
            // dtCutomerDetails = objMDP.LoadCustomerDetails_TM(strUserId);
            dtCutomerDetails = objBudget.LoadCustomerDetails("ALL", "TM", strUserId, null, "C");
            if (dtCutomerDetails != null)
            {
                DataTable dtDeatils = new DataTable();
                dtDeatils.Columns.Add("customer_number", typeof(string));
                dtDeatils.Columns.Add("customer_name", typeof(string));


                for (int i = 0; i < dtCutomerDetails.Rows.Count; i++)
                {
                    //dtDeatils.Rows.Add(dtCutomerDetails.Rows[i].ItemArray[1].ToString(), dtCutomerDetails.Rows[i].ItemArray[2].ToString() + " " + "(" + dtCutomerDetails.Rows[i].ItemArray[1].ToString() + ")");
                    dtDeatils.Rows.Add(dtCutomerDetails.Rows[i].ItemArray[1].ToString(), dtCutomerDetails.Rows[i].ItemArray[2].ToString() + " " + "(" + dtCutomerDetails.Rows[i].ItemArray[1].ToString() + ")");

                }
                ddlCustomerList.DataSource = dtDeatils;
                ddlCustomerList.DataTextField = "customer_name";
                ddlCustomerList.DataValueField = "customer_number";
                ddlCustomerList.DataBind();
                ddlCustomerList.Items.Insert(0, "-- SELECT CUSTOMER --");

            }
        }

        protected void LoadCustomerDetails_HO()
        {
            dtCutomerDetails = new DataTable();
            //dtCutomerDetails = objMDP.LoadCustomerDetails_HO();
            dtCutomerDetails = objBudget.LoadCustomerDetails(null, "HO", null, cter, "C");
            if (dtCutomerDetails != null)
            {
                DataTable dtDeatils = new DataTable();
                dtDeatils.Columns.Add("customer_number", typeof(string));
                dtDeatils.Columns.Add("customer_name", typeof(string));


                for (int i = 0; i < dtCutomerDetails.Rows.Count; i++)
                {
                    dtDeatils.Rows.Add(dtCutomerDetails.Rows[i].ItemArray[1].ToString(), dtCutomerDetails.Rows[i].ItemArray[2].ToString() + " " + "(" + dtCutomerDetails.Rows[i].ItemArray[1].ToString() + ")");
                }
                ddlCustomerList.DataSource = dtDeatils;
                ddlCustomerList.DataTextField = "customer_name";
                ddlCustomerList.DataValueField = "customer_number";
                ddlCustomerList.DataBind();
                ddlCustomerList.Items.Insert(0, "-- SELECT CUSTOMER --");

            }
        }


        protected void LoadCustomerDetails_SE(string strUserId)
        {
            dtCutomerDetails = new DataTable();
            // dtCutomerDetails = objMDP.LoadCustomerDetails_SE(strUserId);
            dtCutomerDetails = objBudget.LoadCustomerDetails(strUserId, "SE", null, null, "C");
            if (dtCutomerDetails != null)
            {
                DataTable dtDeatils = new DataTable();
                dtDeatils.Columns.Add("customer_number", typeof(string));
                dtDeatils.Columns.Add("customer_name", typeof(string));


                for (int i = 0; i < dtCutomerDetails.Rows.Count; i++)
                {
                    // dtDeatils.Rows.Add(dtCutomerDetails.Rows[i].ItemArray[1].ToString(), dtCutomerDetails.Rows[i].ItemArray[2].ToString() + " " + "(" + dtCutomerDetails.Rows[i].ItemArray[1].ToString() + ")");
                    dtDeatils.Rows.Add(dtCutomerDetails.Rows[i].ItemArray[1].ToString(), dtCutomerDetails.Rows[i].ItemArray[2].ToString() + " " + "(" + dtCutomerDetails.Rows[i].ItemArray[1].ToString() + ")");
                }
                ddlCustomerList.DataSource = dtDeatils;
                ddlCustomerList.DataTextField = "customer_name";
                ddlCustomerList.DataValueField = "customer_number";
                ddlCustomerList.DataBind();
                ddlCustomerList.Items.Insert(0, "-- SELECT CUSTOMER --");


            }
        }
        protected void LoadCustomerDetails_BM(string strUserId)
        {
            dtCutomerDetails = new DataTable();
            // dtCutomerDetails = objMDP.LoadCustomerDetails_BM(branchCode);
            dtCutomerDetails = objBudget.LoadCustomerDetails(strUserId, "BM", null, null, "C");
            if (dtCutomerDetails != null)
            {
                DataTable dtDeatils = new DataTable();
                dtDeatils.Columns.Add("customer_number", typeof(string));
                dtDeatils.Columns.Add("customer_name", typeof(string));


                for (int i = 0; i < dtCutomerDetails.Rows.Count; i++)
                {
                    //dtDeatils.Rows.Add(dtCutomerDetails.Rows[i].ItemArray[1].ToString(), dtCutomerDetails.Rows[i].ItemArray[2].ToString() + " " + "(" + dtCutomerDetails.Rows[i].ItemArray[1].ToString() + ")");
                    dtDeatils.Rows.Add(dtCutomerDetails.Rows[i].ItemArray[0].ToString(), dtCutomerDetails.Rows[i].ItemArray[1].ToString() + "(" + dtCutomerDetails.Rows[i].ItemArray[0].ToString() + ")");
                }

                ddlCustomerList.DataSource = dtDeatils;
                ddlCustomerList.DataTextField = "customer_name";
                ddlCustomerList.DataValueField = "customer_number";
                ddlCustomerList.DataBind();
                ddlCustomerList.Items.Insert(0, "-- SELECT CUSTOMER --");

            }
        }

        #endregion

        #region Load Projects
        protected DataTable loadProjects(string customerNumber)
        {
            //string id = lblEventID.Text;
            DataTable dtProjects = new DataTable();
            dtProjects.Columns.Add("Project_Number");
            dtProjects.Columns.Add("Project_Title");
            dtProjects.Columns.Add("EngineerName");
            dtProjectDetails = new DataTable();
            dtProjectDetails = objVisitEntry.getProjectDetails(customerNumber);
            for (int i = 0; i < dtProjectDetails.Rows.Count; i++)
            {
                dtProjects.Rows.Add(dtProjectDetails.Rows[i].Field<string>("Project_Number"), dtProjectDetails.Rows[i].Field<string>("Project_Title"), dtProjectDetails.Rows[i].Field<string>("EngineerName"));
            }
            return dtProjects;
        }

        #endregion

        //class order
        //{
        //    string branch { get; set; }
        //    string se { get; set; }
        //}

        #region customer Class Code
        protected string getCustomerClassCode(string fullClassName)
        {
            string code = "";
            if (!string.IsNullOrEmpty(fullClassName))
            {
                if (fullClassName.Contains("KEY FOCUSED CUSTOMER"))
                {
                    code = "KA";
                }
                else if (fullClassName.Contains("‘A’ CLASS CUSTOMER"))
                {
                    code = "KF";
                }
                else if (fullClassName.Contains("OTHERS"))
                {
                    code = "OT";
                }
            }
            return code;
        }

        #endregion

        #region Enable/Disable form
        public void DisableForm()
        {
            lbxAgenda.Attributes.Add("disabled", "true");
            lbxAgenda.Enabled = false;
            ddlCustomerList.Enabled = false;
            ddlCustomer.Enabled = false;
            ddlDistributorList.Enabled = false;
            ddlProject.Enabled = false;
            txtBxCustomerClass.Enabled = false;
            txtBxProjectMember.Enabled = false;
            txtRemarks.Enabled = false;
            btnSave.Enabled = false;
            btnSubmit.Enabled = false;
        }
        public void EnableForm()
        {
            lbxAgenda.Enabled = true;
            ddlCustomerList.Enabled = true;
            ddlCustomer.Enabled = true;
            ddlProject.Enabled = true;
            txtRemarks.Enabled = true;
            btnSave.Enabled = true;
            btnSubmit.Enabled = true;
        }
        #endregion

        #region loading branch
        protected void LoadBranches()
        {
            if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }
            string roleId = Session["RoleId"].ToString();
            string userId = Session["UserId"].ToString();
            string branchcode = Session["BranchCode"].ToString();
            DataTable dtData = new DataTable();
            objRSum.BranchCode = userId; // passing here territory Engineer Id  as branch code IF role is TM 
            objRSum.roleId = roleId;
            objRSum.flag = "Branch";
            objRSum.cter = cter;
            dtData = objRSum.getFilterAreaValue(objRSum);

            fltrddlBranch.DataSource = dtData;
            fltrddlBranch.DataTextField = "BranchDesc";
            fltrddlBranch.DataValueField = "BranchCode";
            fltrddlBranch.DataBind();
            if (roleId == "BM" || roleId == "SE")
            {
                fltrddlBranch.Items.Clear();
                fltrddlBranch.DataTextField = "BranchDesc";
                fltrddlBranch.DataValueField = "BranchCode";
                fltrddlBranch.Items.Insert(0, new ListItem(Convert.ToString(Session["BranchDesc"]), branchcode));
            }
            else
            {
                fltrddlBranch.Items.Insert(0, "ALL");
            }
            fltrddlBranch_SelectedIndexChanged(null, null);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "fltrBranch", "triggerScript();", true);

        }
        #endregion

        #region Log file
        public void LogFile(string use1, string count, string names, string use)
        {
            StreamWriter log;

            if (!File.Exists("logfile_Vist.txt"))
            {
                log = new StreamWriter(("C:\\TSBA_Log\\logfile_Vist.txt"), true);
            }
            else
            {
                log = File.AppendText(("C:\\TSBA_Log\\logfile_Vist.txt"));
            }

            // Write to the file:
            log.WriteLine("Date Time:" + DateTime.Now.ToString());
            log.WriteLine(use1);
            log.WriteLine(count);
            log.WriteLine(names);
            log.WriteLine(use + "\n");
            // Close the stream:
            log.Close();

        }
        #endregion

        #region Load agenda
        void loadAgendas()
        {
            DataTable dt = objVisitEntry.getAgenda();
            lbxAgenda.DataSource = dt;
            lbxAgenda.DataTextField = "agenda_name";
            lbxAgenda.DataValueField = "agenda_name";
            lbxAgenda.DataBind();

        }
        #endregion

    }

}