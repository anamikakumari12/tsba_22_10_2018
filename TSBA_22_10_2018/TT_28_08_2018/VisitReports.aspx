﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="VisitReports.aspx.cs" Inherits="TaegutecSalesBudget.VisitReports" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <link href="GridviewScroll.css" rel="stylesheet" />
    <script type="text/javascript" src="gridscroll_visitreport.js"></script>
    <script type="text/javascript" src="js/app.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <asp:ScriptManager ID="ScriptManager1" runat="server" AsyncPostBackTimeout="360" EnablePageMethods="true">
    </asp:ScriptManager>

    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div class="crumbs">
                <!-- Start : Breadcrumbs -->
                <ul id="breadcrumbs" class="breadcrumb">
                    <li>
                        <i class="fa fa-home"></i>
                        <a>Visit</a>
                    </li>
                    <li>Visit Made Reports</li>
                    <div>
                        <ul>
                            <li class="title_bedcrum" style="list-style: none;">VISIT MADE REPORTS</li>
                        </ul>
                    </div>
                </ul>
            </div>
            <!-- End : Breadcrumbs -->
            <div id="collapsebtn" class="row">
                <img id="product_image" src="images/up_arrow.png" align="left" style="margin-left: 46%;" />

            </div>
            <div class="row filter_panel" id="reportdrpdwns">

                <div>
                    <ul class="btn-info rbtn_panel">
                        <li><span style="margin-right: 4px; vertical-align: text-bottom;">TAEGUTEC</span>

                            <asp:RadioButton ID="rdBtnTaegutec" AutoPostBack="true" Checked="true" OnCheckedChanged="rdBtnTaegutec_CheckedChanged" GroupName="byCmpnyCodeInradiobtn" runat="server" />
                            <span style="margin-right: 4px; margin-left: 4px; vertical-align: text-bottom;">DURACARB</span>
                            <asp:RadioButton ID="rdBtnDuraCab" AutoPostBack="true" OnCheckedChanged="rdBtnTaegutec_CheckedChanged" GroupName="byCmpnyCodeInradiobtn" runat="server" />
                        </li>
                    </ul>
                </div>

                <div class="col-md-4" runat="server" id="divBranch">
                    <div class="form-group">
                        <label class="col-md-4 control-label">BRANCH</label>
                        <div class="col-md-4">
                            <asp:DropDownList ID="ddlBranchList" runat="server" AutoPostBack="True"
                                OnSelectedIndexChanged="ddlBranchList_SelectedIndexChanged"
                                CssClass="form-control" Width="230px" ViewStateMode="Enabled">
                            </asp:DropDownList>
                        </div>
                    </div>

                </div>

                <div class="col-md-4" runat="server" id="divSE">
                    <div class="form-group">
                        <label class="col-md-4 control-label">SALES ENGINEER </label>
                        <div class="col-md-4">

                            <asp:DropDownList ID="ddlSalesEngineerList" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlSalesEngineerList_SelectedIndexChanged"
                                CssClass="form-control" Width="230px" ViewStateMode="Enabled">
                            </asp:DropDownList>


                        </div>
                    </div>

                </div>

                <div class="col-md-4">
                    <div class="form-group">
                        <label class="col-md-4 control-label">CUSTOMER TYPE </label>
                        <div class="col-md-4">
                            <asp:DropDownList ID="ddlcustomertype" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlcustomertype_SelectedIndexChanged"
                                CssClass="form-control" Width="230px" ViewStateMode="Enabled">
                                <asp:ListItem Text="ALL" Value="ALL" />
                                <asp:ListItem Text="CUSTOMER" Value="C" />
                                <asp:ListItem Text="CHANNEL PARTNER" Value="D" />
                            </asp:DropDownList>
                        </div>
                    </div>

                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label class="col-md-4 control-label">INDUSTRY  </label>
                        <div class="col-md-4">
                            <asp:DropDownList ID="ddlIndustry" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlIndustry_SelectedIndexChanged"
                                CssClass="form-control" Width="230px" ViewStateMode="Enabled">
                               
                            </asp:DropDownList>
                        </div>
                    </div>

                </div>
                
                <div class="col-md-4">
                    <div class="form-group">
                        <label class="col-md-4 control-label">CUSTOMER NAME </label>
                        <div class="col-md-4">
                            <asp:DropDownList ID="ddlCustomerList" runat="server"
                                CssClass="form-control" Width="230px" ViewStateMode="Enabled">
                                <asp:ListItem>--SELECT CUSTOMER --</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>

                </div>

                <div class="col-md-4 ">
                    <div class="form-group">
                        <label class="col-md-4 control-label ">CUSTOMER NUMBER</label>
                        <div class="col-md-4">
                            <asp:DropDownList ID="ddlCustomerNumber" runat="server" CssClass="form-control" Width="230px" ViewStateMode="Enabled">
                                <asp:ListItem>--SELECT CUSTOMER NUMBER--</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>
                </div>

                <div class="col-md-4">

                    <div class="form-group">
                        <label class="col-md-4 control-label">VISIT DATE</label>
                        <div class="col-md-4 drf">
                            <div class="input-group input-large">
                                <asp:TextBox runat="server" ID="txtFromDate" class="form-control dpd1" onKeyDown="preventBackspace();" onkeypress="return allowOnlyNumber(event);"></asp:TextBox>
                                <span class="input-group-addon">To</span>
                                <asp:TextBox runat="server" ID="txtToDate" class="form-control dpd2" onKeyDown="preventBackspace();" onkeypress="return allowOnlyNumber(event);"></asp:TextBox>
                            </div>
                            <span class="help-block">Select date range</span>
                        </div>
                    </div>
                    <!--/form-group-->

                </div>

                <div class="col-md-4 ">
                    <div class="form-group">
                        <div class="col-md-3" style="    margin-top: 6px;">
                            <asp:Button ID="btnFilter" runat="server" Text="FILTER" CssClass="btn green" OnClick="btnFilter_Click" OnClientClick="showProgress()" />
                        </div>
                    </div>
                </div>
            </div>

            <br />
            <div class="row" style="min-height:300px">
                <label runat="server" style="color: red; font-size: 12" id="lbl_Message" visible="false">&nbsp &nbsp No Records to Display For this Selection.</label>
                
                <asp:GridView runat="server" ID="gvVisits" Width="100%" Style="text-align: center; min-height:40%" AutoGenerateColumns="false" HeaderStyle-Height="30px" ShowHeaderWhenEmpty="true" OnRowDataBound="gvVisits_RowDataBound" DataKeyNames="customer_number,engineer_id">
                    
                    <Columns>
                        <asp:TemplateField HeaderStyle-CssClass="HeadergridAll" HeaderStyle-Width="15px" Visible="true">
            <ItemTemplate>
                <a href="JavaScript:divexpandcollapse('div<%# Eval("customer_number") %><%# Eval("engineer_name") %>');">

                            <img id='imgdiv<%# Eval("customer_number") %><%# Eval("engineer_name") %>' style="cursor: pointer" src="images/button_plus.gif" /></a>
                    
            </ItemTemplate>
        </asp:TemplateField>
                        <asp:TemplateField HeaderText="SALES ENGINEER" HeaderStyle-CssClass="HeadergridAll" HeaderStyle-Width="195px"  Visible="true" SortExpression="engineer_name">
                            <HeaderTemplate>
                                    <asp:Label ID="engineer_name" runat="server" Text="SALES ENGINEER"></asp:Label>
                                    <asp:ImageButton ToolTip="sorting" Width="15px" runat="server" ImageUrl="~/images/sort_neutral.png" ID="sort_salesengineer" OnClick="sort_salesengineer_Click"/>
                                </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblSE" runat="server" Text='<%# Eval("engineer_name") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="DIRECT/CP" HeaderStyle-CssClass="HeadergridAll" HeaderStyle-Width="195px" Visible="true" SortExpression="customer_type">
                            <HeaderTemplate>
                                    <asp:Label ID="customer_type" runat="server" Text="DIRECT/CP"></asp:Label>
                                    <asp:ImageButton ToolTip="sorting" Width="15px" runat="server" ImageUrl="~/images/sort_neutral.png" ID="sort_customertype" OnClick="sort_customertype_Click" />
                                </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblctype" runat="server" Text='<%# Eval("customer_type") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="CUSTOMER NAME" HeaderStyle-CssClass="HeadergridAll" HeaderStyle-Width="195px" Visible="true" SortExpression="customer_short_name">
                            <HeaderTemplate>
                                    <asp:Label ID="customer_short_name" runat="server" Text="CUSTOMER NAME"></asp:Label>
                                    <asp:ImageButton ToolTip="sorting" Width="15px" runat="server" ImageUrl="~/images/sort_neutral.png" ID="sort_customername" OnClick="sort_customername_Click"/>
                                </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblcname" runat="server" Text='<%# Eval("customer_short_name") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="CHANNEL PARTNER NAME" HeaderStyle-CssClass="HeadergridAll" HeaderStyle-Width="195px" Visible="true" SortExpression="distributor_name_v">
                            <HeaderTemplate>
                                    <asp:Label ID="distributor_name_v" runat="server" Text="CHANNEL PARTNER NAME"></asp:Label>
                                    <asp:ImageButton ToolTip="sorting" Width="15px" runat="server" ImageUrl="~/images/sort_neutral.png" ID="sort_channelpatnername" OnClick="sort_channelpatnername_Click"/>
                                </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbldname" runat="server" Text='<%# Eval("distributor_name_v") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="CUSTOMER CLASS" HeaderStyle-CssClass="HeadergridAll" HeaderStyle-Width="195px" Visible="true"  SortExpression="customer_class">
                            <HeaderTemplate>
                                    <asp:Label ID="customer_class" runat="server" Text="CUSTOMER CLASS"></asp:Label>
                                    <asp:ImageButton ToolTip="sorting" Width="15px" runat="server" ImageUrl="~/images/sort_neutral.png" ID="sort_customerclass" OnClick="sort_customerclass_Click" />
                                </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblcclass" runat="server" Text='<%# Eval("customer_class") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="NO. OF VISITS" HeaderStyle-CssClass="HeadergridAll" HeaderStyle-Width="115px" Visible="true"  ItemStyle-CssClass="HeadergridALL_no"  SortExpression="no_of_visits">
                            <HeaderTemplate>
                                    <asp:Label ID="no_of_visits" runat="server" Text="NO. OF VISITS"></asp:Label>
                                    <asp:ImageButton ToolTip="sorting" Width="15px" runat="server" ImageUrl="~/images/sort_neutral.png" ID="sort_noofvisits" OnClick="sort_noofvisits_Click" />
                                </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblnvisits" runat="server" Text='<%# Eval("no_of_visits") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="NO. OF HOURS" HeaderStyle-CssClass="HeadergridAll" HeaderStyle-Width="105px" Visible="true" ItemStyle-CssClass="HeadergridALL_no" >
                            <HeaderTemplate>
                                    <asp:Label ID="no_of_hours" runat="server" Text="NO. OF HOURS"></asp:Label>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblnhours" runat="server" Text='<%# Eval("no_of_hours") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        
                        <asp:TemplateField HeaderStyle-CssClass="HeadergridAll" HeaderStyle-Width="1px" Visible="true">
                            <ItemTemplate>
                                <tr>
                            <td colspan="100%">
                                <div id='div<%# Eval("customer_number") %><%# Eval("engineer_name") %>' style="display:none; overflow:scroll;">
                        
                    <asp:GridView ID="gvVisitDetails" runat="server" AutoGenerateColumns="false" CssClass = "ChildGrid">
                        <Columns>
                            <asp:TemplateField HeaderText="DATE" HeaderStyle-CssClass="Headergridchild" HeaderStyle-Width="105px" ItemStyle-CssClass="HeadergridALL_no" >
                            <HeaderTemplate>
                                    <asp:Label ID="date" runat="server" Text="DATE"></asp:Label>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbldate" runat="server" Text='<%# Eval("visit_date_start") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                            <asp:TemplateField HeaderText="AGENDA" HeaderStyle-CssClass="Headergridchild" HeaderStyle-Width="105px" ItemStyle-CssClass="HeadergridALL_no" >
                            <HeaderTemplate>
                                    <asp:Label ID="agenda" runat="server" Text="AGENDA"></asp:Label>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblAGENDA" runat="server" Text='<%# Eval("agenda") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                            <asp:TemplateField HeaderText="PROJECT" HeaderStyle-CssClass="Headergridchild" HeaderStyle-Width="105px" ItemStyle-CssClass="HeadergridALL_no" >
                            <HeaderTemplate>
                                    <asp:Label ID="project" runat="server" Text="PROJECT"></asp:Label>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblproject" runat="server" Text='<%# Eval("project") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                            <asp:TemplateField HeaderText="REMARKS" HeaderStyle-CssClass="Headergridchild" HeaderStyle-Width="105px" ItemStyle-CssClass="HeadergridALL_no" >
                            <HeaderTemplate>
                                    <asp:Label ID="remarks" runat="server" Text="REMARKS"></asp:Label>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblremarks" runat="server" Text='<%# Eval("remarks") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                              <asp:TemplateField HeaderText="HOURS" HeaderStyle-CssClass="Headergridchild" HeaderStyle-Width="105px" ItemStyle-CssClass="HeadergridALL_no" >
                            <HeaderTemplate>
                                    <asp:Label ID="Hours" runat="server" Text="HOURS"></asp:Label>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblHours" runat="server" Text='<%# Eval("Hours") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                         </div> </td>  </tr>
                        
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </div>


        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="ddlBranchList" EventName="SelectedIndexChanged" />
            <asp:AsyncPostBackTrigger ControlID="ddlSalesEngineerList" EventName="SelectedIndexChanged" />
            <asp:AsyncPostBackTrigger ControlID="ddlcustomertype" EventName="SelectedIndexChanged" />
            <asp:PostBackTrigger ControlID="btnFilter" />


        </Triggers>
    </asp:UpdatePanel>

 <asp:UpdateProgress id="updateProgress" runat="server" DynamicLayout="false" AssociatedUpdatePanelID="UpdatePanel1">
    <ProgressTemplate>
        <div style="position: fixed; text-align: left; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.7;">          
             <span style="border-width: 0px; position: fixed; padding: 50px; font-size: 30px; left: 40%; top: 40%; color:#fff" >Please wait</span>
        </div>
    </ProgressTemplate>
 </asp:UpdateProgress>

    <script type="text/javascript">
        function preventBackspace(e) {
            var evt = e || window.event;
            if (evt) {
                var keyCode = evt.charCode || evt.keyCode;
                if (keyCode === 8) {
                    if (evt.preventDefault) {
                        evt.preventDefault();
                    } else {
                        evt.returnValue = false;
                    }
                }
            }
        }
        function showProgress() {
            var updateProgress = $get("<%= updateProgress.ClientID %>");
             updateProgress.style.display ="inline";
         }
        function divexpandcollapse(divname) {
            debugger;
            var div = document.getElementById(divname);

            var img = document.getElementById('img' + divname);

            if (div.style.display == "none") {
                div.style.display = "inline";

                img.src = "images/button_minus.gif";

            } else {

                div.style.display = "none";

                img.src = "images/button_plus.gif";

            }

        }


        $(document).ready(function () {
            triggerPostGridLodedActions();

            $(window).resize(function () {
                gridviewScroll();
            });

            /*setTimeout(function () {
                jQuery('.GridCellDiv').on('click', function () {
                    if (jQuery('#MainContent_gvVisitsPanelItem').length > 0) {
                        var height = jQuery('#MainContent_gvVisitsPanelItem').css('height');
                        height = parseInt(height) * 2;
                        jQuery('#MainContent_gvVisitsPanelItem').css('height',height+'px');
                    }
                });
            }, 1500);*/

        });
        function triggerPostGridLodedActions() {
            gridviewScroll();
            $('#product_image').unbind('click').bind('click', function (e) {
                var attr = $('#product_image').attr('src');
                $("#reportdrpdwns").slideToggle();
                if (attr == "images/up_arrow.png") {
                    $("#product_image").attr("src", "images/down_arrow.png");
                } else {
                    $("#product_image").attr("src", "images/up_arrow.png");
                }
            });

            $('#MainContent_ddlCustomerList').change(function () {
                var ddlslectedText = $("#MainContent_ddlCustomerList option:selected").val();
                $("#MainContent_ddlCustomerNumber").val(ddlslectedText);
            });

            $('#MainContent_ddlCustomerNumber').change(function () {

                var ddlslectedText = $("#MainContent_ddlCustomerNumber").val();
                $("#MainContent_ddlCustomerList").val(ddlslectedText);
            });

            $('#MainContent_ddlCustomerList').change(function () {
                var ddlslectedText = $("#MainContent_ddlCustomerList option:selected").val();
                $("#MainContent_ddlCustomerNumber").val(ddlslectedText);
            });

            $('#MainContent_ddlCustomerNumber').change(function () {

                var ddlslectedText = $("#MainContent_ddlCustomerNumber").val();
                $("#MainContent_ddlCustomerList").val(ddlslectedText);
            });

            $('#product_image').unbind('click').bind('click', function (e) {
                var attr = $('#product_image').attr('src');
                $("#reportdrpdwns").slideToggle();
                if (attr == "images/up_arrow.png") {
                    $("#product_image").attr("src", "images/down_arrow.png");
                } else {
                    $("#product_image").attr("src", "images/up_arrow.png");
                }
            });

            $("#MainContent_txtFromDate").datepicker({
                showOn: "both",
                buttonImageOnly: true,
                buttonText: "",
                changeYear: true,
                changeMonth: true,
                yearRange: "c-20:c+50",
                // minDate: new Date(),
                dateFormat: 'dd/mm/yy',
                //buttonImage: "images/calander_icon.png",
            });

            $("#MainContent_txtToDate").datepicker({
                showOn: "both",
                buttonImageOnly: true,
                buttonText: "",
                changeYear: true,
                changeMonth: true,
                yearRange: "c-20:c+50",
                //minDate: new Date(),
                dateFormat: 'dd/mm/yy',
                // buttonImage: "images/calander_icon.png",
            });

            $(document).on('change', '#MainContent_txtToDate', function () {
                var Target_from = $.datepicker.parseDate('dd/mm/yy', $('#MainContent_txtFromDate').val());
                var Target_To = $.datepicker.parseDate('dd/mm/yy', $('#MainContent_txtToDate').val());
                if (Target_To != "" && Target_To != undefined && Target_To != null)
                    if (new Date(Target_To) < new Date(Target_from)) {
                        alert("To VISIT Date can not be less than the VISIT From Date");
                        var vaue = "";
                        $("#MainContent_txtToDate").val(vaue);
                        return false;
                    }
                return true;

            });

            $(document).on('change', '#MainContent_txtFromDate', function () {
                var Target_from = $.datepicker.parseDate('dd/mm/yy', $('#MainContent_txtFromDate').val());
                var Target_To = $.datepicker.parseDate('dd/mm/yy', $('#MainContent_txtToDate').val());
                if (Target_from != "" && Target_from != undefined && Target_from != null)
                    if (new Date(Target_To) < new Date(Target_from)) {
                        alert("VISIT From Date can not be greater than the VISIT To Date");
                        var vaue = "";
                        $("#MainContent_txtFromDate").val(vaue);
                        return false;
                    }
                return true;

            });


            allowOnlyNumber(function allowOnlyNumber(evt) {
                debugger;
                var charCode = (evt.which) ? evt.which : event.keyCode
                if (charCode > 31 && (charCode < 48 || charCode > 57))
                    return false;
                return false;
            });
        }

        function allowOnlyNumber(evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode
            if (charCode > 31 && (charCode < 48 || charCode > 57))
                return false;
            return false;
        }

        function gridviewScroll() {

            gridView1 = $('#MainContent_gvVisits').gridviewScroll({
                width: $(window).width() - 70,
                height: findminofscreenheight($(window).height(), 1000),
                railcolor: "#F0F0F0",
                barcolor: "#606060",
                barhovercolor: "#606060",
                bgcolor: "#F0F0F0",
                freezesize: 9,
                arrowsize: 30,
                varrowtopimg: "Images/arrowvt.png",
                varrowbottomimg: "Images/arrowvb.png",
                harrowleftimg: "Images/arrowhl.png",
                harrowrightimg: "Images/arrowhr.png",
                headerrowcount: 1,
                railsize: 16,
                barsize: 14,
                verticalbar: "auto",
                horizontalbar: "auto",
                wheelstep: 1,
            });
        }
        function findminofscreenheight(a, b) {
            a = a - $("#product_image").offset().top;
            return a < b ? a : b;
        }
    </script>

    <style type="text/css">
          td {
            
            text-align: left;
            padding: 3px 10px 3px 10px;
            overflow:hidden;
        }
        
        .noclose .ui-dialog-titlebar-close {
            display: none;
        }

        .row {
            margin-left: 10px;
            margin-right: -15px;
        }

        .HeadergridAll {
            background: #006780;
            color: #fff;
            font-weight: 600;
            text-align: center;
            height: 37px;
        }
        .Headergridchild {
            background: grey;
            color:black;
            font-weight: 600;
            text-align: center;
            height: 37px;
        }
        .HeadergridALL_no {
            text-align: center !important;
        }

    </style>


</asp:Content>
