﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Web.UI.DataVisualization.Charting;
using System.Drawing;
using System.IO.Compression;
using System.Globalization;

namespace TaegutecSalesBudget
{
    public partial class VisitReports : System.Web.UI.Page
    {
        Budget objBudget = new Budget();
        AdminConfiguration objConfig = new AdminConfiguration();
        Reports objReports = new Reports();
        Review objRSum = new Review();
        Visit objvisit = new Visit();
        MDP objMDP = new MDP();

        public static string cter;
        public static bool potSort;
        static DataTable sortedReports = null;

        #region Page Load
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }

            if (!IsPostBack)
            {
                cter = null;
                string strUserId = Session["UserId"].ToString();
                string roleId = Session["RoleId"].ToString();

                LoadAllIndustries();

                #region Role by data loading
                if (Session["RoleId"].ToString() == "HO" || Session["RoleId"].ToString() == "TM")
                {
                    if (Session["RoleId"].ToString() == "HO")
                    {
                        if (Session["cter"] == null && roleId == "HO")
                        {
                            Session["cter"] = "TTA";
                            cter = "TTA";

                        }
                        if (Session["cter"].ToString() == "DUR")
                        {
                            rdBtnDuraCab.Checked = true;
                            rdBtnTaegutec.Checked = false;
                            cter = "DUR";
                        }
                        else
                        {
                            rdBtnTaegutec.Checked = true;
                            rdBtnDuraCab.Checked = false;
                            cter = "TTA";
                        }

                    }
                    LoadBranches();
                    ddlBranchList_SelectedIndexChanged(null, null);
                    ddlSalesEngineerList_SelectedIndexChanged(null, null);
                }
                else if (Session["RoleId"].ToString() == "BM")
                {
                    string username = Session["UserName"].ToString();
                    string branchcode = Session["BranchCode"].ToString();
                    string branchDec = Session["BranchDesc"].ToString();

                    //bind branch
                    ddlBranchList.Items.Insert(0, branchcode);
                    divBranch.Visible = false;

                    // sales engineers loading
                    DataTable dtSalesEngDetails = objReports.LoadUserInfo(null, branchcode);
                    if (dtSalesEngDetails != null)
                    {
                        ddlSalesEngineerList.DataSource = dtSalesEngDetails;
                        ddlSalesEngineerList.DataTextField = "EngineerName";
                        ddlSalesEngineerList.DataValueField = "EngineerId";
                        ddlSalesEngineerList.DataBind();
                        ddlSalesEngineerList.Items.Insert(0, "ALL");
                    }
                    //load customer details
                    ddlSalesEngineerList_SelectedIndexChanged(null, null);

                }
                else if (Session["RoleId"].ToString() == "SE")
                {

                    string username = Session["UserName"].ToString();
                    string branchcode = Session["BranchCode"].ToString();
                    string branchDec = Session["BranchDesc"].ToString();

                    //bind branch
                    ddlBranchList.Items.Insert(0, branchcode);
                    divBranch.Visible = false;
                    //bind Sales engineer
                    ddlSalesEngineerList.Items.Insert(0, strUserId);
                    divSE.Visible = false;

                    // customers loading
                    DataTable dtCutomerDetails = objBudget.LoadCustomerDetails(strUserId, "SE"); ;
                    if (dtCutomerDetails != null)
                    {
                        DataTable dtDeatils = new DataTable();
                        dtDeatils.Columns.Add("customer_number", typeof(string));
                        dtDeatils.Columns.Add("customer_name", typeof(string));
                        for (int i = 0; i < dtCutomerDetails.Rows.Count; i++)
                        {
                            dtDeatils.Rows.Add(dtCutomerDetails.Rows[i].ItemArray[1].ToString(), dtCutomerDetails.Rows[i].ItemArray[2].ToString() + "(" + dtCutomerDetails.Rows[i].ItemArray[1].ToString() + ")");
                        }
                        ddlCustomerList.DataSource = dtDeatils;
                        ddlCustomerList.DataTextField = "customer_name";
                        ddlCustomerList.DataValueField = "customer_number";
                        ddlCustomerList.DataBind();
                        //ddlCustomerList.Items.Insert(0, "-- SELECT CUSTOMER --");
                        ddlCustomerList.Items.Insert(0, "ALL");

                        ddlCustomerNumber.DataSource = dtCutomerDetails;
                        ddlCustomerNumber.DataTextField = "customer_number";
                        ddlCustomerNumber.DataValueField = "customer_number";
                        ddlCustomerNumber.DataBind();
                        //ddlCustomerNumber.Items.Insert(0, "-- SELECT CUSTOMER NUMBER --");
                        ddlCustomerNumber.Items.Insert(0, "ALL");
                    }

                }
                #endregion

                loadVisitdates();

                btnFilter_Click(null, null);
            }
        }
        #endregion

        #region Filter Tab data Loading
        protected void LoadBranches()
        {
            if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }
            string roleId = Session["RoleId"].ToString();
            string userId = Session["UserId"].ToString();
            string branchcode = Session["BranchCode"].ToString();
            DataTable dtData = new DataTable();
            objRSum.BranchCode = userId; // passing here territory Engineer Id  as branch code IF role is TM 
            objRSum.roleId = roleId;
            objRSum.flag = "Branch";
            objRSum.cter = cter;
            dtData = objRSum.getFilterAreaValue(objRSum);

            ddlBranchList.DataSource = dtData;
            ddlBranchList.DataTextField = "BranchDesc";
            ddlBranchList.DataValueField = "BranchCode";
            ddlBranchList.DataBind();
            ddlBranchList.Items.Insert(0, "ALL");

            ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "triggerPostGridLodedActions();", true);

        }

        protected void ddlBranchList_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }

            string roleId = Session["RoleId"].ToString();
            string userId = Session["UserId"].ToString();
            string branchcode = Session["BranchCode"].ToString();
            objRSum.BranchCode = roleId == "TM" && ddlBranchList.SelectedItem.Value == "ALL" ? userId : ddlBranchList.SelectedItem.Value;
            objRSum.roleId = roleId;
            objRSum.flag = "SalesEngineer";
            DataTable dtData = objRSum.getFilterAreaValue(objRSum);

            if (dtData.Rows.Count != 0)
            {
                ddlSalesEngineerList.DataSource = dtData;
                ddlSalesEngineerList.DataTextField = "EngineerName";
                ddlSalesEngineerList.DataValueField = "EngineerId";
                ddlSalesEngineerList.DataBind();
                ddlSalesEngineerList.Items.Insert(0, "ALL");
            }
            else
            {
                ddlSalesEngineerList.DataSource = dtData;
                ddlSalesEngineerList.DataTextField = "EngineerName";
                ddlSalesEngineerList.DataValueField = "EngineerId";
                ddlSalesEngineerList.DataBind();
                ddlSalesEngineerList.Items.Insert(0, "NO SALES ENGINEER");
            }


            ddlSalesEngineerList_SelectedIndexChanged(null, null);
            DataTable dt = ViewState["Chart"] as DataTable;
            //  if (dt != null) { if (dt.Rows.Count != 0) bindchart(dt); }
            ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "triggerPostGridLodedActions();", true);

        }

        protected void ddlSalesEngineerList_SelectedIndexChanged(object sender, EventArgs e)
        {

            if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }

            string roleId = Session["RoleId"].ToString();
            string userId = Session["UserId"].ToString();
            string branchcode = Session["BranchCode"].ToString();
            objRSum.BranchCode = (roleId == "TM" && ddlBranchList.SelectedItem.Value == "ALL") ? userId : ddlBranchList.SelectedItem.Value;
            objRSum.salesengineer_id = ddlSalesEngineerList.SelectedItem.Value;
            objRSum.customer_type = ddlcustomertype.SelectedItem.Value;
            objRSum.roleId = roleId;
            objRSum.flag = "CustomerType";
            DataTable dtData = objRSum.getFilterAreaValue(objRSum);

            if (dtData.Rows.Count != 0)
            {
                ddlCustomerList.DataSource = dtData;
                ddlCustomerList.DataTextField = "customer_short_name";
                ddlCustomerList.DataValueField = "customer_number";
                ddlCustomerList.DataBind();
                ddlCustomerList.Items.Insert(0, "ALL");
            }
            else
            {
                ddlCustomerList.DataSource = dtData;
                ddlCustomerList.DataTextField = "customer_short_name";
                ddlCustomerList.DataValueField = "customer_number";
                ddlCustomerList.DataBind();
                ddlCustomerList.Items.Insert(0, "NO CUSTOMER");
            }
            if (dtData.Rows.Count != 0)
            {
                ddlCustomerNumber.DataSource = dtData;
                ddlCustomerNumber.DataTextField = "customer_number";
                ddlCustomerNumber.DataValueField = "customer_number";
                ddlCustomerNumber.DataBind();
                ddlCustomerNumber.Items.Insert(0, "ALL");
            }
            else
            {
                ddlCustomerNumber.DataSource = dtData;
                ddlCustomerNumber.DataTextField = "customer_number";
                ddlCustomerNumber.DataValueField = "customer_number";
                ddlCustomerNumber.DataBind();
                ddlCustomerNumber.Items.Insert(0, "NO CUSTOMER");
            }
            ddlcustomertype_SelectedIndexChanged(null, null);
            DataTable dt = ViewState["Chart"] as DataTable;
            //    if (dt != null) { if (dt.Rows.Count != 0) bindchart(dt); }
            ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "triggerPostGridLodedActions();", true);
        }

        protected void ddlcustomertype_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }

            LoadCustomers();
            ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "triggerPostGridLodedActions();", true);
        }

        protected void rdBtnTaegutec_CheckedChanged(object sender, EventArgs e)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "alert('Please click on FILTER to view results');triggerPostGridLodedActions();", true);
            //ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "triggerPostGridLodedActions();$('#alertmsg').show().delay(5000).fadeOut();", true);

            if (rdBtnTaegutec.Checked)
            {
                Session["cter"] = "TTA";
                cter = "TTA";
            }
            if (rdBtnDuraCab.Checked)
            {
                Session["cter"] = "DUR";
                cter = "DUR";
            }

            LoadBranches();
            ddlBranchList_SelectedIndexChanged(null, null);
            ddlSalesEngineerList_SelectedIndexChanged(null, null);

        }

        protected void ddlIndustry_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }

            LoadCustomers();
            ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "triggerPostGridLodedActions();", true);

        }


        protected void LoadAllIndustries()
        {
            DataTable dtIndustry = new DataTable();
            dtIndustry = objMDP.GetAllIndustriesReports();
            if (dtIndustry != null)
            {

                ddlIndustry.DataSource = dtIndustry;
                ddlIndustry.DataTextField = "IndustryName";
                ddlIndustry.DataValueField = "IndustryId";
                ddlIndustry.DataBind();
                ddlIndustry.Items.Insert(0, "ALL");


            }
        }

        protected void LoadCustomers()
        {
            string roleId = Session["RoleId"].ToString();
            string userId = Session["UserId"].ToString();
            objvisit.branchcode = ddlBranchList.SelectedItem.Value == "ALL" ? null : ddlBranchList.SelectedItem.Value;
            if (roleId == "TM" && ddlBranchList.SelectedItem.Value == "ALL") { objvisit.branchcode = userId; }
            objvisit.seid = ddlSalesEngineerList.SelectedItem.Value == "ALL" ? null : ddlSalesEngineerList.SelectedItem.Value;
            objvisit.customer_type = ddlcustomertype.SelectedItem.Value == "ALL" ? null : ddlcustomertype.SelectedItem.Value;
            objvisit.industryid = ddlIndustry.SelectedItem.Value == "ALL" ? null : ddlIndustry.SelectedItem.Value;

            DataTable dtData = objvisit.getCustomersbyIndustry(objvisit);

            if (dtData.Rows.Count != 0)
            {
                ddlCustomerList.DataSource = dtData;
                ddlCustomerList.DataTextField = "customer_short_name";
                ddlCustomerList.DataValueField = "customer_number";
                ddlCustomerList.DataBind();
                ddlCustomerList.Items.Insert(0, "ALL");
            }
            else
            {
                ddlCustomerList.DataSource = dtData;
                ddlCustomerList.DataTextField = "customer_short_name";
                ddlCustomerList.DataValueField = "customer_number";
                ddlCustomerList.DataBind();
                ddlCustomerList.Items.Insert(0, "NO CUSTOMER");
            }
            if (dtData.Rows.Count != 0)
            {
                ddlCustomerNumber.DataSource = dtData;
                ddlCustomerNumber.DataTextField = "customer_number";
                ddlCustomerNumber.DataValueField = "customer_number";
                ddlCustomerNumber.DataBind();
                ddlCustomerNumber.Items.Insert(0, "ALL");
            }
            else
            {
                ddlCustomerNumber.DataSource = dtData;
                ddlCustomerNumber.DataTextField = "customer_number";
                ddlCustomerNumber.DataValueField = "customer_number";
                ddlCustomerNumber.DataBind();
                ddlCustomerNumber.Items.Insert(0, "NO CUSTOMER");
            }
        }
        #endregion

        #region Filter Button Click
        protected void btnFilter_Click(object sender, EventArgs e)
        {
            ScriptManager scriptManager = ScriptManager.GetCurrent(this.Page);
            scriptManager.RegisterPostBackControl(this.btnFilter);
            if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }

            //bind grid view 
            gridBind();
            ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "triggerPostGridLodedActions();", true);

        }
        #endregion

        #region Grid Bind
        void gridBind()
        {
            if (Session["UserName"] == null) { Response.Redirect("Login.aspx"); return; }
            try
            {
                string roleId = Session["RoleId"].ToString();
                string userid = Session["UserId"].ToString();
                objvisit.branchcode = ddlBranchList.SelectedItem.Value == "ALL" ? null : ddlBranchList.SelectedItem.Value;
                if (roleId == "TM" && ddlBranchList.SelectedItem.Value == "ALL") { objvisit.branchcode = userid; }
                objvisit.seid = ddlSalesEngineerList.SelectedItem.Value == "ALL" ? null : ddlSalesEngineerList.SelectedItem.Value;
                objvisit.customer_type = ddlcustomertype.SelectedItem.Value == "ALL" ? null : ddlcustomertype.SelectedItem.Value;
                objvisit.customer_number = ddlCustomerNumber.SelectedItem.Value == "ALL" ? null : ddlCustomerNumber.SelectedItem.Value;
                objvisit.industryid = ddlIndustry.SelectedItem.Value == "ALL" ? null : ddlIndustry.SelectedItem.Value;

                string UIpattern = "dd/MM/yyyy";
                string DBPattern = "MM-dd-yyyy";
                DateTime parsedDate;
                if (DateTime.TryParseExact(txtFromDate.Text, UIpattern, null, DateTimeStyles.None, out parsedDate))
                {
                    string frmdate = parsedDate.ToString(DBPattern);
                    objvisit.creationdate_from = frmdate;
                }
                if (DateTime.TryParseExact(txtToDate.Text, UIpattern, null, DateTimeStyles.None, out parsedDate))
                {
                    string frmdate = parsedDate.AddDays(1).ToString(DBPattern); // one extra day is adding for existing date. sql 
                    objvisit.creationdate_to = frmdate;
                }

                DataTable dtVisits = objvisit.getVisitorReports(objvisit);
                if (dtVisits.Rows.Count != 0)
                {
                    gvVisits.DataSource = dtVisits;
                    gvVisits.DataBind();
                    gvVisits.Visible = true;
                    lbl_Message.Visible = false;                   
                }               
                else
                {
                    DataTable dtnovisits = new DataTable();
                    gvVisits.DataSource = dtnovisits;
                    gvVisits.DataBind();
                    gvVisits.Visible = false;
                    lbl_Message.Visible = true;
                }
               sortedReports = dtVisits.Copy();
            }
            catch (Exception ex) { }
        }
        #endregion

        #region load default dates 
        void loadVisitdates()
        {
            try
            {
                DateTime now = DateTime.Now;
                var startDate = new DateTime(now.Year, now.Month, 1);
                var endDate = startDate.AddMonths(1).AddDays(-1);
                txtFromDate.Text = startDate.ToString("dd/MM/yyyy");
                txtToDate.Text = endDate.ToString("dd/MM/yyyy");
            }
            catch (Exception ex) { }
        }
        #endregion

        #region Grid Header Sorting
        private string GetSortDirection(SortDirection sortDirection)
        {
            string newSortDirection = String.Empty;

            try
            {
                switch (sortDirection)
                {
                    case SortDirection.Ascending:
                        newSortDirection = "ASC";
                        break;
                    case SortDirection.Descending:
                        newSortDirection = "DESC";
                        break;
                }
            }
            catch (Exception ex) { }
            return newSortDirection;
        }

        protected void ReportGridView_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {
                //Retrieve the table from the session object.
                DataTable dt = sortedReports.Copy();
                EnumerableRowCollection<DataRow> dr1 = null;
                //dt.Rows[dt.Rows.Count - 1].Delete();

                if (dt != null)
                {
                    //Sort the data.

                    if (GetSortDirection(e.SortDirection) == "ASC")
                    {
                        dr1 = (from row in dt.AsEnumerable()

                               orderby row[e.SortExpression] ascending

                               select row);
                    }
                    else
                    {
                        dr1 = (from row in dt.AsEnumerable()

                               orderby row[e.SortExpression] descending

                               select row);
                    }
                    DataTable dx = dr1.AsDataView().ToTable();
                  
                    gvVisits.DataSource = dx;
                    gvVisits.DataBind();


                }
            }
            catch (Exception ex) { }

        }

        protected void sort_salesengineer_Click(object sender, ImageClickEventArgs e)
        {

            if (potSort == false)
            {
                ReportGridView_Sorting(null, new GridViewSortEventArgs("engineer_name", SortDirection.Ascending));
                potSort = true;
            }
            else
            {
                ReportGridView_Sorting(null, new GridViewSortEventArgs("engineer_name", SortDirection.Descending));
                potSort = false;
            }
            ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "triggerPostGridLodedActions();", true);
        }
        protected void sort_customertype_Click(object sender, ImageClickEventArgs e)
        {
            if (potSort == false)
            {
                ReportGridView_Sorting(null, new GridViewSortEventArgs("customer_type", SortDirection.Ascending));
                potSort = true;
            }
            else
            {
                ReportGridView_Sorting(null, new GridViewSortEventArgs("customer_type", SortDirection.Descending));
                potSort = false;
            }
            ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "triggerPostGridLodedActions();", true);
        }

        protected void sort_customername_Click(object sender, ImageClickEventArgs e)
        {
            if (potSort == false)
            {
                ReportGridView_Sorting(null, new GridViewSortEventArgs("customer_short_name", SortDirection.Ascending));
                potSort = true;
            }
            else
            {
                ReportGridView_Sorting(null, new GridViewSortEventArgs("customer_short_name", SortDirection.Descending));
                potSort = false;
            }
            ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "triggerPostGridLodedActions();", true);
        }

        protected void sort_channelpatnername_Click(object sender, ImageClickEventArgs e)
        {
            if (potSort == false)
            {
                ReportGridView_Sorting(null, new GridViewSortEventArgs("distributor_name_v", SortDirection.Ascending));
                potSort = true;
            }
            else
            {
                ReportGridView_Sorting(null, new GridViewSortEventArgs("distributor_name_v", SortDirection.Descending));
                potSort = false;
            }
            ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "triggerPostGridLodedActions();", true);
        }

        protected void sort_customerclass_Click(object sender, ImageClickEventArgs e)
        {
            if (potSort == false)
            {
                ReportGridView_Sorting(null, new GridViewSortEventArgs("customer_class", SortDirection.Ascending));
                potSort = true;
            }
            else
            {
                ReportGridView_Sorting(null, new GridViewSortEventArgs("customer_class", SortDirection.Descending));
                potSort = false;
            }
            ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "triggerPostGridLodedActions();", true);
        }

        protected void sort_noofvisits_Click(object sender, ImageClickEventArgs e)
        {
            if (potSort == false)
            {
                ReportGridView_Sorting(null, new GridViewSortEventArgs("no_of_visits", SortDirection.Ascending));
                potSort = true;
            }
            else
            {
                ReportGridView_Sorting(null, new GridViewSortEventArgs("no_of_visits", SortDirection.Descending));
                potSort = false;
            }
            ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "triggerPostGridLodedActions();", true);
        }
        #endregion

        protected void gvVisits_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                string customerId = gvVisits.DataKeys[e.Row.RowIndex].Values[0].ToString();
                string engineerId = gvVisits.DataKeys[e.Row.RowIndex].Values[1].ToString();
                GridView gvVisitDetails = e.Row.FindControl("gvVisitDetails") as GridView;
                gvVisitDetails.DataSource = objvisit.getVisitDetails(customerId, objvisit, engineerId);
                gvVisitDetails.DataBind();
            }
        }


    }
}
