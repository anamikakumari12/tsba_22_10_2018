<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="MDPEntry.aspx.cs" Inherits="TaegutecSalesBudget.MDPEntryPage" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <link href="css/Projectsmain.css" rel="stylesheet" type="text/css" />
    <script src="js/MdpEntry.js" type="text/javascript"></script>
    <script type="text/javascript">
        function disable() {
            $("#tab_home").tabs({
                active: [0],
                disabled: [1, 2, 3, 4, 5, 6, 7, 8, 9]
            });
        }

        function enabletab2() {
            $("#tab_home").tabs({
                active: [0, 1],
                disabled: [2, 3, 4, 5, 6, 7, 8, 9]
            });
        }

        function enabletab3() {
            $("#tab_home").tabs({
                active: [0, 1, 2],
                disabled: [3, 4, 5, 6, 7, 8, 9]

            });
        }

        function enabletab4() {
            $("#tab_home").tabs({
                active: [0, 1, 2, 3],
                disabled: [4, 5, 6, 7, 8, 9]
            });
        }

        function enabletab5() {
            $("#tab_home").tabs({
                active: [0, 1, 2, 3, 4],
                disabled: [5, 6, 7, 8, 9]
            });
        }

        function enabletab6() {
            $("#tab_home").tabs({
                active: [0, 1, 2, 3, 4, 5],
                disabled: [6, 7, 8, 9]
            });
        }

        function enabletab7() {
            $("#tab_home").tabs({
                active: [0, 1, 2, 3, 4, 5, 6],
                disabled: [7, 8, 9]
            });
        }

        function enabletab8() {
            $("#tab_home").tabs({
                active: [0, 1, 2, 3, 4, 5, 6, 7],
                disabled: [8, 9]

            });
        }

        function enabletab9() {
            $("#tab_home").tabs({
                active: [0, 1, 2, 3, 4, 5, 6, 7, 8],
                disabled: [9]

            });
        }

        function enabletab10() {
            $("#tab_home").tabs({
                active: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]

            });
        }

        function enablecontrols() {
            $("#MainContent_TargetDate").datepicker("enable");
        }

        function disablecontrols() {
            $("#MainContent_TargetDate").datepicker("disable");
        }

        function isNumberKey(evt, obj) {
            debugger;
            var charCode = (evt.which) ? evt.which : event.keyCode
            var value = obj.value;
            var dotcontains = value.indexOf(".") != -1;
            if (dotcontains) {
                var match = ('' + value).match(/(?:\.(\d+))?(?:[eE]([+-]?\d+))?$/);
                if (!match) { return 0; }
                var decCount = Math.max(0,
                     // Number of digits right of decimal point.
                     (match[1] ? match[1].length : 0)
                     // Adjust for scientific notation.
                     - (match[2] ? +match[2] : 0));
                if (decCount > 1) return false;

                if (charCode == 46) return false;
            }
            else {
                if (value.length > 2) {
                    if (charCode == 46) return true;
                    else return false;
                }
            }
            if (charCode == 46) return true;
            if (charCode > 31 && (charCode < 48 || charCode > 57))
                return false;
            return true;
        }
    </script>
    <style>
        .addtext {
            margin-left: 75px;
        }

        .uploadcontrol {
            margin-top:-25px; 
            width:90px;
        }

        .stagecontrol {
            width: 10%!important;
        }

        .stageinput {
            width: 90%!important;
        }

        .stagecontrols input {
            border: solid 1px #ddd;
            padding: 0 5px;
            margin: 0 0 5px 0;
            color: black;
            height: 30px;
            width:200px;
            border-radius: 4px!important;
        }

        .margin {
            margin-top: -35px;
            border-radius: 5px !important;
            box-shadow: 0 1px 10px rgba(10, 10, 10, 0.25);
        }

        .image {
            top: -15px!important;
            background: transparent!important;
        }

        .row {
            margin-top: -10px;
            margin-left: -15px;
            margin-right: -15px;
        }

        input[disabled], select[disabled], textarea[disabled], input[readonly], select[readonly], textarea[readonly] {
            cursor: not-allowed;
            background-color: #eee;
            color: #adadad!important;
        }

        .portlet-body {
            margin-top: 50px;
        }

        .portlet-body1 {
            margin-top: 40px;
        }

        .nopadding1 {
            margin-top: -20px;
        }

        .nopadding2 {
            margin-top: -10px;
        }

        .nopadding3 {
            margin-top: 10px;
        }

        .control-label {
            float: left;
            text-align: left;
            padding: 5px;
            width: 41%;
            font-size: 13px;
            font-weight: normal;
        }

        .mn_box {
            background-color: #ebeef5;
            box-shadow: 0 1px 10px rgba(10, 10, 10, 0.4);
            float: left;
            margin: 4px 0 0 0;
            padding: 3px;
            width: 100%;
        }

        .form-horizontal h2 {
            font-size: 14px;
            margin: 10px 0 20px 0;
            color: #006780;
            font-weight: bold;
            text-align: center;
            border-bottom: 1px solid;
            padding: 0 0 5px;
        }

        .controls input, .controls select {
            /*width: 150px;
              height: 27px;*/
            border: solid 1px #ddd;
            padding: 0 5px;
            margin: 0 0 5px 0;
            color: black;
            width: 180px;
            height: 28px;
        }

        .controls {
            position: relative;
            float: left;
            text-align: left;
            /*padding: 5px;*/
            width: 41%;
            font-size: 13px;
            font-weight: normal;
        }

        /*label {
            float: left;
            text-align: left;
            padding: 5px;
            width: 41%;
            font-size: 20px;
            display: inline-block;
            max-width: 100%;
            margin-bottom: 7px;
            font-weight: bold;
        }*/


        .control-group {
            margin-bottom: 20px !important;
        }

        .ui-state-disabled {
            pointer-events: none;
        }

        .ui-datepicker-trigger {
            float: right;
            margin-top: -31px;
            padding: 4px;
            border-left: 1px solid #ccc;
            cursor: pointer;
            margin-right: 25%;
        }
        .target-date {
            margin-right:25% !important;
        }
        .lbl_color {
            color: blue !important;
        }

        .HeadergridAll {
            background: #006780;
            color: #fff;
            font-weight: 600;
            text-align: center;
        }

        .file {
            max-width: 400px;
        }

        .portlet.box > .portlet-title {
            background-color: #667F67;
        }

        .ui-widget-content1 {
            border-top: 1px solid #ccc;
        }

        .portlet.lightgray > .portlet-title > .caption {
            color: #fff;
        }

        .nav-tabs > li.active > a, .nav-tabs > li.active > a:hover, .nav-tabs > li.active > a:focus {
            color: #fff;
            cursor: pointer;
            background: #016780;
            -webkit-border-top-left-radius: 8px !important;
            border-top-left-radius: 8px !important;
            -webkit-border-top-right-radius: 8px !important;
            border-top-right-radius: 8px !important;
        }

        .container {
            padding-left: 18px;
        }

        .tabbable-custom > .nav-tabs > li.active {
            border-top: none;
            margin-top: 0;
            position: relative;
        }

        .mn_input input {
            width: 100px;
            padding: 4px 11px;
            background: none;
            border:solid 1px #ccc;
            float: left;
        }

        .mn_input .active {
            background: #e0e0e0;
        }

        .mn_width_100 {
            width:auto;
        }

        .mn_width_100 img{
            margin-right: 10px;
        }
    </style>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server" AsyncPostBackTimeout="360" EnableHistory="true" EnablePartialRendering="true" EnablePageMethods="true">
    </asp:ScriptManager>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div>
                <div class='block-web' style='float: left; width: 100%; height: 36px;'>
                    <div class="header">
                        <div class="crumbs">
                            <!-- Start : Breadcrumbs -->
                            <ul id="breadcrumbs" class="breadcrumb">
                                <li>
                                    <a class="mn_breadcrumb">Projects</a>
                                </li>
                                <li class="current">MDP Entry</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <%-- <div id="divCter" runat="server" visible="false" >
                 
      </div> --%>

                <div class="col-md-12" style="padding-right: 3px; margin-top: 45px;">
                    <ul id="divCter" runat="server" visible="false" class="btn-info rbtn_panel" style="float: left">
                        <li><span style="margin-right: 4px; vertical-align: text-bottom;">TAEGUTEC</span>
                            <asp:RadioButton ID="rdBtnTaegutec" AutoPostBack="true" Checked="true" OnCheckedChanged="rdBtnTaegutec_CheckedChanged" GroupName="byCmpnyCodeInradiobtn" runat="server" />
                            <span style="margin-right: 4px; margin-left: 4px; vertical-align: text-bottom;">DURACARB</span>
                            <asp:RadioButton ID="rdBtnDuraCab" AutoPostBack="true" OnCheckedChanged="rdBtnTaegutec_CheckedChanged" GroupName="byCmpnyCodeInradiobtn" runat="server" />
                        </li>
                    </ul>
                  <%--  <asp:ImageButton ID="tab1_new_customer" runat="server" ToolTip="Create Project" ImageUrl="~/images/create-project.png" Style="width: 48px; float: right;" OnClick="Enable_NewCustomer" />--%>
                </div>
                <div class="clearfix"></div>

                <%--                <div class="col-md-6 " style="padding-right: 15px;">
                    <div class="portlet box lightgray">
                        <div id="collapsebtn" class="portlet-title">
                            <img id="product_image" src="images/button_minus.gif" align="left" />
                    </div>
                </div>
            </div>--%>
                <div class="mn_sales">
                    <%-- <div class="mn_header"> <img src="images/head.png" alt=""> </div>--%>

                    <%-- <div class="col-md-12 nopadding">
                    <div class="crumbs">--%>
                    <div class="col-md-12 mn_box">
                        <div class="col-md-3 nopadding">
                            <div class="form-horizontal">
                                <div>
                                    <h2>CUSTOMER</h2>
                                    <div class="nopadding2">
                                        <div class="col-md-6">
                                            <asp:RadioButton ID="direct_customer" GroupName="customer" runat="server" />
                                            <%--  <label class="control-label ">
                                   Direct Customer
                                </label>--%>
                                            <span class="control-label2">Direct Customer</span>
                                        </div>
                                        <div class="col-md-6">
                                            <asp:RadioButton ID="channel_partner" GroupName="customer" runat="server" />
                                            <%-- <label class="control-label ">
                                    Channel Partner
                                </label>--%>
                                            <span class="control-label2">Channel Partner</span>
                                        </div>
                                    </div>
                                    <div class="portlet-body" id="dvcustomer">
                                        <div>
                                            <div class="control-group" id="dv_channel_partner_1">
                                                <label class="control-label ">
                                                    Channel Partner
                                                </label>
                                                <div class="controls">
                                                    <asp:DropDownList ID="ddlDistributorList" runat="server" OnSelectedIndexChanged="ddlDistributorName_SelectedIndexChanged" ViewStateMode="Enabled" AutoPostBack="true"></asp:DropDownList>
                                                </div>
                                                <div id="errchannelpartner" style="width: 90%;"></div>
                                            </div>

                                            <div class="control-group" id="dv_channel_partner_2">
                                                <label class="control-label">
                                                    Customer
                                                </label>
                                                <div class="controls">
                                                    <asp:DropDownList ID="ddlCustomerforDistributor" runat="server" OnSelectedIndexChanged="ddlCustomerforDistributor_SelectedIndexChanged" ViewStateMode="Enabled" AutoPostBack="true"></asp:DropDownList>

                                                </div>
                                                <div id="errDistcustname" style="width: 90%;"></div>
                                            </div>

                                            <div class="control-group" id="dv_direct_customer">
                                                <label class="control-label">Customer Name</label>
                                                <div class="controls">
                                                    <asp:DropDownList ID="ddlCustomerName" runat="server" OnSelectedIndexChanged="ddlCustomerName_SelectedIndexChanged" ViewStateMode="Enabled" AutoPostBack="true"></asp:DropDownList>
                                                </div>
                                            </div>

                                            <div class="control-group">
                                                <label class="control-label">Customer Class </label>
                                                <div class="controls">
                                                    <asp:TextBox ID="txtCustomerClass" runat="server" ReadOnly="true"></asp:TextBox>
                                                </div>
                                            </div>

                                            <div class="control-group" style="display: none;">
                                                <label class="control-label">Customer NO </label>
                                                <div class="controls">
                                                    <asp:DropDownList ID="ddlCustomerNo" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlCustomerNo_SelectedIndexChanged"></asp:DropDownList>
                                                </div>
                                            </div>

                                            <div class="control-group">
                                                <label class="control-label">Industry </label>
                                                <div class="controls">
                                                    <asp:DropDownList ID="ddlIndustry" runat="server"></asp:DropDownList>
                                                </div>
                                            </div>

                                            <div class="control-group" style="display: none">
                                                <label class="control-label">
                                                    Channel Partner / Direct
                                                </label>
                                                <div class="col-md-8">
                                                    <asp:DropDownList ID="ddlDistibutor" runat="server">
                                                        <asp:ListItem>--SELECT--</asp:ListItem>
                                                        <asp:ListItem>Channel Partner</asp:ListItem>
                                                        <asp:ListItem>Direct</asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6 nopadding ">
                            <div class="col-md-12 form-horizontal">
                                <h2>PROJECT DETAILS</h2>
                                <div class="form-horizontal">
                                    <div class="col-md-6 nopadding1">
                                        <div class="col-md-12 nopadding3">
                                            <div class="col-md-6">
                                                <asp:RadioButton ID="existing_project" GroupName="project" Enabled="true" runat="server" OnCheckedChanged="existing_project_CheckedChanged" AutoPostBack="true" />
                                                <span class="control-label2">Existing Project</span>
                                            </div>
                                            <div class="col-md-6">
                                                <asp:RadioButton ID="new_project" GroupName="project" runat="server" Enabled="true" Checked="true" OnCheckedChanged="new_project_CheckedChanged" AutoPostBack="true" />
                                                <span class="control-label2">New Project</span>
                                            </div>
                                        </div>
                                        <div>
                                            <div id="details" class="portlet-body1">
                                                <div class="control-group" id="dvnew_project">
                                                    <label class="control-label">Project Title </label>
                                                    <div class="controls">
                                                        <asp:TextBox ID="txtTitle" runat="server"> </asp:TextBox>
                                                    </div>
                                                </div>

                                                <div class="control-group" id="dvexisting_project">
                                                    <label class="control-label">Project Title </label>
                                                    <div class="controls">
                                                        <asp:DropDownList ID="ddlTitle" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlTitle_SelectedIndexChanged"></asp:DropDownList>
                                                    </div>
                                                </div>

                                                <div class="control-group">
                                                    <label class="control-label">Project TYPE</label>
                                                    <div class="controls">
                                                        <asp:DropDownList ID="ddlProjectType" runat="server"></asp:DropDownList>
                                                    </div>
                                                </div>

                                                <div class="control-group">
                                                    <label class="control-label">Existing Brand</label>
                                                    <div class="controls">
                                                        <asp:TextBox ID="ExistingProduct" runat="server"> </asp:TextBox>
                                                    </div>
                                                </div>

                                                <div class="control-group">
                                                    <label class="control-label">Component </label>
                                                    <div class="controls">
                                                        <asp:TextBox ID="Component" runat="server"> </asp:TextBox>
                                                    </div>
                                                </div>
                                                <div class="control-group">
                                                    <label class="control-label">Competition Spec </label>
                                                    <div class="controls">
                                                        <asp:TextBox ID="CompetitionSpec" runat="server"> </asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <%--  <div class="col-md-6 nopadding">--%>
                                    <div class="col-md-6 nopadding1">
                                        <div id="Div1" class="portlet-body1">
                                            <div class="control-group">
                                                <label class="control-label">No.of Stages </label>
                                                <div class="controls">
                                                    <asp:DropDownList ID="Stages" runat="server" OnSelectedIndexChanged="Stages_SelectedIndexChanged" AutoPostBack="true">
                                                        <asp:ListItem>--SELECT STAGES--</asp:ListItem>
                                                        <asp:ListItem>1</asp:ListItem>
                                                        <asp:ListItem>2</asp:ListItem>
                                                        <asp:ListItem>3</asp:ListItem>
                                                        <asp:ListItem>4</asp:ListItem>
                                                        <asp:ListItem>5</asp:ListItem>
                                                        <asp:ListItem>6</asp:ListItem>
                                                        <asp:ListItem>7</asp:ListItem>
                                                        <asp:ListItem>8</asp:ListItem>
                                                        <asp:ListItem>9</asp:ListItem>
                                                        <asp:ListItem>10</asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Target Date</label>
                                                <div class="controls mn_width_100">
                                                    <asp:TextBox ID="TargetDate" runat="server" onchange="TargetdatesValidations()"> </asp:TextBox>
                                                    <asp:HiddenField ID="Target_flag" runat="server" />
                                                    <asp:HiddenField ID="Target_Date" runat="server" />
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Owner</label>
                                                <div class="controls">
                                                    <asp:DropDownList ID="ddlSalesEngName" runat="server"></asp:DropDownList>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Reviewer</label>
                                                <div class="controls">
                                                    <asp:DropDownList ID="ddlReviewer" runat="server"></asp:DropDownList>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Escalate to</label>
                                                <div class="controls">
                                                    <asp:DropDownList ID="ddlEscalate" runat="server"></asp:DropDownList>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-horizontal">
                                <div>
                                    <!-- Address form -->
                                    <h2>ANNUAL BUSINESS VALUE (LAKHS)</h2>
                                    <!-- address-line1 input-->

                                    <div class="control-group">
                                        <label class="control-label">Customer Potential</label>
                                        <div class="controls">
                                            <asp:TextBox ID="OverAllPotential" runat="server" autocomplete="off" onkeypress="return isNumberKey(event,this);"> </asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="control-group">
                                        <label class="control-label">Project Potential</label>
                                        <div class="controls">
                                            <asp:TextBox ID="Potential" runat="server" autocomplete="off" onkeypress="return isNumberKey(event,this);"> </asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="control-group">
                                        <label class="control-label">Business Expected</label>
                                        <div class="controls">
                                            <asp:TextBox ID="Business" runat="server" autocomplete="off" onkeypress="return isNumberKey(event,this);"> </asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


            <div class="row">
                <div class="col-md-12 col-sm-12 margin">
                    <div class="portlet-body" style="min-height: 350px !important;">
                        <div class="col-md-12 ">
                            <div class="tabbable tabbable-custom" id="tab_home">
                                <ul class="nav nav-tabs">
                                    <li class="active"><a href="#tab_1_1" data-toggle="tab">Stage 1</a></li>
                                    <li><a id="stage2" href="#tab_1_2" data-toggle="tab">Stage 2</a></li>
                                    <li><a href="#tab_1_3" data-toggle="tab">Stage 3</a></li>
                                    <li><a href="#tab_1_4" data-toggle="tab">Stage 4</a></li>
                                    <li><a href="#tab_1_5" data-toggle="tab">Stage 5</a></li>
                                    <li><a href="#tab_1_6" data-toggle="tab">Stage 6</a></li>
                                    <li><a href="#tab_1_7" data-toggle="tab">Stage 7</a></li>
                                    <li><a href="#tab_1_8" data-toggle="tab">Stage 8</a></li>
                                    <li><a href="#tab_1_9" data-toggle="tab">Stage 9</a></li>
                                    <li><a href="#tab_1_10" data-toggle="tab">Stage 10</a></li>
                                </ul>

                                <div class="tab-content" id="tab" runat="server">
                                    <asp:HiddenField ID="tab_number" runat="server" />
                                    <asp:HiddenField ID="final_stage" runat="server" />
                                    <div class="tab-pane active" id="tab_1_1">
                                        <div class="col-md-7">
                                            <label class="control-label stagecontrol">GOAL</label>
                                            <p>
                                                <asp:TextBox ID="tab1txtGoal" CssClass="form-control stageinput" TextMode="multiline" Style="resize: vertical" Columns="100" Rows="2" runat="server" />
                                            </p>
                                            <p>
                                                <div id="tab1Remarks" class="form-group" runat="server">
                                                    <label class="control-label stagecontrol">REMARKS</label>
                                                    <asp:TextBox ID="tab1txtRemarks_1" CssClass="form-control stageinput" TextMode="multiline" Style="resize: vertical" Columns="100" Rows="2" runat="server" />
                                                    <p></p>
                                                </div>
                                                <p>
                                                </p>
                                        </div>
                                        <div class="col-md-5 ">
                                            <div class="portlet box lightgray">
                                                <div>
                                                    <div class="form-group">
                                                        <label class="col-md-6  control-label2 ">
                                                            Target Date <span style="color: Red; font-size: 12px; display: none;" class="lbltab1Target">*</span>
                                                        </label>
                                                        <div class="col-md-6">
                                                            <div class="stagecontrols" style="width: 100%">
                                                                <asp:TextBox ID="tab1Target" runat="server" class="form-control" AutoPostBack="true" OnTextChanged="tab1Target_TextChanged"> </asp:TextBox>
                                                                <asp:HiddenField ID="tab1Target_flag" runat="server" />
                                                                <asp:HiddenField ID="tab1Target_date" runat="server" />
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-md-6  control-label2 ">
                                                            Actual Completion Date <span style="color: Red; font-size: 12px; display: none;" class="lbltab1Complete">*</span>
                                                        </label>
                                                        <div class="col-md-6">
                                                            <div class="stagecontrols" style="width: 100%">
                                                                <asp:TextBox ID="tab1Completion" runat="server" class="form-control" Enabled="false"> </asp:TextBox>
                                                                <%--<span class="input-group-addon"><i class="fa fa-calendar"></i></span>--%>
                                                                <asp:HiddenField ID="tab1Completion_flag" runat="server" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group tab1_hideout" runat="server" id="tab1_radio">
                                                        <div class="col-md-6 control-label2" style="text-align:right;">
                                                            <asp:RadioButton ID="tab1_radio_success" Text="Success" GroupName="status" runat="server" />
                                                        </div>
                                                        <div class="col-md-6 control-label2">
                                                            <asp:RadioButton ID="tab1_radio_failure" Text="Failure" GroupName="status" runat="server" />
                                                        </div>
                                                    </div>
                                                    <div class="form-group tab1_monthly" runat="server">
                                                        <div class="col-md-6">
                                                            <label class=" control-label2 ">
                                                                Monthly Business Expected
                                                            </label>
                                                            <br />
                                                            <span style="color: red; font-size: 11px;">
                                                                (Value should be in lakhs.
                                                                Only 3 digits are allowed.)
                                                            </span>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="stagecontrols">
                                                                <asp:TextBox ID="tab1_monthly_expected" runat="server" class="form-control" onkeypress="return isNumberKey(event,this)" MaxLength="6"> </asp:TextBox>
                                                                
                                                                <span style="color: Red;font-size: 11px; display: none;" class="lbltab1_monthly">Only Numeric values are allowed.</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                    <div class="form-group tab1_hideout" runat="server" id="tab1_dvComment">
                                                        <label class="col-md-6  control-label2 ">
                                                            Comments
                                                        </label>
                                                        <div class="col-md-6 ">
                                                            <div class="stagecontrols">
                                                                <asp:TextBox ID="tab1_comments" runat="server" class="form-control" TextMode="multiline" Columns="100" Rows="3"> </asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="col-md-12 mn_input">
                                            <p>
                                                <%--<button id="tab1btnRemarks" type="button" runat="server" onserverclick="tab1AddOneMoreRemark"   class="btn green" style="width: 150px;margin-top: 19px;" ><i class="fa fa-plus"></i> New Remarks</button>	--%>
                                                <%-- <asp:Button ID="tab1btnRemarks" runat="server" CssClass="btn green" Style="width: 150px; margin-top: 19px;" Text="New Remarks" OnClick="tab1AddOneMoreRemark" Enabled="false" />--%>
                                                <asp:ImageButton ID="tab1btnRemarks" runat="server" Visible="false" ToolTip="New Remarks"  ImageUrl="~/images/shejiyeiconz0muj0ftihh.png" OnClick="tab1AddOneMoreRemark"/>
                                                <asp:ImageButton ID="tab1saveRemarks" runat="server" Visible="false" ToolTip="Save Remarks"  ImageUrl="~/images/35297964-checklist-icon-green-round-button.png" OnClick="tab1btnSave_Click" CausesValidation="true" />
                                                <asp:ImageButton ID="tab1btnSaveApprove" runat="server" ToolTip="Submit For Approval" ImageUrl="~/images/approved.png" Visible="false" OnClick="tab1btnSave_Click" CausesValidation="true"  OnClientClick="return ValidateTab1()" />
                                                <asp:ImageButton ID="tab1btnSave" runat="server" ToolTip="Save and Continue" ImageUrl="~/images/torrents-icon-25.png" OnClick="tab1btnSave_Click" OnClientClick="return ValidateTab1()" CausesValidation="true" />
                                                <asp:ImageButton ID="tab1btnSubmit" runat="server" ToolTip="Close Stage" Visible="false" Enabled="false" ImageUrl="~/images/22151610-button-close-on-white-background-icon-green.png" OnClick="tab1btnSubmit_Click" CausesValidation="true" OnClientClick="return ValidateSubmitTab1()" />
                                                <label runat="server" class="tab1_hideout_every" style="color: red; font-size: 12px; font-weight: 800;" id="lbl_tab1_close">&nbsp &nbsp To close the stage, click on Close Stage.</label>
                                            </p>
                                            <br />
                                            <br />
                                            <br />
                                            <br />
                                            <asp:Label ID="tab1_lblApprove" CssClass="control-label stagecontrol" runat="server" Visible="false">APPROVAL REMARKS</asp:Label>
                                            <p>
                                                <asp:TextBox ID="tab1_txtApprove" Visible="false" CssClass="form-control stageinput" TextMode="multiline" Columns="100" Rows="2" runat="server" />
                                            </p>
                                            <p>
                                                <asp:ImageButton ID="tab1_btnApprove" runat="server"  CssClass="mn_input" ToolTip="Approve" ImageUrl="~/images/approve.png" Visible="false" OnClick="tab1btnApprove_Click" CausesValidation="true" />
                                                <asp:ImageButton ID="tab1_btnReject" runat="server"  CssClass="mn_input" ToolTip="Reject"  ImageUrl="~/images/reject_1.png" Visible="false"  OnClick="tab1btnReject_Click" CausesValidation="true" />
                                            </p>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="col-md-12 ui-widget-content1">
                                            <div id="myDiv1" class="form-group col-md-12 tab1_hideout_upload">
                                                <asp:Label ID="tab1doc" class="col-md-2  control-label2 " runat="server" Visible="false">Attach Documents</asp:Label>                   
                                                <asp:FileUpload ID="FileUpload1" runat="server" AllowMultiple="true" CssClass="col-md-5 file" Visible="false"/>
                                                <br>
                                                <asp:ImageButton ID="tab1btnUpload" runat="server" ToolTip="Upload" ImageUrl="~/images/upload_1.png"  CssClass="col-md-3 uploadcontrol"  OnClick="tab1_UploadMultipleFiles" Visible="false" />
                                                <asp:Label ID="tab1lblSuccess" class="col-md-2" runat="server" ForeColor="Green" />
                                                <br>
                                            </div>
                                            <div class="form-group col-md-12">
                                                <asp:GridView ID="tab1_FileList" runat="server" Height="100px" Style="text-align: center;" AutoGenerateColumns="false" ShowHeader="true">
                                                    <Columns>
                                                        <asp:TemplateField HeaderText="Uploaded Docs" HeaderStyle-CssClass="HeadergridAll">
                                                            <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                                            <ItemTemplate>
                                                                <asp:LinkButton ID="tab1_file_lbl" CssClass="lbl_color" CommandArgument='<%# Eval("file_number") %>' runat="server" Text='<%# Eval("file_name") %>' OnClick="downloadStage_Files"></asp:LinkButton>
                                                                <asp:LinkButton ID="tab1_file_Delete" Text="Delete" CommandArgument='<%# Eval("file_number") %>' runat="server" OnClick="DeleteFile_tab1" />
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                    </Columns>
                                                </asp:GridView>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane active" id="tab_1_2">
                                        <div class="col-md-7">
                                            <label class="control-label stagecontrol">GOAL</label>
                                            <p>
                                                <asp:TextBox ID="tab2txtGoal" CssClass="form-control stageinput" TextMode="multiline" Style="resize: vertical" Columns="100" Rows="2" runat="server" />
                                            </p>
                                            <p>
                                                <div id="tab2Remarks" class="form-group" runat="server">
                                                    <label class="control-label stagecontrol">REMARKS</label>
                                                    <asp:TextBox ID="tab2txtRemarks_1" CssClass="form-control stageinput" TextMode="multiline" Style="resize: vertical" Columns="100" Rows="2" runat="server" />
                                                    <p></p>
                                                </div>
                                                <p>
                                                </p>
                                                <p>
                                                </p>
                                                <p>
                                                </p>
                                                <p>
                                                </p>
                                                <p>
                                                </p>
                                                <p>
                                                </p>
                                                <p>
                                                </p>
                                                <p>
                                                </p>
                                                <p>
                                                </p>
                                                <p>
                                                </p>
                                                <p>
                                                </p>
                                                <p>
                                                </p>
                                                <p>
                                                </p>
                                                <p>
                                                </p>
                                                <p>
                                                </p>
                                                <p>
                                                </p>
                                                <p>
                                                </p>
                                                <p>
                                                </p>
                                                <p>
                                                </p>
                                                <p>
                                                </p>
                                                <p>
                                                </p>
                                                <p>
                                                </p>
                                                <p>
                                                </p>
                                                <p>
                                                </p>
                                            </p>
                                        </div>

                                        <div class="col-md-5 ">
                                            <div class="portlet box lightgray">
                                                <div>
                                                    <div class="form-group">
                                                        <label class="col-md-6  control-label2 ">
                                                            Target Date <span style="color: Red; font-size: 12px; display: none;" class="lbltab2Target">*</span>
                                                        </label>
                                                        <div class="col-md-6">
                                                            <div class="stagecontrols" style="width: 100%">
                                                                <asp:TextBox ID="tab2Target" runat="server" class="form-control"> </asp:TextBox>
                                                                <asp:HiddenField ID="tab2Target_date" runat="server" />
                                                                <asp:HiddenField ID="tab2Target_flag" runat="server" />
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-md-6  control-label2 ">
                                                            Actual Completion Date <span style="color: Red; font-size: 12px; display: none;" class="lbltab2Complete">*</span>
                                                        </label>
                                                        <div class="col-md-6">
                                                            <div class="stagecontrols" style="width: 100%">
                                                                <asp:TextBox ID="tab2Completion" runat="server" class="form-control" Enabled="false"> </asp:TextBox>
                                                                <asp:HiddenField ID="tab2Completion_flag" runat="server" />
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="form-group tab2_hideout">
                                                        <div class="col-md-6 control-label2" style="text-align:right;">
                                                            <asp:RadioButton ID="tab2_radio_success" Text="Success" GroupName="tab2_status" runat="server" />
                                                        </div>
                                                        <div class="col-md-6 control-label2">
                                                            <asp:RadioButton ID="tab2_radio_failure" Text="Failure" GroupName="tab2_status" runat="server" />
                                                        </div>
                                                    </div>

                                                    <div class="form-group tab2_monthly">
                                                        <div class="col-md-6">
                                                        <label class=" control-label2 ">
                                                            Monthly Business Expected
                                                        </label>
                                                            <br />
                                                        <span style="color: red; font-size: 12px">
                                                            (Value should be in lakhs.<br />
                                                            Only 3 digits are allowed.)
                                                        </span>
                                                            </div>
                                                        <div class="col-md-6">
                                                            <div class="stagecontrols">
                                                                <asp:TextBox ID="tab2_monthly_expected" runat="server" class="form-control" onkeypress="return isNumberKey(event,this)" MaxLength="6"> </asp:TextBox>
                                                                <span style="color: Red; font-size: 12px; display: none;" class="lbltab2_monthly">Only Numeric values are allowed.</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                        <div class="clearfix"></div>
                                                    <div class="form-group tab2_hideout">
                                                        <label class="col-md-6  control-label2 ">
                                                            Comments
                                                        </label>
                                                        <div class="col-md-6">
                                                            <div class="stagecontrols">
                                                                <asp:TextBox ID="tab2_comments" runat="server" class="form-control" TextMode="multiline" Columns="100" Rows="3"> </asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="col-md-12 mn_input">
                                            <p>
                                                <%-- <button id="tab2btnRemarks" type="button" runat="server" class="btn green" onserverclick="tab2AddOneMoreRemark" style="width: 150px;margin-top: 19px; " ><i class="fa fa-plus"></i> New Remarks</button>	--%>
                                                <asp:ImageButton ID="tab2btnRemarks" runat="server" ToolTip="New Remarks" ImageUrl="~/images/shejiyeiconz0muj0ftihh.png" OnClick="tab2AddOneMoreRemark" visible="false" />
                                                <asp:ImageButton ID="tab2saveRemarks" runat="server" ToolTip="Save Remarks" ImageUrl="~/images/35297964-checklist-icon-green-round-button.png" Visible="false"  OnClick="tab2btnSave_Click" CausesValidation="true" />
                                                <asp:ImageButton ID="tab2btnSave" runat="server" ToolTip="Save and Continue" ImageUrl="~/images/torrents-icon-25.png"  OnClick="tab2btnSave_Click" CausesValidation="true" OnClientClick="return ValidateTab2()" />
                                                <asp:ImageButton ID="tab2btnSaveApprove" Visible="false" runat="server" ToolTip="Submit For Approval" ImageUrl="~/images/approved.png" OnClick="tab2btnSave_Click" CausesValidation="true" OnClientClick="return ValidateTab2()" />
                                                <asp:ImageButton ID="tab2btnSubmit" runat="server" ToolTip="Close Stage" Visible="false" ImageUrl="~/images/22151610-button-close-on-white-background-icon-green.png" OnClick="tab2btnSubmit_Click"  CausesValidation="true" OnClientClick="return ValidateSubmitTab2()" />
                                                <label runat="server" class="tab2_hideout_every" style="color: red; font-size: 12px; font-weight: 800;" id="Label1">&nbsp &nbsp To close the stage, click on Close Stage.</label>
                                            </p>
                                            <br />
                                            <br />
                                            <br />
                                            <br />
                                            <asp:Label ID="tab2_lblApprove" CssClass="control-label stagecontrol" runat="server" Visible="false">APPROVAL REMARKS</asp:Label>
                                            <p>
                                                <asp:TextBox ID="tab2_txtApprove" Visible="false" CssClass="form-control stageinput" TextMode="multiline" Columns="100" Rows="2" runat="server" />
                                            </p>
                                            <p>
                                                <asp:ImageButton ID="tab2_btnApprove" runat="server"  CssClass="mn_input" ToolTip="Approve" ImageUrl="~/images/approve.png" Visible="false"  OnClick="tab2btnApprove_Click" CausesValidation="true" />
                                                <asp:ImageButton ID="tab2_btnReject" Visible="false" runat="server"  CssClass="mn_input" ToolTip="Reject"  ImageUrl="~/images/reject_1.png"  OnClick="tab2btnReject_Click" CausesValidation="true" />
                                            </p>
                                        </div>

                                        <div class="clearfix"></div>
                                        <div class="col-md-12 ui-widget-content1 ">
                                            <div class="form-group col-md-12 tab2_hideout_upload">
                                                <asp:Label ID="tab2doc" class="col-md-2  control-label2 " runat="server" Visible="false">Attach Documents</asp:Label>
                                                <%--<label id="tab2doc" class="col-md-2  control-label2 ">Attach Documents</label>--%>
                                                <asp:FileUpload ID="FileUpload2" runat="server" AllowMultiple="true" CssClass="col-md-5 file" Visible="false"/>
                                                <br>
                                                <asp:ImageButton ID="tab2btnUpload" runat="server" ToolTip="Upload"  ImageUrl="~/images/upload_1.png"  CssClass="col-md-3 uploadcontrol"  OnClick="tab2_UploadMultipleFiles" Text="Upload" Visible="false"/>
                                                <asp:Label ID="tab2lblSuccess" class="col-md-2" runat="server" ForeColor="Green" />
                                                <br>
                                            </div>

                                            <div class="form-group col-md-12">
                                                <asp:GridView ID="tab2_FileList" runat="server" Height="100px" Style="text-align: center;" AutoGenerateColumns="false" ShowHeader="true">
                                                    <Columns>
                                                        <asp:TemplateField HeaderText="Uploaded Docs" HeaderStyle-CssClass="HeadergridAll">
                                                            <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                                            <ItemTemplate>
                                                                <asp:LinkButton ID="tab2_file_lbl" CssClass="lbl_color" runat="server" CommandArgument='<%# Eval("file_number") %>' Text='<%# Eval("file_name") %>' OnClick="downloadStage_Files"></asp:LinkButton>
                                                                <asp:LinkButton ID="tab2_file_Delete" Text="Delete" CommandArgument='<%# Eval("file_number") %>' runat="server" OnClick="DeleteFile_tab2" />
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                    </Columns>
                                                </asp:GridView>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="tab-pane" id="tab_1_3">
                                        <div class="col-md-7">
                                            <label class="control-label stagecontrol">GOAL</label>
                                            <p>
                                                <asp:TextBox ID="tab3txtGoal" CssClass="form-control stageinput" TextMode="multiline" Style="resize: vertical" Columns="100" Rows="2" runat="server" />
                                            </p>
                                            <p>
                                                <div id="tab3Remarks" class="form-group" runat="server">
                                                    <label class="control-label stagecontrol">REMARKS</label>
                                                    <asp:TextBox ID="tab3txtRemarks_1" CssClass="form-control stageinput" TextMode="multiline" Style="resize: vertical" Columns="100" Rows="2" runat="server" />
                                                    <p></p>
                                                </div>
                                                <p>
                                                </p>
                                                <p>
                                                </p>
                                                <p>
                                                </p>
                                                <p>
                                                </p>
                                                <p>
                                                </p>
                                                <p>
                                                </p>
                                                <p>
                                                </p>
                                                <p>
                                                </p>
                                                <p>
                                                </p>
                                                <p>
                                                </p>
                                                <p>
                                                </p>
                                                <p>
                                                </p>
                                                <p>
                                                </p>
                                                <p>
                                                </p>
                                                <p>
                                                </p>
                                                <p>
                                                </p>
                                                <p>
                                                </p>
                                                <p>
                                                </p>
                                                <p>
                                                </p>
                                                <p>
                                                </p>
                                                <p>
                                                </p>
                                                <p>
                                                </p>
                                                <p>
                                                </p>
                                                <p>
                                                </p>
                                            </p>
                                        </div>

                                        <div class="col-md-5 ">
                                            <div class="portlet box lightgray">
                                                <div>
                                                    <div class="form-group">
                                                        <label class="col-md-6  control-label2 ">
                                                            Target Date <span style="color: Red; font-size: 12px; display: none;" class="lbltab3Target">*</span>
                                                        </label>
                                                        <div class="col-md-6">
                                                            <div class="stagecontrols" style="width: 100%">
                                                                <asp:TextBox ID="tab3Target" runat="server" class="form-control"> </asp:TextBox>
                                                                <asp:HiddenField ID="tab3Target_flag" runat="server" />
                                                                <asp:HiddenField ID="tab3Target_date" runat="server" />
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-md-6  control-label2 ">
                                                            Actual Completion Date <span style="color: Red; font-size: 12px; display: none;" class="lbltab3Complete">*</span>
                                                        </label>
                                                        <div class="col-md-6">
                                                            <div class="stagecontrols" style="width: 100%">
                                                                <asp:TextBox ID="tab3Completion" Enabled="false" runat="server" class="form-control"> </asp:TextBox>
                                                                <asp:HiddenField ID="tab3Completion_flag" runat="server" />
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="form-group tab3_hideout">
                                                        <div class="col-md-6 control-label2" style="text-align:right;">
                                                            <asp:RadioButton ID="tab3_radio_success" Text="Success" GroupName="tab3_status" runat="server" />
                                                        </div>
                                                        <div class="col-md-6 control-label2">
                                                            <asp:RadioButton ID="tab3_radio_failure" Text="Failure" GroupName="tab3_status" runat="server" />
                                                        </div>
                                                    </div>

                                                    <div class="form-group tab3_monthly">
                                                        <div  class="col-md-6">
                                                        <label class="col-md-6  control-label2 ">
                                                            Monthly Business Expected
                                                        </label>
                                                        <br />
                                                        <span style="color: red; font-size: 11px;">
                                                            (Value should be in lakhs.<br />
                                                            Only 3 digits are allowed.)
                                                        </span>
                                                         </div>
                                                        <div class="col-md-6">
                                                            <div class="stagecontrols">
                                                                <asp:TextBox ID="tab3_monthly_expected" runat="server" class="form-control" onkeypress="return isNumberKey(event,this)" MaxLength="6"> </asp:TextBox>
                                                                <span style="color: Red; font-size: 11px; display: none;" class="lbltab3_monthly">Only Numeric values are allowed.</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                     <div class="clearfix"></div>
                                                    <div class="form-group tab3_hideout">
                                                        <label class="col-md-6  control-label2 ">
                                                            Comments
                                                        </label>
                                                        <div class="col-md-6">
                                                            <div class="stagecontrols">
                                                                <asp:TextBox ID="tab3_comments" runat="server" class="form-control" TextMode="multiline" Columns="100" Rows="3"> </asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="col-md-12 mn_input">
                                            <p>
                                                <%--<button id="tab3btnRemarks" type="button" runat="server" class="btn green" onserverclick="tab3AddOneMoreRemark" style="width: 150px;margin-top: 19px; " ><i class="fa fa-plus"></i> New Remarks</button>	--%>
                                                <asp:ImageButton ID="tab3btnRemarks" runat="server" ToolTip="New Remarks"  ImageUrl="~/images/shejiyeiconz0muj0ftihh.png" OnClick="tab3AddOneMoreRemark" Visible="false" />
                                                <asp:ImageButton ID="tab3saveRemarks" runat="server" ToolTip="Save Remarks" ImageUrl="~/images/35297964-checklist-icon-green-round-button.png" Visible="false"  OnClick="tab3btnSave_Click" CausesValidation="true" />
                                                <asp:ImageButton ID="tab3btnSave" runat="server" ToolTip="Save and Continue" ImageUrl="~/images/torrents-icon-25.png" OnClick="tab3btnSave_Click" CausesValidation="true" OnClientClick="return ValidateTab3()" />
                                                <asp:ImageButton ID="tab3btnSaveApprove" Visible="false" runat="server" ToolTip="Submit For Approval" ImageUrl="~/images/approved.png"  OnClick="tab3btnSave_Click" CausesValidation="true" OnClientClick="return ValidateTab3()" />
                                                <asp:ImageButton ID="tab3btnSubmit" runat="server" ToolTip="Close Stage" Visible="false" ImageUrl="~/images/22151610-button-close-on-white-background-icon-green.png"  OnClick="tab3btnSubmit_Click" CausesValidation="true" OnClientClick="return ValidateSubmitTab3()" />
                                                <label runat="server" class="tab3_hideout_every" style="color: red; font-size: 12px; font-weight: 800;" id="Label2">&nbsp &nbsp To close the stage, click on Close Stage.</label>
                                            </p>
                                            <br />
                                            <br />
                                            <br />
                                            <br />
                                            <asp:Label ID="tab3_lblApprove" CssClass="control-label stagecontrol" runat="server" Visible="false">APPROVAL REMARKS</asp:Label>
                                            <p>
                                                <asp:TextBox ID="tab3_txtApprove" Visible="false" CssClass="form-control stageinput" TextMode="multiline" Columns="100" Rows="2" runat="server" />
                                            </p>
                                            <p>
                                                <asp:ImageButton ID="tab3_btnApprove" runat="server" Visible="false"  CssClass="mn_input"  ToolTip="Approve" ImageUrl="~/images/approve.png"  OnClick="tab3btnApprove_Click" CausesValidation="true" />
                                                <asp:ImageButton ID="tab3_btnReject" runat="server" Visible="false"   CssClass="mn_input" ToolTip="Reject"  ImageUrl="~/images/reject_1.png"  OnClick="tab3btnReject_Click" CausesValidation="true" />
                                            </p>
                                        </div>


                                        <div class="clearfix"></div>
                                        <div class="col-md-12 ui-widget-content1">
                                            <div class="form-group col-md-12 tab3_hideout_upload">
                                                <asp:Label ID="tab3doc" class="col-md-2  control-label2 " runat="server" Visible="false">Attach Documents</asp:Label>
                                                <%--<label class="col-md-2  control-label2 ">Attach Documents</label>--%>
                                                <asp:FileUpload ID="FileUpload3" runat="server" AllowMultiple="true" CssClass="col-md-5 file" Visible="false" />
                                                <br>
                                                <asp:ImageButton ID="tab3btnUpload" runat="server" ToolTip="Upload" ImageUrl="~/images/upload_1.png" CssClass="col-md-3 uploadcontrol" style="margin-top:-10px;" OnClick="tab3_UploadMultipleFiles" Visible="false"/>
                                                <asp:Label ID="tab3lblSuccess" runat="server" ForeColor="Green" />
                                                <br>
                                            </div>
                                            <div class="form-group col-md-12">
                                                <asp:GridView ID="tab3_FileList" runat="server" Style="text-align: center;" AutoGenerateColumns="false" ShowHeader="true">
                                                    <Columns>
                                                        <asp:TemplateField HeaderText="Uploaded Docs" HeaderStyle-CssClass="HeadergridAll">
                                                            <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                                            <ItemTemplate>
                                                                <asp:LinkButton ID="tab3_file_lbl" CssClass="lbl_color" runat="server" CommandArgument='<%# Eval("file_number") %>' Text='<%# Eval("file_name") %>' OnClick="downloadStage_Files"></asp:LinkButton>
                                                                <asp:LinkButton ID="tab3_file_Delete" Text="Delete" CommandArgument='<%# Eval("file_number") %>' runat="server" OnClick="DeleteFile_tab3" />
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                    </Columns>
                                                </asp:GridView>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="tab-pane" id="tab_1_4">
                                        <div class="col-md-7">
                                            <label class="control-label stagecontrol">GOAL</label>
                                            <p>
                                                <asp:TextBox ID="tab4txtGoal" CssClass="form-control stageinput" TextMode="multiline" Style="resize: vertical" Columns="100" Rows="2" runat="server" />
                                            </p>

                                            <p>
                                                <div id="tab4Remarks" class="form-group" runat="server">
                                                    <label class="control-label stagecontrol">REMARKS</label>
                                                    <asp:TextBox ID="tab4txtRemarks_1" CssClass="form-control stageinput" TextMode="multiline" Style="resize: vertical" Columns="100" Rows="2" runat="server" />
                                                    <p></p>
                                                </div>
                                                <p>
                                                </p>
                                                <p>
                                                </p>
                                                <p>
                                                </p>
                                                <p>
                                                </p>
                                                <p>
                                                </p>
                                                <p>
                                                </p>
                                                <p>
                                                </p>
                                                <p>
                                                </p>
                                                <p>
                                                </p>
                                                <p>
                                                </p>
                                                <p>
                                                </p>
                                                <p>
                                                </p>
                                                <p>
                                                </p>
                                                <p>
                                                </p>
                                                <p>
                                                </p>
                                                <p>
                                                </p>
                                                <p>
                                                </p>
                                                <p>
                                                </p>
                                                <p>
                                                </p>
                                                <p>
                                                </p>
                                                <p>
                                                </p>
                                                <p>
                                                </p>
                                                <p>
                                                </p>
                                                <p>
                                                </p>
                                            </p>

                                        </div>
                                        <div class="col-md-5 ">
                                            <div class="portlet box lightgray">
                                                <div>
                                                    <div class="form-group">
                                                        <label class="col-md-6  control-label2 ">
                                                            Target Date <span style="color: Red; font-size: 12px; display: none;" class="lbltab4Target">*</span>
                                                        </label>
                                                        <div class="col-md-6">
                                                            <div class="stagecontrols" style="width: 100%">
                                                                <asp:TextBox ID="tab4Target" runat="server" class="form-control"> </asp:TextBox>
                                                                <asp:HiddenField ID="tab4Target_flag" runat="server" />
                                                                <asp:HiddenField ID="tab4Target_date" runat="server" />
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-md-6  control-label2 ">
                                                            Actual Completion Date <span style="color: Red; font-size: 12px; display: none;" class="lbltab4Complete">*</span>
                                                        </label>
                                                        <div class="col-md-6">
                                                            <div class="stagecontrols" style="width: 100%">
                                                                <asp:TextBox ID="tab4Completion" Enabled="false" runat="server" class="form-control"> </asp:TextBox>
                                                                <asp:HiddenField ID="tab4Completion_flag" runat="server" />
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="form-group tab4_hideout">
                                                        <div class="col-md-6 control-label2" style="text-align:right;">
                                                            <asp:RadioButton ID="tab4_radio_success" Text="Success" GroupName="tab4_status" runat="server" />
                                                        </div>
                                                        <div class="col-md-6 control-label2">
                                                            <asp:RadioButton ID="tab4_radio_failure" Text="Failure" GroupName="tab4_status" runat="server" />
                                                        </div>
                                                    </div>

                                                    <div class="form-group tab4_monthly">
                                                          <div class="col-md-6">
                                                        <label class="control-label2">
                                                            Monthly Business Expected
                                                        </label>
                                                        <span style="color: red; font-size: 11px">
                                                            (Value should be in lakhs.<br />
                                                            Only 3 digits are allowed.)
                                                        </span>
                                                              </div>
                                                        <div class="col-md-6">
                                                            <div class="stagecontrols">
                                                                <asp:TextBox ID="tab4_monthly_expected" runat="server" class="form-control" onkeypress="return isNumberKey(event,this)" MaxLength="6"> </asp:TextBox>
                                                                <span style="color: Red; font-size: 11px; display: none;" class="lbltab4_monthly">Only Numeric values are allowed.</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                    <div class="form-group tab4_hideout">
                                                        <label class="col-md-6  control-label2 ">
                                                            Comments
                                                        </label>
                                                        <div class="col-md-6">
                                                            <div class="stagecontrols">
                                                                <asp:TextBox ID="tab4_comments" runat="server" class="form-control" TextMode="multiline" Columns="100" Rows="3"> </asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="col-md-12 mn_input">
                                            <p>
                                                <%--<button id="tab4btnRemarks" type="button" runat="server" class="btn green" onserverclick="tab4AddOneMoreRemark" style="width: 150px;margin-top: 19px; " ><i class="fa fa-plus"></i> New Remarks</button>	--%>
                                                <asp:ImageButton ID="tab4btnRemarks" runat="server"  ImageUrl="~/images/shejiyeiconz0muj0ftihh.png" OnClick="tab4AddOneMoreRemark" Visible="false" />
                                                <asp:ImageButton ID="tab4saveRemarks" runat="server" ImageUrl="~/images/35297964-checklist-icon-green-round-button.png" Visible="false"  OnClick="tab4btnSave_Click" CausesValidation="true" />
                                                <asp:ImageButton ID="tab4btnSave" runat="server" ToolTip="Save and Continue" ImageUrl="~/images/torrents-icon-25.png" OnClick="tab4btnSave_Click" CausesValidation="true" OnClientClick="return ValidateTab4()" />
                                                <asp:ImageButton ID="tab4btnSaveApprove" Visible="false" runat="server" ToolTip="Submit For Approval" ImageUrl="~/images/approved.png"  OnClick="tab4btnSave_Click" CausesValidation="true" OnClientClick="return ValidateTab4()" />
                                                <asp:ImageButton ID="tab4btnSubmit" runat="server" ToolTip="Close Stage" Visible="false" ImageUrl="~/images/22151610-button-close-on-white-background-icon-green.png"  OnClick="tab4btnSubmit_Click" CausesValidation="true" OnClientClick="return ValidateSubmitTab4()" />
                                                <label runat="server" class="tab4_hideout_every" style="color: red; font-size: 12px; font-weight: 800;" id="Label3">&nbsp &nbsp To close the stage, click on Close Stage.</label>
                                            </p>
                                              <br />
                                            <br />
                                            <br />
                                            <br />
                                            <asp:Label ID="tab4_lblApprove" CssClass="control-label stagecontrol" runat="server" Visible="false">APPROVAL REMARKS</asp:Label>
                                            <p>
                                                <asp:TextBox ID="tab4_txtApprove" Visible="false" CssClass="form-control stageinput" TextMode="multiline" Columns="100" Rows="2" runat="server" />
                                            </p>
                                            <p>
                                                <asp:ImageButton ID="tab4_btnApprove" runat="server" Visible="false"  CssClass="mn_input" ToolTip="Approve" ImageUrl="~/images/approve.png" OnClick="tab4btnApprove_Click" CausesValidation="true" />
                                                <asp:ImageButton ID="tab4_btnReject" runat="server" Visible="false" CssClass="mn_input" ToolTip="Reject"  ImageUrl="~/images/reject_1.png"  OnClick="tab4btnReject_Click" CausesValidation="true" />
                                            </p>
                                        </div>


                                        <div class="clearfix"></div>
                                        <div class="col-md-12 ui-widget-content1">
                                            <div class="form-group col-md-12 tab4_hideout_upload">
                                                 <asp:Label ID="tab4doc" class="col-md-2  control-label2 " runat="server" Visible="false">Attach Documents</asp:Label>   
                                                <%--<label class="col-md-2  control-label2 ">Attach Documents</label>--%>
                                                <asp:FileUpload ID="FileUpload4" runat="server" AllowMultiple="true" CssClass="col-md-5 file" Visible="false" />
                                                <br>
                                                <asp:ImageButton ID="tab4btnUpload" runat="server" ToolTip="Upload" ImageUrl="~/images/upload_1.png" CssClass="col-md-3 uploadcontrol"  OnClick="tab4_UploadMultipleFiles" Visible="false" />
                                                <asp:Label ID="tab4lblSuccess"  class="col-md-2"  runat="server" ForeColor="Green" />
                                                <br>
                                            </div>
                                            <div class="form-group col-md-12">
                                                <asp:GridView ID="tab4_FileList" runat="server" Height="100px" Style="text-align: center;" AutoGenerateColumns="false" ShowHeader="true">
                                                    <Columns>
                                                        <asp:TemplateField HeaderText="Uploaded Docs" HeaderStyle-CssClass="HeadergridAll">
                                                            <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                                            <ItemTemplate>
                                                                <asp:LinkButton ID="tab4_file_lbl" CssClass="lbl_color" runat="server" CommandArgument='<%# Eval("file_number") %>' Text='<%# Eval("file_name") %>' OnClick="downloadStage_Files"></asp:LinkButton>
                                                                <asp:LinkButton ID="tab4_file_Delete" Text="Delete" CommandArgument='<%# Eval("file_number") %>' runat="server" OnClick="DeleteFile_tab4" />
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                    </Columns>
                                                </asp:GridView>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="tab-pane" id="tab_1_5">
                                        <div class="col-md-7">
                                            <label class="control-label stagecontrol">GOAL</label>
                                            <p>
                                                <asp:TextBox ID="tab5txtGoal" CssClass="form-control stageinput" TextMode="multiline" Style="resize: vertical" Columns="100" Rows="2" runat="server" />
                                            </p>

                                            <p>
                                                <div id="tab5Remarks" class="form-group" runat="server">
                                                    <label class="control-label stagecontrol">REMARKS</label>
                                                    <asp:TextBox ID="tab5txtRemarks_1" CssClass="form-control stageinput" TextMode="multiline" Style="resize: vertical" Columns="100" Rows="2" runat="server" />
                                                    <p></p>
                                                </div>
                                                <p>
                                                </p>
                                                <p>
                                                </p>
                                                <p>
                                                </p>
                                                <p>
                                                </p>
                                                <p>
                                                </p>
                                                <p>
                                                </p>
                                                <p>
                                                </p>
                                                <p>
                                                </p>
                                                <p>
                                                </p>
                                                <p>
                                                </p>
                                                <p>
                                                </p>
                                                <p>
                                                </p>
                                                <p>
                                                </p>
                                                <p>
                                                </p>
                                                <p>
                                                </p>
                                                <p>
                                                </p>
                                                <p>
                                                </p>
                                                <p>
                                                </p>
                                                <p>
                                                </p>
                                                <p>
                                                </p>
                                                <p>
                                                </p>
                                                <p>
                                                </p>
                                                <p>
                                                </p>
                                                <p>
                                                </p>
                                            </p>

                                        </div>
                                        <div class="col-md-5 ">
                                            <div class="portlet box lightgray">
                                                <div>
                                                    <div class="form-group">
                                                        <label class="col-md-6  control-label2 ">
                                                            Target Date <span style="color: Red; font-size: 12px; display: none;" class="lbltab5Target">*</span>
                                                        </label>
                                                        <div class="col-md-6">
                                                            <div class="stagecontrols" style="width: 100%">
                                                                <asp:TextBox ID="tab5Target" runat="server" class="form-control"> </asp:TextBox>
                                                                <asp:HiddenField ID="tab5Target_flag" runat="server" />
                                                                <asp:HiddenField ID="tab5Target_date" runat="server" />
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-md-6  control-label2 ">
                                                            Actual Completion Date <span style="color: Red; font-size: 12px; display: none;" class="lbltab5Complete">*</span>
                                                        </label>
                                                        <div class="col-md-6">
                                                            <div class="stagecontrols" style="width: 100%">
                                                                <asp:TextBox ID="tab5Completion" Enabled="false" runat="server" class="form-control"> </asp:TextBox>
                                                                <asp:HiddenField ID="tab5Completion_flag" runat="server" />
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="form-group tab5_hideout">
                                                        <div class="col-md-6 control-label2"style="text-align:right;">
                                                            <asp:RadioButton ID="tab5_radio_success" Text="Success" GroupName="tab5_status" runat="server" />
                                                        </div>
                                                        <div class="col-md-6 control-label2">
                                                            <asp:RadioButton ID="tab5_radio_failure" Text="Failure" GroupName="tab5_status" runat="server" />
                                                        </div>
                                                    </div>

                                                    <div class="form-group tab5_monthly">
                                                         <div class="col-md-6">
                                                        <label class=" control-label2 ">
                                                            Monthly Business Expected
                                                        </label>
                                                             <br />
                                                        <span style="color: red; font-size: 11px">
                                                            (Value should be in lakhs.<br />
                                                            Only 3 digits are allowed.)
                                                        </span>
                                                             </div>
                                                        <div class="col-md-6">
                                                            <div class="stagecontrols">
                                                                <asp:TextBox ID="tab5_monthly_expected" runat="server" class="form-control" onkeypress="return isNumberKey(event,this)" MaxLength="6"> </asp:TextBox>
                                                                <span style="color: Red; font-size: 11px; display: none;" class="lbltab5_monthly">Only Numeric values are allowed.</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                    <div class="form-group tab5_hideout">
                                                        <label class="col-md-6  control-label2 ">
                                                            Comments
                                                        </label>
                                                        <div class="col-md-6">
                                                            <div class="stagecontrols">
                                                                <asp:TextBox ID="tab5_comments" runat="server" class="form-control" TextMode="multiline" Columns="100" Rows="3"> </asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="clearfix"></div>

                                        <div class="col-md-12 mn_input">
                                            <p>
                                                <%-- <button id="tab5btnRemarks" type="button" runat="server" class="btn green" onserverclick="tab5AddOneMoreRemark" style="width: 150px;margin-top: 19px; " ><i class="fa fa-plus"></i> New Remarks</button>	--%>
                                                <asp:ImageButton ID="tab5btnRemarks" runat="server" ToolTip="New Remarks" ImageUrl="~/images/shejiyeiconz0muj0ftihh.png"  OnClick="tab5AddOneMoreRemark" Visible="false" />
                                                <asp:ImageButton ID="tab5saveRemarks" runat="server" ToolTip="Save Remarks" ImageUrl="~/images/35297964-checklist-icon-green-round-button.png" Visible="false"  OnClick="tab5btnSave_Click" CausesValidation="true" />
                                                <asp:ImageButton ID="tab5btnSave" runat="server" ToolTip="Save and Continue" ImageUrl="~/images/torrents-icon-25.png" OnClick="tab5btnSave_Click" CausesValidation="true" OnClientClick="return ValidateTab5()" />
                                                <asp:ImageButton ID="tab5btnSaveApprove" Visible="false" runat="server" ToolTip="Submit For Approval" ImageUrl="~/images/approved.png"  OnClick="tab5btnSave_Click" CausesValidation="true" OnClientClick="return ValidateTab5()" />
                                                <asp:ImageButton ID="tab5btnSubmit" runat="server" ToolTip="Close Stage" Visible="false" ImageUrl="~/images/22151610-button-close-on-white-background-icon-green.png" OnClick="tab5btnSubmit_Click"  CausesValidation="true" OnClientClick="return ValidateSubmitTab5()" />
                                                <label runat="server" class="tab5_hideout_every" style="color: red; font-size: 12px; font-weight: 800;" id="Label4">&nbsp &nbsp To close the stage, click on Close Stage.</label>
                                            </p>
                                            <br />
                                            <br />
                                            <br />
                                            <br />
                                            <asp:Label ID="tab5_lblApprove" CssClass="control-label stagecontrol" runat="server" Visible="false">APPROVAL REMARKS</asp:Label>
                                            <p>
                                                <asp:TextBox ID="tab5_txtApprove" Visible="false" CssClass="form-control stageinput" TextMode="multiline" Columns="100" Rows="2" runat="server" />
                                            </p>
                                            <p>
                                                <asp:ImageButton ID="tab5_btnApprove" runat="server"  CssClass="mn_input" ToolTip="Approve" ImageUrl="~/images/approve.png" Visible="false"  OnClick="tab5btnApprove_Click" CausesValidation="true" />
                                                <asp:ImageButton ID="tab5_btnReject" runat="server"  CssClass="mn_input" ToolTip="Reject"  ImageUrl="~/images/reject_1.png" Visible="false"  OnClick="tab5btnReject_Click" CausesValidation="true" />
                                            </p>
                                        </div>


                                        <div class="clearfix"></div>
                                        <div class="col-md-12 ui-widget-content1">
                                            <div class="form-group col-md-12 tab5_hideout_upload">
                                                 <asp:Label ID="tab5doc" class="col-md-2  control-label2 " runat="server" Visible="false">Attach Documents</asp:Label>   
                                               <%-- <label class="col-md-2  control-label2 ">Attach Documents</label>--%>
                                                <asp:FileUpload ID="FileUpload5" runat="server" AllowMultiple="true" CssClass="col-md-5 file" Visible="false" />
                                                <br>

                                                <asp:ImageButton ID="tab5btnUpload" runat="server" ToolTip="Upload" ImageUrl="~/images/upload_1.png" CssClass="col-md-3 uploadcontrol" OnClick="tab5_UploadMultipleFiles"  Visible="false" />
                                                <asp:Label ID="tab5lblSuccess" class="col-md-2" runat="server" ForeColor="Green" />
                                                <br>
                                            </div>
                                            <div class="form-group col-md-12">
                                                <asp:GridView ID="tab5_FileList" runat="server" Height="100px" Style="text-align: center;" AutoGenerateColumns="false" ShowHeader="true">
                                                    <Columns>
                                                        <asp:TemplateField HeaderText="Uploaded Docs" HeaderStyle-CssClass="HeadergridAll">
                                                            <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                                            <ItemTemplate>
                                                                <asp:LinkButton ID="tab5_file_lbl" CssClass="lbl_color" runat="server" CommandArgument='<%# Eval("file_number") %>' Text='<%# Eval("file_name") %>' OnClick="downloadStage_Files"></asp:LinkButton>
                                                                <asp:LinkButton ID="tab5_file_Delete" Text="Delete" CommandArgument='<%# Eval("file_number") %>' runat="server" OnClick="DeleteFile_tab5" />
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                    </Columns>
                                                </asp:GridView>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="tab-pane" id="tab_1_6">
                                        <div class="col-md-7">
                                            <label class="control-label stagecontrol">GOAL</label>
                                            <p>
                                                <asp:TextBox ID="tab6txtGoal" CssClass="form-control stageinput" TextMode="multiline" Style="resize: vertical" Columns="100" Rows="2" runat="server" />
                                            </p>

                                            <p>
                                                <div id="tab6Remarks" class="form-group" runat="server">
                                                    <label class="control-label stagecontrol">REMARKS</label>
                                                    <asp:TextBox ID="tab6txtRemarks_1" CssClass="form-control stageinput" TextMode="multiline" Style="resize: vertical" Columns="100" Rows="2" runat="server" />
                                                    <p></p>
                                                </div>
                                                <p>
                                                </p>
                                                <p>
                                                </p>
                                                <p>
                                                </p>
                                                <p>
                                                </p>
                                                <p>
                                                </p>
                                                <p>
                                                </p>
                                                <p>
                                                </p>
                                                <p>
                                                </p>
                                                <p>
                                                </p>
                                                <p>
                                                </p>
                                                <p>
                                                </p>
                                                <p>
                                                </p>
                                                <p>
                                                </p>
                                                <p>
                                                </p>
                                                <p>
                                                </p>
                                                <p>
                                                </p>
                                                <p>
                                                </p>
                                                <p>
                                                </p>
                                                <p>
                                                </p>
                                                <p>
                                                </p>
                                                <p>
                                                </p>
                                                <p>
                                                </p>
                                                <p>
                                                </p>
                                                <p>
                                                </p>
                                            </p>
                                        </div>
                                        <div class="col-md-5">
                                            <div class="portlet box lightgray">
                                                <div>
                                                    <div class="form-group">
                                                        <label class="col-md-6  control-label2 ">
                                                            Target Date <span style="color: Red; font-size: 12px; display: none;" class="lbltab6Target">*</span>
                                                        </label>
                                                        <div class="col-md-6">
                                                            <div class="stagecontrols" style="width: 100%">
                                                                <asp:TextBox ID="tab6Target" runat="server" class="form-control"> </asp:TextBox>
                                                                <asp:HiddenField ID="tab6Target_flag" runat="server" />
                                                                <asp:HiddenField ID="tab6Target_date" runat="server" />
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-md-6  control-label2 ">
                                                            Actual Completion Date <span style="color: Red; font-size: 12px; display: none;" class="lbltab6Complete">*</span>
                                                        </label>
                                                        <div class="col-md-6">
                                                            <div class="stagecontrols" style="width: 100%">
                                                                <asp:TextBox ID="tab6Completion" Enabled="false" runat="server" class="form-control"> </asp:TextBox>
                                                                <asp:HiddenField ID="tab6Completion_flag" runat="server" />
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="form-group tab6_hideout">
                                                        <div class="col-md-6 control-label2">
                                                            <asp:RadioButton ID="tab6_radio_success" Text="Success" GroupName="tab6_status" runat="server" />
                                                        </div>
                                                        <div class="col-md-6 control-label2">
                                                            <asp:RadioButton ID="tab6_radio_failure" Text="Failure" GroupName="tab6_status" runat="server" />
                                                        </div>
                                                    </div>

                                                    <div class="form-group tab6_monthly">
                                                        <div class="col-md-6 ">
                                                        <label class=" control-label2 ">
                                                            Monthly Business Expected
                                                        </label>
                                                        <span style="color: red; font-size: 11px">
                                                            (Value should be in lakhs.<br />
                                                            Only 3 digits are allowed.)
                                                        </span>
                                                            </div>
                                                        <div class="col-md-6">
                                                            <div class="stagecontrols">
                                                                <asp:TextBox ID="tab6_monthly_expected" runat="server" class="form-control" onkeypress="return isNumberKey(event,this)" MaxLength="6"> </asp:TextBox>
                                                                <span style="color: Red; font-size: 11px; display: none;" class="lbltab6_monthly">Only Numeric values are allowed.</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                    <div class="form-group tab6_hideout">
                                                        <label class="col-md-6  control-label2 ">
                                                            Comments
                                                        </label>
                                                        <div class="col-md-6">
                                                            <div class="stagecontrols">
                                                                <asp:TextBox ID="tab6_comments" runat="server" class="form-control" TextMode="multiline" Columns="500" Rows="4"> </asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="clearfix"></div>
                                        <div class="col-md-12 mn_input">
                                            <p>
                                                <%--<button id="tab6btnRemarks" type="button" runat="server" class="btn green" onserverclick="tab6AddOneMoreRemark" style ="width: 150px;margin-top: 19px; " ><i class="fa fa-plus"></i> New Remarks</button>	--%>
                                                <asp:ImageButton ID="tab6btnRemarks" runat="server"  ImageUrl="~/images/shejiyeiconz0muj0ftihh.png" OnClick="tab6AddOneMoreRemark" Visible="false" />
                                                <asp:ImageButton ID="tab6saveRemarks" runat="server" ImageUrl="~/images/35297964-checklist-icon-green-round-button.png" Visible="false"  OnClick="tab6btnSave_Click" CausesValidation="true" />
                                                <asp:ImageButton ID="tab6btnSave" runat="server" ToolTip="Save and Continue" ImageUrl="~/images/torrents-icon-25.png" OnClick="tab6btnSave_Click"  CausesValidation="true" OnClientClick="return ValidateTab6()" />
                                                <asp:ImageButton ID="tab6btnSaveApprove" Visible="false" runat="server" ToolTip="Submit For Approval" ImageUrl="~/images/approved.png"  OnClick="tab6btnSave_Click" CausesValidation="true" OnClientClick="return ValidateTab6()" />
                                                <asp:ImageButton ID="tab6btnSubmit" runat="server" ToolTip="Close Stage" Visible="false" ImageUrl="~/images/22151610-button-close-on-white-background-icon-green.png" OnClick="tab6btnSubmit_Click"  CausesValidation="true" OnClientClick="return ValidateSubmitTab6()" />
                                                <label runat="server" class="tab6_hideout_every" style="color: red; font-size: 12px; font-weight: 800;" id="Label5">&nbsp &nbsp To close the stage, click on Close Stage.</label>
                                            </p>
                                            <br />
                                            <br />
                                            <br />
                                            <br />
                                            <asp:Label ID="tab6_lblApprove" CssClass="control-label stagecontrol" runat="server" Visible="false">APPROVAL REMARKS</asp:Label>
                                            <p>
                                                <asp:TextBox ID="tab6_txtApprove" Visible="false" CssClass="form-control stageinput" TextMode="multiline" Columns="100" Rows="2" runat="server" />
                                            </p>
                                            <p>
                                                <asp:ImageButton ID="tab6_btnApprove" runat="server" Visible="false"  CssClass="mn_input" ToolTip="Approve" ImageUrl="~/images/approve.png" OnClick="tab6btnApprove_Click" CausesValidation="true" />
                                                <asp:ImageButton ID="tab6_btnReject" runat="server" Visible="false"  CssClass="mn_input" ToolTip="Reject"  ImageUrl="~/images/reject_1.png" OnClick="tab6btnReject_Click" CausesValidation="true" />
                                            </p>
                                        </div>


                                        <div class="clearfix"></div>
                                        <div class="col-md-12 ui-widget-content1">
                                            <div class="form-group col-md-12 tab6_hideout_upload">
                                                 <asp:Label ID="tab6doc" class="col-md-2  control-label2 " runat="server" Visible="false">Attach Documents</asp:Label>   
                                             <%--   <label class="col-md-2  control-label2 ">Attach Documents</label>--%>
                                                <asp:FileUpload ID="FileUpload6" runat="server" AllowMultiple="true" CssClass="col-md-5 file" Visible="false" />
                                                <br>
                                                <asp:ImageButton ID="tab6btnUpload" runat="server" ToolTip="Upload" ImageUrl="~/images/upload_1.png" CssClass="col-md-3 uploadcontrol"  OnClick="tab6_UploadMultipleFiles" Visible="false" />
                                                <asp:Label ID="tab6lblSuccess"  class="col-md-2" runat="server" ForeColor="Green" />
                                                <br>
                                            </div>

                                            <div class="form-group col-md-12">
                                                <asp:GridView ID="tab6_FileList" runat="server"  Height="100px" Style="text-align: center;" AutoGenerateColumns="false" ShowHeader="true">
                                                    <Columns>
                                                        <asp:TemplateField HeaderText="Uploaded Docs" HeaderStyle-CssClass="HeadergridAll">
                                                            <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                                            <ItemTemplate>
                                                                <asp:LinkButton ID="tab6_file_lbl" CssClass="lbl_color" runat="server" CommandArgument='<%# Eval("file_number") %>' Text='<%# Eval("file_name") %>' OnClick="downloadStage_Files"></asp:LinkButton>
                                                                <asp:LinkButton ID="tab6_file_Delete" Text="Delete" CommandArgument='<%# Eval("file_number") %>' runat="server" OnClick="DeleteFile_tab6" />
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                    </Columns>
                                                </asp:GridView>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="tab-pane" id="tab_1_7">
                                        <div class="col-md-7">
                                            <label class="control-label stagecontrol">GOAL</label>
                                            <p>
                                                <asp:TextBox ID="tab7txtGoal" CssClass="form-control stageinput" TextMode="multiline" Style="resize: vertical" Columns="100" Rows="2" runat="server" />
                                            </p>

                                            <p>
                                                <div id="tab7Remarks" class="form-group" runat="server">
                                                    <label class="control-label stagecontrol">REMARKS</label>
                                                    <asp:TextBox ID="tab7txtRemarks_1" CssClass="form-control stageinput" TextMode="multiline" Style="resize: vertical" Columns="100" Rows="2" runat="server" />
                                                    <p></p>
                                                </div>
                                                <p>
                                                </p>
                                                <p>
                                                </p>
                                                <p>
                                                </p>
                                                <p>
                                                </p>
                                                <p>
                                                </p>
                                                <p>
                                                </p>
                                                <p>
                                                </p>
                                                <p>
                                                </p>
                                                <p>
                                                </p>
                                                <p>
                                                </p>
                                                <p>
                                                </p>
                                                <p>
                                                </p>
                                                <p>
                                                </p>
                                                <p>
                                                </p>
                                                <p>
                                                </p>
                                                <p>
                                                </p>
                                                <p>
                                                </p>
                                                <p>
                                                </p>
                                                <p>
                                                </p>
                                                <p>
                                                </p>
                                                <p>
                                                </p>
                                                <p>
                                                </p>
                                                <p>
                                                </p>
                                                <p>
                                                </p>
                                            </p>
                                        </div>
                                        <div class="col-md-5">
                                            <div class="portlet box lightgray">
                                                <div>
                                                    <div class="form-group">
                                                        <label class="col-md-6  control-label2">
                                                            Target Date <span style="color: Red; font-size: 12px; display: none;"  class="lbltab7Target">*</span>
                                                        </label>
                                                        <div class="col-md-6">
                                                            <div class="stagecontrols" style="width: 100%">
                                                                <asp:TextBox ID="tab7Target" runat="server" class="form-control"> </asp:TextBox>
                                                                <asp:HiddenField ID="tab7Target_flag" runat="server" />
                                                                <asp:HiddenField ID="tab7Target_date" runat="server" />
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-md-6  control-label2 ">
                                                            Actual Completion Date <span style="color: Red; font-size: 12px; display: none;" class="lbltab7Complete">*</span>
                                                        </label>
                                                        <div class="col-md-6">
                                                            <div class="satgecontrols" style="width: 100%">
                                                                <asp:TextBox ID="tab7Completion" Enabled="false" runat="server" class="form-control"></asp:TextBox>
                                                                <asp:HiddenField ID="tab7Completion_flag" runat="server" />
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="form-group tab7_hideout">
                                                        <div class="col-md-6 control-label2" style="text-align:right;">
                                                            <asp:RadioButton ID="tab7_radio_success" Text="Success" GroupName="tab7_status" runat="server" />
                                                        </div>
                                                        <div class="col-md-6 control-label2">
                                                            <asp:RadioButton ID="tab7_radio_failure" Text="Failure" GroupName="tab7_status" runat="server" />
                                                        </div>
                                                    </div>

                                                    <div class="form-group tab7_monthly">
                                                        <div class="col-md-6">
                                                        <label class="control-label2 ">
                                                            Monthly Business Expected
                                                        </label>
                                                            <br />
                                                        <span style="color: red; font-size: 11px">
                                                            (Value should be in lakhs.
                                                            Only 3 digits are allowed.)
                                                        </span>
                                                            </div>
                                                        <div class="col-md-6">
                                                            <div class="stagecontrols">
                                                                <asp:TextBox ID="tab7_monthly_expected" runat="server" class="form-control" onkeypress="return isNumberKey(event,this)" MaxLength="6"> </asp:TextBox>
                                                                <span style="color: Red; font-size: 12px; display: none;" class="lbltab7_monthly">Only Numeric values are allowed.</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                    <div class="form-group tab7_hideout">
                                                        <label class="col-md-6  control-label2 ">
                                                            Comments
                                                        </label>
                                                        <div class="col-md-6">
                                                            <div class="stagecontrols">
                                                                <asp:TextBox ID="tab7_comments" runat="server" class="form-control" TextMode="multiline" Columns="100" Rows="3"> </asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="clearfix"></div>
                                        <div class="col-md-12 mn_input">
                                            <p>
                                                <%-- <button id="tab7btnRemarks" type="button" runat="server" class="btn green" onserverclick="tab7AddOneMoreRemark" style="width: 150px;margin-top: 19px; " ><i class="fa fa-plus"></i> New Remarks</button>	--%>
                                                <asp:ImageButton ID="tab7btnRemarks" runat="server" ToolTip="New Remarks"  ImageUrl="~/images/shejiyeiconz0muj0ftihh.png" OnClick="tab7AddOneMoreRemark" Visible="false" />
                                                <asp:ImageButton ID="tab7saveRemarks" runat="server" ToolTip="Save Remarks" ImageUrl="~/images/35297964-checklist-icon-green-round-button.png" Visible="false"  OnClick="tab7btnSave_Click" CausesValidation="true" />
                                                <asp:ImageButton ID="tab7btnSave" runat="server" ToolTip="Save and Continue" ImageUrl="~/images/torrents-icon-25.png" OnClick="tab7btnSave_Click"  CausesValidation="true" OnClientClick="return ValidateTab7()" />
                                                <asp:Button ID="tab7btnSaveApprove" Visible="false" runat="server" ToolTip="Submit For Approval" ImageUrl="~/images/approved.png"  OnClick="tab7btnSave_Click" CausesValidation="true" OnClientClick="return ValidateTab7()" />
                                                <asp:ImageButton ID="tab7btnSubmit" runat="server" ToolTip="Close Stage" Visible="false" ImageUrl="~/images/22151610-button-close-on-white-background-icon-green.png" OnClick="tab7btnSubmit_Click"  CausesValidation="true" OnClientClick="return ValidateSubmitTab7()" />
                                                <label runat="server" class="tab7_hideout_every" style="color: red; font-size: 12px; font-weight: 800;" id="Label6">&nbsp &nbsp To close the stage, click on Close Stage.</label>
                                            </p>
                                            <br />
                                            <br />
                                            <br />
                                            <br />
                                            <asp:Label ID="tab7_lblApprove" CssClass="control-label stagecontrol" runat="server" Visible="false">APPROVAL REMARKS</asp:Label>
                                            <p>
                                                <asp:TextBox ID="tab7_txtApprove" Visible="false" CssClass="form-control stageinput" TextMode="multiline" Columns="100" Rows="2" runat="server" />
                                            </p>
                                            <p>
                                                <asp:ImageButton ID="tab7_btnApprove" runat="server" Visible="false" CssClass="mn_input" ToolTip="Approve" ImageUrl="~/images/approve.png"  OnClick="tab7btnApprove_Click" CausesValidation="true" />
                                                <asp:ImageButton ID="tab7_btnReject" runat="server" Visible="false"  CssClass="mn_input" ToolTip="Reject"  ImageUrl="~/images/reject_1.png" OnClick="tab7btnReject_Click" CausesValidation="true" />
                                            </p>
                                        </div>


                                        <div class="clearfix"></div>
                                        <div class="col-md-12 ui-widget-content1">
                                            <div class="form-group col-md-12 tab7_hideout_upload">
                                                 <asp:Label ID="tab7doc" class="col-md-2  control-label2 " runat="server" Visible="false">Attach Documents</asp:Label>   
                                              <%--  <label class="col-md-2  control-label2 ">Attach Documents</label>--%>
                                                <asp:FileUpload ID="FileUpload7" runat="server" AllowMultiple="true" CssClass="col-md-5 file" Visible="false" />
                                                <br>
                                                <asp:ImageButton ID="tab7btnUpload" runat="server" ToolTip="Upload" ImageUrl="~/images/upload_1.png" CssClass="col-md-3 uploadcontrol" OnClick="tab7_UploadMultipleFiles"  Visible="false" />
                                                <asp:Label ID="tab7lblSuccess"  class="col-md-2" runat="server" ForeColor="Green" />
                                                <br>
                                            </div>
                                            <div class="form-group col-md-12">
                                                <asp:GridView ID="tab7_FileList" runat="server" Height="100px" Style="text-align: center;" AutoGenerateColumns="false" ShowHeader="true">
                                                    <Columns>
                                                        <asp:TemplateField HeaderText="Uploaded Docs" HeaderStyle-CssClass="HeadergridAll">
                                                            <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                                            <ItemTemplate>
                                                                <asp:LinkButton ID="tab7_file_lbl" CssClass="lbl_color" runat="server" CommandArgument='<%# Eval("file_number") %>' Text='<%# Eval("file_name") %>' OnClick="downloadStage_Files"></asp:LinkButton>
                                                                <asp:LinkButton ID="tab7_file_Delete" Text="Delete" CommandArgument='<%# Eval("file_number") %>' runat="server" OnClick="DeleteFile_tab7" />
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                    </Columns>
                                                </asp:GridView>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane" id="tab_1_8">
                                        <div class="col-md-7">
                                            <label class="control-label stagecontrol">GOAL</label>
                                            <p>
                                                <asp:TextBox ID="tab8txtGoal" CssClass="form-control stageinput" TextMode="multiline" Style="resize: vertical" Columns="100" Rows="2" runat="server" />
                                            </p>
                                            <p>
                                                <div id="tab8Remarks" class="form-group" runat="server">
                                                    <label class="control-label stagecontrol">REMARKS</label>
                                                    <asp:TextBox ID="tab8txtRemarks_1" CssClass="form-control stageinput" TextMode="multiline" Style="resize: vertical" Columns="100" Rows="2" runat="server" />
                                                    <p></p>
                                                </div>
                                                <p>
                                                </p>
                                                <p>
                                                </p>
                                                <p>
                                                </p>
                                                <p>
                                                </p>
                                                <p>
                                                </p>
                                                <p>
                                                </p>
                                                <p>
                                                </p>
                                                <p>
                                                </p>
                                                <p>
                                                </p>
                                                <p>
                                                </p>
                                                <p>
                                                </p>
                                                <p>
                                                </p>
                                                <p>
                                                </p>
                                                <p>
                                                </p>
                                                <p>
                                                </p>
                                                <p>
                                                </p>
                                                <p>
                                                </p>
                                                <p>
                                                </p>
                                                <p>
                                                </p>
                                                <p>
                                                </p>
                                                <p>
                                                </p>
                                                <p>
                                                </p>
                                                <p>
                                                </p>
                                                <p>
                                                </p>
                                            </p>
                                        </div>
                                        <div class="col-md-5 ">
                                            <div class="portlet box lightgray">
                                                <div>
                                                    <div class="form-group">
                                                        <label class="col-md-6  control-label2 ">
                                                            Target Date <span style="color: Red; font-size: 12px; display: none;" class="lbltab8Target">*</span>
                                                        </label>
                                                        <div class="col-md-6">
                                                            <div class="stagecontrols" style="width: 100%">
                                                                <asp:TextBox ID="tab8Target" runat="server" class="form-control"> </asp:TextBox>
                                                                <asp:HiddenField ID="tab8Target_flag" runat="server" />
                                                                <asp:HiddenField ID="tab8Target_date" runat="server" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-md-6  control-label2 ">
                                                            Actual Completion Date <span style="color: Red; font-size: 12px; display: none;" class="lbltab8Complete">*</span>
                                                        </label>
                                                        <div class="col-md-6">
                                                            <div class="stagecontrols" style="width: 100%">
                                                                <asp:TextBox ID="tab8Completion" Enabled="false" runat="server" class="form-control"> </asp:TextBox>
                                                                <asp:HiddenField ID="tab8Completion_flag" runat="server" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group tab8_hideout">
                                                        <div class="col-md-6 control-label2"style="text-align:right;">
                                                            <asp:RadioButton ID="tab8_radio_success" Text="Success" GroupName="tab8_status" runat="server" />
                                                        </div>
                                                        <div class="col-md-6 control-label2">
                                                            <asp:RadioButton ID="tab8_radio_failure" Text="Failure" GroupName="tab8_status" runat="server" />
                                                        </div>
                                                    </div>

                                                    <div class="form-group tab8_monthly">
                                                        <div class="col-md-6 ">
                                                        <label class="control-label2 ">
                                                            Monthly Business Expected
                                                        </label>
                                                        <br />
                                                        <span style="color: red; font-size: 11px">
                                                            (Value should be in lakhs.<br />
                                                            Only 3 digits are allowed.)
                                                        </span>
                                                            </div>
                                                        <div class="col-md-6">
                                                            <div class="stagecontrols">
                                                                <asp:TextBox ID="tab8_monthly_expected" runat="server" class="form-control" onkeypress="return isNumberKey(event,this)" MaxLength="6"> </asp:TextBox>
                                                                <span style="color: Red; font-size: 11px; display: none;" class="lbltab8_monthly">Only Numeric values are allowed.</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                      <div class="clearfix"></div>
                                                    <div class="form-group tab8_hideout">
                                                        <label class="col-md-6  control-label2 ">
                                                            Comments
                                                        </label>
                                                        <div class="col-md-6">
                                                            <div class="stagecontrols">
                                                                <asp:TextBox ID="tab8_comments" runat="server" class="form-control" TextMode="multiline" Columns="100" Rows="3"> </asp:TextBox>

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="clearfix"></div>
                                        <div class="col-md-12 mn_input">
                                            <p>
                                                <%--<button id="tab8btnRemarks" type="button" runat="server" class="btn green" onserverclick="tab8AddOneMoreRemark" style="width: 150px;margin-top: 19px; " ><i class="fa fa-plus"></i> New Remarks</button>	--%>
                                                <asp:ImageButton ID="tab8btnRemarks" runat="server" ToolTip="New Remarks"  ImageUrl="~/images/shejiyeiconz0muj0ftihh.png" OnClick="tab8AddOneMoreRemark" Visible="false" />
                                                <asp:ImageButton ID="tab8saveRemarks" runat="server" ToolTip="Save Remarks" ImageUrl="~/images/35297964-checklist-icon-green-round-button.png" visible="false" OnClick="tab8btnSave_Click" CausesValidation="true" />
                                                <asp:ImageButton ID="tab8btnSave" runat="server" ToolTip="Save and Continue" ImageUrl="~/images/torrents-icon-25.png" OnClick="tab8btnSave_Click"  CausesValidation="true" OnClientClick="return ValidateTab8()" />
                                                <asp:ImageButton ID="tab8btnSaveApprove" Visible="false" runat="server" ToolTip="Submit For Approval" ImageUrl="~/images/approved.png"  OnClick="tab8btnSave_Click" CausesValidation="true" OnClientClick="return ValidateTab8()" />
                                                <asp:ImageButton ID="tab8btnSubmit" runat="server" ToolTip="Close Stage" Visible="false" ImageUrl="~/images/22151610-button-close-on-white-background-icon-green.png" OnClick="tab8btnSubmit_Click" CausesValidation="true" OnClientClick="return ValidateSubmitTab8()" />
                                                <label runat="server" class="tab8_hideout_every" style="color: red; font-size: 12px; font-weight: 800;" id="Label7">&nbsp &nbsp To close the stage, click on Close Stage.</label>
                                            </p>
                                            <br />
                                            <br />
                                            <br />
                                            <br />
                                            <asp:Label ID="tab8_lblApprove" CssClass="control-label stagecontrol" runat="server" Visible="false">APPROVAL REMARKS</asp:Label>
                                            <p>
                                                <asp:TextBox ID="tab8_txtApprove" Visible="false" CssClass="form-control stageinput" TextMode="multiline" Columns="100" Rows="2" runat="server" />
                                            </p>
                                            <p>
                                                <asp:ImageButton ID="tab8_btnApprove" runat="server" Visible="false"  CssClass="mn_input" ToolTip="Approve" ImageUrl="~/images/approve.png" OnClick="tab8btnApprove_Click" CausesValidation="true" />
                                                <asp:ImageButton ID="tab8_btnReject" runat="server" Visible="false"  CssClass="mn_input" ToolTip="Reject"  ImageUrl="~/images/reject_1.png" OnClick="tab8btnReject_Click" CausesValidation="true" />
                                            </p>
                                        </div>


                                        <div class="clearfix"></div>
                                        <div class="col-md-12 ui-widget-content1">
                                            <div class="form-group col-md-12 tab8_hideout_upload">
                                                 <asp:Label ID="tab8doc" class="col-md-2  control-label2 " runat="server" Visible="false">Attach Documents</asp:Label>   
                                              <%--  <label class="col-md-2  control-label2 ">Attach Documents</label>--%>
                                                <asp:FileUpload ID="FileUpload8" runat="server" AllowMultiple="true" CssClass="col-md-5 file" Visible="false" />
                                                  <br>
                                                <asp:ImageButton ID="tab8btnUpload" runat="server" ToolTip="Upload" ImageUrl="~/images/upload_1.png" CssClass="col-md-3 uploadcontrol" OnClick="tab8_UploadMultipleFiles"  Visible="false" />
                                                <asp:Label ID="tab8lblSuccess" class="col-md-2" runat="server" ForeColor="Green" />
                                                <br>
                                            </div>

                                            <div class="form-group col-md-12">
                                                <asp:GridView ID="tab8_FileList" runat="server" Height="100px"  Style="text-align: center;" AutoGenerateColumns="false" ShowHeader="true">
                                                    <Columns>
                                                        <asp:TemplateField HeaderText="Uploaded Docs" HeaderStyle-CssClass="HeadergridAll">
                                                            <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                                            <ItemTemplate>
                                                                <asp:LinkButton ID="tab8_file_lbl" CssClass="lbl_color" runat="server" CommandArgument='<%# Eval("file_number") %>' Text='<%# Eval("file_name") %>' OnClick="downloadStage_Files"></asp:LinkButton>
                                                                <asp:LinkButton ID="tab8_file_Delete" Text="Delete" CommandArgument='<%# Eval("file_number") %>' runat="server" OnClick="DeleteFile_tab8" />
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                    </Columns>
                                                </asp:GridView>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="tab-pane" id="tab_1_9">
                                        <div class="col-md-7">
                                            <label class="control-label stagecontrol">GOAL</label>
                                            <p>
                                                <asp:TextBox ID="tab9txtGoal" CssClass="form-control stageinput" TextMode="multiline" Style="resize: vertical" Columns="100" Rows="2" runat="server" />
                                            </p>
                                            <p>
                                                <div id="tab9Remarks" class="form-group" runat="server">
                                                    <label class="control-label stagecontrol">REMARKS</label>
                                                    <asp:TextBox ID="tab9txtRemarks_1" CssClass="form-control stageinput" TextMode="multiline" Style="resize: vertical" Columns="100" Rows="2" runat="server" />
                                                    <p></p>
                                                </div>
                                                <p>
                                                </p>
                                                <p>
                                                </p>
                                                <p>
                                                </p>
                                                <p>
                                                </p>
                                                <p>
                                                </p>
                                                <p>
                                                </p>
                                                <p>
                                                </p>
                                                <p>
                                                </p>
                                                <p>
                                                </p>
                                                <p>
                                                </p>
                                                <p>
                                                </p>
                                                <p>
                                                </p>
                                                <p>
                                                </p>
                                                <p>
                                                </p>
                                                <p>
                                                </p>
                                                <p>
                                                </p>
                                                <p>
                                                </p>
                                                <p>
                                                </p>
                                                <p>
                                                </p>
                                                <p>
                                                </p>
                                                <p>
                                                </p>
                                                <p>
                                                </p>
                                                <p>
                                                </p>
                                                <p>
                                                </p>
                                            </p>
                                        </div>
                                        <div class="col-md-5 ">
                                            <div class="portlet box lightgray">
                                                <div>
                                                    <div class="form-group">
                                                        <label class="col-md-6  control-label2 ">
                                                            Target Date <span style="color: Red; font-size: 12px; display: none;" class="lbltab9Target">*</span>
                                                        </label>
                                                        <div class="col-md-6">
                                                            <div class="stagecontrols" style="width: 100%">
                                                                <asp:TextBox ID="tab9Target" runat="server" class="form-control"> </asp:TextBox>
                                                                <asp:HiddenField ID="tab9Target_flag" runat="server" />
                                                                <asp:HiddenField ID="tab9Target_date" runat="server" />
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-md-6  control-label2 ">
                                                            Actual Completion Date <span style="color: Red; font-size: 12px; display: none;" class="lbltab9Complete">*</span>
                                                        </label>
                                                        <div class="col-md-6">
                                                            <div class="stagecontrols" style="width: 100%">
                                                                <asp:TextBox ID="tab9Completion" Enabled="false" runat="server" class="form-control"> </asp:TextBox>
                                                                <asp:HiddenField ID="tab9Completion_flag" runat="server" />
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="form-group tab9_hideout">
                                                        <div class="col-md-6 control-label2" style="text-align:right;">
                                                            <asp:RadioButton ID="tab9_radio_success" Text="Success" GroupName="tab9_status" runat="server" />
                                                        </div>
                                                        <div class="col-md-6 control-label2">

                                                            <asp:RadioButton ID="tab9_radio_failure" Text="Failure" GroupName="tab9_status" runat="server" />
                                                        </div>
                                                    </div>

                                                    <div class="form-group tab9_monthly">
                                                          <div class="col-md-6">
                                                        <label class="control-label2 ">
                                                            Monthly Business Expected
                                                        </label>
                                                              <br />
                                                        <span style="color: red; font-size: 11px">
                                                            (Value should be in lakhs.
                                                            Only 3 digits are allowed.)
                                                        </span>
                                                              </div>
                                                        <div class="col-md-6">
                                                            <div class="stagecontrols">
                                                                <asp:TextBox ID="tab9_monthly_expected" runat="server" class="form-control" onkeypress="return isNumberKey(event,this)" MaxLength="6"> </asp:TextBox>
                                                                <span style="color: Red; font-size: 11px; display: none;" class="lbltab9_monthly">Only Numeric values are allowed.</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                       <div class="clearfix"></div>
                                                    <div class="form-group tab9_hideout">
                                                        <label class="col-md-6  control-label2 ">
                                                            Comments
                                                        </label>
                                                        <div class="col-md-6 ">
                                                            <div class="stagecontrols">
                                                                <asp:TextBox ID="tab9_comments" runat="server" class="form-control" TextMode="multiline" Columns="100" Rows="3"> </asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="clearfix"></div>
                                        <div class="col-md-12 mn_input">
                                            <p>
                                                <%--<button id="tab9btnRemarks" type="button" runat="server" class="btn green" onserverclick="tab9AddOneMoreRemark" style="width: 150px;margin-top: 19px; " ><i class="fa fa-plus"></i> New Remarks</button>	--%>
                                                <asp:ImageButton ID="tab9btnRemarks" runat="server" ToolTip="New Remarks"  ImageUrl="~/images/shejiyeiconz0muj0ftihh.png" OnClick="tab9AddOneMoreRemark" Visible="false" />
                                                <asp:ImageButton ID="tab9saveRemarks" runat="server" ToolTip="Save Remarks" ImageUrl="~/images/35297964-checklist-icon-green-round-button.png" Visible="false"  OnClick="tab9btnSave_Click" CausesValidation="true" />
                                                <asp:ImageButton ID="tab9btnSave" runat="server" ToolTip="Save and Continue" ImageUrl="~/images/torrents-icon-25.png"  OnClick="tab9btnSave_Click" CausesValidation="true" OnClientClick="return ValidateTab9()" />
                                                <asp:ImageButton ID="tab9btnSaveApprove" Visible="false" runat="server" ToolTip="Submit For Approval" ImageUrl="~/images/approved.png"  OnClick="tab9btnSave_Click" CausesValidation="true" OnClientClick="return ValidateTab9()" />
                                                <asp:ImageButton ID="tab9btnSubmit" runat="server" ToolTip="Close Stage" Visible="false" ImageUrl="~/images/22151610-button-close-on-white-background-icon-green.png" OnClick="tab9btnSubmit_Click"  CausesValidation="true" OnClientClick="return ValidateSubmitTab9()" />
                                                <label runat="server" class="tab9_hideout_every" style="color: red; font-size: 12px; font-weight: 800;" id="Label8">&nbsp &nbsp To close the stage, click on Close Stage.</label>
                                            </p>
                                            <br />
                                            <br />
                                            <br />
                                            <br />
                                            <asp:Label ID="tab9_lblApprove" CssClass="control-label stagecontrol" runat="server" Visible="false">APPROVAL REMARKS</asp:Label>
                                            <p>
                                                <asp:TextBox ID="tab9_txtApprove" Visible="false" CssClass="form-control stageinput" TextMode="multiline" Columns="100" Rows="2" runat="server" />
                                            </p>
                                            <p>
                                                <asp:ImageButton ID="tab9_btnApprove" runat="server" Visible="false" CssClass="mn_input" ToolTip="Approve" ImageUrl="~/images/approve.png"  OnClick="tab9btnApprove_Click" CausesValidation="true" />
                                                <asp:ImageButton ID="tab9_btnReject" runat="server" Visible="false" CssClass="mn_input" ToolTip="Reject"  ImageUrl="~/images/reject_1.png"  OnClick="tab9btnReject_Click" CausesValidation="true" />
                                            </p>
                                        </div>


                                        <div class="clearfix"></div>
                                        <div class="col-md-12 ui-widget-content1">
                                            <div class="form-group col-md-12 tab9_hideout_upload">
                                                 <asp:Label ID="tab9doc" class="col-md-2  control-label2 " runat="server" Visible="false">Attach Documents</asp:Label>   
                                               <%-- <label class="col-md-2  control-label2 ">Attach Documents</label>--%>
                                                <asp:FileUpload ID="FileUpload9" runat="server" AllowMultiple="true" CssClass="col-md-5 file" Visible="false" />
                                                <br>

                                                <asp:ImageButton ID="tab9btnUpload" runat="server" ToolTip="Upload" ImageUrl="~/images/upload_1.png" CssClass="col-md-3 uploadcontrol"  OnClick="tab9_UploadMultipleFiles"  Visible="false"/>
                                                <asp:Label ID="tab9lblSuccess" class="col-md-2" runat="server" ForeColor="Green" />
                                                <br>
                                            </div>

                                            <div class="form-group col-md-8">
                                                <asp:GridView ID="tab9_FileList" runat="server" Style="text-align: center;" AutoGenerateColumns="false" ShowHeader="true">
                                                    <Columns>
                                                        <asp:TemplateField HeaderText="Uploaded Docs" HeaderStyle-CssClass="HeadergridAll">
                                                            <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                                            <ItemTemplate>
                                                                <asp:LinkButton ID="tab9_file_lbl" CssClass="lbl_color" runat="server" CommandArgument='<%# Eval("file_number") %>' Text='<%# Eval("file_name") %>' OnClick="downloadStage_Files"></asp:LinkButton>
                                                                <asp:LinkButton ID="tab9_file_Delete" Text="Delete" CommandArgument='<%# Eval("file_number") %>' runat="server" OnClick="DeleteFile_tab9" />
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                    </Columns>
                                                </asp:GridView>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="tab-pane" id="tab_1_10">
                                        <div class="col-md-7">
                                            <label class="control-label stagecontrol">GOAL</label>
                                            <p>
                                                <asp:TextBox ID="tab10txtGoal" CssClass="form-control stageinput" TextMode="multiline" Style="resize: vertical" Columns="100" Rows="2" runat="server" />
                                            </p>

                                            <p>
                                                <div id="tab10Remarks" class="form-group" runat="server">
                                                    <label class="control-label stagecontrol">REMARKS</label>
                                                    <asp:TextBox ID="tab10txtRemarks_1" TextMode="multiline" CssClass="form-control stageinput" Style="resize: vertical" Columns="100" Rows="2" runat="server" />
                                                    <p></p>
                                                </div>
                                                <p>
                                                </p>
                                                <p>
                                                </p>
                                                <p>
                                                </p>
                                                <p>
                                                </p>
                                                <p>
                                                </p>
                                                <p>
                                                </p>
                                                <p>
                                                </p>
                                                <p>
                                                </p>
                                                <p>
                                                </p>
                                                <p>
                                                </p>
                                                <p>
                                                </p>
                                                <p>
                                                </p>
                                                <p>
                                                </p>
                                                <p>
                                                </p>
                                                <p>
                                                </p>
                                                <p>
                                                </p>
                                                <p>
                                                </p>
                                                <p>
                                                </p>
                                                <p>
                                                </p>
                                                <p>
                                                </p>
                                                <p>
                                                </p>
                                                <p>
                                                </p>
                                                <p>
                                                </p>
                                                <p>
                                                </p>
                                            </p>

                                        </div>
                                        <div class="col-md-5">
                                            <div class="portlet box lightgray">
                                                <div>
                                                    <div class="form-group">
                                                        <label class="col-md-6  control-label2 ">
                                                            Target Date <span style="color: Red; font-size: 12px; display: none;" class="lbltab10Target">*</span>
                                                        </label>
                                                        <div class="col-md-6">
                                                            <div class="stagecontrols" style="width: 100%">
                                                                <asp:TextBox ID="tab10Target" runat="server" class="form-control"> </asp:TextBox>
                                                                <asp:HiddenField ID="tab10Target_flag" runat="server" />
                                                                <asp:HiddenField ID="tab10Target_date" runat="server" />
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-md-6  control-label2 ">
                                                            Actual Completion Date <span style="color: Red; font-size: 12px; display: none;" class="lbltab10Complete">*</span>
                                                        </label>
                                                        <div class="col-md-6">
                                                            <div class="stagecontrols" style="width: 100%">
                                                                <asp:TextBox ID="tab10Completion" Enabled="false" runat="server" class="form-control"> </asp:TextBox>
                                                                <asp:HiddenField ID="tab10Completion_flag" runat="server" />
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="form-group tab10_hideout">
                                                        <div class="col-md-6 control-label2" style="text-align:right;">
                                                            <asp:RadioButton ID="tab10_radio_success" Text="Success" GroupName="tab10_status" runat="server" />
                                                        </div>
                                                        <div class="col-md-6 control-label2">
                                                            <asp:RadioButton ID="tab10_radio_failure" Text="Failure" GroupName="tab10_status" runat="server" />
                                                        </div>
                                                    </div>

                                                    <div class="form-group tab10_monthly">
                                                        <div class="col-md-6">
                                                        <label class="control-label2 ">
                                                            Monthly Business Expected
                                                        </label>
                                                            <br />
                                                        <span style="color: red; font-size: 12px">
                                                            (Value should be in lakhs.<br />
                                                            Only 3 digits are allowed.)
                                                        </span>
                                                            </div>
                                                        <div class="col-md-6">
                                                            <div class="stagecontrols">
                                                                <asp:TextBox ID="tab10_monthly_expected" runat="server" class="form-control" onkeypress="return isNumberKey(event,this)" MaxLength="6"> </asp:TextBox>
                                                                <span style="color: Red; font-size: 12px; display: none;" class="lbltab10_monthly">Only Numeric values are allowed.</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                  <div class="clearfix"></div>
                                                    <div class="form-group tab10_hideout">
                                                        <label class="col-md-6  control-label2 ">
                                                            Comments
                                                        </label>
                                                        <div class="col-md-6">
                                                            <div class="stagecontrols">
                                                                <asp:TextBox ID="tab10_comments" runat="server" class="form-control" TextMode="multiline" Columns="100" Rows="3"> </asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="clearfix"></div>
                                        <div class="col-md-12 mn_input">
                                            <p>
                                                <%--<button id="tab10btnRemarks" type="button" runat="server" class="btn green" onserverclick="tab10AddOneMoreRemark" style="width: 150px;margin-top: 19px; " ><i class="fa fa-plus"></i> New Remarks</button>	--%>
                                                <asp:ImageButton ID="tab10btnRemarks" runat="server" ToolTip="New Remarks"  ImageUrl="~/images/shejiyeiconz0muj0ftihh.png" OnClick="tab10AddOneMoreRemark" Visible="false" />
                                                <asp:ImageButton ID="tab10saveRemarks" runat="server" ToolTip="Save Remarks" ImageUrl="~/images/35297964-checklist-icon-green-round-button.png"  OnClick="tab10btnSave_Click" CausesValidation="true" Visible="false" />
                                                <asp:ImageButton ID="tab10btnSave" runat="server" ToolTip="Save and Continue" ImageUrl="~/images/torrents-icon-25.png"   OnClick="tab10btnSave_Click" CausesValidation="true" OnClientClick="return ValidateTab10()" />
                                                <asp:ImageButton ID="tab10btnSaveApprove" Visible="false" runat="server" ToolTip="Submit For Approval" ImageUrl="~/images/approved.png"  OnClick="tab10btnSave_Click" CausesValidation="true" OnClientClick="return ValidateTab10()" />
                                                <asp:ImageButton ID="tab10btnSubmit" runat="server" ToolTip="Close Stage" Visible="false" ImageUrl="~/images/22151610-button-close-on-white-background-icon-green.png" OnClick="tab10btnSubmit_Click" CausesValidation="true" OnClientClick="return ValidateSubmitTab10()" />
                                                <label runat="server" class="tab10_hideout_every" style="color: red; font-size: 12px; font-weight: 800;" id="Label9">&nbsp &nbsp To close the stage, click on Close Stage.</label>
                                            </p>
                                            <br />
                                            <br />
                                            <br />
                                            <br />
                                            <asp:Label ID="tab10_lblApprove" CssClass="control-label stagecontrol" runat="server" Visible="false">APPROVAL REMARKS</asp:Label>
                                            <p>
                                                <asp:TextBox ID="tab10_txtApprove" Visible="false" CssClass="form-control stageinput" TextMode="multiline" Columns="100" Rows="2" runat="server" />
                                            </p>
                                            <p>
                                                <asp:ImageButton ID="tab10_btnApprove" runat="server" Visible="false"  CssClass="mn_input" ToolTip="Approve" ImageUrl="~/images/approve.png"  OnClick="tab10btnApprove_Click" CausesValidation="true" />
                                                <asp:ImageButton ID="tab10_btnReject" runat="server" Visible="false"  CssClass="mn_input" ToolTip="Reject"  ImageUrl="~/images/reject_1.png"  OnClick="tab10btnReject_Click" CausesValidation="true" />
                                            </p>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="col-md-12 ui-widget-content1">
                                            <div class="form-group col-md-12 tab2_hideout_upload">
                                                 <asp:Label ID="tab10doc" class="col-md-2  control-label2 " runat="server" Visible="false">Attach Documents</asp:Label>   
                                               <%-- <label class="col-md-2  control-label2 ">Attach Documents</label>--%>
                                                <asp:FileUpload ID="FileUpload10" runat="server" AllowMultiple="true" CssClass="col-md-5 file" Visible="false" />
                                                <br>
                                                   <%-- <br>
                                                        <br></br>--%>
                                                        <asp:ImageButton ID="tab10btnUpload" runat="server" ToolTip="Upload" ImageUrl="~/images/upload_1.png" CssClass="col-md-3 uploadcontrol" OnClick="tab10_UploadMultipleFiles"  Visible="false" />
                                                        <asp:Label ID="tab10lblSuccess" class="col-md-2" runat="server" ForeColor="Green" />
                                                        <br>
                                                           <%-- <br>
                                                                <br></br>
                                                                <br>
                                                                    <br></br>
                                                                    <br>
                                                                        <br></br>
                                                                        <br></br>
                                                                        <br></br>
                                                                        <br>
                                                                            <br></br>
                                                                            <br>
                                                                                <br></br>
                                                                            </br>
                                                                        </br>
                                                                    </br>
                                                                </br>
                                                            </br>
                                                        </br>
                                                    </br>
                                                </br>--%>
                                            </div>
                                            <div class="form-group col-md-12">
                                                <asp:GridView ID="tab10_FileList" runat="server" Height="100px" Style="text-align: center;" AutoGenerateColumns="false" ShowHeader="true">
                                                    <Columns>
                                                        <asp:TemplateField HeaderText="Uploaded Docs" HeaderStyle-CssClass="HeadergridAll">
                                                            <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                                            <ItemTemplate>
                                                                <asp:LinkButton ID="tab10_file_lbl" CssClass="lbl_color" runat="server" CommandArgument='<%# Eval("file_number") %>' Text='<%# Eval("file_name") %>' OnClick="downloadStage_Files"></asp:LinkButton>
                                                                <asp:LinkButton ID="tab10_file_Delete" Text="Delete" CommandArgument='<%# Eval("file_number") %>' runat="server" OnClick="DeleteFile_tab10" />
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                    </Columns>
                                                </asp:GridView>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </ContentTemplate>
        <Triggers>
            <%--<asp:AsyncPostBackTrigger ControlID="tab1_new_customer" EventName="Click" />--%>

            <asp:AsyncPostBackTrigger ControlID="ddlDistributorList" EventName="SelectedIndexChanged" />
            <asp:AsyncPostBackTrigger ControlID="ddlCustomerforDistributor" EventName="SelectedIndexChanged" />
            <asp:AsyncPostBackTrigger ControlID="ddlCustomerName" EventName="SelectedIndexChanged" />


            <asp:AsyncPostBackTrigger ControlID="ddlTitle" EventName="SelectedIndexChanged" />
            <asp:AsyncPostBackTrigger ControlID="tab1btnRemarks" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="tab1saveRemarks" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="tab1btnSaveApprove" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="tab1btnSave" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="tab1btnSubmit" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="tab1_btnApprove" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="tab1_btnReject" EventName="Click" />
            <asp:PostBackTrigger ControlID="tab1btnUpload" />


            <asp:AsyncPostBackTrigger ControlID="tab2btnRemarks" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="tab2saveRemarks" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="tab2btnSaveApprove" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="tab2btnSave" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="tab2btnSubmit" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="tab2_btnApprove" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="tab2_btnReject" EventName="Click" />
            <asp:PostBackTrigger ControlID="tab2btnUpload" />


            <asp:AsyncPostBackTrigger ControlID="tab3btnRemarks" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="tab3saveRemarks" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="tab3btnSaveApprove" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="tab3btnSave" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="tab3btnSubmit" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="tab3_btnApprove" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="tab3_btnReject" EventName="Click" />
            <asp:PostBackTrigger ControlID="tab3btnUpload" />

            <asp:AsyncPostBackTrigger ControlID="tab4btnRemarks" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="tab4saveRemarks" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="tab4btnSaveApprove" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="tab4btnSave" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="tab4btnSubmit" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="tab4_btnApprove" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="tab4_btnReject" EventName="Click" />
            <asp:PostBackTrigger ControlID="tab4btnUpload" />


            <asp:AsyncPostBackTrigger ControlID="tab5btnRemarks" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="tab5saveRemarks" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="tab5btnSaveApprove" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="tab5btnSave" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="tab5btnSubmit" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="tab5_btnApprove" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="tab5_btnReject" EventName="Click" />
            <asp:PostBackTrigger ControlID="tab5btnUpload" />


            <asp:AsyncPostBackTrigger ControlID="tab6btnRemarks" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="tab6saveRemarks" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="tab6btnSaveApprove" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="tab6btnSave" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="tab6btnSubmit" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="tab6_btnApprove" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="tab6_btnReject" EventName="Click" />
            <asp:PostBackTrigger ControlID="tab6btnUpload" />


            <asp:AsyncPostBackTrigger ControlID="tab7btnRemarks" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="tab7saveRemarks" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="tab7btnSaveApprove" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="tab7btnSave" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="tab7btnSubmit" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="tab7_btnApprove" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="tab7_btnReject" EventName="Click" />
            <asp:PostBackTrigger ControlID="tab7btnUpload" />


            <asp:AsyncPostBackTrigger ControlID="tab8btnRemarks" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="tab8saveRemarks" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="tab8btnSaveApprove" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="tab8btnSave" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="tab8btnSubmit" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="tab8_btnApprove" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="tab8_btnReject" EventName="Click" />
            <asp:PostBackTrigger ControlID="tab8btnUpload" />

            <asp:AsyncPostBackTrigger ControlID="tab9btnRemarks" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="tab9saveRemarks" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="tab9btnSaveApprove" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="tab9btnSave" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="tab9btnSubmit" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="tab9_btnApprove" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="tab9_btnReject" EventName="Click" />
            <asp:PostBackTrigger ControlID="tab9btnUpload" />

            <asp:AsyncPostBackTrigger ControlID="tab10btnRemarks" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="tab10saveRemarks" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="tab10btnSaveApprove" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="tab10btnSave" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="tab10btnSubmit" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="tab10_btnApprove" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="tab10_btnReject" EventName="Click" />
            <asp:PostBackTrigger ControlID="tab10btnUpload" />

        </Triggers>
    </asp:UpdatePanel>


    <asp:UpdateProgress ID="updateProgress" runat="server">
        <ProgressTemplate>
            <div style="position: fixed; text-align: left; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.7;">

                <span style="border-width: 0px; position: fixed; padding: 50px; font-size: 30px; left: 40%; top: 40%; color: #fff">Please wait</span>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>

</asp:Content>



