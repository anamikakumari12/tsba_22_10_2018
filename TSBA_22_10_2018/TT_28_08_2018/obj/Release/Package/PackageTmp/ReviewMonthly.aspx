﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ReviewMonthly.aspx.cs" Inherits="TaegutecSalesBudget.ReviewMonthly" %>

<%@ Register Assembly="System.Web.DataVisualization, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" Namespace="System.Web.UI.DataVisualization.Charting" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <script type="text/javascript" src="js/Chart.js"></script>
    <%--  <link href="css/sol.css" rel="stylesheet" />
    <script src="js/sol.js"></script>--%>
    <script src="js/jquery.dataTables.min.js"></script>
    <link href="css/jquery.dataTables.min.css" rel="stylesheet" />
    <link href="css/buttons.dataTables.min.css" rel="stylesheet" />
    <script src="js/jquery.sumoselect.min.js"></script>
    <link href="css/sumoselect.css" rel="stylesheet" />

    <style type="text/css">
        .table-bordered > thead > tr > th, .table-bordered > tbody > tr > th, .table-bordered > tfoot > tr > th, .table-bordered > thead > tr > td, .table-bordered > tbody > tr > td, .table-bordered > tfoot > tr > td {
            border: 1px solid #ddd;
            padding: 5px;
        }

        .height {
            text-space-collapse: collapse;
        }

        .HeadergridAll {
            background: #ebeef5;
            color: black;
            font-weight: 600;
            text-align: center !important;
            /*border-color:#ebeef5;*/
        }

        td {
            border-color: #ebeef5;
            background: #fff;
            text-align: right;
        }

        #MainContent_grdviewAllValues td:first-child {
            text-align: left;
        }

        #MainContent_grdviewAllQuantites td:first-child {
            text-align: left;
        }

        /*.HeadergridAll {
            background: #ebeef5;
            color: black;
            font-weight: 600;
            text-align: center !important;
        }*/

        .control_dropdown {
            width: 180px;
            height: 30px;
            border-radius: 4px!important;
        }

        .label {
            padding-top: 9px;
            width: 100%;
            color: black;
        }

        .control {
            padding-top: 2px;
        }

        .SelectBox {
            padding-top: 9px;
            width: 180px!important;
            border-radius: 4px!important;
        }

        .btn.green {
            margin-top: 22px;
        }
    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server" AsyncPostBackTimeout="360" EnableHistory="true" EnablePartialRendering="true" EnablePageMethods="true">
    </asp:ScriptManager>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div class="crumbs">
                <!-- Start : Breadcrumbs -->
                <ul id="breadcrumbs" class="breadcrumb">
                    <li>
                        <i class="fa fa-home"></i>
                        <a>Reports</a>
                    </li>
                    <li>Reviews</li>
                    <li class="current">Monthly Review</li>
                    <div>
                        <ul>
                            <li class="title_bedcrum" style="list-style: none;">MONTHLY REVIEW</li>
                        </ul>
                    </div>
                    <div>
                        <ul style="float: right; list-style: none; margin-top: -4px; width: 265px; margin-right: -5px;" class="alert alert-danger fade in">
                            <li>
                                <span style="margin-right: -1px; margin-left: -35px; vertical-align: text-bottom;">Val In Units</span>
                                <asp:RadioButton ID="rbtn_Units" OnCheckedChanged="Units_CheckedChanged" AutoPostBack="true" GroupName="customer" runat="server" />
                                <span style="margin-right: -1px; margin-left: 5px; vertical-align: text-bottom;">Val In '000</span>
                                <asp:RadioButton ID="rbtn_Thousand" OnCheckedChanged="Thousand_CheckedChanged" AutoPostBack="true" GroupName="customer" runat="server" Checked="True" />
                                <span style="margin-right: 0px; margin-left: 6px; vertical-align: text-bottom;">Val In Lakhs</span>
                                <asp:RadioButton ID="rbtn_Lakhs" OnCheckedChanged="Lakhs_CheckedChanged" AutoPostBack="true" GroupName="customer" runat="server" />
                            </li>
                        </ul>
                    </div>
                </ul>
                <!-- End : Breadcrumbs -->
            </div>

            <div id="collapsebtn" class="row">
                <img id="product_image" src="images/up_arrow.png" align="left" style="margin-left: 46%;" />
            </div>

            <div class="row filter_panel" id="reportdrpdwns" runat="server">
                <div runat="server" id="cterDiv" visible="false">
                    <ul id="divCter" runat="server" class="btn-info rbtn_panel">
                        <li><span style="margin-right: 4px; vertical-align: text-bottom;">TAEGUTEC</span>
                            <asp:RadioButton ID="rdBtnTaegutec" AutoPostBack="true" Checked="true" OnCheckedChanged="rdBtnTaegutec_CheckedChanged" GroupName="byCmpnyCodeInradiobtn" runat="server" />
                            <span style="margin-right: 4px; margin-left: 4px; vertical-align: text-bottom;">DURACARB</span>
                            <asp:RadioButton ID="rdBtnDuraCab" AutoPostBack="true" OnCheckedChanged="rdBtnTaegutec_CheckedChanged" GroupName="byCmpnyCodeInradiobtn" runat="server" />
                        </li>
                    </ul>
                </div>

                <div class="col-md-2 control" runat="server" id="divBranch">
                    <label class="label">BRANCH</label>
                    <asp:ListBox runat="server" CssClass="control_dropdown" ID="BranchList" SelectionMode="Multiple" OnSelectedIndexChanged="BranchList_SelectedIndexChanged" AutoPostBack="true"></asp:ListBox>
                    <%--  <asp:DropDownList ID="ddlBranchList" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlBranchList_SelectedIndexChanged"
                                CssClass="form-control select2" Width="230px">
                            </asp:DropDownList>--%>
                </div>

                <div class="col-md-2 control" runat="server" id="divSE">
                    <label class="label">SALES ENGINEER </label>
                    <asp:ListBox runat="server" CssClass="control_dropdown" ID="SalesEngList" SelectionMode="Multiple" OnSelectedIndexChanged="SalesEngList_SelectedIndexChanged" AutoPostBack="true"></asp:ListBox>
                    <%--<asp:DropDownList ID="ddlSalesEngineerList" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlSalesEngineerList_SelectedIndexChanged"
                                CssClass="form-control" Width="230px">
                                <asp:ListItem>--SELECT SALES ENGINEER--</asp:ListItem>
                            </asp:DropDownList>--%>
                </div>

                <div class="col-md-2 control">
                    <label class="label">CUSTOMER TYPE </label>
                    <asp:DropDownList ID="ddlcustomertype" runat="server" CssClass="control_dropdown" AutoPostBack="True" OnSelectedIndexChanged="ddlcustomertype_SelectedIndexChanged">
                        <asp:ListItem Text="ALL" Value="ALL" />
                        <asp:ListItem Text="CUSTOMER" Value="C" />
                        <asp:ListItem Text="CHANNEL PARTNER" Value="D" />
                    </asp:DropDownList>
                </div>

                <div class="col-md-2 control">
                    <label class="label">CUSTOMER NAME </label>
                    <%-- <asp:DropDownList ID="ddlCustomerList" runat="server"
                                CssClass="form-control" Width="230px">
                                <asp:ListItem>--SELECT CUSTOMER --</asp:ListItem>
                            </asp:DropDownList>--%>
                    <asp:ListBox runat="server" CssClass="control_dropdown" ID="CustNameList" SelectionMode="Multiple" OnSelectedIndexChanged="CustNameList_SelectedIndexChanged" AutoPostBack="true" class="search-txt"></asp:ListBox>
                </div>

                <div class="col-md-2 control">
                    <label class="label ">CUSTOMER NUMBER</label>
                    <%-- <asp:DropDownList ID="ddlCustomerNumber" runat="server" CssClass="form-control" Width="230px">
                                <asp:ListItem>SELECT CUSTOMER NUMBER</asp:ListItem>
                            </asp:DropDownList>--%>
                    <asp:ListBox runat="server" CssClass="control_dropdown" ID="CustNumList" SelectionMode="Multiple" OnSelectedIndexChanged="CustNumList_SelectedIndexChanged" AutoPostBack="true" class="search-txt"></asp:ListBox>
                </div>

                <div class="col-md-2 control">
                    <label class="label ">PRODUCT GROUP</label>
                    <%-- <asp:DropDownList ID="ddlProductGroup" runat="server" CssClass="form-control" Width="230px" OnSelectedIndexChanged="ddlProductGroup_SelectedIndexChanged" AutoPostBack="true">
                                <asp:ListItem Value="ALL" Text="ALL"></asp:ListItem>
                                <asp:ListItem Value="GOLD" Text="GOLD"></asp:ListItem>
                                <asp:ListItem Value="BB" Text="BB"></asp:ListItem>
                                <asp:ListItem Value="5YRS" Text="5YRS"></asp:ListItem>
                                <asp:ListItem Value="SPC" Text="SPC"></asp:ListItem>
                                <asp:ListItem Value="TOP" Text="TOP"></asp:ListItem>
                            </asp:DropDownList>--%>
                    <asp:ListBox runat="server" CssClass="control_dropdown" ID="ProductGrpList" SelectionMode="Multiple" OnSelectedIndexChanged="ProductGrpList_SelectedIndexChanged" AutoPostBack="true">
                        <asp:ListItem Value="GOLD" Text="GOLD"></asp:ListItem>
                        <asp:ListItem Value="BB" Text="BB"></asp:ListItem>
                        <asp:ListItem Value="5YRS" Text="5YRS"></asp:ListItem>
                        <asp:ListItem Value="SPC" Text="SPC"></asp:ListItem>
                        <asp:ListItem Value="TOP" Text="TOP"></asp:ListItem>
                    </asp:ListBox>
                </div>

                <div class="col-md-2 control">
                    <label class="label ">PRODUCT FAMILY</label>
                    <%-- <asp:DropDownList ID="ddlProductFamliy" runat="server" CssClass="form-control" Width="230px" AutoPostBack="True" OnSelectedIndexChanged="ddlProductFamliy_SelectedIndexChanged">
                                <asp:ListItem>SELECT FAMILY</asp:ListItem>
                            </asp:DropDownList>--%>
                    <asp:ListBox runat="server" CssClass="control_dropdown" ID="ProductFamilyList" SelectionMode="Multiple" OnSelectedIndexChanged="ProductFamilyList_SelectedIndexChanged" AutoPostBack="true"></asp:ListBox>
                </div>

                <div class="col-md-2 control">
                    <label class="label">APPLICATION</label>
                    <%--  <asp:DropDownList ID="ddlApplication" runat="server" CssClass="form-control" Width="230px">
                                <asp:ListItem>SELECT Application</asp:ListItem>
                            </asp:DropDownList>--%>
                    <asp:ListBox runat="server" CssClass="control_dropdown" ID="ApplicationList" SelectionMode="Multiple" OnSelectedIndexChanged="ApplicationList_SelectedIndexChanged" AutoPostBack="true"></asp:ListBox>
                </div>

                <div class="col-md-4 ">
                    <asp:Button ID="reports" runat="server" Text="FILTER" OnClick="reports_Click" CssClass="btn green" Style="top: -5px !important;" />
                    <asp:Button ID="btnExport" Text="Export" runat="server" Visible="false"  OnClick="ExportToImage" CssClass="btn green" Style="top: -5px !important;"/>
                    <label id="alertmsg" style="display: none; font-weight: bold; color: #0582b7;">Now click on Filter to view results</label>
                </div>
            </div>

            <br />
            <div id="div1" runat="server">
                <ul style="margin-left: -20px; width: 150px" class="btn-info rbtn_panel">
                    <li><span style="margin-right: 4px; vertical-align: text-bottom;">VALUE</span>
                        <asp:RadioButton ID="rbtn_value" Checked="true" GroupName="ByValueorQty" runat="server" onclick="Value_or_Qty_Change();" />
                        <span style="margin-right: 4px; margin-left: 4px; vertical-align: text-bottom;">QTY</span>
                        <asp:RadioButton ID="rbtn_quantity" GroupName="ByValueorQty" runat="server" onclick="Value_or_Qty_Change();" />
                    </li>
                </ul>
            </div>
            <br />

            <div class="row" runat="server" id="divgridchart" visible="false">
                <div class="row">
                    <div style="float: left; padding-right: 10px; padding-left: 10px; width: 100%;">
                        <asp:GridView ID="grdviewAllValues" runat="server" ViewStateMode="Enabled" class="table table-bordered " AutoGenerateColumns="False" ShowHeader="false" Width="100%">
                            <Columns>
                                <asp:TemplateField HeaderStyle-CssClass="HeadergridAll">
                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                    <ItemTemplate>
                                        <asp:Label runat="server" ID="lblTitle" Text='<%# Eval("title") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderStyle-CssClass="HeadergridAll">
                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    <ItemTemplate>
                                        <asp:Label runat="server" ID="lbljan" Text='<%# (Eval("flag").ToString()=="YTDSALE CurrentYear" && Eval("jan").ToString() == "0")? "NA" : Eval("jan") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderStyle-CssClass="HeadergridAll">
                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    <ItemTemplate>
                                        <asp:Label runat="server" ID="lblfeb" Text='<%# (Eval("flag").ToString()=="YTDSALE CurrentYear" && Eval("feb").ToString() == "0")? "NA" : Eval("feb") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderStyle-CssClass="HeadergridAll">
                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    <ItemTemplate>
                                        <asp:Label runat="server" ID="lblmar" Text='<%# (Eval("flag").ToString()=="YTDSALE CurrentYear" && Eval("mar").ToString() == "0")? "NA" : Eval("mar") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderStyle-CssClass="HeadergridAll">
                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    <ItemTemplate>
                                        <asp:Label runat="server" ID="lblapr" Text='<%# (Eval("flag").ToString()=="YTDSALE CurrentYear" && Eval("apr").ToString() == "0")? "NA" : Eval("apr") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderStyle-CssClass="HeadergridAll">
                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    <ItemTemplate>
                                        <asp:Label runat="server" ID="lblmay" Text='<%# (Eval("flag").ToString()=="YTDSALE CurrentYear" && Eval("may").ToString() == "0")? "NA" :  Eval("may") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderStyle-CssClass="HeadergridAll">
                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    <ItemTemplate>
                                        <asp:Label runat="server" ID="lbljun" Text='<%# (Eval("flag").ToString()=="YTDSALE CurrentYear" && Eval("jun").ToString() == "0")? "NA" : Eval("jun") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderStyle-CssClass="HeadergridAll">
                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    <ItemTemplate>
                                        <asp:Label runat="server" ID="lbljul" Text='<%# (Eval("flag").ToString()=="YTDSALE CurrentYear" && Eval("jul").ToString() == "0")? "NA" : Eval("jul") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderStyle-CssClass="HeadergridAll">
                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    <ItemTemplate>
                                        <asp:Label runat="server" ID="lblAug" Text='<%# (Eval("flag").ToString()=="YTDSALE CurrentYear" && Eval("aug").ToString() == "0")? "NA" : Eval("aug") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderStyle-CssClass="HeadergridAll">
                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    <ItemTemplate>
                                        <asp:Label runat="server" ID="lblsep" Text='<%# (Eval("flag").ToString()=="YTDSALE CurrentYear" && Eval("sep").ToString() == "0")? "NA" : Eval("sep") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderStyle-CssClass="HeadergridAll">
                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    <ItemTemplate>
                                        <asp:Label runat="server" ID="lbloct" Text='<%# (Eval("flag").ToString()=="YTDSALE CurrentYear" && Eval("oct").ToString() == "0")? "NA" : Eval("oct") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderStyle-CssClass="HeadergridAll">
                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    <ItemTemplate>
                                        <asp:Label runat="server" ID="lblnov" Text='<%# (Eval("flag").ToString()=="YTDSALE CurrentYear" && Eval("nov").ToString() == "0")? "NA" : Eval("nov") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderStyle-CssClass="HeadergridAll">
                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    <ItemTemplate>
                                        <asp:Label runat="server" ID="lbldec" Text='<%# (Eval("flag").ToString()=="YTDSALE CurrentYear" && Eval("dec").ToString() == "0")? "NA" : Eval("dec") %>'></asp:Label>
                                        <asp:Label runat="server" ID="lblFlag" Text='<%# Eval("flag") %>' Visible="false"></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                    </div>
                </div>

                <div class="row">
                    <div style="float: left; padding-right: 10px; padding-left: 10px; width: 100%;" class="col-md-12">
                        <asp:Chart ID="Chart2" runat="server" Width="1307px" Visible="False" ViewStateMode="Enabled" Style="max-width: 100% !important" BackColor="#E1E1E1">
                            <Titles>
                                <asp:Title Text="Summary of Sales Budget Monthly "></asp:Title>
                            </Titles>
                            <Legends>
                                <asp:Legend Alignment="Center" Docking="Top" IsTextAutoFit="true" Name="Legend2" LegendStyle="Row" />
                            </Legends>
                            <Series>
                                <asp:Series Name="YTD SALE" ShadowOffset="1" Enabled="True" LabelForeColor="White" LabelAngle="-90"
                                    CustomProperties="LabelStyle= Bottom, DrawingStyle=Cylinder" Color="#FAA43A" IsVisibleInLegend="true" ChartType="Column">
                                </asp:Series>
                                <asp:Series Name="YTD PLAN" ShadowOffset="1" ChartType="Line" LabelBorderWidth="1"></asp:Series>
                                <asp:Series Name="Series4" IsValueShownAsLabel="True" ShadowOffset="1" ChartType="Point"></asp:Series>
                                <asp:Series Name="YTD SALE PREVIOUS YEAR" ShadowOffset="1" ChartType="Line"></asp:Series>
                                <asp:Series Name="Series5" IsValueShownAsLabel="True" ShadowOffset="1" ChartType="Point"></asp:Series>
                            </Series>
                            <ChartAreas>
                                <asp:ChartArea Name="ChartArea1" BackColor="#ACD1E9">
                                    <AxisX LabelAutoFitStyle="LabelsAngleStep45,LabelsAngleStep90,WordWrap">
                                        <MajorGrid LineWidth="0" />
                                        <LabelStyle Font="Verdana, 8.25pt" />
                                    </AxisX>
                                    <AxisY>
                                        <MajorGrid LineWidth="0" />
                                    </AxisY>
                                </asp:ChartArea>
                            </ChartAreas>
                            <BorderSkin BackColor="Transparent" PageColor="Transparent"
                                SkinStyle="Emboss" />
                        </asp:Chart>
                    </div>
                    <div style="float: left; padding-right: 10px; padding-left: 10px; width: 100%;" class="col-md-12">
                    </div>
                </div>
            </div>
            <div class="row" runat="server" id="divgridchartqty" visible="false">
                <div class="row">
                    <div style="float: left; padding-right: 10px; padding-left: 10px; width: 100%;">
                        <asp:GridView ID="grdviewAllQuantites" runat="server" ViewStateMode="Enabled" class="table table-bordered " AutoGenerateColumns="False" ShowHeader="false" Width="100%">
                            <Columns>
                                <asp:TemplateField>
                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                    <ItemTemplate>
                                        <asp:Label runat="server" ID="lblTitle" Text='<%# Eval("title") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    <ItemTemplate>
                                        <asp:Label runat="server" ID="lbljan" Text='<%# (Eval("flag").ToString()=="YTDSALE CurrentYear" && Eval("jan").ToString() == "0")? "NA" : Eval("jan") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    <ItemTemplate>
                                        <asp:Label runat="server" ID="lblfeb" Text='<%# (Eval("flag").ToString()=="YTDSALE CurrentYear" && Eval("feb").ToString() == "0")? "NA" : Eval("feb") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    <ItemTemplate>
                                        <asp:Label runat="server" ID="lblmar" Text='<%# (Eval("flag").ToString()=="YTDSALE CurrentYear" && Eval("mar").ToString() == "0")? "NA" : Eval("mar") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    <ItemTemplate>
                                        <asp:Label runat="server" ID="lblapr" Text='<%# (Eval("flag").ToString()=="YTDSALE CurrentYear" && Eval("apr").ToString() == "0")? "NA" : Eval("apr") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    <ItemTemplate>
                                        <asp:Label runat="server" ID="lblmay" Text='<%# (Eval("flag").ToString()=="YTDSALE CurrentYear" && Eval("may").ToString() == "0")? "NA" :  Eval("may") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    <ItemTemplate>
                                        <asp:Label runat="server" ID="lbljun" Text='<%# (Eval("flag").ToString()=="YTDSALE CurrentYear" && Eval("jun").ToString() == "0")? "NA" : Eval("jun") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    <ItemTemplate>
                                        <asp:Label runat="server" ID="lbljul" Text='<%# (Eval("flag").ToString()=="YTDSALE CurrentYear" && Eval("jul").ToString() == "0")? "NA" : Eval("jul") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    <ItemTemplate>
                                        <asp:Label runat="server" ID="lblAug" Text='<%# (Eval("flag").ToString()=="YTDSALE CurrentYear" && Eval("aug").ToString() == "0")? "NA" : Eval("aug") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    <ItemTemplate>
                                        <asp:Label runat="server" ID="lblsep" Text='<%# (Eval("flag").ToString()=="YTDSALE CurrentYear" && Eval("sep").ToString() == "0")? "NA" : Eval("sep") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    <ItemTemplate>
                                        <asp:Label runat="server" ID="lbloct" Text='<%# (Eval("flag").ToString()=="YTDSALE CurrentYear" && Eval("oct").ToString() == "0")? "NA" : Eval("oct") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    <ItemTemplate>
                                        <asp:Label runat="server" ID="lblnov" Text='<%# (Eval("flag").ToString()=="YTDSALE CurrentYear" && Eval("nov").ToString() == "0")? "NA" : Eval("nov") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    <ItemTemplate>
                                        <asp:Label runat="server" ID="lbldec" Text='<%# (Eval("flag").ToString()=="YTDSALE CurrentYear" && Eval("dec").ToString() == "0")? "NA" : Eval("dec") %>'></asp:Label>
                                        <asp:Label runat="server" ID="lblFlag" Text='<%# Eval("flag") %>' Visible="false"></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                    </div>
                </div>
                <div class="row">
                    <div style="float: left; padding-right: 10px; padding-left: 10px; width: 100%;" class="col-md-12">
                        <asp:Chart ID="Chart3" runat="server" Width="1307px" ViewStateMode="Enabled" Style="max-width: 100% !important" BackColor="#E1E1E1">
                            <Titles>
                                <asp:Title Text="Summary of Sales Budget Monthly "></asp:Title>
                            </Titles>
                            <Legends>
                                <asp:Legend Alignment="Center" Docking="Top" IsTextAutoFit="true" Name="Legend2" LegendStyle="Row" />
                            </Legends>
                            <Series>
                                <asp:Series Name="YTD SALE" ShadowOffset="1" Enabled="True" LabelForeColor="White" LabelAngle="-90"
                                    CustomProperties="LabelStyle= Bottom, DrawingStyle=Cylinder" Color="#FAA43A" IsVisibleInLegend="true" ChartType="Column">
                                </asp:Series>
                                <asp:Series Name="YTD PLAN" ShadowOffset="1" ChartType="Line" LabelBorderWidth="1"></asp:Series>
                                <asp:Series Name="Series4" IsValueShownAsLabel="True" ShadowOffset="1" ChartType="Point"></asp:Series>
                                <asp:Series Name="YTD SALE PREVIOUS YEAR" ShadowOffset="1" ChartType="Line"></asp:Series>
                                <asp:Series Name="Series5" IsValueShownAsLabel="True" ShadowOffset="1" ChartType="Point"></asp:Series>
                            </Series>
                            <ChartAreas>
                                <asp:ChartArea Name="ChartArea1" BackColor="#ACD1E9">
                                    <AxisX LabelAutoFitStyle="LabelsAngleStep45,LabelsAngleStep90,WordWrap">
                                        <MajorGrid LineWidth="0" />
                                        <LabelStyle Font="Verdana, 8.25pt" />
                                    </AxisX>
                                    <AxisY>
                                        <MajorGrid LineWidth="0" />
                                    </AxisY>
                                </asp:ChartArea>
                            </ChartAreas>
                            <BorderSkin BackColor="Transparent" PageColor="Transparent"
                                SkinStyle="Emboss" />
                        </asp:Chart>
                    </div>
                </div>
            </div>
            <br />
            <br />
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="BranchList" EventName="SelectedIndexChanged" />
            <asp:AsyncPostBackTrigger ControlID="SalesEngList" EventName="SelectedIndexChanged" />
            <asp:AsyncPostBackTrigger ControlID="ddlcustomertype" EventName="SelectedIndexChanged" />
            <asp:AsyncPostBackTrigger ControlID="ProductFamilyList" EventName="SelectedIndexChanged" />
            <asp:AsyncPostBackTrigger ControlID="reports" EventName="Click" />
            <asp:PostBackTrigger ControlID="btnExport"/>
            

            <%-- <asp:AsyncPostBackTrigger ControlID="rbtn_Thousand" EventName="OnCheckedChanged"/>
           <asp:AsyncPostBackTrigger ControlID="rbtn_Lakhs" EventName="OnCheckedChanged"/>--%>
        </Triggers>
    </asp:UpdatePanel>

    <asp:UpdateProgress ID="updateProgress" runat="server">
        <ProgressTemplate>
            <div style="position: fixed; text-align: left; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.7;">

                <span style="border-width: 0px; position: fixed; padding: 50px; font-size: 30px; left: 40%; top: 40%; color: #fff">Please wait</span>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <script type="text/javascript" class="init">
        
        $(document).ready(function () {
           
         <%--  $(<%=BranchList.ClientID%>).SumoSelect({ selectAll: true, search: true});
            $(<%=SalesEngList.ClientID%>).SumoSelect({ selectAll: true, search: true });
            $(<%=CustNameList.ClientID%>).SumoSelect({ selectAll: true, search: true });
            $(<%=CustNumList.ClientID%>).SumoSelect({ selectAll: true, search: true });
            $(<%=ProductGrpList.ClientID%>).SumoSelect({ selectAll: true, search: true });
            $(<%=ProductFamilyList.ClientID%>).SumoSelect({ selectAll: true, search: true });
            $(<%=ApplicationList.ClientID%>).SumoSelect({ selectAll: true, search: true }); --%>

            //triggerPostGridLodedActions();            
            //$('#product_image').unbind('click').bind('click', function (e) {
            //    var attr = $('#product_image').attr('src');
            //    $("#MainContent_reportdrpdwns").slideToggle();
            //    if (attr == "images/up_arrow.png") {
            //        $("#product_image").attr("src", "images/down_arrow.png");
            //    } else {
            //        $("#product_image").attr("src", "images/up_arrow.png");
            //    }
            //});

        });
        function hidediv()
        {
            $("#MainContent_btnExport").hide();
            $("#MainContent_divgridchart").hide();
            $("#MainContent_divgridchartqty").hide();
        }
        function bindGridView() {
            var head_content = $('#MainContent_grdviewAllValues tr:first').html();
            $('#MainContent_grdviewAllValues').prepend('<thead></thead>')
            $('#MainContent_grdviewAllValues thead').html('<tr>' + head_content + '</tr>');
            $('#MainContent_grdviewAllValues tbody tr:first').hide();
            $('#MainContent_grdviewAllValues').DataTable(
                     {
                         "info": false
                     });
        }
      
        function triggerPostGridLodedActions() {
            bindGridView();
            var RoleID = '<%=Session["RoleId"].ToString()%>';
                if (RoleID == "HO" || RoleID == "TM") {
                    $(<%=BranchList.ClientID%>).SumoSelect({ selectAll: true, search: true });
                    $(<%=SalesEngList.ClientID%>).SumoSelect({ selectAll: true, search: true });
                    $(<%=CustNameList.ClientID%>).SumoSelect({ selectAll: true, search: true });
                    $(<%=CustNumList.ClientID%>).SumoSelect({ selectAll: true, search: true });
                }

                if (RoleID == "BM") {
                    $(<%=SalesEngList.ClientID%>).SumoSelect({ selectAll: true, search: true });
                    $(<%=CustNameList.ClientID%>).SumoSelect({ selectAll: true, search: true });
                    $(<%=CustNumList.ClientID%>).SumoSelect({ selectAll: true, search: true });
                }

                if (RoleID == "SE") {
                    $(<%=CustNameList.ClientID%>).SumoSelect({ selectAll: true, search: true });
                    $(<%=CustNumList.ClientID%>).SumoSelect({ selectAll: true, search: true });
                }

                $(<%=ProductGrpList.ClientID%>).SumoSelect({ selectAll: true, search: true });
                $(<%=ProductFamilyList.ClientID%>).SumoSelect({ selectAll: true, search: true });
                $(<%=ApplicationList.ClientID%>).SumoSelect({ selectAll: true, search: true });

            Value_or_Qty_Change();

           
                //$('#MainContent_ddlCustomerList').change(function () {

                //    var ddlslectedText = $("#MainContent_ddlCustomerList option:selected").val();
                //    $("#MainContent_ddlCustomerNumber").val(ddlslectedText);
                //});
                //$('#MainContent_ddlCustomerNumber').change(function () {

                //    var ddlslectedText = $("#MainContent_ddlCustomerNumber").val();
                //    $("#MainContent_ddlCustomerList").val(ddlslectedText);
                //});
                $('#product_image').unbind('click').bind('click', function (e) {
                    var attr = $('#product_image').attr('src');
                    $("#MainContent_reportdrpdwns").slideToggle();
                    if (attr == "images/up_arrow.png") {
                        $("#product_image").attr("src", "images/down_arrow.png");
                    } else {
                        $("#product_image").attr("src", "images/up_arrow.png");
                    }
                });

            }
      


            function Value_or_Qty_Change() {
                if ($("#MainContent_rbtn_value").is(':checked')) {
                    //    $('#MainContent_reports').click();
                    $("#MainContent_divgridchart").show();
                    $("#MainContent_divgridchartqty").hide();
                }
                if ($("#MainContent_rbtn_quantity").is(':checked')) {
                    //     $('#MainContent_reports').click();
                    $("#MainContent_divgridchart").hide();
                    $("#MainContent_divgridchartqty").show();

                }
            }

    </script>
</asp:Content>
