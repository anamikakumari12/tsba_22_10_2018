         $("#Div4 input[type='submit']").attr("disabled", true);
            $("#Div3 input[type='submit']").attr("disabled", true);
            $("#Div2 input[type='submit']").attr("disabled", true);
            $("#Div1 input[type='submit']").attr("disabled", true);
            $("#dropid input[type='submit']").attr("disabled", true);
           
            $("#Div4 INPUT[type='checkbox']").change(function(){
                
                var checkboxes = $("#Div4 input[type='checkbox']")
                var submitButt = $("#Div4 input[type='submit']");
                    submitButt.attr("disabled", !checkboxes.is(":checked"));
                });
            $("#Div3 INPUT[type='checkbox']").change(function () {
                
                var checkboxes = $("#Div3 input[type='checkbox']")
                var submitButt = $("#Div3 input[type='submit']");
                submitButt.attr("disabled", !checkboxes.is(":checked"));
            });
            $("#Div2 INPUT[type='checkbox']").change(function () {
                
                var checkboxes = $("#Div2 input[type='checkbox']")
                var submitButt = $("#Div2 input[type='submit']");
                submitButt.attr("disabled", !checkboxes.is(":checked"));
            });
            $("#dropid INPUT[type='checkbox']").change(function () {
                
                var checkboxes = $(" #dropid input[type='checkbox']")
                var submitButt = $("#dropid input[type='submit']");
                submitButt.attr("disabled", !checkboxes.is(":checked"));
            });
            $("#Div1 INPUT[type='checkbox']").change(function () {
                
                var checkboxes = $(" #Div1 input[type='checkbox']")
                var submitButt = $("#Div1 input[type='submit']");
                submitButt.attr("disabled", !checkboxes.is(":checked"));
            });
        
            $('#MainContent_lnkbtnBulkapproval').bind("click", function (e) {
                dclg = $("#DivBranchApprove").dialog(
                            {
                                resizable: false,
                                draggable: true,
                                modal: false,
                                title: "BRANCH WISE APPROVAL",
                                width: "350",
                                height: "180",
                                open: function () {
                                    $(".ui-widget-overlay").css("display", 'none');
                                },
                                close: function () {
                                    $("#overlay").removeClass("ui-widget-overlay");
                                }
                            });
                //divPopUpBranchApproval = dclg;
                dclg.parent().appendTo(jQuery("form:first"));
                e.preventDefault();
            });

            /*   Display or close pop up customer type Start*/
            var dlg_panelCustomerType = $('#MainContent_panelCustomerType').dialog({
                autoOpen: false,
                dialogClass: 'noTitleStuff',
                minHeight: 100,
                width: 100,
                position: { my: "left top", at: "left bottom", of: $('#MainContent_grdviewAllValues_ibtnCtype_0').closest("td") },
                draggable: true,
                resizable: false,
                modal: false,
                closeText: 'Close',
                open: function () {
                    $(this).dialog('open');
                    closedialog = 1;
                   // $(document).bind('click', overlayclickclose);
                },
                focus: function () {
                    closedialog = 0;
                },
                close: function () {
                    $(this).dialog('close');
                    closedialog = 0;
                    //$(document).unbind('click');
                }
            });
            dlg_panelCustomerType.parent().appendTo(jQuery("form:first"));

            $('#MainContent_grdviewAllValues_ibtnCtype_0').click(function () {
               
                if (closedialog == 1) {
                    $('#MainContent_panelCustomerType').dialog('close');
                } else {
                    $('#MainContent_panelCustomerType').dialog('open');
                    closedialog = 1;
                }
            });
            var closedialog;
            
            /*   Display or close pop up customer type END*/


            /*  Display or close pop up customer REGION Start*/
            var dlg_panelRegion = $('#MainContent_panelRegion').dialog({
                autoOpen: false,
                dialogClass: 'noTitleStuff',
                minHeight: 100,
                width: 100,
                position: { my: "left top", at: "left bottom", of: $('#MainContent_grdviewAllValues_ibtnRegion_0').closest("td") },
                draggable: true,
                resizable: false,
                modal: false,
                closeText: 'Close',
                open: function () {
                    $(this).dialog('open');
                    closedialog_panelRegion = 1;
                  
                },
                focus: function () {
                    closedialog_panelRegion = 0;
                },
                close: function () {
                    $(this).dialog('close');
                    closedialog_panelRegion = 0;
                   
                }
            });
            dlg_panelRegion.parent().appendTo(jQuery("form:first"));

            $('#MainContent_grdviewAllValues_ibtnRegion_0').click(function () {
                
                if (closedialog_panelRegion == 1) {
                    $('#MainContent_panelRegion').dialog('close');
                } else {
                    $('#MainContent_panelRegion').dialog('open');
                    closedialog_panelRegion = 1;
                }
            });
            var closedialog_panelRegion;
            
            /*  Display or close pop up SE Status Start*/
            var dlg_panelSEStatus = $('#MainContent_panelSEStatus').dialog({
                autoOpen: false,
                dialogClass: 'noTitleStuff',
                minHeight: 100,
                width: 200,
                position: { my: "left top", at: "left bottom", of: $('#MainContent_grdviewAllValues_ibtnSEStatus_0').closest("td") },
                draggable: true,
                resizable: false,
                modal: false,
                closeText: 'Close',
                open: function () {
                    $(this).dialog('open');
                    closedialog_panelSEStatus = 1;
                   
                },
                focus: function () {
                    closedialog_panelSEStatus = 0;
                },
                close: function () {
                    $(this).dialog('close');
                    closedialog_panelSEStatus = 0;
                   
                }
            });
            dlg_panelSEStatus.parent().appendTo(jQuery("form:first"));

            $('#MainContent_grdviewAllValues_ibtnSEStatus_0').click(function () {
               
                if (closedialog_panelSEStatus == 1) {
                    $('#MainContent_panelSEStatus').dialog('close');
                } else {
                    $('#MainContent_panelSEStatus').dialog('open');
                    closedialog_panelSEStatus = 1;
                }
            });
            var closedialog_panelSEStatus;
           
            /*   Display or close pop up SE Status END*/

            /*  Display or close pop up BM Status Start*/
            var dlg_panelBMStatus = $('#MainContent_panelBMStatus').dialog({
                autoOpen: false,
                dialogClass: 'noTitleStuff',
                minHeight: 100,
                width: 200,
                position: { my: "left top", at: "left bottom", of: $('#MainContent_grdviewAllValues_ibtnBMStatus_0').closest("td") },
                draggable: true,
                resizable: false,
                modal: false,
                closeText: 'Close',
                open: function () {
                    $(this).dialog('open');
                    closedialog_panelBMStatus = 1;
                    
                },
                focus: function () {
                    closedialog_panelBMStatus = 0;
                },
                close: function () {
                   
                    $(this).dialog('close');
                    closedialog_panelBMStatus = 0;
                }
            });
            dlg_panelBMStatus.parent().appendTo(jQuery("form:first"));

            $('#MainContent_grdviewAllValues_ibtnBMStatus_0').click(function () {
                if (closedialog_panelBMStatus == 1) {
                    $('#MainContent_panelBMStatus').dialog('close');
                } else {
                    $('#MainContent_panelBMStatus').dialog('open');
                    closedialog_panelBMStatus = 1;
                }
            });
            var closedialog_panelBMStatus;
            
            /*   Display or close pop up BM Status END*/

            /*  Display or close pop up HO Status Start*/
           
            var dlg_panelHOStatus = $('#MainContent_panelHOStatus').dialog({
               
                autoOpen: false,
                dialogClass: 'noTitleStuff',
                minHeight: 100,
                width: 160,
                position: { my: "left top", at: "left bottom", of: $('#MainContent_grdviewAllValues_ibtnHOStatus_0').closest("td") },
                draggable: true,
                resizable: false,
                modal: false,
                closeText: 'Close',
                open: function () {
                    $(this).dialog('open');
                   
                    closedialog_panelHOStatus = 1;
                },
                focus: function () {
                    closedialog_panelHOStatus = 0;
                },
                close: function () {
                    $(this).dialog('close');
                    closedialog_panelHOStatus = 0;
                   
                }
            });
            dlg_panelHOStatus.parent().appendTo(jQuery("form:first"));

            $('#MainContent_grdviewAllValues_ibtnHOStatus_0').click(function () {
               
                if (closedialog_panelHOStatus == 1) {
                    $('#MainContent_panelHOStatus').dialog('close');
                } else {
                    $('#MainContent_panelHOStatus').dialog('open');
                    closedialog_panelHOStatus = 1;
                }
            });
            var closedialog_panelHOStatus;
           
            /*   Display or close pop up HO Status END*/

      
